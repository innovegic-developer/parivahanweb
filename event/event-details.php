<?php //echo SITE_ABS_PATH.MYUT_SUBFOLDER.'uploads/event/'.$singleeventdetails[0]['image'];?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Event Details</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ABS_PATH.MYUT_SUBFOLDER.'css/slick/slick.css';?>"/>
 <link rel="stylesheet" type="text/css" href="<?php echo SITE_ABS_PATH.MYUT_SUBFOLDER.'css/slick/slick-theme.css';?>"/>
<style type="text/css">
  body{padding: 0px; margin: 0px;font-family: 'Roboto', sans-serif;}
  .main{float: left; width: 100%}
  .margin{min-width: 320px;width: 100%;margin: 0 auto;}
  .main_sec{float: left; width: 100%; background: #e9e8e8;}
  .slick-slide img {
    display: block;
    float: left;
    width: 100%;
}
.slick-dots li button::before{font-size: 45px; color: #fff; opacity: 1;}
.slick-dots li.slick-active button::before{color: #08a2da; opacity: 1;}
.slick-dots{bottom: 10px;}
.slick-dotted.slick-slider{margin: 0px;}
.events-det{float: left; width: 100%;background: #fff; padding:0px; box-sizing: border-box;}
.events-det_top{float: left; width: 100%; padding: 10px 10px 0; box-sizing: border-box;}
.events-det_top_top{float: left; width: 100%;}
.events-det_top_top_lh{float: left; width: auto;margin-top: 5px;}
.events-det_top_top_rh{float: right;width: 100%;}
.events-det_top_top_rh h2{ font-weight: 400; font-size: 24px; color: #1f1f1f; margin: 0px;}
.events-det_top_top_rh p{font-weight: 400; font-size: 15px; color: #3c3c3c;margin: 5px 0 0 0;}
.events_box_rh_bot {
    float: left;
    width: 100%;
    
    padding: 19px 0;
}
.events_box_rh_bot_in {
    float: left;
    width: 100%;
}
.events_box_rh_bot_lh {
    float: left;
    width: auto;
}
.colck_in {
    float: left;
    width: auto;
}
.colck_in img {
    float: left;
}
.colck_in h2 {
    text-align: left;
    float: left;
    width: auto;
    color: #000000;
    font-size: 14px;
    font-weight: normal;
    margin: 1px 0 0 4px;
}
.events_box_rh_bot_rh {
    float: right;
    width: auto;
    margin-top: 2px;
}
.events-det_bot{float: left;width: 100%;border-top: 1px solid #d3d3d3;padding: 16px 10px; box-sizing: border-box; }
.events-det_bot_text{float: left; width: 100%;}
.events-det_bot_text h2{  text-align: left;
    float: left;
    width: 100%;
    color: #0277a2;
    font-size: 20px;
    font-weight: normal;
    line-height: 28px;
    margin: 1px 0 0 0px;}
.events-det_bot_text p{  text-align: left;
    float: left;
    width: 100%;
    color: #3c3c3c;
    font-size: 14px;
    line-height: 26px;
    font-weight: normal;
    margin: 20px 0 0 4px;}
    .slick-next.slick-arrow{display: none !important;}
    .slider-img > img {
    width: 100%;
     margin-bottom: 10px;
}
</style>
</head>
  
<body>
  <div class="main">
    <div class="margin">
      <div class="main_sec">
       
        <div class="events-det">
          <div class="events-det_top">
            <div class="events-det_top_top">
             <!--  <div class="events-det_top_top_lh">
                <img src="icon-01.png" width="20">
              </div> -->
              <div class="events-det_top_top_rh">
                <h2><?php echo $singleeventdetails[0]['title']?></h2>
                <p><?php echo $singleeventdetails[0]['address']?> </p>
              </div>
            </div>

            <div class="events-det_top_bot">
              <div class="events_box_rh_bot">
              <div class="events_box_rh_bot_in">
                <div class="events_box_rh_bot_lh">
                  <div class="colck_in">
                    <!-- <img src="<?php echo SITE_ABS_UPLOAD_PATH.'img/icon-02.png';?>" width="15px"> -->
                    <img src="<?php echo SITE_ABS_UPLOAD_PATH.'img/clock-icon.png';?>" width="15px">
                    <h2><?php echo date('d/m/Y h:i A',strtotime($singleeventdetails[0]['start_time']));?></h2>
                  </div>
                </div>
                <div class="events_box_rh_bot_rh">
                  <div class="colck_in">
                    <img  src="<?php echo SITE_ABS_UPLOAD_PATH.'img/clock-icon.png';?>" width="15px">
                    <h2><?php echo date('d/m/Y h:i A',strtotime($singleeventdetails[0]['end_time']));?></h2>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
           <div class="main_sec_slider">
          <div class="one-timeasd">
            <?php 
              if(file_exists(SITE_REL_PATH.MYUT_SUBFOLDER.'uploads/event/'.$singleeventdetails[0]['image']) AND !empty($singleeventdetails[0]['image'])) 
              {
            ?>
            <div>
              <a class="slider-img">
                <img src="<?php echo SITE_ABS_PATH.MYUT_SUBFOLDER.'uploads/event/'.$singleeventdetails[0]['image'];?>">
              </a>
            </div>
            <?php 
              }
            ?>
            <?php 
              if(file_exists(SITE_REL_PATH.MYUT_SUBFOLDER.'uploads/event/'.$singleeventdetails[0]['image2']) AND !empty($singleeventdetails[0]['image2'])) 
              {
            ?>
            <div>
              <a class="slider-img">
                <img src="<?php echo SITE_ABS_PATH.MYUT_SUBFOLDER.'uploads/event/'.$singleeventdetails[0]['image2'];?>">
              </a>
            </div>
            <?php 
            }
            ?>
            <?php 
              if(file_exists(SITE_REL_PATH.MYUT_SUBFOLDER.'uploads/event/'.$singleeventdetails[0]['image3']) AND !empty($singleeventdetails[0]['image3'])) 
              {
            ?>
            <div>
              <a class="slider-img">
                <img src="<?php echo SITE_ABS_PATH.MYUT_SUBFOLDER.'uploads/event/'.$singleeventdetails[0]['image3'];?>">
              </a>
            </div>
            <?php 
            }
            ?>
        </div>
        </div>
          <div class="events-det_bot">
            <div class="events-det_bot_text">
              <h2><?php echo $singleeventdetails[0]['address']?> </h2>
              <p><?php echo $singleeventdetails[0]['description']?></p>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script src="<?php echo SITE_ABS_PATH.MYUT_SUBFOLDER.'js/jquery-3.2.1.min.js'?>"></script>
  <script src="<?php echo SITE_ABS_PATH.MYUT_SUBFOLDER.'js/jquery.fancybox.min.js'?>"></script>
  <script type="text/javascript" src="<?php echo SITE_ABS_PATH.MYUT_SUBFOLDER.'js/jquery-migrate-1.2.1.min.js'?>"></script>

  <script type="text/javascript" src="<?php echo SITE_ABS_PATH.MYUT_SUBFOLDER.'css/slick/slick.min.js';?>"></script>

  <script type="text/javascript">
    $(document).ready(function(){
     $('.one-time').slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  autoplay: true,
  adaptiveHeight: false
});
    });
  </script>
</body>
</html>
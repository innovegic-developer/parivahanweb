<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="width=device-width,initial-scale=1">

<title>Event Listing</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet"> 
 <link href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css" rel="stylesheet">
        <style>
          .btn.share-btn {
              border: medium none;
              box-shadow: 0 2px 0 0 rgba(0, 0, 0, 0.2);
              color: #fff;
              cursor: pointer;
              display: inline-block;
              font-size: 1.2em;
              margin-top: 6px;
              opacity: 0.9;
              outline: medium none;
              padding: 8px 16px;
          }
            .share-btn {
                display: inline-block;
                color: #ffffff;
                border: none;
                padding: 0.5em;
                width: 4em;
                opacity: 0.9;
                box-shadow: 0 2px 0 0 rgba(0,0,0,0.2);
                outline: none;
                text-align: center;
            }
            .share-btn:hover,
            .share-btn:focus {
              color: #eeeeee;
            }
            .share-btn:active {
              position: relative;
              top: 2px;
              box-shadow: none;
              color: #e2e2e2;
              opacity: 1;
              outline: medium none;
              position: relative;
              top: 2px;
          } 
          .twitter {
              background: #55acee none repeat scroll 0 0;
          }
          .google-plus {
              background: #dd4b39 none repeat scroll 0 0;
          }
          .facebook {
              background: #3b5998 none repeat scroll 0 0;
          }
          .stumbleupon {
              background: #eb4823 none repeat scroll 0 0;
          }
          .reddit {
              background: #ff5700 none repeat scroll 0 0;
          }
          .linkedin {
              background: #4875b4 none repeat scroll 0 0;
          }
          .email {
              background: #444444 none repeat scroll 0 0;
          }
        </style>

<style type="text/css">
	body{padding: 0px; margin: 0px;font-family: 'Roboto', sans-serif;}
	.main{float: left; width: 100%}
	.margin{min-width: 320px;width: 100%;margin: 0 auto;}
	.main_sec{float: left; width: 100%; background: #e9e8e8;}
	.events_box{display: table; width: 100%; background: #fff;/* border-bottom: 2px solid #0277a2;margin-bottom: 10px;*/}
	.events_box_lh{display: table-cell; width: 80px; float: left; background: #0277a2; position: relative;}
	.block-content {
    bottom: 0;
    left: 0;
    padding: 0;
    right: 0;
    text-align: center;
    
    top: 0;
   /*position: absolute;*/
}
.align {
    display: table;
    height: 100%;
    width: 100%;
}
.align-middle {
    display: table-cell;
    height: 100%;
    vertical-align: middle;
}
.events_box_lh_titile{float: left; width: 100%; text-align: center;height: 100%;}
.events_box_lh_titile h2{text-align: center; width: 100%; color: #fff; font-size: 24px; font-weight: normal;}
.events_box_rh{display: table-cell;width: 84%;padding-left: 17px;padding-top: 20px;}
.events_box_rh_top{float: left; width: auto;}
.events_box_rh_title{float: left; width: auto;}
.events_box_rh_title h2{text-align: left; width: 100%; color: #202020; font-size: 22px; font-weight: 500; margin: 0px;}
.events_box_rh_title p{text-align: left; width: 100%; color: #5a5a5a; font-size: 12px; font-weight: 500; line-height: 18px;}
.events_box_rh_bot{float: left; width: 100%;border-top: 1px solid #d3d3d3;padding: 13px 0;}
.events_box_rh_bot_in{float: left; width: 100%;}
.events_box_rh_bot_lh{float: left; width: auto;}
.colck_in{float: left; width: auto;}
.colck_in img{float: left;}
.colck_in h2{text-align: left; float: left; width: auto; color: #000000; font-size: 13px; font-weight: normal;margin: 1px 0 0 4px;}
.events_box_rh_bot_rh {float: right; width: auto;margin-top: 2px;}
.events_box_rh_bot_rh .colck_in h2{ font-size: 15px; margin: -2px 0 0 4px;}
.events_box_rh_bot_rh .colck_in {margin: 0 6px;}
.dropbtn {

    color: white;
    padding: 0px;
    font-size: 16px;
    border: none;
    float: left;
    cursor: pointer;
}



.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    padding: 10px;
    right: 0;
    top: 20px;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown a:hover {background-color: #ddd}

.show {display:block;}
.events_box_lh_titile > img {
       height: 172px;
    object-fit: cover;
    width: 100%;
}
.events_new_ad{float: left; width: 100%; cursor: pointer;}
.events_box_rh > a {
    float: left;
    width: 100%;
}
</style>
</head>
	<div class="main">
		<div class="margin">
			<div class="main_sec">
			<?php
    
					if(count($geteventdetails) > 0)
					{
					$counter=1;
					foreach($geteventdetails as $values)
					{
				?>  
      



       
						<div class="events_box">
           <?php /*
							<div class="events_box_lh">
               <a href="<?php echo Yii::$app->request->baseUrl."/eventapi/event-details/".$values['id']?>">
								<div class="block-content">
									<div class="align">
										<div class="align-middle">
											<div class="events_box_lh_titile">
											 <?php 
                           $file_path = UPLOAD_DIR_PATH.'event/'.$values['image'];
                    if (!empty($values['image']) && file_exists($file_path))
                        {
                      ?>

                              <img src="<?php echo SITE_ABS_UPLOAD_PATH.'event/'.$values['image'];?>">
                      <?php 
                        }
                        else
                        {
                      ?>
                       <img src="<?php echo SITE_ABS_UPLOAD_PATH.'event/no-images.png';?>">
                      <?php 
                        }
                      ?>
											</div>
										</div>
									</div>
								</div>
                </a>
							</div>
              */ ?>
							<div class="events_box_rh">
              <a href="<?php echo Yii::$app->request->baseUrl."/api/eventapi/event-details?id=".$values['id']?>">
								<div class="events_box_rh_top">
									<div class="events_box_rh_title">
										<h2><?php echo $values['title']?></h2>
										<p>
											<?php echo $values['description']?>
										</p>
									</div>
								</div>
                </a>
								<div class="events_box_rh_bot">
									<div class="events_box_rh_bot_in">
										<div class="events_box_rh_bot_lh">
											<div class="colck_in">
												<img width="15px" src="<?php echo SITE_ABS_UPLOAD_PATH.'img/clock-icon.png';?>">
												<h2><?php echo date('d/m/Y h:i A',strtotime($values['start_time']));?></h2>

											</div>
										</div>
										<div class="events_box_rh_bot_rh">
											
												<div class="colck_in">
												<div class="dropdown">
											 
                       <div class="colck_in">
                        <img width="15px" src="<?php echo SITE_ABS_UPLOAD_PATH.'img/clock-icon.png';?>">
                        <h2 style="font-size:13px;"><?php echo date('d/m/Y h:i A',strtotime($values['end_time']));?></h2>

                      </div>
											<!-- 	<div class="colck_in">
													<img onclick="myFunction(<?php echo $counter;?>)" class="dropbtn" src="<?= SITE_ABS_UPLOAD_PATH.'img/share-icon.png'; ?>" width="15px">
												</div> -->
											
										  <div id="myDropdown<?php echo $counter;?>" class="dropdown-content">
											    <a href="http://twitter.com/share?text=<?php echo $values['title']?>&url=www.google.com" target="_blank" class="btn share-btn twitter"><i class="fa fa-twitter"></i>
												</a>
                        <a href="https://plus.google.com/share?url=<?php echo SITE_ABS_PATH.'admin/eventapi/event-details/'.$values['id']?>" target="_blank" class="btn share-btn google-plus"><i class="fa fa-google-plus"></i></a>

                        <?php /*<a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo Yii::$app->request->baseUrl.'/eventapi/event-details/'.$values['id'] ?>" target="_blank" class="btn share-btn facebook"><i class="fa fa-facebook"></i></a>
                        */ ?>
                         <a  href="http://www.facebook.com/sharer.php?u=<?php echo SITE_ABS_PATH.'admin/eventapi/event-details/'.$values['id']?>&quote=<?php echo $values['title']; ?>" title="<?php echo $values['title']; ?>" target="_blank" class="btn share-btn facebook"><i class="fa fa-facebook"></i></a>

												 <a href="mailto:?subject=<?php echo $values['title']?>&body=<?php echo strip_tags($values['description']);?>" target="_blank" class="btn share-btn email"><i class="fa fa-envelope"></i>
												</a>
										  </div>
											</div>
												
													 
												</div>
											
										
										</div>
									</div>
								</div>
							</div>


						</div>
            
			<?php 
          $counter++;
					}
					}
					else
					{
			?>
					<div class="events_box">
              <h3 style="text-align: center;">EVENTS</h3>
							<h4 style="text-align: center;">Sorry there are no event now.</h4>
					</div>
			<?php 
					}
			?>
			</div>
			</div>
		</div>
	</div>
    <script src="<?= Yii::$app->request->baseUrl; ?>/web/js/jquery.min.js"></script> 
	<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction(parameter1) {
  $('.dropdown-content').removeClass('show');
	parameter1='myDropdown'+parameter1;
    document.getElementById(parameter1).classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<body>
</body>
</html>

<?php

namespace common\components;
use Yii;
// namespace app\components; // For Yii2 Basic (app folder won't actually exist)
class MyHelpers
{
    public static function hello($name) {
        return "Hello $name";
    }

    function rand_color() {
        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }

    function time_ago_new($datetime) {

        
        $date2 = date("Y-m-d h:i:s");
        $date1 = $datetime;
        $diff = abs(strtotime($date2) - strtotime($date1));

        $min = 60;
        $hour = 60 * 60;
        $day = 60 * 60 * 24;
        $month = $day * 30;

        if ($diff < 60) { //Under a min
            $timeago = $diff . " seconds";
        } elseif ($diff < $hour) { //Under an hour
            $timeago = round($diff / $min) . " mins";
        } elseif ($diff < $day) { //Under a day
            $timeago = round($diff / $hour) . " hours";
        } elseif ($diff < $month) { //Under a day
            $timeago = round($diff / $day) . " days";
        } else {
            $timeago = round($diff / $month) . " months";
        }
        return $timeago;
    }
    
    function sorttextlen($text, $limit) {
        if (strlen($text) < $limit) {
            $sort_text = mb_substr($text, 0, $limit);
        } else if (strlen($text) > $limit) {
            $sort_text = mb_substr($text, 0, $limit) . '...';
        }

        return $sort_text;
    }

    public function getApiData($method,$apiName,$json_data)
    {   //$session = Yii::$app->session;
        //$location = $session->get('frontLocation');
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $apiName);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 120);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        //"LOCATION: $location",
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE, ]);
        if($method == 'POST'){
            curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data);    
        }   
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $output = json_decode($response,true);
        return $output;
    }


    public function getApiDataV2($method,$apiName,$json_data) {   
        $session = Yii::$app->session;
        $location = $session->get('frontLocation');

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $apiName.'&web=true',
         CURLOPT_HTTPHEADER => array(
            'USERNAME: '.API_USERNAME,
            'PASSWORD: '.API_PASSWORD,
            "LOCATION: $location",
            'TOKEN: '.API_TOKEN,
            'LANGUAGE: '.API_LANGUAGE,
        ),
        CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
   
      return $response;
    }

    public function convertLowercaseCapword($string) {   
        $returnString = ucwords(strtolower($string));
        return $returnString;
    }

    public function convertDateToDMY($passDate) {   
        $date = str_replace('/', '-', $passDate);
        $newDate = date('d-m-Y', strtotime($date));
        return $newDate;
    }

    public function check_file_exists_here($url){
       $result=get_headers($url);
       return stripos($result[0],"200 OK")?true:false; //check if $result[0] has 200 OK
    }


    public function get_img($path) {

        if( MyHelpers::check_file_exists_here($path))
            $data = $path;
        else
           $data = NO_IMAGE;


        // $path = trim($path);
        // if (!empty($path)) {
        //         if (file_exists($path )) {
        //             $data = $path;
        //         } else {
        //             $data = NO_IMAGE;
        //         }
        //     } else {
        //         $data = NO_IMAGE;
        // }
        return $data;
    }

}

<?php

//p(dirname(dirname(__DIR__)));

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'language' => 'en',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'api' => [
            'class' => 'api\Module',        
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to  
        // use your own export download action or custom translation 
        // message source
        // 'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
        ],
    ],
    'components' => [
       'formatter' => [
        // 'class' => 'yii\i18n\Formatter',
        // 'dateFormat' => 'php:d-m-Y',
        // 'datetimeFormat' => 'php:d-m-Y h:i A',
        // 'timeFormat' => 'php:h:i A', 
        'nullDisplay' => 'NA',
        ],
        /*'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
            ],
        ],*/
        'request' => [
            
             'enableCsrfValidation' => false,
        ],
        
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'dev.innoegic@gmail.com',
                'password' => 'Dev@Innovegic',
                'port' => '587',
                'encryption' => 'tls',
                'StreamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => TRUE,
                        'verify_peer' => 0,
                    ],
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'common' => [
            'class' => '\common\components\Common',
        ],
//        'common' => [
//            'class' => 'app\common\components\Common',
//        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => false,
            'showScriptName' => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
    ],
];

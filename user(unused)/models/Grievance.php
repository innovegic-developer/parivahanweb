<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "grievance".
 *
 * @property int $id ID
 * @property int $user_id User ID
 * @property string $user_type User Type
 * @property int $grievance_user_id Grievance User ID
 * @property string $grievance_user_type Grievance User Type
 * @property string $comment Comment
 * @property string $status
 * @property string $created_at Created At
 * @property string $updated_at Updated At
 *
 * @property User $user
 * @property User $grievanceUser
 */
class Grievance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grievance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'user_type', 'grievance_user_id', 'grievance_user_type', 'comment'], 'required'],
            [['user_id', 'grievance_user_id'], 'integer'],
            [['user_type', 'grievance_user_type', 'status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['comment'], 'string', 'max' => 5000],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['grievance_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['grievance_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'user_type' => Yii::t('app', 'User Type'),
            'grievance_user_id' => Yii::t('app', 'Grievance User ID'),
            'grievance_user_type' => Yii::t('app', 'Grievance User Type'),
            'comment' => Yii::t('app', 'Comment'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrievanceUser()
    {
        return $this->hasOne(User::className(), ['id' => 'grievance_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return GrievanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GrievanceQuery(get_called_class());
    }
}

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QuotationTruck;

/**
 * QuotationTruckSearch represents the model behind the search form of `app\models\QuotationTruck`.
 */
class QuotationTruckSearch extends QuotationTruck
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'quotation_id', 'load_id', 'truck_id', 'user_id', 'assigned_driver_id', 'created_by', 'updated_by'], 'integer'],
            [['vehicle_no', 'gps_installed', 'weight', 'truck_type', 'rc_no', 'rc_photo', 'license_no', 'license_photo', 'permit_type', 'permit_photo', 'insurance_policy_no', 'insurance_policy_photo', 'pollution_no', 'pollution_photo', 'rto_fitness_certificate_no', 'rto_fitness_certificate_photo', 'local_registration_no', 'local_registration_photo', 'registration_address', 'registration_mobile', 'office_landline_no', 'email', 'gross_weight', 'verified', 'assigned_driver_name', 'assigned_driver_mobile', 'assigned_driver_aadhaar_number', 'assigned_driver_at', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QuotationTruck::find();

        // add conditions that should always apply here
		$query->orderBy(['id' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'quotation_id' => $this->quotation_id,
            'load_id' => $this->load_id,
            'truck_id' => $this->truck_id,
            'user_id' => $this->user_id,
            'assigned_driver_id' => $this->assigned_driver_id,
            'assigned_driver_at' => $this->assigned_driver_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'vehicle_no', $this->vehicle_no])
            ->andFilterWhere(['like', 'gps_installed', $this->gps_installed])
            ->andFilterWhere(['like', 'weight', $this->weight])
            ->andFilterWhere(['like', 'truck_type', $this->truck_type])
            ->andFilterWhere(['like', 'rc_no', $this->rc_no])
            ->andFilterWhere(['like', 'rc_photo', $this->rc_photo])
            ->andFilterWhere(['like', 'license_no', $this->license_no])
            ->andFilterWhere(['like', 'license_photo', $this->license_photo])
            ->andFilterWhere(['like', 'permit_type', $this->permit_type])
            ->andFilterWhere(['like', 'permit_photo', $this->permit_photo])
            ->andFilterWhere(['like', 'insurance_policy_no', $this->insurance_policy_no])
            ->andFilterWhere(['like', 'insurance_policy_photo', $this->insurance_policy_photo])
            ->andFilterWhere(['like', 'pollution_no', $this->pollution_no])
            ->andFilterWhere(['like', 'pollution_photo', $this->pollution_photo])
            ->andFilterWhere(['like', 'rto_fitness_certificate_no', $this->rto_fitness_certificate_no])
            ->andFilterWhere(['like', 'rto_fitness_certificate_photo', $this->rto_fitness_certificate_photo])
            ->andFilterWhere(['like', 'local_registration_no', $this->local_registration_no])
            ->andFilterWhere(['like', 'local_registration_photo', $this->local_registration_photo])
            ->andFilterWhere(['like', 'registration_address', $this->registration_address])
            ->andFilterWhere(['like', 'registration_mobile', $this->registration_mobile])
            ->andFilterWhere(['like', 'office_landline_no', $this->office_landline_no])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'gross_weight', $this->gross_weight])
            ->andFilterWhere(['like', 'verified', $this->verified])
            ->andFilterWhere(['like', 'assigned_driver_name', $this->assigned_driver_name])
            ->andFilterWhere(['like', 'assigned_driver_mobile', $this->assigned_driver_mobile])
            ->andFilterWhere(['like', 'assigned_driver_aadhaar_number', $this->assigned_driver_aadhaar_number])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

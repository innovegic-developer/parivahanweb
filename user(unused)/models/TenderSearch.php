<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tender;

/**
 * TenderSearch represents the model behind the search form of `app\models\Tender`.
 */
class TenderSearch extends Tender
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['tender_title', 'tender_desc', 'tender_title_hi', 'tender_desc_hi', 'tender_title_gu', 'tender_desc_gu', 'tender_pdf', 'created_on', 'updated_on', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tender::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_on' => $this->created_on,
            'updated_on' => $this->updated_on,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'tender_title', $this->tender_title])
            ->andFilterWhere(['like', 'tender_desc', $this->tender_desc])
            ->andFilterWhere(['like', 'tender_title_hi', $this->tender_title_hi])
            ->andFilterWhere(['like', 'tender_desc_hi', $this->tender_desc_hi])
            ->andFilterWhere(['like', 'tender_title_gu', $this->tender_title_gu])
            ->andFilterWhere(['like', 'tender_desc_gu', $this->tender_desc_gu])
            ->andFilterWhere(['like', 'tender_pdf', $this->tender_pdf])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

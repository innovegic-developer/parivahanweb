<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "industry".
 *
 * @property int $id ID
 * @property int $user_id User ID
 * @property string $first_name First Name
 * @property string $last_name Last Name
 * @property string $email Email
 * @property string $mobile_number Mobile No.
 * @property string $company_name Company Name
 * @property int $company_type_id Company Type ID
 * @property string $company_type Company Type
 * @property string $incorporation_certificate_number Incorporation Certificate Number
 * @property string $shop_establishment_certificate_number Shop Establishment Certificate Number
 * @property string $pan_number PAN No.
 * @property string $incorporation_certificate_photo Incorporation Certificate Photo
 * @property string $shop_establishment_certificate_photo Shop Establishment Certificate Photo
 * @property string $gst_number GST No.
 * @property string $aadhaar_number Aadhaar No.
 * @property string $aadhaar_photo Aadhaar Photo
 * @property string $pan_photo PAN Photo
 * @property string $address_line1 Address line1
 * @property string $address_line2 Address line2
 * @property string $pincode Pincode
 * @property string $country Country
 * @property string $state State
 * @property string $district District
 * @property string $city City
 * @property string $area Area
 * @property string $landmark
 * @property string $remark Remark
 * @property int $verified Department Verification
 * @property string $status Status
 * @property string $created_at Created On
 * @property string $updated_at Updated On
 * @property int $created_by Created By
 * @property int $updated_by Updated By
 *
 * @property User $user
 */
class Industry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'industry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'company_type_id', 'verified', 'created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['first_name', 'last_name', 'email', 'company_name', 'company_type', 'incorporation_certificate_photo', 'shop_establishment_certificate_photo', 'aadhaar_photo', 'pan_photo', 'address_line1', 'address_line2', 'area', 'landmark', 'remark'], 'string', 'max' => 250],
            [['mobile_number', 'pincode'], 'string', 'max' => 10],
            [['incorporation_certificate_number', 'shop_establishment_certificate_number', 'state', 'district', 'city'], 'string', 'max' => 100],
            [['pan_number', 'gst_number', 'aadhaar_number', 'country'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'mobile_number' => Yii::t('app', 'Mobile No.'),
            'company_name' => Yii::t('app', 'Company Name'),
            'company_type_id' => Yii::t('app', 'Company Type ID'),
            'company_type' => Yii::t('app', 'Company Type'),
            'incorporation_certificate_number' => Yii::t('app', 'Incorporation Certificate Number'),
            'shop_establishment_certificate_number' => Yii::t('app', 'Shop Establishment Certificate Number'),
            'pan_number' => Yii::t('app', 'PAN No.'),
            'incorporation_certificate_photo' => Yii::t('app', 'Incorporation Certificate Photo'),
            'shop_establishment_certificate_photo' => Yii::t('app', 'Shop Establishment Certificate Photo'),
            'gst_number' => Yii::t('app', 'GST No.'),
            'aadhaar_number' => Yii::t('app', 'Aadhaar No.'),
            'aadhaar_photo' => Yii::t('app', 'Aadhaar Photo'),
            'pan_photo' => Yii::t('app', 'PAN Photo'),
            'address_line1' => Yii::t('app', 'Address line1'),
            'address_line2' => Yii::t('app', 'Address line2'),
            'pincode' => Yii::t('app', 'Pincode'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'district' => Yii::t('app', 'District'),
            'city' => Yii::t('app', 'City'),
            'area' => Yii::t('app', 'Area'),
            'landmark' => Yii::t('app', 'Landmark'),
            'remark' => Yii::t('app', 'Remark'),
            'verified' => Yii::t('app', 'Department Verification'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return IndustryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IndustryQuery(get_called_class());
    }
}

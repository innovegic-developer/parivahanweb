<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Loads;

/**
 * LoadsSearch represents the model behind the search form of `app\models\Loads`.
 */
class LoadsSearch extends Loads
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ref_rejected_load_id', 'industry_user_id', 'industry_id', 'source_address_id', 'no_of_trucks', 'max_bid_amount', 'assigned_user_id', 'assigned_bid_amount', 'rejected_by', 'created_by', 'updated_by'], 'integer'],
            [['booking_id', 'full_name', 'mobile_number', 'load_address_type', 'company_name', 'source_address_line1', 'source_address_line2', 'source_pincode', 'source_state', 'source_district', 'source_city', 'source_area', 'source_landmark', 'destination_address_line1', 'destination_address_line2', 'destination_pincode', 'destination_state', 'destination_district', 'destination_city', 'destination_area', 'destination_landmark', 'load_type', 'scheduling_type', 'material_type', 'weight', 'truck_type', 'bid_closing_date', 'bid_closing_time', 'reporting_date', 'reporting_time', 'description', 'share_to_preferred', 'assigned_status', 'assigned_at', 'assigned_full_name', 'assigned_mobile_number', 'assigned_company_name', 'assigned_truck_id', 'part_load_quotation_date', 'reject_reason', 'rejected_from', 'rejected_at', 'status', 'created_at', 'updated_at', 'quotations'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Loads::find();

        // add conditions that should always apply here
		$query->orderBy(['id' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ref_rejected_load_id' => $this->ref_rejected_load_id,
            'industry_user_id' => $this->industry_user_id,
            'industry_id' => $this->industry_id,
            'source_address_id' => $this->source_address_id,
            'no_of_trucks' => $this->no_of_trucks,
            'bid_closing_date' => $this->bid_closing_date,
            'reporting_date' => $this->reporting_date,
            'max_bid_amount' => $this->max_bid_amount,
            'assigned_at' => $this->assigned_at,
            'assigned_user_id' => $this->assigned_user_id,
            'assigned_bid_amount' => $this->assigned_bid_amount,
            'rejected_by' => $this->rejected_by,
            'rejected_at' => $this->rejected_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'booking_id', $this->booking_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'mobile_number', $this->mobile_number])
            ->andFilterWhere(['like', 'load_address_type', $this->load_address_type])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'source_address_line1', $this->source_address_line1])
            ->andFilterWhere(['like', 'source_address_line2', $this->source_address_line2])
            ->andFilterWhere(['like', 'source_pincode', $this->source_pincode])
            ->andFilterWhere(['like', 'source_state', $this->source_state])
            ->andFilterWhere(['like', 'source_district', $this->source_district])
            ->andFilterWhere(['like', 'source_city', $this->source_city])
            ->andFilterWhere(['like', 'source_area', $this->source_area])
            ->andFilterWhere(['like', 'source_landmark', $this->source_landmark])
            ->andFilterWhere(['like', 'destination_address_line1', $this->destination_address_line1])
            ->andFilterWhere(['like', 'destination_address_line2', $this->destination_address_line2])
            ->andFilterWhere(['like', 'destination_pincode', $this->destination_pincode])
            ->andFilterWhere(['like', 'destination_state', $this->destination_state])
            ->andFilterWhere(['like', 'destination_district', $this->destination_district])
            ->andFilterWhere(['like', 'destination_city', $this->destination_city])
            ->andFilterWhere(['like', 'destination_area', $this->destination_area])
            ->andFilterWhere(['like', 'destination_landmark', $this->destination_landmark])
            ->andFilterWhere(['like', 'load_type', $this->load_type])
            ->andFilterWhere(['like', 'scheduling_type', $this->scheduling_type])
            ->andFilterWhere(['like', 'material_type', $this->material_type])
            ->andFilterWhere(['like', 'weight', $this->weight])
            ->andFilterWhere(['like', 'truck_type', $this->truck_type])
            ->andFilterWhere(['like', 'bid_closing_time', $this->bid_closing_time])
            ->andFilterWhere(['like', 'reporting_time', $this->reporting_time])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'share_to_preferred', $this->share_to_preferred])
            ->andFilterWhere(['like', 'assigned_status', $this->assigned_status])
            ->andFilterWhere(['like', 'assigned_full_name', $this->assigned_full_name])
            ->andFilterWhere(['like', 'assigned_mobile_number', $this->assigned_mobile_number])
            ->andFilterWhere(['like', 'assigned_company_name', $this->assigned_company_name])
            ->andFilterWhere(['like', 'assigned_truck_id', $this->assigned_truck_id])
            ->andFilterWhere(['like', 'part_load_quotation_date', $this->part_load_quotation_date])
            ->andFilterWhere(['like', 'reject_reason', $this->reject_reason])
            ->andFilterWhere(['like', 'rejected_from', $this->rejected_from])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'quotations', $this->quotations]);

        return $dataProvider;
    }
}

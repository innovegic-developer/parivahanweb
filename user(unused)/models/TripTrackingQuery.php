<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TripTracking]].
 *
 * @see TripTracking
 */
class TripTrackingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TripTracking[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TripTracking|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Circular]].
 *
 * @see Circular
 */
class CircularQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Circular[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Circular|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

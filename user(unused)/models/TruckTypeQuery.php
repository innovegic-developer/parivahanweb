<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TruckType]].
 *
 * @see TruckType
 */
class TruckTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TruckType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TruckType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TruckDriver;

/**
 * TruckDriverSearch represents the model behind the search form of `app\models\TruckDriver`.
 */
class TruckDriverSearch extends TruckDriver
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'created_by', 'updated_by'], 'integer'],
            [['driver_name', 'driver_mobile', 'driver_current_address', 'driver_original_address', 'driver_aadhaar_number', 'driver_licence_number', 'driver_pan_number', 'driver_aadhaar_photo', 'driver_licence_photo', 'driver_pan_photo', 'verified', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TruckDriver::find();

        // add conditions that should always apply here
		$query->orderBy(['id' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'driver_name', $this->driver_name])
            ->andFilterWhere(['like', 'driver_mobile', $this->driver_mobile])
            ->andFilterWhere(['like', 'driver_current_address', $this->driver_current_address])
            ->andFilterWhere(['like', 'driver_original_address', $this->driver_original_address])
            ->andFilterWhere(['like', 'driver_aadhaar_number', $this->driver_aadhaar_number])
            ->andFilterWhere(['like', 'driver_licence_number', $this->driver_licence_number])
            ->andFilterWhere(['like', 'driver_pan_number', $this->driver_pan_number])
            ->andFilterWhere(['like', 'driver_aadhaar_photo', $this->driver_aadhaar_photo])
            ->andFilterWhere(['like', 'driver_licence_photo', $this->driver_licence_photo])
            ->andFilterWhere(['like', 'driver_pan_photo', $this->driver_pan_photo])
            ->andFilterWhere(['like', 'verified', $this->verified])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

<?php

namespace backend\controllers;

use Yii;
use app\models\Tender;
use app\models\TenderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * TenderController implements the CRUD actions for Tender model.
 */
class TenderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tender models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TenderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tender model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tender model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tender();

   if (Yii::$app->request->isPost) {
            if (empty($validate)) {

                foreach ($_POST['Tender'] as $key => $val) {
                    if (!empty($_POST['Tender'][$key])) {
                        $model->$key = $_POST['Tender'][$key];
                    }
                }
                $profile_pic = $_FILES['Tender']['name']['tender_pdf'];

                if (!empty($profile_pic)) {
                    $image = UploadedFile::getInstance($model, 'tender_pdf');

                    $extensions = explode(".", $image->name);
                    $extension = end($extensions);
                    $new_image_name = "Tender_".date('dmYHis'). ".{$extension}";

                    $original_path = UPLOAD_DIR_PATH. 'tender/'  . $new_image_name;
                    //$thumb_path = UPLOAD_DIR_PATH. 'fishing_zone/thumbnail/'  . $new_image_name;

                    $upload_pic = $image->saveAs($original_path);
                    if (file_exists($original_path)) {
                        $model->tender_pdf = $new_image_name;
                        //Start: For delete previous image
                        if (!empty($pre_profile_picture) && file_exists(UPLOAD_DIR_PATH. 'tender/'  . $pre_profile_picture)) {
                            unlink(UPLOAD_DIR_PATH. 'tender/'  . $pre_profile_picture);
                        }
                        //#unlink(PROFILE_PICTURE_ORIGINAL.$pre_profile_picture);
                       /* if (!empty($pre_profile_picture) && file_exists(UPLOAD_DIR_PATH. 'fishing_zone/thumbnail/'  . $pre_profile_picture)) {
                            unlink(UPLOAD_DIR_PATH. 'fishing_zone/thumbnail/'  . . $pre_profile_picture);
                        }*/
                        //End: For delete previous image
                        /*Image::thumbnail($original_path, THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT)
                                ->save($thumb_path, ['quality' => 100]);*/
                    } /*else {
                        $model->fishing_zone_pdf = $pre_profile_picture;
                    }*/
                }

                if ($model->save(False)) {
                    \Yii::$app->getSession()->setFlash('success', "Data has been updated successfully.");
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('tender/index/'));
                    Yii::$app->end();
                } else {
                    \Yii::$app->getSession()->setFlash('error', "Data not updated.");
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('tender/index/'));
                    Yii::$app->end();
                }
            } else {

                if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            }
        }
else {
        return $this->render('create', [
            'model' => $model,
        ]);
    }
}

    /**
     * Updates an existing Tender model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            if (empty($validate)) {

                foreach ($_POST['Tender'] as $key => $val) {
                    if (!empty($_POST['Tender'][$key])) {
                        $model->$key = $_POST['Tender'][$key];
                    }
                }
                $profile_pic = $_FILES['Tender']['name']['tender_pdf'];

                if (!empty($profile_pic)) {
                    $image = UploadedFile::getInstance($model, 'tender_pdf');

                    $extensions = explode(".", $image->name);
                    $extension = end($extensions);
                    $new_image_name = "Tender_".date('dmYHis'). ".{$extension}";

                    $original_path = UPLOAD_DIR_PATH. 'tender/'  . $new_image_name;
                    //$thumb_path = UPLOAD_DIR_PATH. 'fishing_zone/thumbnail/'  . $new_image_name;

                    $upload_pic = $image->saveAs($original_path);
                    if (file_exists($original_path)) {
                        $model->tender_pdf = $new_image_name;
                        //Start: For delete previous image
                        if (!empty($pre_profile_picture) && file_exists(UPLOAD_DIR_PATH. 'tender/'  . $pre_profile_picture)) {
                            unlink(UPLOAD_DIR_PATH. 'tender/'  . $pre_profile_picture);
                        }
                        //#unlink(PROFILE_PICTURE_ORIGINAL.$pre_profile_picture);
                       /* if (!empty($pre_profile_picture) && file_exists(UPLOAD_DIR_PATH. 'fishing_zone/thumbnail/'  . $pre_profile_picture)) {
                            unlink(UPLOAD_DIR_PATH. 'fishing_zone/thumbnail/'  . . $pre_profile_picture);
                        }*/
                        //End: For delete previous image
                        /*Image::thumbnail($original_path, THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT)
                                ->save($thumb_path, ['quality' => 100]);*/
                    } /*else {
                        $model->fishing_zone_pdf = $pre_profile_picture;
                    }*/
                }

                if ($model->save(False)) {
                    \Yii::$app->getSession()->setFlash('success', "Data has been updated successfully.");
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('tender/index/'));
                    Yii::$app->end();
                } else {
                    \Yii::$app->getSession()->setFlash('error', "Data not updated.");
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('tender/index/'));
                    Yii::$app->end();
                }
            } else {

                if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            }
        } else {

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
    /**
     * Deletes an existing Tender model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tender model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tender the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tender::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

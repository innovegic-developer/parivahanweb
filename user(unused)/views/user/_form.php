<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\Users */
/* @var $form yii\widgets\ActiveForm */

$user_roles = \app\models\UserTypeMaster::find()->where('TypeID IN(7,8,9)')
->andWhere(['IsDeleted' => '0'])->orderBy(['RoleType' => SORT_ASC])->asArray()->all();
?>

<body onload="UserType('<?php echo $model->user_level;?>')">

<div class="users-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>  

	<div class="col-xs-12 col-sm-6 col-md-6 col-xl-6">
		<?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-6 col-md-6 col-xl-6">
		<?= $form->field($model, 'mobile_number')->textInput(['type'=>'number', 'maxlength' => '10', 'onKeyDown' => "if((this.value.length==10 && event.keyCode!=8 && event.keyCode!=9) || event.keyCode==69 || event.keyCode==101) return false;"]) ?>
	</div>  
	
	<div class="col-xs-12 col-sm-6 col-md-6 col-xl-6">
		<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
	</div>    
	 
	 <div class="col-xs-12 col-sm-6 col-md-6 col-xl-6">  
		<?= $form->field($model, 'user_level')->dropDownList(ArrayHelper::map($user_roles, 'TypeID', 'RoleType'),['onchange' => 'UserType(this.value)']) ?>
	 </div>    
	 
	 <div class="col-xs-12 col-sm-6 col-md-6 col-xl-6">
		<?= $form->field($model, 'username')->textInput(['maxlength' => true])->label('Username') ?>
	</div>    
	
	<?php  if(empty($model->id)) { ?> 
	<div class="col-xs-12 col-sm-6 col-md-6 col-xl-6"> 
			<?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
	</div> 
	<?php } ?>
  
	

    <div class="col-xs-12 col-sm-6 col-md-6 col-xl-6">
    <?=
        $form->field($model, 'user_image')->fileInput()->label('Profile Photo')
        //$form->field($model, 'fishing_zone_pdf')->textInput(['maxlength' => '250'])
     ?>                                     
    </div>  

  <?php 
    if($model->id=='')
    {
       //echo $form->field($model, 'created_on')->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false);
       //echo $form->field($model, 'created_by')->hiddenInput(['value' => Yii::$app->user->id])->label(false);
    }
    else
    {    
       //echo $form->field($model, 'updated_on')->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false);
     
       //echo $form->field($model, 'updated_by')->hiddenInput(['value' => Yii::$app->user->id])->label(false);
    }
    ?>


    <div class="clearfix"></div>
    <div class="form-group col-sm-6 col-md-6 col-xl-6" style="margin-top:20px;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <?= Html::a('Cancel', ['/user/index'], ['class'=>'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

    </div>


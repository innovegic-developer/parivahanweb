<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Alerts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="circular-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
        <?= $form->field($model, 'circular_title')->textInput(['maxlength' => '250']) ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
        <?= $form->field($model, 'circular_desc')->textarea(['rows' => 4]) ?>
    </div>   
     <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
        <?php
        if($model->circular_pdf){
            echo $form->field($model, 'circular_pdf', ['enableClientValidation' => false])->fileInput()->label('Upload PDF');
        }
        else{
               echo $form->field($model, 'circular_pdf')->fileInput()->label('Upload PDF');  
        }
        if($model->circular_pdf){
              echo '<a target="_blank" href="'.SITE_PATH.'uploads/circular/'.$model->circular_pdf.'" >'.$model->circular_pdf.'</a>';
     }

     ?>                 
     </div> 
    <div class="col-xs-12 col-sm-6 col-md-6 col-xl-6">
        <?= $form->field($model, 'circular_date')->textInput(['type'=>'date']) ?>
    </div>    
     <div class="col-xs-12 col-sm-6 col-md-6 col-xl-6">
        <?= $form->field($model, 'status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', 'Deleted' => 'Deleted', ]) ?>
    </div>  
   <?php 
    if($model->id=='')
    {
       //echo $form->field($model, 'created_on')->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false);
       echo $form->field($model, 'created_by')->hiddenInput(['value' => Yii::$app->user->id])->label(false);
    }
    else
    {    
       //echo $form->field($model, 'updated_on')->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false);
       echo $form->field($model, 'updated_by')->hiddenInput(['value' => Yii::$app->user->id])->label(false);
    }
    ?>

    <div class="clearfix"></div>
    <div class="col-sm-6 col-md-6 col-xl-6" style="margin-top:20px;">
	<div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <?= Html::a('Cancel', ['/circular/index'], ['class'=>'btn btn-primary']) ?>
    </div>
	</div>

    <?php ActiveForm::end(); ?>

</div>


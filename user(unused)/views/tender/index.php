<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TenderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tenders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tender-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="pull-right margin_bottom_15">
   
    <?= Html::a(Yii::t('app', '<i class="fa fa-plus" aria-hidden="true"></i> New Tender'), ['create'], ['class' => 'btn btn-success']) ?>
  </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',


           [
                'attribute' =>  'tender_title',
                //'label' => 'Title (En)',
                'contentOptions' => ['style' => 'width:550px'],
            ],
           /* [
                'attribute' =>  'tender_title_hi',
                'label' => 'Title (Hi)',
                'contentOptions' => ['style' => 'width:550px'],
            ],
            [
                'attribute' =>  'tender_title_gu',
                'label' => 'Title (Gu)',
                'contentOptions' => ['style' => 'width:550px'],
            ],*/


            // 'tender_title',
             'tender_desc',
            // 'tender_title_hi',
            //'tender_desc_hi',
            // 'tender_title_gu',
            //'tender_desc_gu',
            //'tender_pdf',
			[
                'attribute'=>'created_on',
                'label' => 'Date',
                'format'=>['Date','php:d-m-Y'],
                'contentOptions' => ['style' => 'width:250px'],
            ],
            //'created_on',
            //'updated_on',
            //'created_by',
            //'updated_by',
            //'status',

            [
                'header' => 'Action',
                'headerOptions' => ['width' => '5%'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',             
                'visible' => TRUE
            ],
        ],
    ]); ?>
</div>

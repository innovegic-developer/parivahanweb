<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Transporter */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Transporters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="transporter-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'user_level',
            'first_name',
            'last_name',
            'email:email',
            'mobile_number',
            'designation',
            'company_name',
            'incorporation_certificate_number',
            'shop_establishment_certificate_number',
            'pan_number',
            'incorporation_certificate_photo',
            'shop_establishment_certificate_photo',
            'gst_number',
            'aadhaar_number',
            'aadhaar_photo',
            'pan_photo',
            'driver_operator_type',
            'address_line1',
            'address_line2',
            'pincode',
            'country',
            'state',
            'district',
            'city',
            'area',
            'landmark',
            'remark',
            'verified',
            'available_status',
            'status',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>

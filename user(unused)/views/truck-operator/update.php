<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Transporter */

$this->title = Yii::t('app', 'Update Transporter: {name}', [
    'name' => $model->first_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Transporters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->first_name, 'url' => ['index', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="transporter-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

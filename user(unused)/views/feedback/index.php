<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Feedbacks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-index">
	<?php Pjax::begin(); ?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
			//'user_id',
            'full_name',
            'mobile_number',
            'subject',
            'description',
            //'status',
            [
				'label' => 'Created At',
				'attribute' => 'created_at',
				'format' => ['date', 'php:d-m-Y'],
				'headerOptions' => ['style' => 'width:9%'],
				//'filter' => false,
            ],
            //'updated_at',
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

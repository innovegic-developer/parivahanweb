<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Loads */

$this->title = $model->booking_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Loads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="loads-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'ref_rejected_load_id',
            'booking_id',
            //'industry_user_id',
            //'industry_id',
            'full_name',
            'mobile_number',
            //'load_address_type',
            'company_name',
            //'source_address_id',
            'source_address_line1',
            'source_address_line2',
            'source_pincode',
            'source_state',
            'source_district',
            'source_city',
            'source_area',
            'source_landmark',
            'destination_address_line1',
            'destination_address_line2',
            'destination_pincode',
            'destination_state',
            'destination_district',
            'destination_city',
            'destination_area',
            'destination_landmark',
            'load_type',
            'scheduling_type',
            'material_type',
            'weight',
            'truck_type',
            'no_of_trucks',
            'bid_closing_date',
            'bid_closing_time',
            'reporting_date',
            'reporting_time',
            'max_bid_amount',
            'description',
            'share_to_preferred',
            'assigned_status',
            'assigned_at',
            //'assigned_user_id',
            'assigned_full_name',
            'assigned_mobile_number',
            'assigned_company_name',
            'assigned_bid_amount',
            //'assigned_truck_id',
            'part_load_quotation_date',
            'reject_reason',
            'rejected_from',
            'rejected_by',
            'rejected_at',
            //'status',
            'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',
            //'quotations',
        ],
    ]) ?>

</div>

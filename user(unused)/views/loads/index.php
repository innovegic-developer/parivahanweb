<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LoadsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Loads');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loads-index">
	<!--Success/Error Message --->
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="row col-xs-12 col-sm-12 col-md-12 col-xl-12 alert alert-success alert-dismissable" style="width: 100%; margin-left: 0px;">
        <div class="col-md-12">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
        </div>
    <?php endif; ?>
	<?php if (Yii::$app->session->hasFlash('error')): ?>
		<div class="row col-xs-12 col-sm-12 col-md-12 col-xl-12 alert alert-error alert-dismissable" style="width: 100%; margin-left: 0px;">
		<div class="col-md-12">
		 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			 <!--<h4><i class="icon fa fa-check"></i>Saved!</h4>-->
			 <?= Yii::$app->session->getFlash('error') ?>
		</div>
		</div>
   <?php endif; ?>
    <!--Success/Error Message --->
	
	<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'ref_rejected_load_id',
            'booking_id',
            //'industry_user_id',
            //'industry_id',
			'company_name',
            'full_name',
            'mobile_number',
            //'load_address_type',
            //'source_address_id',
            //'source_address_line1',
            //'source_address_line2',
            //'source_pincode',
            //'source_state',
            //'source_district',
            'source_city',
            //'source_area',
            //'source_landmark',
            //'destination_address_line1',
            //'destination_address_line2',
            //'destination_pincode',
            //'destination_state',
            //'destination_district',
            'destination_city',
            //'destination_area',
            //'destination_landmark',
            //'load_type',
            //'scheduling_type',
            'material_type',
            //'weight',
            'truck_type',
            //'no_of_trucks',
            //'bid_closing_date',
            //'bid_closing_time',
            'reporting_date',
            //'reporting_time',
            //'max_bid_amount',
            //'description',
            //'share_to_preferred',
            'assigned_status',
            //'assigned_at',
            //'assigned_user_id',
            //'assigned_full_name',
            //'assigned_mobile_number',
            //'assigned_company_name',
            //'assigned_bid_amount',
            //'assigned_truck_id',
            //'part_load_quotation_date',
            //'reject_reason',
            //'rejected_from',
            //'rejected_by',
            //'rejected_at',
            //'status',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',
            //'quotations',

            [
                'header' => 'Action',
                'headerOptions' => ['width' => '4%','class' => ' table-th'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{detail}',
                'buttons' =>
                [
                    'detail' => function($url, $model) {
                         $url = Yii::$app->urlManager->createUrl(['/loads/detail']).'&id=' . $model->id;
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'Details'),
                                    'data-pjax' => '0',
                        ]);
                    },
					
                ],
                'visible' => TRUE
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

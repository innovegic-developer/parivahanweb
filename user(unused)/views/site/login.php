<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use yii\captcha\Captcha;
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
#bg{
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
			height: 100%;
    } 
.logo-img img {
    width: 300px;
    margin: 80px 0 0px 0;
}
.logo-img1 img {
    width: 100%;
    margin: 80px 0 0px 0px;
}
.form-group.login-screen-btn {
    text-align: center;
}
.form-group.login-screen-btn button {
    text-align: center;
    border-radius: 0;
    padding: 0 70px;
    font-size: 16px;
    margin-top: 25px;
}

.checkbox {
    margin: 0 14px;
}
.box {
    position: relative;
    border-radius: 3px;
    background: transparent;
    border-top: none;
    margin-bottom: 20px;
    width: 100%;
    box-shadow: none;
}
.login-page {
   /* background: #2e7957;*/
}
</style>



<div class="site-login">
    <img src="<?=  SITE_PATH ?>img/login-screen-bg.jpg" id="bg" alt="login bg" />
    <div class="row">
        <div class="col-md-12 text-center logo-img"> 
            <img src="<?=  SITE_PATH ?>img/admin-logo.png" alt="<?=SITE_TITLE?>" title="<?=SITE_TITLE?>">
		</div>

        <div class="login-box">
            <div class="col-md-12 box box-radius">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username', ['template' => '
                        <div class="col-sm-12" style="margin-top:15px;">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-user"></span>
                                </span>
                                {input}
                            </div>{error}{hint}
                        </div>'])->textInput(['autofocus' => true])
                                ->input('text', ['placeholder'=>'Username']) ?>

                <?= $form->field($model, 'password', ['template' => '
                        <div class="col-sm-12" style="margin-top:15px;">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-lock"></span>
                                </span>
                                {input}
                            </div>{error}{hint}
                        </div>'])->passwordInput()
                                ->input('password', ['placeholder'=>'Password'])?>

                <?php //echo $form->field($model, 'rememberMe')->checkbox() ?>
                <div>
                 <?php /*echo $form->field($model, 'captcha')->widget(Captcha::className(),[
                                                    'template' =>
                                                    '{image}{input}',
                                                    'options' => [
                                                    'placeholder' => 'Enter code',
                                                    'class' => 'form-control captcha-txtbox',
                                                    ],
                                                    ])->label(false) */ ?>
                </div>  
                <div class="form-group login-screen-btn">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
	<div class="col-md-12 text-center logo-img1"> 
		<img src="<?=  SITE_PATH ?>img/admin-footer-logo.png" alt="<?=SITE_TITLE?>" title="<?=SITE_TITLE?>">
	</div>	
</div>

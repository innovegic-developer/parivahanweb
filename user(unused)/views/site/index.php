<?php

/* @var $this yii\web\View */

$this->title = '';
$user_level = Yii::$app->user->identity->user_level;
?>
	<style type="text/css">
          .dashboard_table-positive caption {
            background-color: #1a2226;
            color: #fff;
            text-transform: uppercase;
            font-weight: 600;
            font-size: 16px;
          }
          .dashboard_table-positive .table-bordered td {
				border: 0px solid #fff;
				/*text-align: center;*/
				background-color: #3c8dbc;
				color: #fff;
				padding: 3px 8px;
			}
          .dashboard_table-positive .table {
            margin-bottom: 0 !important;
          }
          .dashboard_table-positive .table-bordered tr {
            border-bottom: 2px solid #fff !important;
          }
          .first-td {
            padding-top: 40px !important;
			text-align: center;
          }
          .dashboard_table-positive .table-bordered h4 {
              text-transform: uppercase;
              margin: 0;
              font-weight: 600;
              font-size: 26px;
              color: #fff;
            }
          .dashboard_table-positive .table-bordered h2 {
              text-transform: uppercase;
              margin: 0;
              font-weight: 600;
              font-size: 42px;
              color: #fff;
            }
            .dashboard_table-positive .table-bordered td small {
              color: #fff;
			  font-size: 100%;
            }
            .dashboard_table-positive {
              padding-bottom: 15px;
            }
            .table-bordered {
              border: 0px solid #f4f4f4;
            }
            @media only screen and (max-width: 676px) {
              .dashboard_table-positive {
                padding-bottom: 0px;
              }
              .table-responsive {
                border: 0px solid #ddd;
              }
            }
    </style>
<div class="site-index">
	<?php if($user_level==2) { ?>
		<!--<div class="box-body no-padding" style="margin-top:-25px; text-align: right;">
           <div class="mailbox-controls">
              <a href="<?php echo Yii::$app->urlManager->createUrl('site/index');?>&action=refresh" class="btn btn-default btn-sm "><i class="fa fa-refresh"></i></a>                     
           </div>
		</div>-->
		<div class="row">
            <div class="col-lg-4">
              <div class="dashboard_table-positive table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <caption class="text-center">service seeker (dnh)</caption>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="first-td" rowspan="3" style="border-right: 2px solid !important;">
                        TOTAL REGISTERED
                        <h2><?=$TotalIndustryDnh?></h2>
                      </td>
                      
                    </tr>
                    
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="dashboard_table-positive table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <caption class="text-center">service seeker (Daman)</caption>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="first-td" rowspan="3" style="border-right: 2px solid !important;">
                        TOTAL REGISTERED
                        <h2><?=$TotalIndustryDaman?></h2>
                      </td>
                      
                    </tr>
                    
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="dashboard_table-positive table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <caption class="text-center">service seeker (Diu)</caption>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="first-td" rowspan="3" style="border-right: 2px solid !important;">
                        TOTAL REGISTERED
                        <h2><?=$TotalIndustryDiu?></h2>
                      </td>
                      
                    </tr>
                    
                  </tbody>
                </table>
              </div>
            </div>
            
			
			<div class="col-lg-4">
              <div class="dashboard_table-positive table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <caption class="text-center">transporter (dnh)</caption>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="first-td" rowspan="3" style="border-right: 2px solid !important;">
                        TOTAL REGISTERED
                        <h2><?=$TotalTransporterDnh?></h2>
                      </td>
                      
                    </tr>
                    
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="dashboard_table-positive table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <caption class="text-center">transporter (Daman)</caption>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="first-td" rowspan="3" style="border-right: 2px solid !important;">
                        TOTAL REGISTERED
                        <h2><?=$TotalTransporterDaman?></h2>
                      </td>
                      
                    </tr>
                    
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="dashboard_table-positive table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <caption class="text-center">transporter (Diu)</caption>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="first-td" rowspan="3" style="border-right: 2px solid !important;">
                        TOTAL REGISTERED
                        <h2><?=$TotalTransporterDiu?></h2>
                      </td>
                      
                    </tr>
                    
                  </tbody>
                </table>
              </div>
            </div>
            
        </div>
		
		
	<?php } else { ?>
		<div class="jumbotron">
		   <center> <h2>Welcome To <?=SITE_TITLE?></h2></center>
		</div>
	<?php } ?>
</div>

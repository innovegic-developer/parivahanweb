<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\QuotationTruck */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Quotation Trucks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="quotation-truck-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'quotation_id',
            'load_id',
            'truck_id',
            'user_id',
            'vehicle_no',
            'gps_installed',
            'weight',
            'truck_type',
            'rc_no',
            'rc_photo',
            'license_no',
            'license_photo',
            'permit_type',
            'permit_photo',
            'insurance_policy_no',
            'insurance_policy_photo',
            'pollution_no',
            'pollution_photo',
            'rto_fitness_certificate_no',
            'rto_fitness_certificate_photo',
            'local_registration_no',
            'local_registration_photo',
            'registration_address',
            'registration_mobile',
            'office_landline_no',
            'email:email',
            'gross_weight',
            'verified',
            'assigned_driver_id',
            'assigned_driver_name',
            'assigned_driver_mobile',
            'assigned_driver_aadhaar_number',
            'assigned_driver_at',
            'status',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>

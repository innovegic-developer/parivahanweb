<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\QuotationTruck */

$this->title = Yii::t('app', 'Create Quotation Truck');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Quotation Trucks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quotation-truck-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

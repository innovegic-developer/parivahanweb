<?php

use adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;
if (isset(Yii::$app->user->identity->user_image)) {
    $user_image = SITE_ABS_PATH_PROFILE_PICTURE . Yii::$app->user->identity->user_image;
} else {
    $user_image = SITE_ABS_PATH_PROFILE_PICTURE . 'default.jpg';
}
if(isset(Yii::$app->user->identity->user_level))
$user_level = Yii::$app->user->identity->user_level;
else
$user_level = '';
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
        <?= Html::img($user_image, ['class' => 'img-circle', 'alt' => 'User Image']) ?>
            </div>
            <div class="pull-left info">
                <p>
                    <?php
                    if (isset(Yii::$app->user->identity->first_name)) {
                        echo Yii::$app->user->identity->first_name." ";
                    }
                    if (isset(Yii::$app->user->identity->last_name)) {
                        echo Yii::$app->user->identity->last_name;
                    }
                    ?>       
                </p>
                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
            </div>
        </div>
        <!-- search form -->
       <!--  <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form> -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <?= Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Menu', 'options' => ['class' => 'header']],
                        ['label' => Yii::t('msg', 'Dashboard'), 'icon' => 'fa fa-dashboard', 
                            'url' => ['/'], 'active' => $this->context->route == 'site/index',
							'visible' => ($user_level==2 || $user_level==7 || $user_level==8),
                        ],
						/*['label' => Yii::t('msg', 'Industry'), 'icon' => 'fa fa-users', 
                            'url' => ['/user/industry'], 'active' => $this->context->route == 'user/industry'
                        ],
						['label' => Yii::t('msg', 'Transporter'), 'icon' => 'fa fa-users', 
                            'url' => ['/user/transporter'], 'active' => $this->context->route == 'user/transporter'
                        ],
						['label' => Yii::t('msg', 'Driver'), 'icon' => 'fa fa-users', 
                            'url' => ['/user/driver'], 'active' => $this->context->route == 'user/driver'
                        ],*/
						['label' => Yii::t('msg', 'Service Seeker'), 'icon' => 'fa fa-users', 
                            'url' => ['/industry'], 'active' => $this->context->route == 'industry',
							'visible' => ($user_level==2 || $user_level==7 || $user_level==9 || $user_level==11 || $user_level==13),
                        ],
						['label' => Yii::t('msg', 'Transporter'), 'icon' => 'fa fa-users', 
                            'url' => ['/transporter'], 'active' => $this->context->route == 'transporter',
							'visible' => ($user_level==2 || $user_level==8 || $user_level==10 || $user_level==12 || $user_level==14),
                        ],
						/*['label' => Yii::t('msg', 'Truck Owner'), 'icon' => 'fa fa-users', 
                            'url' => ['/truck-operator'], 'active' => $this->context->route == 'truck-operator'
                        ],*/
						['label' => Yii::t('msg', 'Loads'), 'icon' => 'fa fa-tasks', 
                            'url' => ['/loads'], 'active' => $this->context->route == 'loads/index',
							'visible' => ($user_level==2 || $user_level==7 || $user_level==8),
                        ],
						['label' => Yii::t('msg', 'Import Bulk Loads'), 'icon' => 'fa fa-upload', 
                            'url' => ['/loads/import'], 'active' => $this->context->route == 'loads/import',
							'visible' => ($user_level==2),
                        ],
						['label' => Yii::t('msg', 'Grievances'), 'icon' => 'fa fa-comment', 
                            'url' => ['/grievance'], 'active' => $this->context->route == 'grievance/index',
							'visible' => ($user_level==2 || $user_level==7 || $user_level==8),
                        ],
						['label' => Yii::t('msg', 'Feedbacks'), 'icon' => 'fa fa-commenting', 
                            'url' => ['/feedback'], 'active' => $this->context->route == 'feedback/index',
							'visible' => ($user_level==2 || $user_level==7 || $user_level==8),
                        ],
						['label' => Yii::t('msg', 'Tenders'), 'icon' => 'fa fa-icon fa fa-file-pdf-o', 
                            'url' => ['/tender'], 'active' => $this->context->route == 'tender/index',
							'visible' => ($user_level==2 || $user_level==7 || $user_level==8),
                        ],
						['label' => Yii::t('msg', 'Circulars'), 'icon' => 'fa fa-book', 
                            'url' => ['/circular'], 'active' => $this->context->route == 'circular/index',
							'visible' => ($user_level==2 || $user_level==7 || $user_level==8),
                        ],
                    ],
                ]
				
        )
        ?>
        
    </section>
    <!-- /.sidebar -->
</aside>

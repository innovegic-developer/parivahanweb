<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Grievance */

$this->title = Yii::t('app', 'Create Grievance');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Grievances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grievance-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

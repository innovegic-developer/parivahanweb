<?php
namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\Industry;
use frontend\models\Transporter;
use frontend\models\Loads;
//use frontend\models\SignupForm;
//use frontend\models\ContactForm;
//use api\models\Owners;
//use api\models\Fishermen;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    //public $baseURL = Url::base(true);
    /*public $baseURL  = 'http://localhost/df-web/index.php/';
    public $USERNAME = 'Df2018';
    public $PASSWORD = '355AF8D1795C8EC9D7547F2165AD4B49';
    public $TOKEN    = 'DFD27884-781G-05D4-0R11-DFD10EF83123';
    public $LANGUAGE = 'En';*/

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    //  [
                    //     'actions' => ['about'],
                    //     'allow' => true,
                    //     'roles' => ['@'],
                    // ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$IndustryCount = Industry::find()->where(['status'=>'Active'])
		->andWhere('first_name!="test" and last_name!="test" and company_name!="test"')->count();
		$TransporterCount = Transporter::find()->where(['user_level'=>'4', 'status'=>'Active'])
		->andWhere('first_name!="test" and last_name!="test" and company_name!="test"')->count();
		$OperatorCount = Transporter::find()->where(['user_level'=>'5', 'status'=>'Active'])
		->andWhere('first_name!="test" and last_name!="test" and company_name!="test"')->count();
      /*$vCount = Owners::find()->where(['status'=>'Active'])->count();
      $fCount = fishermen::find()->where(['status'=>'Active'])->count();
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "alerts",
      CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $alertData = (!empty($response))?json_decode($response):array();*/
      // print_r($objectiveData); exit;
      //return $this->render('index', array('alert' => $alertData, 'circular' => $circularData,  'news' =>$newsData, 'videogallery'=>$videogalleryData, 'photogallery'=>$photogalleryData, 'objective' => $objectiveData, 'fishspiecies' => $fishspieciesData, 'vesselCount' => $vCount, 'fishCount' => $fCount));
	  $data['IndustryCount'] = $IndustryCount;
	  $data['TransporterCount'] = $TransporterCount;
	  $data['OperatorCount'] = $OperatorCount;
	  $data['loads'] = Loads::find()->where(['assigned_status' => 'Open'])->andWhere('bid_closing_date >="'.date('Y-m-d').'"')
	  ->orderBy(['id' => SORT_DESC])->limit(8)->all();
	  return $this->render('index', $data);
    }


    public function actionAbout()
    {
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "about-us-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $aboutusData = json_decode($response);
     //print_r($aboutusData); exit;
        return $this->render('about', array('aboutus' => $aboutusData));
       
    }

     public function actionObjective()
    {

      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "objective-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $objectiveData = json_decode($response);
      
        return $this->render('objective', array('objective' => $objectiveData));
       
    }

    public function actionGeneralInformation()
    {
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "general-information-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $generalData = json_decode($response);
        
        return $this->render('general-information', array('general' => $generalData));
       
    }

  public function actionFacility(){

    return $this->render('facility');
  }


  public function actionFunctionsDuties(){
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "functions-duties-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $functionData = json_decode($response);
       // print_r($functionData); exit;
    return $this->render('functions-duties', array('function' => $functionData));
  }
    

 public function actionFishCatch(){
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "fish-catch-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $fishcatchData = json_decode($response);
      //print_r($fishData); exit;

    return $this->render('fish-catch', array('fishcatch' => $fishcatchData));
  }

    public function actionFishCraft(){
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "fish-craft-data",
      CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $fishcraftData = json_decode($response);
      //print_r($fishcraftData); exit;

    return $this->render('fish-craft', array('fishcraft' => $fishcraftData));
  }

   public function actionCitizenCharter(){

    $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "citizen-charters-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    $citizenData = (!empty($response))?json_decode($response):array();
    //print_r($response);exit;
    return $this->render('citizen-charter', array('citizen' => $citizenData));

  }


   public function actionSuomotoInformation(){

      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "suomoto-information-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $suomotoData = json_decode($response);


    return $this->render('suomoto-information', array('suomoto' => $suomotoData));
  }

  public function actionCircularOrder(){

      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "circulars-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $circularData = json_decode($response);
      return $this->render('circular-order', array('circular' => $circularData));
  }


public function actionActsRules(){

      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "acts-rules-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $actsData = json_decode($response);
   // print_r($actsData); exit;
    return $this->render('acts-rules', array('acts' => $actsData));
  }

  public function actionSubsidy(){
     $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "subsidy-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $subsidyData = json_decode($response);
    return $this->render('subsidy', array('subsidy' => $subsidyData));
  }

  public function actionTender(){
    
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "tender-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $tenderData = json_decode($response);
      //print_r($tenderData); exit;

    return $this->render('tender', array('tender' => $tenderData));
  }

  public function actionDownloadForms(){
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "forms-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $formsData = json_decode($response);
      //print_r($formsData); exit;
    return $this->render('download-forms', array('forms' => $formsData));
  }

  public function actionContact(){

    return $this->render('contact');
  }
 public function actionFeedback(){


  return $this->render('feedback');

  }

  public function actionForm(){
  
  $this->enableCsrfValidation = false;
    return $this->render('form');
    
  }

public function actionPhotoGallery1(){

      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "photo-gallery-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $photogalleryData = json_decode($response);
      
    return $this->render('photo-gallery-1', array('photogallery' => $photogalleryData));
  }

  public function actionPhotoGallery(){

    return $this->render('photo-gallery');
  }

  public function actionVideo(){
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "video-gallery-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $videogalleryData = json_decode($response);

    return $this->render('video', array('videogallery' => $videogalleryData));
  }

   public function actionVissonMission(){

    return $this->render('visson-mission');
  }

  public function actionIntroduction(){

    return $this->render('introduction');
  }

   public function actionAchievement(){

    return $this->render('achievement');
  }

   public function actionNews(){
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "news-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $newsData = json_decode($response);

    return $this->render('news', array('news' => $newsData));
  }


public function actionOrganizationChart(){

    return $this->render('organization-chart');
  }

  public function actionSitemap(){

    return $this->render('sitemap');
  }

  public function actionScreenReaderAccess(){

    return $this->render('screen-reader-access');
  }

  public function actionFishSpieciesAvailable(){
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => SITE_API . "fish-spiecies-data",
       CURLOPT_HTTPHEADER => array(
        'USERNAME: '.API_USERNAME,
        'PASSWORD: '.API_PASSWORD,
        'TOKEN: '.API_TOKEN,
        'LANGUAGE: '.API_LANGUAGE,
      ),
      CURLOPT_RETURNTRANSFER => true,
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      $fishspieciesData = json_decode($response);


  return $this->render('fish-spiecies-available', array('fishspiecies' => $fishspieciesData));
  }
  
}

<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\ContactForm;
use common\components\MyHelpers;

/**
 * Feedback Controller
 */
class FeedbackController extends Controller
{
   
    public $location = '';
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $session = Yii::$app->session;
        $this->location = $session->get('frontLocation');
    }
    /**
     * {@inheritdoc}
     */
    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'only' => ['logout', 'signup'],
    //             'rules' => [
    //                 [
    //                     'actions' => ['signup','setlocation'],
    //                     'allow' => true,
    //                     'roles' => ['?'],
    //                 ],
    //                 [
    //                     'actions' => ['logout'],
    //                     'allow' => true,
    //                     'roles' => ['@'],
    //                 ],
    //             ],
    //         ],
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'logout' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
	public function actionIndex()
    {
		if (Yii::$app->request->isPost) {
				$url =  API_URL . 'submit-feedback-web';
				$curl = curl_init();
				$full_name = Yii::$app->request->post('full_name');
                $mobile_number = Yii::$app->request->post('mobile_number');
                $subject = Yii::$app->request->post('subject');
                $description = Yii::$app->request->post('description');
				$web = '1';
                curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"web\"\r\n\r\n$web\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"full_name\"\r\n\r\n$full_name\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"mobile_number\"\r\n\r\n$mobile_number\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"subject\"\r\n\r\n$subject\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"description\"\r\n\r\n$description\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data;",
				CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
				"language: Gu",
				"password: 355AF8D1795C8EC9D7547F2165AD4B49",
				"postman-token: d299dad5-e002-3307-da1c-e3eac79cb12e",
				"token: DFD27884-781G-05D4-0R11-DFD10EF83123",
				"username: Df2018"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			if ($err) {
				$session = Yii::$app->session;
                Yii::$app->getSession()->setFlash('success', 'Something goint to be wrong.');
                $this->redirect(\Yii::$app->getUrlManager()->createUrl('feedback/index'));
                Yii::$app->end();
			} else {
                $json_result = json_decode($response, true);
               // print_r($json_result);exit;
                if($json_result['status'] == 1){
					$session = Yii::$app->session;
                    Yii::$app->getSession()->setFlash('success', $json_result['message']);
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('feedback/index'));
                    Yii::$app->end();
                }
                // print_r($json_result);exit;
                $session = Yii::$app->session;
                Yii::$app->getSession()->setFlash('error', $json_result['message']);
                $this->redirect(\Yii::$app->getUrlManager()->createUrl('feedback/index'));
                Yii::$app->end();
            }
        }
		return $this->render('index');
    }
	
}

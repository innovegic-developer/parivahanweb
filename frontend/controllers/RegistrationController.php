<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\ContactForm;
use common\components\MyHelpers;
use api\models\TruckType;
use api\models\Industry;
use api\models\IndustryAddresses;
use api\models\Transporter;
use api\models\Loads;
use api\models\Truck;
use api\models\Quotation;
use api\models\TruckDriver;
use api\models\QuotationTruck;
use api\models\TripTracking;
use api\models\PreferredList;

/**
 * Registration Controller
 */
class RegistrationController extends Controller
{
   
    public $location = '';
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $session = Yii::$app->session;
        $this->location = $session->get('frontLocation');
    }
    /**
     * {@inheritdoc}
     */
    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'only' => ['logout', 'signup'],
    //             'rules' => [
    //                 [
    //                     'actions' => ['signup','setlocation'],
    //                     'allow' => true,
    //                     'roles' => ['?'],
    //                 ],
    //                 [
    //                     'actions' => ['logout'],
    //                     'allow' => true,
    //                     'roles' => ['@'],
    //                 ],
    //             ],
    //         ],
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'logout' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
	public function actionIndustryRegistration()
    {
		$companyType = MyHelpers::getApiData('POST',API_URL . "get-company-type-master",array());
        if (Yii::$app->request->isPost) {
            // print_r(Yii::$app->request->post('first_name'));
            // print_r($_FILES['incorporation_certificate_photo']);
            // // print_r(Yii::$app->request->file())   ;
            // exit;

//              $data = array("first_name" => Yii::$app->request->post('first_name'), 
//                 "last_name" => Yii::$app->request->post('last_name'), 
//                 "email" => Yii::$app->request->post('email'), 
//                 "mobile_number" => Yii::$app->request->post('mobile_number'), 
//                 "address_line1" => Yii::$app->request->post('address_line1'), 
//                 "address_line2" => Yii::$app->request->post('address_line2'), 
//                 "pincode" => Yii::$app->request->post('pincode'), 
//                 "district" => Yii::$app->request->post('district'), 
//                 "area" => Yii::$app->request->post('area'), 
//                 "company_name" => Yii::$app->request->post('company_name'), 
//                 "company_type_id" => Yii::$app->request->post('company_type_id'), 
//                 "incorporation_certificate_number" => Yii::$app->request->post('incorporation_certificate_number'), 
//                 "state" => 'state', 
//                 "pan_number" => Yii::$app->request->post('pan_number'), 
//                 "shop_establishment_certificate_number" => Yii::$app->request->post('shop_establishment_certificate_number'), 
//                 "incorporation_certificate_photo" => Yii::$app->request->post('incorporation_certificate_photo'), 
//                 "pan_photo" => Yii::$app->request->post('pan_photo'), 
//                 "shop_establishment_certificate_photo" => Yii::$app->request->post('shop_establishment_certificate_photo')
//                 );

//                 $data_string = json_encode($data);
//              
                $url =  API_URL . 'industry-register';
// // print_r($data_string);exit;
// //                  $ch = curl_init($url);
// //                   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
// //                   curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
// //                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// //                   curl_setopt($ch, CURLOPT_HTTPHEADER, array(
// //                                      'Content-Type: application/json',
// //                                  'Content-Length: ' . strlen($data_string))
// //                    );
// //                   $result = curl_exec($ch);
// //                   curl_close($ch);
// //                   echo $result;
// //                   exit;


				$curl = curl_init();
				$first_name = Yii::$app->request->post('first_name');
                $last_name = Yii::$app->request->post('last_name');
                $email = Yii::$app->request->post('email');
                $mobile_number = Yii::$app->request->post('mobile_number');
                $address_line1 = Yii::$app->request->post('address_line1'); 
                $address_line2 = Yii::$app->request->post('address_line2'); 
                $pincode = Yii::$app->request->post('pincode');
                $district = Yii::$app->request->post('district');
                $area = Yii::$app->request->post('area');
                $company_name = Yii::$app->request->post('company_name');
                $company_type_id = Yii::$app->request->post('company_type_id');
				if(Yii::$app->request->post('state')!='')
				$state = Yii::$app->request->post('state');	
				else
                $state = 'Dadra and Nagar Haveli and Daman and Diu';
                $city = '';
                //$incorporation_certificate_number = Yii::$app->request->post('incorporation_certificate_number');
                //$pan_number = Yii::$app->request->post('pan_number');
                //$shop_establishment_certificate_number = Yii::$app->request->post('shop_establishment_certificate_number');
                //$incorporation_certificate_photo = '';
                //$pan_photo = '';
                //$shop_establishment_certificate_photo = '';
				$any_government_doc_number = Yii::$app->request->post('any_government_doc_number');
				$any_government_doc_photo = '';
				$udhyog_aadhaar_certificate_number = Yii::$app->request->post('udhyog_aadhaar_certificate_number');
				$udhyog_aadhaar_certificate_photo = '';
				$web = '1';
				curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"web\"\r\n\r\n$web\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"first_name\"\r\n\r\n$first_name\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"last_name\"\r\n\r\n$last_name\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"mobile_number\"\r\n\r\n$mobile_number\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"company_name\"\r\n\r\n$company_name\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"company_type_id\"\r\n\r\n$company_type_id\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"address_line1\"\r\n\r\n$address_line1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"email\"\r\n\r\n$email\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"address_line2\"\r\n\r\n$address_line2\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"pincode\"\r\n\r\n$pincode\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"state\"\r\n\r\n$state\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"district\"\r\n\r\n$district\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"city\"\r\n\r\n$city\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"area\"\r\n\r\n$area\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"any_government_doc_number\"\r\n\r\n$any_government_doc_number\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"udhyog_aadhaar_certificate_number\"\r\n\r\n$udhyog_aadhaar_certificate_number\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"any_government_doc_photo\"; filename=\"$any_government_doc_photo\"\r\nContent-Type: image/png\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
				  CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
				"language: Gu",
				"password: 355AF8D1795C8EC9D7547F2165AD4B49",
				"postman-token: d299dad5-e002-3307-da1c-e3eac79cb12e",
				"token: DFD27884-781G-05D4-0R11-DFD10EF83123",
				"username: Df2018"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			if ($err) {
				$session = Yii::$app->session;
                Yii::$app->getSession()->setFlash('success', 'Something will be wrong');
                $this->redirect(\Yii::$app->getUrlManager()->createUrl('registration/industry-registration'));
                Yii::$app->end();
			} else {
                $json_result = json_decode($response, true);
               // print_r($json_result);exit;
                if($json_result['status'] == 1){
                $model1 = Industry::find()->where(['user_id' => $json_result['data']['id']])->one();
                $path = UPLOAD_DIR_PATH . 'industry/';
                    /*if(isset($_FILES['incorporation_certificate_photo']['name'])  && trim($_FILES['incorporation_certificate_photo']['name'])!='') {    
                        $tmpName = $_FILES['incorporation_certificate_photo']['tmp_name'];
                        $str = explode(".",$_FILES['incorporation_certificate_photo']['name']);
                        $l=count($str);
                        $img =  "incorporation_certificate_photo"."_".time().".".$str[$l-1];
                        if(move_uploaded_file($tmpName,$path.$img)){
                            $model1->incorporation_certificate_photo = $img;
                        }
                    }
                    if(isset($_FILES['shop_establishment_certificate_photo']['name'])  && trim($_FILES['shop_establishment_certificate_photo']['name'])!='') {  
                        $tmpName = $_FILES['shop_establishment_certificate_photo']['tmp_name'];
                        $str = explode(".",$_FILES['shop_establishment_certificate_photo']['name']);
                        $l=count($str);
                        $img =  "shop_establishment_certificate_photo"."_".time().".".$str[$l-1];
                        if(move_uploaded_file($tmpName,$path.$img)){
                            $model1->shop_establishment_certificate_photo = $img;
                        }
                    }
                    if(isset($_FILES['pan_photo']['name'])  && trim($_FILES['pan_photo']['name'])!='') {    
                        $tmpName = $_FILES['pan_photo']['tmp_name'];
                        $str = explode(".",$_FILES['pan_photo']['name']);
                        $l=count($str);
                        $img =  "pan_photo"."_".time().".".$str[$l-1];
                        if(move_uploaded_file($tmpName,$path.$img)){
                            $model1->pan_photo = $img;
                        }
                    }*/
					if(isset($_FILES['any_government_doc_photo']['name'])  && trim($_FILES['any_government_doc_photo']['name'])!='') {    
                        $tmpName = $_FILES['any_government_doc_photo']['tmp_name'];
                        $str = explode(".",$_FILES['any_government_doc_photo']['name']);
                        $l=count($str);
                        $img =  "any_government_doc_photo"."_".time().".".$str[$l-1];
                        if(move_uploaded_file($tmpName,$path.$img)){
                            $model1->any_government_doc_photo = $img;
                        }
                    }
					if(isset($_FILES['udhyog_aadhaar_certificate_photo']['name'])  && trim($_FILES['udhyog_aadhaar_certificate_photo']['name'])!='') {    
                        $tmpName = $_FILES['udhyog_aadhaar_certificate_photo']['tmp_name'];
                        $str = explode(".",$_FILES['udhyog_aadhaar_certificate_photo']['name']);
                        $l=count($str);
                        $img =  "udhyog_aadhaar_certificate_photo"."_".time().".".$str[$l-1];
                        if(move_uploaded_file($tmpName,$path.$img)){
                            $model1->udhyog_aadhaar_certificate_photo = $img;
                        }
                    }
                    $model1->save(false);
                    $session = Yii::$app->session;
                    Yii::$app->getSession()->setFlash('success', $json_result['message']);
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('registration/industry-registration'));
                    Yii::$app->end();
                }
                // print_r($json_result);exit;
                $session = Yii::$app->session;
                Yii::$app->getSession()->setFlash('error', $json_result['message']);
                $this->redirect(\Yii::$app->getUrlManager()->createUrl('registration/industry-registration'));
                Yii::$app->end();
            }
        }
		$data['companyType'] = $companyType['data'];
		$state = MyHelpers::getApiData('POST',API_URL . "state-list",array());
		$data['state'] = $state['data'];
		$district = MyHelpers::getApiData('POST',API_URL . "district-list",array('state_id'=>9));
		$data['district'] = $district['data'];
        return $this->render('industry-registration',$data);
    }
	public function actionTransporterRegistration()
    {
		$companyType = MyHelpers::getApiData('POST',API_URL . "get-company-type-master",array());
        if (Yii::$app->request->isPost) {
            // print_r(Yii::$app->request->post());
            // print_r($_FILES['incorporation_certificate_photo']);
            //print_r(Yii::$app->request->file());
            // exit;
 
                $first_name = Yii::$app->request->post('first_name');
                $last_name = Yii::$app->request->post('last_name');
                $email = Yii::$app->request->post('email');
                $mobile_number = Yii::$app->request->post('mobile_number');
                $address_line1 = Yii::$app->request->post('address_line1'); 
                $address_line2 = Yii::$app->request->post('address_line2'); 
                $pincode = Yii::$app->request->post('pincode');
                $district = Yii::$app->request->post('district');
                $designation = Yii::$app->request->post('designation');
                $area = Yii::$app->request->post('area');
                $company_name = Yii::$app->request->post('company_name');
				if(Yii::$app->request->post('state')!='')
				$state = Yii::$app->request->post('state');	
				else
                $state = 'Dadra and Nagar Haveli and Daman and Diu';
                $city = '';
				//$incorporation_certificate_number = Yii::$app->request->post('incorporation_certificate_number');
                //$pan_number = Yii::$app->request->post('pan_number');
                //$shop_establishment_certificate_number = Yii::$app->request->post('shop_establishment_certificate_number');
                //$incorporation_certificate_photo = '';
                //$pan_photo = '';
                //$shop_establishment_certificate_photo = '';
				$rcc_number = Yii::$app->request->post('rcc_number');
				$rcc_photo = '';
				$web = '1';
				$curl = curl_init();
				$url =  API_URL . 'transporter-register';
				curl_setopt_array($curl, array(
				  CURLOPT_URL => $url,
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"web\"\r\n\r\n$web\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"first_name\"\r\n\r\n$first_name\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"last_name\"\r\n\r\n$last_name\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"email\"\r\n\r\n$email\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"mobile_number\"\r\n\r\n$mobile_number\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"company_name\"\r\n\r\n$company_name\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"designation\"\r\n\r\n$designation\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"address_line1\"\r\n\r\n$address_line1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"address_line2\"\r\n\r\n$address_line2\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"pincode\"\r\n\r\n$pincode\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"state\"\r\n\r\nstate\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"district\"\r\n\r\n$district\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"city\"\r\n\r\n$city\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"area\"\r\n\r\n$area\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"rcc_number\"\r\n\r\n$rcc_number\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data;  name=\"rcc_photo\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
				  CURLOPT_HTTPHEADER => array(
					"cache-control: no-cache",
					"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
					"language: Gu",
					"password: 355AF8D1795C8EC9D7547F2165AD4B49",
					"postman-token: 28ac152e-b130-4e1a-be07-44024b58461f",
					"token: DFD27884-781G-05D4-0R11-DFD10EF83123",
					"username: Df2018"
				  ),
				));

				$response = curl_exec($curl);
				// print_r($response);exit;
				$err = curl_error($curl);
				curl_close($curl);
				if ($err) {
				   $session = Yii::$app->session;
					Yii::$app->getSession()->setFlash('success', 'Something will be wrong');
					$this->redirect(\Yii::$app->getUrlManager()->createUrl('registration/transporter-registration'));
					Yii::$app->end();
				} else {
					$json_result = json_decode($response, true);
					//   print_r($json_result);exit;
                if($json_result['status'] == 1){
                $model1 = Transporter::find()->where(['user_id' => $json_result['data']['id']])->one();
                $path = UPLOAD_DIR_PATH . 'transporter/';
                    /*if(isset($_FILES['incorporation_certificate_photo']['name'])  && trim($_FILES['incorporation_certificate_photo']['name'])!='') {    
                        $tmpName = $_FILES['incorporation_certificate_photo']['tmp_name'];
                        $str = explode(".",$_FILES['incorporation_certificate_photo']['name']);
                        $l=count($str);
                        $img =  "incorporation_certificate_photo"."_".time().".".$str[$l-1];
                        if(move_uploaded_file($tmpName,$path.$img)){
                            $model1->incorporation_certificate_photo = $img;
                        }
                    }
                    if(isset($_FILES['shop_establishment_certificate_photo']['name'])  && trim($_FILES['shop_establishment_certificate_photo']['name'])!='') {  
                        $tmpName = $_FILES['shop_establishment_certificate_photo']['tmp_name'];
                        $str = explode(".",$_FILES['shop_establishment_certificate_photo']['name']);
                        $l=count($str);
                        $img =  "shop_establishment_certificate_photo"."_".time().".".$str[$l-1];
                        if(move_uploaded_file($tmpName,$path.$img)){
                            $model1->shop_establishment_certificate_photo = $img;
                        }
                    }
                    if(isset($_FILES['pan_photo']['name'])  && trim($_FILES['pan_photo']['name'])!='') {    
                        $tmpName = $_FILES['pan_photo']['tmp_name'];
                        $str = explode(".",$_FILES['pan_photo']['name']);
                        $l=count($str);
                        $img =  "pan_photo"."_".time().".".$str[$l-1];
                        if(move_uploaded_file($tmpName,$path.$img)){
                            $model1->pan_photo = $img;
                        }
                    }*/
					if(isset($_FILES['rcc_photo']['name'])  && trim($_FILES['rcc_photo']['name'])!='') {    
                        $tmpName = $_FILES['rcc_photo']['tmp_name'];
                        $str = explode(".",$_FILES['rcc_photo']['name']);
                        $l=count($str);
                        $img =  "rcc_photo"."_".time().".".$str[$l-1];
                        if(move_uploaded_file($tmpName,$path.$img)){
                            $model1->rcc_photo = $img;
                        }
                    }
                    $model1->save(false);
                    $session = Yii::$app->session;
                    Yii::$app->getSession()->setFlash('success', $json_result['message']);
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('registration/transporter-registration'));
                    Yii::$app->end();
                }
                // print_r($json_result);exit;
                $session = Yii::$app->session;
                Yii::$app->getSession()->setFlash('error', $json_result['message']);
                $this->redirect(\Yii::$app->getUrlManager()->createUrl('registration/transporter-registration'));
                Yii::$app->end();
            }
        }

		$data['companyType'] = $companyType['data'];
		$state = MyHelpers::getApiData('POST',API_URL . "state-list",array());
		$data['state'] = $state['data'];
		$district = MyHelpers::getApiData('POST',API_URL . "district-list",array('state_id'=>9));
		$data['district'] = $district['data'];
        return $this->render('transporter-registration',$data);
    }
	public function actionTruckOperatorRegistration()
    {
		$companyType = MyHelpers::getApiData('POST',API_URL . "get-company-type-master",array());

        if (Yii::$app->request->isPost) {
            // print_r(Yii::$app->request->post());
            // print_r($_FILES);
            // exit;
                $first_name = Yii::$app->request->post('first_name');
                $last_name = Yii::$app->request->post('last_name');
                $email = Yii::$app->request->post('email');
                $mobile_number = Yii::$app->request->post('mobile_number');
                $address_line1 = Yii::$app->request->post('address_line1'); 
                $address_line2 = Yii::$app->request->post('address_line2'); 
                $pincode = Yii::$app->request->post('pincode');
                $district = Yii::$app->request->post('district');
                $area = Yii::$app->request->post('area');
                $state = 'state';
                $city = '';
                $pan_number = Yii::$app->request->post('pan_number');
                $driver_operator_type = Yii::$app->request->post('driver_operator_type');
                $driver_licence_number = Yii::$app->request->post('driver_licence_number');
                $aadhaar_number = Yii::$app->request->post('aadhaar_number');
                $pan_photo = '';
                $incorporation_certificate_number = '';
                $shop_establishment_certificate_number = '';
				$web = '1';
        $url =  API_URL . 'truck-operator-register';
       $curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"web\"\r\n\r\n$web\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"first_name\"\r\n\r\n$first_name\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"last_name\"\r\n\r\n$last_name\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"email\"\r\n\r\n$email\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"mobile_number\"\r\n\r\n$mobile_number\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"address_line1\"\r\n\r\n$address_line1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"address_line2\"\r\n\r\n$address_line2\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"pincode\"\r\n\r\n$pincode\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"state\"\r\n\r\n$state\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"district\"\r\n\r\n$district\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"city\"\r\n\r\n$city\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"driver_operator_type\"\r\n\r\n$driver_operator_type\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"area\"\r\n\r\n$area\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data;  name=\"aadhaar_number\"\r\n\r\n$aadhaar_number\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"driver_licence_number\"\r\n\r\n$driver_licence_number\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"incorporation_certificate_number\"\r\n\r\n$incorporation_certificate_number\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"shop_establishment_certificate_number\"\r\n\r\n$shop_establishment_certificate_number\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"pan_number\"\r\n\r\n$pan_number\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
  CURLOPT_HTTPHEADER => array( 
    "cache-control: no-cache",
    "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
    "language: Gu",
    "password: 355AF8D1795C8EC9D7547F2165AD4B49",
    "postman-token: 34d09fd2-a526-8b07-8084-94a4b140e89c",
    "token: DFD27884-781G-05D4-0R11-DFD10EF83123",
    "username: Df2018"
  ),
));
        $response = curl_exec($curl);
        // print_r($response);exit;
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
           $session = Yii::$app->session;
                Yii::$app->getSession()->setFlash('success', 'Something will be wrong');
                $this->redirect(\Yii::$app->getUrlManager()->createUrl('registration/truck-operator-registration'));
                Yii::$app->end();
        } else {
                $json_result = json_decode($response, true);
                if($json_result['status'] == 1){
                $model1 = Transporter::find()->where(['user_id' => $json_result['data']['id']])->one();
                $path = UPLOAD_DIR_PATH . 'transporter/';
                    if(isset($_FILES['pan_photo']['name'])  && trim($_FILES['pan_photo']['name'])!='') {    
                        $tmpName = $_FILES['pan_photo']['tmp_name'];
                        $str = explode(".",$_FILES['pan_photo']['name']);
                        $l=count($str);
                        $img =  "pan_photo"."_".time().".".$str[$l-1];
                        if(move_uploaded_file($tmpName,$path.$img)){
                            $model1->pan_photo = $img;
                        }
                    }
                    if(isset($_FILES['aadhaar_photo']['name'])  && trim($_FILES['aadhaar_photo']['name'])!='') {    
                        $tmpName = $_FILES['aadhaar_photo']['tmp_name'];
                        $str = explode(".",$_FILES['aadhaar_photo']['name']);
                        $l=count($str);
                        $img =  "aadhaar_photo"."_".time().".".$str[$l-1];
                        if(move_uploaded_file($tmpName,$path.$img)){
                            $model1->aadhaar_photo = $img;
                        }
                    }
                    if(isset($_FILES['driver_licence_photo']['name'])  && trim($_FILES['driver_licence_photo']['name'])!='') {  
                        $tmpName = $_FILES['driver_licence_photo']['tmp_name'];
                        $str = explode(".",$_FILES['driver_licence_photo']['name']);
                        $l=count($str);
                        $img =  "driver_licence_photo"."_".time().".".$str[$l-1];
                        if(move_uploaded_file($tmpName,$path.$img)){
                            $model1->driver_licence_photo = $img;
                        }
                    }
                    $model1->save(false);
                    $session = Yii::$app->session;
                    Yii::$app->getSession()->setFlash('success', $json_result['message']);
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('registration/truck-operator-registration'));
                    Yii::$app->end();
                }
                // print_r($json_result);exit;
                $session = Yii::$app->session;
                Yii::$app->getSession()->setFlash('error', $json_result['message']);
                $this->redirect(\Yii::$app->getUrlManager()->createUrl('registration/truck-operator-registration'));
                Yii::$app->end();
            }
        }

		$data['companyType'] = $companyType['data'];
		$state = MyHelpers::getApiData('POST',API_URL . "state-list",array());
		$data['state'] = $state['data'];
		$district = MyHelpers::getApiData('POST',API_URL . "district-list",array('state_id'=>9));
		$data['district'] = $district['data'];
        return $this->render('truck-operator-registration',$data);
    }
	public function actionGetAreas()
    {
		//$district = is_numeric($_POST['district'])?$_POST['district']:157; // temporary
        $district = $_POST['district']; // temporary
		$area = MyHelpers::getApiData('POST',API_URL . "district-area-list",array('district_id'=>$district));
		//return $area;
		//return $this->render('register',$area);
		return \yii\helpers\Json::encode($area['data']); 
    }
	
   /* public function actionSubmit()
    {
       $user_id = Yii::$app->user->id;
       $topic_id = $_POST['tid'];
       $description = $_POST['description'];
       $latestIndustryObj = MyHelpers::getApiData('POST',API_URL . "submit-debate",array('user_id' =>$user_id,'topic_id' =>$topic_id,'description' =>$description));
       $this->redirect(array('debate/detail/'.$topic_id));
    }*/

}

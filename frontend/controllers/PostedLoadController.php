<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\ContactForm;
use frontend\models\Loads;
use common\components\MyHelpers;
use yii\data\Pagination;
/**
 * PostedLoad Controller
 */
class PostedLoadController extends Controller
{
   
    public $location = '';
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $session = Yii::$app->session;
        $this->location = $session->get('frontLocation');
    }
    /**
     * {@inheritdoc}
     */
    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'only' => ['logout', 'signup'],
    //             'rules' => [
    //                 [
    //                     'actions' => ['signup','setlocation'],
    //                     'allow' => true,
    //                     'roles' => ['?'],
    //                 ],
    //                 [
    //                     'actions' => ['logout'],
    //                     'allow' => true,
    //                     'roles' => ['@'],
    //                 ],
    //             ],
    //         ],
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'logout' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
	
	
   /* public function actionIndex()
    {
       $data['loads'] = Loads::find()->where(['assigned_status' => 'Open'])->andWhere('bid_closing_date >="'.date('Y-m-d').'"')
	  ->orderBy(['id' => SORT_DESC])->limit(20)->all();
	   return $this->render('index', $data);
    }*/
	
	public function actionIndex()
	{
		$query = Loads::find()->where(['assigned_status' => 'Open'])->andWhere('bid_closing_date >="'.date('Y-m-d').'"');
		$countQuery = clone $query;
		$pages = new Pagination(['totalCount' => $countQuery->count()]);
		$models = $query->offset($pages->offset)
			->limit($pages->limit)
			->orderBy(['id' => SORT_DESC])
			->all();
		//print_r($models);exit;
		return $this->render('index', [
			 'loads' => $models,
			 'pages' => $pages,
		]);
	}
}

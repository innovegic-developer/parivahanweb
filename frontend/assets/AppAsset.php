<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $sourcePath = '@bower/df/';
    public $css = [
        // 'css/site.css',
        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
        'css/style.css',
        'css/responsive.css',
        'css/menu.css',
        'css/owl.carousel.min.css',
         'css/owl.theme.default.min.css',
        // 'css/sitemap.css',
        
        

    ];
    public $js = [
         'js/jquery.min.js',
         'js/bootstrap.min.js',
        'js/accordion.min.js',
        'js/owl.carousel.min.js',
       
        // 'js/jquery-1.7.1.min.js',
        'js/main.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    
}

<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Loads;

/**
 * LoadsSearch represents the model behind the search form of `frontend\models\Loads`.
 */
class LoadsSearch extends Loads
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'industry_id', 'no_of_trucks', 'created_by', 'updated_by'], 'integer'],
            [['scheduling_type', 'source_city', 'destination_city', 'material_type', 'weight', 'truck_type', 'reporting_date', 'reporting_time', 'description', 'current_status', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Loads::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'industry_id' => $this->industry_id,
            'no_of_trucks' => $this->no_of_trucks,
            'reporting_date' => $this->reporting_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'scheduling_type', $this->scheduling_type])
            ->andFilterWhere(['like', 'source_city', $this->source_city])
            ->andFilterWhere(['like', 'destination_city', $this->destination_city])
            ->andFilterWhere(['like', 'material_type', $this->material_type])
            ->andFilterWhere(['like', 'weight', $this->weight])
            ->andFilterWhere(['like', 'truck_type', $this->truck_type])
            ->andFilterWhere(['like', 'reporting_time', $this->reporting_time])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'current_status', $this->current_status])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[Loads]].
 *
 * @see Loads
 */
class LoadsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Loads[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Loads|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

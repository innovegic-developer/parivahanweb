<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "loads".
 *
 * @property int $id ID
 * @property int $user_id User ID
 * @property int $industry_id Industry ID
 * @property string $scheduling_type Scheduling Type
 * @property string|null $source_city Source City
 * @property string|null $destination_city Destination City
 * @property string|null $material_type Material Type
 * @property string|null $weight Weight
 * @property string|null $truck_type Truck Type
 * @property int|null $no_of_trucks No. of Trucks
 * @property string|null $reporting_date Selected Date
 * @property string|null $reporting_time Reporting Time
 * @property string|null $description Description
 * @property string|null $current_status Current Status
 * @property string $status Status
 * @property string|null $created_at Created On
 * @property string|null $updated_at Updated On
 * @property int|null $created_by Created By
 * @property int|null $updated_by Updated By
 *
 * @property User $user
 * @property Industry $industry
 */
class Loads extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'industry_id'], 'required'],
            [['user_id', 'industry_id', 'no_of_trucks', 'created_by', 'updated_by'], 'integer'],
            [['scheduling_type', 'status'], 'string'],
            [['reporting_date', 'created_at', 'updated_at'], 'safe'],
            [['source_city', 'destination_city', 'material_type', 'truck_type', 'description'], 'string', 'max' => 250],
            [['weight'], 'string', 'max' => 50],
            [['reporting_time', 'current_status'], 'string', 'max' => 20],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['industry_id'], 'exist', 'skipOnError' => true, 'targetClass' => Industry::className(), 'targetAttribute' => ['industry_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'industry_id' => Yii::t('app', 'Industry ID'),
            'scheduling_type' => Yii::t('app', 'Scheduling Type'),
            'source_city' => Yii::t('app', 'Source City'),
            'destination_city' => Yii::t('app', 'Destination City'),
            'material_type' => Yii::t('app', 'Material Type'),
            'weight' => Yii::t('app', 'Weight'),
            'truck_type' => Yii::t('app', 'Truck Type'),
            'no_of_trucks' => Yii::t('app', 'No. of Trucks'),
            'reporting_date' => Yii::t('app', 'Selected Date'),
            'reporting_time' => Yii::t('app', 'Reporting Time'),
            'description' => Yii::t('app', 'Description'),
            'current_status' => Yii::t('app', 'Current Status'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[Industry]].
     *
     * @return \yii\db\ActiveQuery|IndustryQuery
     */
    public function getIndustry()
    {
        return $this->hasOne(Industry::className(), ['id' => 'industry_id']);
    }

    /**
     * {@inheritdoc}
     * @return LoadsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LoadsQuery(get_called_class());
    }
}

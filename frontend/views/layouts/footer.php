<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="footer-box">
					<h5>Parivahan Suvidha</h5>
					<ul>
						<li><i class="fas fa-map-marker"></i> <address>Omnibus Industrial Development Corporation of 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Daman & Diu and Dadra & Nagar Haveli <br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Corporate Office, Plot No. 35, <br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Somnath, Nani Daman, Daman - 396210</address></li>
						<li><i class="fas fa-mobile-alt"></i>&nbsp; 0260-2240976</li>
						<li><i class="fas fa-fax"></i> 0260-2241108</li>
						<li><i class="fas fa-envelope"></i> oidcparivahan@gmail.com</li>
					</ul>
				</div>
			</div>
			<div class="col-md-5">
				<div class="footer-box">
					<h5>Location</h5>
				</div>
				<div class="map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d59836.55197320924!2d72.80884188523021!3d20.39177340833313!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be0da75935e4811%3A0x6339b54f4106b826!2sDaman%2C%20Daman%20and%20Diu!5e0!3m2!1sen!2sin!4v1595268977327!5m2!1sen!2sin" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				</div>
			</div>
			<div class="col-md-3">
				<div class="footer-box">
					<h5>Useful Links</h5>
					<ul>
						<li><a href="<?= SITE_PATH; ?>content/terms">Terms and Conditions</a></li>
						<li><a href="<?= SITE_PATH; ?>content/privacy">Privacy Policy</a></li>
						<li><a href="<?= SITE_PATH; ?>content/disclaimer">Disclaimer</a></li>
						<li><a href="<?= SITE_PATH; ?>feedback">Feedback</a></li>
						<!--<li class="visitor"><a href="#">VISITOR : 554</a></li>-->
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright">© Content owned by U.T. Administration of Daman & Diu. Government of India.</div>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="<?= SITE_PATH; ?>js/bootstrap.bundle.min.js"></script>
<script src="<?= SITE_PATH; ?>js/all.min.js"></script>
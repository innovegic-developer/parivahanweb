<?php
//echo $url;exit;
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
$assets = frontend\assets\AppAsset::register($this);
$baseUrl = $assets->baseUrl;
AppAsset::register($this);
$url = trim($_SERVER['REQUEST_URI']);
//if(!isset($_GET['dev']) && ($url=='/' || $url=='')) {
if(false) {
?>

<!DOCTYPE html>
<html>
<head>
<title>PARIVAHAN SUVIDHA</title>
<style>
.main {
background:#082755;
color:#fff;
/*font-size:17px;*/
font-family:Calibri;
}
</style>
</head>
<body class="main">
<center><h1>COMING SOON</h1></center>
<center><h3>Our website is under construction</h3></center>
</body>
</html>

<?php } else { ?>

<?php $this->beginPage() ?>
<body>
<?php $this->beginBody() ?>
	<?php echo $this->render('header'); ?>
	
    <?= $content ?>
		
	<?php echo $this->render('footer'); ?>
   
<?php $this->endBody() ?>
</body>
<?php $this->endPage() ?>

<?php } ?>
<?php 
$url = trim($_SERVER['REQUEST_URI']);
$IsMobile = false;
$useragent=$_SERVER['HTTP_USER_AGENT'];
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
	{
		$IsMobile = true;
	}?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v4.0.1">
    <link rel="icon" href="<?= SITE_PATH; ?>img/logo.png">
    <title>Parivahan suvidha</title>

    <!-- Bootstrap core CSS -->
	<link href="<?= SITE_PATH; ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= SITE_PATH; ?>css/style.css" rel="stylesheet">
  </head>
<header>
    <div class="headertop">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-6">
		    		<div class="logo">
						<a href="<?= SITE_PATH; ?>">
		    			<img src="<?= SITE_PATH; ?>img/logo.png" width="75" height="48">
						</a>
		    			<div class="logo-text">
		    				<h4>Parivahan suvidha</h4>
		    				<p>UT Administration of Dadra and Nagar Haveli and Daman and Diu</p>
		    			</div>
		    		</div>
		    	</div>
    			<div class="col-md-6">
		    		<div class="top-menu text-right">
		    			<div class="langue">
		    				<!--<a href="#" onclick="decreaseFont();" style="font-size: 14px;">A-</a>
                            <a href="#" onclick="resetFont();" style="font-size: 16px;">A</a>
                            <a href="#" onclick="increaseFont();" style="font-size: 17px;">A+</a>
		    				<select>
		    					<option value="english">English</option>
		    					<option value="हिन्दी">हिन्दी</option>		    				
		    				</select>-->
		    			</div>
		    			<!--<a href="javascript:void(0)" id="skip_main_content_start">Skip to main content</a> |--> <a href="<?= SITE_PATH; ?>feedback">Feedback</a> | <a href="<?= SITE_PATH; ?>sitemap">Sitemap</a> | <?php if(!$IsMobile) { ?> <a href="<?= SITE_PATH; ?>contactus">Contact Us</a> | <?php } else { ?> <a href="<?= SITE_PATH; ?>register">Free Registration</a> |<?php } ?> 
						<a href="#" onclick="decreaseFont();" style="font-size: 14px;">A-</a>
                        <a href="#" onclick="resetFont();" style="font-size: 16px;">A</a>
                        <a href="#" onclick="increaseFont();" style="font-size: 17px;">A+</a>
		    		</div>
		    	</div>
    		</div>
    	</div>
    </div>	
  	<nav class="navbar navbar-expand-md navbar-dark">
  		<div class="container">
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse" id="navbarCollapse">
	      <ul class="navbar-nav mr-auto">
	        <li class="nav-item <?=($url=='/' || $url=='')?'active':''?>">
	          <a class="nav-link" href="<?= SITE_PATH; ?>">Home <span class="sr-only">(current)</span></a>
	        </li>
			<li class="nav-item <?=(strpos($url,'posted-load')!== false)?'active':''?>">
	          <a class="nav-link" href="<?= SITE_PATH; ?>posted-load">Posted Loads</a>
	        </li>
	        <li class="nav-item <?=(strpos($url,'tender')!== false)?'active':''?>">
	          <a class="nav-link" href="<?= SITE_PATH; ?>tender">Tenders</a>
	        </li>
			<li class="nav-item <?=(strpos($url,'circular')!== false)?'active':''?>">
	          <a class="nav-link" href="<?= SITE_PATH; ?>circular">Circulars</a>
	        </li>
	        <li class="nav-item <?=(strpos($url,'contactus')!== false)?'active':''?>">
	          <a class="nav-link" href="<?= SITE_PATH; ?>contactus">Contact Us</a>
	        </li>
			 <li class="nav-item active" style="margin-left:462px">
	          <a class="nav-link" href="<?= SITE_PATH; ?>register">Free Registration</a>
	        </li>
	      </ul>
	    </div>
	</div>
  	</nav>
</header>

<script type="text/javascript">
  var section;
    var factor = 0.9;
    var count = 0;
      $(document).ready(function(){          
          $(document).keypress(function (e) {
              if (e.which == 13) {
                  if($('#search_txt').val() != ''){
                      redirect_search_page();
                  }
              }
          });
          $('#search_btn').click(function () {
              redirect_search_page();
          });

          function redirect_search_page() {
              var request = encodeURIComponent($('#search_txt').val());
              //alert(request);
             //window.location.href = "<?php echo Yii::$app->urlManager->createUrl(['site/advancesearch']); ?>?search=" + request;
              $( "#searchFrm" ).submit();
          }
          
      });
     function getFontSize(el)
    {
        var fs = $(el).css('font-size');
        if (!el.originalFontSize)
            el.originalFontSize = fs; //set dynamic property for later reset  
        return  parseFloat(fs);
    }
    function setFontSize(fact) {
        if (section == null)
            section = $('body').not('.font-size-change').find('*')
                    .filter(
                            function () {
                                return  $(this).clone()
                                        .children()
                                        .remove()
                                        .end()
                                        .text().trim().length > 0;
                            }); //filter -> exclude all elements without text

        section.each(function () {
            var newsize = fact ? getFontSize(this) * fact : this.originalFontSize;
            if (newsize)
                $(this).css('font-size', newsize);
        });
    }
  function resetFont() {
        setFontSize();
        count = 0;
    }

    function increaseFont() {
        if (count < 1)
        {
            setFontSize(1 / factor);
            count++
        }
    }

    function decreaseFont() {
        if (count > -1)
        {
            setFontSize(factor);
            count--
        }
    }

/*$(document).ready(function () {
    $('#skip_main_content_start').click(function () {
        $('html, body').animate({
            scrollTop: $("#skip_main_content_end").offset().top
        }, 2000);
    });
  });*/
  function goToByScroll(id){
      //id = id.replace("link", "");
      //$('html,body').animate({
      //scrollTop: $("#"+id).offset().top-120},'slow');
      $('html,body').animate({
        scrollTop: $("#"+id).offset().top-120},'slow');
    }
$(document).ready(function () {
    $('#skip_main_content_start').click(function (e) {
        e.preventDefault(); 
        goToByScroll("skip_main_content");                          
    });
  });
</script>
<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<section class="circular-main">
	<div class="container">
		<div class="recent">
			<h4>Recent Load/Freight Bookings</h4>
		</div>
		<table class="table table-striped">
		  <thead class="thead-primery">
		    <tr>
			  <th scope="col">ID</th>	
		      <th scope="col">Source City</th>
		      <th scope="col">Destination City</th>
		      <th scope="col">Weight(MT)</th>
		      <th scope="col">Schd Date</th>
		      <th scope="col">Material</th>
		      <th scope="col">Truck Type</th>
			  <th scope="col">No. of Truck</th>
		      <th scope="col">Quotation</th>
		    </tr>
		  </thead>
		  <tbody>
		 <?php $i=0; if(count($loads)>0) { foreach($loads as $load) { $i++; ?> 
		    <tr>
			  <th><?=$load->booking_id?></th>
		      <th><?=$load->source_city?></th>
		      <th><?=$load->destination_city?></th>
		      <th><?=$load->weight?></th>
		      <th><?=date('d/m/Y',strtotime($load->reporting_date))?></th>
		      <th><?=$load->material_type?></th>
		      <th><?=$load->truck_type?></th>
			  <th><?=$load->no_of_trucks?></th>
		      <td><a class="<?=(($i%2)==0)?'pink':'blue'?>" href="<?= SITE_PATH; ?>register">Quotation</a></td>	
		    </tr>
		  <?php } } else { ?>
			<tr><td colspan="8"><center><strong>No post available.</strong></center></td></tr>
		  <?php } ?>  
		  </tbody>
		</table>
		<div style="float: right;">
		<?= LinkPager::widget(['pagination' => $pages,
		'prevPageLabel' => ' Prev ',
		'nextPageLabel' => ' Next ',
		]) ?>
		</div>
	</div>
</section>
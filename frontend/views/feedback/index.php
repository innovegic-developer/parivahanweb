
<section class="form-section">
    <div class="container">
        <div class="form-heading text-center">
            <h3>Feedback Form</h3>
        </div>
        <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="row col-xs-12 col-sm-12 col-md-12 col-xl-12 alert alert-success alert-dismissable" style="">
                <div class="col-md-12">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
                </div>
            <?php endif; ?>  
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="row col-xs-12 col-sm-12 col-md-12 col-xl-12 alert alert-danger alert-dismissable" style="">
                <div class="col-md-12">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
                </div>
            <?php endif; ?>
        <form method="post" class="" id="" enctype="multipart/form-data" autocomplete="off">
            <div class="form-main">
                <p>Please fill the following form.</p>
                <div class="row">
					<div class="col-md-6 form-group">
                        <label>Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="full_name" placeholder="Name" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Mobile No.<span class="required">*</span></label>
                        <input type="number" class="form-control Number" maxlength="10" minlength="10" name="mobile_number" placeholder="Mobile No." onKeyDown = "if((this.value.length==10 && event.keyCode!=8 && event.keyCode!=9) || event.keyCode==69 || event.keyCode==101) return false;" required >
                    </div>
                    <div class="col-md-12 form-group">
                        <label>Subject<span class="required">*</span></label>
                        <input type="text" class="form-control" name="subject" placeholder="Subject" required>
                    </div>
					<div class="col-md-12 form-group">
                        <label>Description<span class="required">*</span></label>
                        <textarea class="form-control" name="description" placeholder="Description" ></textarea>
                    </div>
                    <div class="col-md-6 form-group">
                        <input type="submit" class="button w100" value="Submit" name="">
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

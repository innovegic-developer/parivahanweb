<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="img/banner-1.png" alt="banner-1">
      </div>
      <div class="carousel-item">
        <img src="img/banner-1.png" alt="banner-2">
      </div>
      <div class="carousel-item">
        <img src="img/banner-1.png" alt="banner-3">
      </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>

<div class="container">
	<div class="bg">
		<div class="row">
			<div class="col-md-6">
				<div class="industries">
					<h1><?=$IndustryCount?></h1>
					<h4>Service Seeker<br>(Industry, Trader and Private User) </h4>
				</div>
			</div>
			<div class="col-md-6">
				<div class="industries border0">
					<h1><?=$TransporterCount?></h1>
					<h4>Transporter<br>(Truck Provider)</h4>
				</div>
			</div>
			<!--<div class="col-md-4">
				<div class="industries border0">
					<h1><?=$OperatorCount?></h1>
					<h4>Driver<br>(Truck Owner)</h4>
				</div>
			</div>-->
		</div>
	</div>
</div>

<section  id="skip_main_content">
	<div class="container">
		<div class="recent">
			<h4>Recent Load/Freight Bookings</h4>
		</div>
		<table class="table table-striped">
		  <thead class="thead-primery">
		    <tr>
			  <th scope="col">ID</th>
		      <th scope="col">Source City</th>
		      <th scope="col">Destination City</th>
		      <th scope="col">Weight(MT)</th>
		      <th scope="col">Schd Date</th>
		      <th scope="col">Material</th>
		      <th scope="col">Truck Type</th>
			  <th scope="col">No. of Truck</th>
		      <th scope="col">Quotation</th>
		    </tr>
		  </thead>
		  <tbody>
		 <?php $i=0; if(count($loads)>0) { foreach($loads as $load) { $i++; ?> 
		    <tr>
			  <th><?=$load->booking_id?></th>
		      <th><?=$load->source_city?></th>
		      <th><?=$load->destination_city?></th>
		      <th><?=$load->weight?></th>
		      <th><?=date('d/m/Y',strtotime($load->reporting_date))?></th>
		      <th><?=$load->material_type?></th>
		      <th><?=$load->truck_type?></th>
			  <th><?=$load->no_of_trucks?></th>
		      <td><a class="<?=(($i%2)==0)?'pink':'blue'?>" href="<?= SITE_PATH; ?>register">Quotation</a></td>	
		    </tr>
		  <?php } } else { ?>
			<tr><td colspan="8"><center><strong>No post available.</strong></center></td></tr>
		  <?php } ?>
		  </tbody>
		</table>
		<?php if(count($loads)>0) { ?>
		<div class="text-center">
			<a href="<?= SITE_PATH; ?>posted-load" class="viewmore">View More Booking</a>
		</div>
		<?php } ?>
	</div>
</section>

<section class="downloadapp">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="parivahansuvidha">
					<h1>PARIVAHAN SUVIDHA</h1>
					<p>This solution will be helpful for Transporter, Truck Owner/Driver and Industries on One digital platform.</p>
					<p>Industries will easily get the Transporter/Truck Owner/Drivers and can Transport goods on time.</p>
					<!--<p>This will also be helpful for Truck Driver to get more job opportunity.</p>-->
					<p>All this may lead to increase the Transport Business and make the whole process easy and may lead to betterment of Service.</p>
					<p><strong>Download Parvivan Application</strong></p>
					<!--<a href="https://play.google.com/store/apps/details?id=com.parivahan" target="_blank"><img src="<?= SITE_PATH; ?>img/google-play-button.png"></a>-->
					<a href="<?= APK_LINK; ?>" target=""><img src="<?= SITE_PATH; ?>img/google-play-button.png"></a>
				</div>
			</div>
		</div>
		<div class="banner-image">
			<img src="<?= SITE_PATH; ?>img/banner-image.png">
		</div>
	</div>
</section>

<section class="news-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="welcome">
					<h4 class="heading"><span>Welcome to</span> OIDC</h4>
					<p>The Omnibus Industrial Development Corporation of Daman & Diu and Dadra & Nagar Haveli Ltd. (OIDC) was incorporated on 27th March, 1992 under the Companies Act 1956 with authorized share capital of Rs. 5.00 crore and paid up share capital Rs. 1.00 crore. Now the authorized share capital is Rs. 50.00 crore and paid up share capital is Rs. 26.06 crore. All the shares are in the name of the President of India or held by the officers of UT Administration on behalf of the President of India.<br><br>
					To facilitate the Industries and other businesses by streamlining the transport related services in the UT of DNH&amp;DD in a very smooth and transparent manner. OIDC LTD in coordination with the department of National Informatics Centre (NIC) and the Transport &amp; Industries Department of DNH&amp;DD, have developed an Online Transport Facilitation Portal namely <strong><em>&ldquo;OIDC PARIVAHAN SUVIDHA&rdquo;</em></strong><strong>.</strong>&nbsp;This portal shall also be available in the form of mobile/web application namely <strong><em>&ldquo;PARIVAHAN SUVIDHA&rdquo;.</em></strong><em>&nbsp;</em>This portal being totally online will help the consumer in doing their business at ease and save their time. <!--<a href="#">Readmore</a>--></p>
				</div>
			</div>
			<div class="col-md-4" style="display:none;">
				<div class="news">
					<h4 class="heading"><span>latest</span> News</h4>
					<article>
						<div class="date">
							<h3>18</h3>
							<h6>July</h6>
							<img src="img/news-image.png">
						</div>
						<h5>Lorem ipsum dolor</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin maximus ut nulla a varius.</p>
					</article>
					<span class="clearfix"></span>
					<article>
						<div class="date">
							<h3>18</h3>
							<h6>July</h6>
							<img src="img/news-image.png">
						</div>
						<h5>Lorem ipsum dolor</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin maximus ut nulla a varius.</p>
					</article>
					<a href="#">View more</a>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>

<section class="gallerysection" style="display:none;">
	<div class="container">
		<div class="row">
			<ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
			  <li class="nav-item">
			    <a class="nav-link active" id="pills-photo-tab" data-toggle="pill" href="#pills-photo" role="tab" aria-controls="pills-photo" aria-selected="true">Photo gallery </a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="pills-video-tab" data-toggle="pill" href="#pills-video" role="tab" aria-controls="pills-video" aria-selected="false">Video gallery </a>
			  </li>
			</ul>
			<div class="tab-content" id="pills-tabContent">
			  <div class="tab-pane fade show active" id="pills-photo" role="tabpanel" aria-labelledby="pills-photo-tab">
			  	<div class="row">
			  		<div class="col-md-3">
			  			<div class="gallery-image">
			  				<img src="img/gallery-1.jpg">
			  				<h4>Event 1</h4>
			  			</div>
			  		</div>
			  		<div class="col-md-3">
			  			<div class="gallery-image">
			  				<img src="img/gallery-2.jpg">
			  				<h4>Event 2</h4>
			  			</div>
			  		</div>
			  		<div class="col-md-3">
			  			<div class="gallery-image">
			  				<img src="img/gallery-3.jpg">
			  				<h4>Event 3</h4>
			  			</div>
			  		</div>
			  		<div class="col-md-3">
			  			<div class="gallery-image">
			  				<img src="img/gallery-4.jpg">
			  				<h4>Event 4</h4>
			  			</div>
			  		</div>
			  	</div>
			  </div>
			  <div class="tab-pane fade" id="pills-video" role="tabpanel" aria-labelledby="pills-video-tab">
			  	<div class="row">
			  		<div class="col-md-3">
			  			<div class="gallery-image">
			  				 <video width="100%" height="240" controls>
							  <source src="img/mov_bbb.mp4" type="video/mp4">
							  <source src="movie.ogg" type="video/ogg">
							</video> 
			  			</div>
			  		</div>
				  	<div class="col-md-3">
			  			<div class="gallery-image">
			  				 <video width="100%" height="240" controls>
							  <source src="img/mov_bbb.mp4" type="video/mp4">
							  <source src="movie.ogg" type="video/ogg">
							</video> 
			  			</div>
			  		</div>
				  	<div class="col-md-3">
			  			<div class="gallery-image">
			  				 <video width="100%" height="240" controls>
							  <source src="img/mov_bbb.mp4" type="video/mp4">
							  <source src="movie.ogg" type="video/ogg">
							</video> 
			  			</div>
			  		</div>
				  	<div class="col-md-3">
			  			<div class="gallery-image">
			  				 <video width="100%" height="240" controls>
							  <source src="img/mov_bbb.mp4" type="video/mp4">
							  <source src="movie.ogg" type="video/ogg">
							</video> 
			  			</div>
			  		</div>
			  	</div>
			  </div>
			</div>
		</div>
	</div>
</section>

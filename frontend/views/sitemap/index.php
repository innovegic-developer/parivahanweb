<section class="circular-main">
	<div class="container">
		<div class="sitemap">
			<ul class="tree">
				<li class="main_nav theme_1tag" style="">
					<a href="<?= SITE_PATH; ?>posted-load">
						Posted Loads
					</a>
				</li>
			</ul>
			<ul class="tree">
				<li class="main_nav theme_1tag" style="">
					<a href="<?= SITE_PATH; ?>tender">
						Tenders
					</a>
				</li>
			</ul>
			<ul class="tree">
				<li class="main_nav theme_1tag" style="">
					<a href="<?= SITE_PATH; ?>circular">
						Circulars
					</a>
				</li>
			</ul>
			<ul class="tree">
				<li class="main_nav theme_1tag" style="">
					<a href="<?= SITE_PATH; ?>contactus">
						Contact Us
					</a>
				</li>
			</ul>
			
			<ul class="tree">
				<li class="main_nav theme_1tag" style="">
					<a href="<?= SITE_PATH; ?>feedback">
						Feedback
					</a>
				</li>
			</ul>
			<ul class="tree">
				<li class="main_nav theme_1tag" style="">
					<a href="<?= SITE_PATH; ?>register">
						Register
					</a>
					<ul>
						<li style="color: grey; " class="sub_nav">
		    				<label style="background: grey">|</label>			
							<a href="<?= SITE_PATH; ?>registration/transporter-registration">Transporter</a>
			    		</li>
			    	</ul>				    		 
					<!--<ul>
						<li style="color: grey; " class="sub_nav">
		    				<label style="background: grey">|</label>			
							<a href="<?= SITE_PATH; ?>registration/truck-operator-registration">Truck Owner</a>
	    				</li>
			    	</ul>-->
			    	<ul>
						<li style="color: grey; " class="sub_nav">
				    		<label style="background: grey">|</label>			
							<a href="<?= SITE_PATH; ?>registration/industry-registration">Service Seeker</a>
			    		</li>
			    	</ul>
				</li>
			</ul>
		</div>
	</div>
</section>
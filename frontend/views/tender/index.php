<section class="circular-main">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="circular">
					<h3>Tenders</h3>
					<table class="table">
					    <thead class="thead-dark">
					      <tr>
					        <th>Sr. No</th>
					        <th>Title</th>
					        <th>PDF</th>
					      </tr>
					    </thead>
					    <tbody>
					      <?php 
                if(!empty($tender)){ $i=0;
                foreach ($tender as $key => $value) { $i++; ?>	
					      <tr>
					        <td><?=$i?></td>
					        <td><?= $value['tender_title']?></td>
					        <td>
					        	<!--<a href="#"><i class="fas fa-eye"></i></a>-->
					        	<a href="<?php echo $value['tender_pdf'];?>"><i class="fas fa-file-download" target="_parent" download="pdf"></i></a>
					        </td>
					      </tr>
				<?php } } else { ?>
						<tr><td colspan="3">No tender available.</td></tr>
				<?php } ?>
					    </tbody>
					  </table>
				</div>
			</div>
		</div>
	</div>
</section>
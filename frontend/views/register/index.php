<section class="circular-main">
	<div class="container">
		<div class="signup">
			<ul class="row">
				<div class="col-md-9">
					<li>
						<h3>TRANSPORTER</h3>
						<p>Sign Up to take your transport service.</p>
					</li>
				</div>
				<div class="col-md-3">
					<li class="signup-button">
						<a href="<?= SITE_PATH; ?>registration/transporter-registration">FREE SIGN UP</a>
					</li>
				</div>
				<!--<div class="col-md-9">
					<li>
						<h3>TRUCK OWNER</h3>
						<p>Sign Up to take your transport service.</p>
					</li>
				</div>
				<div class="col-md-3">
					<li class="signup-button">
						<a href="<?= SITE_PATH; ?>registration/truck-operator-registration">FREE SIGN UP</a>
					</li>
				</div>-->
				<div class="col-md-9">
					<li>
						<h3>SERVICE SEEKER (INDUSTRY, TRADER AND PRIVATE USER)</h3>
						<p>Sign Up for a solution to all your freight related problems.</p>
					</li>
				</div>
				<div class="col-md-3">
					<li class="signup-button">
						<a href="<?= SITE_PATH; ?>registration/industry-registration">FREE SIGN UP</a>
					</li>
				</div>
			</ul>
		</div>
	</div>
</section>

<section class="downloadapp">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="parivahansuvidha">
					<h1>PARIVAHAN SUVIDHA</h1>
					<p>This solution will be helpful for Transporter, Truck Owner/Driver and Industries on One digital platform.</p>
					<p>Industries will easily get the Transporter/Truck Owner/Drivers and can Transport goods on time.</p>
					<!--<p>This will also be helpful for Truck Driver to get more job opportunity.</p>-->
					<p>All this may lead to increase the Transport Business and make the whole process easy and may lead to betterment of Service.</p>
					<p><strong>Download Parvivan Application</strong></p>
					<a href="<?= SITE_PATH; ?>apk/Parivahan.apk" target=""><img src="<?= SITE_PATH; ?>img/google-play-button.png"></a>
				</div>
			</div>
		</div>
		<div class="banner-image">
			<img src="<?= SITE_PATH; ?>img/banner-image.png">
		</div>
	</div>
</section>
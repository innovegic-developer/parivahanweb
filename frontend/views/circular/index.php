<section class="circular-main">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="circular">
					<h3>Circular / Order</h3>
					<table class="table">
					    <thead class="thead-dark">
					      <tr>
					        <th>Sr. No</th>
					        <th>Title</th>
					        <th>PDF</th>
					      </tr>
					    </thead>
					    <tbody>
				<?php 
                if(!empty($circular)){ $i=0;
                foreach ($circular as $key => $value) { $i++; ?>	
					      <tr>
					        <td><?=$i?></td>
					        <td><?= $value['circular_title']?></td>
					        <td>
					        	<!--<a href="#"><i class="fas fa-eye"></i></a>-->
					        	<a href="<?php echo $value['circular_pdf'];?>"><i class="fas fa-file-download" target="_parent" download="pdf"></i></a>
					        </td>
					      </tr>
				<?php } } else { ?>
						<tr><td colspan="3">No circular available.</td></tr>
				<?php } ?>
					    </tbody>
					  </table>
				</div>
			</div>
		</div>
	</div>
</section>
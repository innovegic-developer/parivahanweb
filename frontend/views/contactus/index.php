<section class="circular-main">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="circular">
					<h3>Contact Us</h3>
					<ul>
						<li style="height:75px"><i class="fas fa-map-marker"></i> Omnibus Industrial Development Corporation of <br>
						Daman & Diu and Dadra & Nagar Haveli <br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Corporate Office, Plot No. 35, <br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Somnath, Nani Daman, Daman - 396210</li>
						<li><i class="fas fa-mobile-alt"></i> 0260-2240976</li>
						<li><i class="fas fa-fax"></i> 0260-2241108</li>
						<li><i class="fas fa-envelope"></i> oidcparivahan@gmail.com</li>
					</ul>
				</div>
			</div>
			<div class="col-md-5">
				
			</div>
			
		</div>
	</div>
</section>
<section class="downloadapp">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="parivahansuvidha">
					<h1>PARIVAHAN SUVIDHA</h1>
					<p>This solution will be helpful for Transporter, Truck Owner/Driver and Industries on One digital platform.</p>
					<p>Industries will easily get the Transporter/Truck Owner/Drivers and can Transport goods on time.</p>
					<!--<p>This will also be helpful for Truck Driver to get more job opportunity.</p>-->
					<p>All this may lead to increase the Transport Business and make the whole process easy and may lead to betterment of Service.</p>
					<p><strong>Download Parvivan Application</strong></p>
					<!--<a href="https://play.google.com/store/apps/details?id=com.parivahan" target="_blank"><img src="<?= SITE_PATH; ?>img/google-play-button.png"></a>-->
					<a href="<?= APK_LINK; ?>" target=""><img src="<?= SITE_PATH; ?>img/google-play-button.png"></a>
				</div>
			</div>
		</div>
		<div class="banner-image">
			<img src="<?= SITE_PATH; ?>img/banner-image.png">
		</div>
	</div>
</section>
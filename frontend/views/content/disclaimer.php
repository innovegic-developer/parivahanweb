<section class="circular-main">
	<div class="container">
		<div class="row">
		<div class="col-md-12">
				
			<h3>Disclaimer</h3>

		<div class="clear"></div>
		
		<p><strong>COPYRIGHT POLICY</strong></p>
<p>Material featured on this website may be reproduced free of charge. However, the material has to be reproduced accurately and not to be used in a derogatory manner or in a misleading context. Wherever the material is being published or issued to others, the source must be prominently acknowledged. However, the permission to reproduce this material shall not extend to any material which is identified as being copyright of a third party. Authorisation to reproduce such material must be obtained from the OIDC copyright holder concerned.</p>

<p><strong><strong>PRIVACY POLICY</strong></strong></p>
<p>This website does not automatically capture any specific personal information from you (like name, phone number or e-mail address), that allows us to identify you individually. If you choose to provide us with your personal information, like names or addresses, when you visit our website, we use it only to fulfil your request for information.</p>
<p>We do not sell or share any personally identifiable information volunteered on this site to any third party (public/private). Any information provided to this website will be protected from loss, misuse, unauthorized access or disclosure, alteration, or destruction.</p>
<p>We gather certain information about the User, such as Internet protocol (IP) address, domain name, browser type, operating system, the date and time of the visit and the pages visited. We make no attempt to link these addresses with the identity of individuals visiting our site unless an attempt to damage the site has been detected.</p>

<p><strong><strong>COOKIES POLICY</strong></strong></p>
<p>A cookie is a piece of software code that an internet web site sends to your browser when you access information at that site. A cookie is stored as a simple text file on your computer or mobile device by a website&rsquo;s server and only that server will be able to retrieve or read the contents of that cookie. Cookies let you navigate between pages efficiently as they store your preferences, and generally improve your experience of a website.</p>

<p>We are using following types of cookies in our site:-</p>
<ol>
<li>Analytics cookies for anonymously remembering your computer or mobile device when you visit our website to keep track of browsing patterns.</li>
<li>Service cookies for helping us to make our website work efficiently, remembering your registration and login details, settings preferences, and keeping track of the pages you view.</li>
</ol>
<ol start="3">
<li>Non-persistent cookies per-session cookies. Per-session cookies serve technical purposes, like providing seamless navigation through this website. These cookies do not collect personal information on users and they are deleted as soon as you leave our website. The cookies do not permanently record data and they are not stored on your computer&rsquo;s hard drive. The cookies are stored in memory and are only available during an active browser session. Again, once you close your browser, the cookie disappears.</li>
</ol>

<p>You may note additionally that when you visit sections of pmindia.gov.in/pmindia.nic.in where you are prompted to log in, or which are customizable, you may be required to accept cookies. If you choose to have your browser refuse cookies, it is possible that some sections of our web site may not function properly.</p>

<p><strong><strong>HYPERLINKING POLICY</strong></strong></p>
<p><strong><strong>Links to external websites/portals</strong></strong></p>
<p>At many places in this website, you shall find links to other websites/portals. These links have been placed for your convenience. OIDC is not responsible for the contents of the linked websites and does not necessarily endorse the views expressed in them. Mere presence of the link or its listing on this website should not be assumed as endorsement of any kind. We cannot guarantee that these links will work all the time and we have no control over availability of linked destinations.</p>
<p>We do not object to you linking directly to the information that is hosted on this web site and no prior permission is required for the same. However, we would like you to inform us about any links provided to this website so that you can be informed of any changes or updates therein. Also, we do not permit our pages to be loaded into frames on your site. The pages belonging to this website must load into a newly opened browser window of the User.</p>

<p><strong><strong>TERMS &amp; CONDITIONS</strong></strong></p>
<p>Website designed &amp; hosted by&nbsp;OIDC(The Omnibus Industrial Development Corporation of Daman &amp; Diu and Dadra &amp; Nagar Haveli Ltd., (OIDC) a Govt. Undertaking was registered under the Companies Act th on 27 March, 1992.)</p>

<p>Though all efforts have been made to ensure the accuracy and currency of the content on this website, the same should not be construed as a statement of law or used for any legal purposes. In case of any ambiguity or doubts, users are advised to verify/check with the OIDC and/or other source(s), and to obtain appropriate professional advice.</p>
<p>Under no circumstances will OIDC be liable for any expense, loss or damage including, without limitation, indirect or consequential loss or damage, or any expense, loss or damage whatsoever arising from use, or loss of use, of data, arising out of or in connection with the use of this website.</p>
<p>These terms and conditions shall be governed by and construed in accordance with the Indian Laws. Any dispute arising under these terms and conditions shall be subject to the jurisdiction of the courts of India.</p>
<p>The information posted on this website could include hypertext links or pointers to information created and maintained by non-Government/private organisations. OIDC is providing these links and pointers solely for your information and convenience. When you select a link to an external website, you are leaving the OIDC website and are subject to the privacy and security policies of the owners/sponsors of the external website. OIDC does not guarantee the availability of such linked pages at all times. OIDC cannot authorise the use of copyrighted materials contained in a linked website. Users are advised to request such authorisation from the owner of the linked website. OIDC does not guarantee that linked websites comply with Indian Government Web Guidelines.</p>

<p><strong><strong>REGIONAL LANGUAGE POLICY</strong></strong></p>
<p>Though all efforts have been made to ensure the accuracy of the content in regional languages on this Portal, the same should not be construed as a statement of law or used for any legal purposes. For any discrepancy in the regional language content you may refer to the original English content. For any error in language, you may report the same through the online feedback form.</p>
<p>OIDC accepts no responsibility in relation to the accuracy, completeness, usefulness or otherwise, of the contents. In no event will the Government or NIC be liable for any expense, loss or damage including, without limitation, indirect or consequential loss or damage, or any expense, loss or damage whatsoever arising from use, or loss of use, of data, arising out of or in connection with the use of this Portal.</p>

<p><strong><strong>DISCLAIMER</strong></strong></p>
<p>This website of the OIDC is being developed for providing digital Platform to Industries, Transporter and Driver of UT Administration of Dadra and Nagar Haveli and Daman and Diu. Even though every effort is taken to provide accurate and up to date information, officers making use of the circulars posted on the website are advised to get in touch with the OIDC whenever there is any doubt regarding the correctness of the information contained therein. In the event of any conflict between the contents of the circulars on the website and the hard copy of the circulars issued by OIDC, the information in the hard copy should be relied upon and the matter shall be brought to the notice of the OIDC.</p>
			 </div>
		  </div>
		</div>
</div>

<section class="form-section">
    <div class="container">
        <div class="form-heading text-center">
            <h3>Transporter Registration Form</h3>
        </div>
<?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="row col-xs-12 col-sm-12 col-md-12 col-xl-12 alert alert-success alert-dismissable" style="">
                <div class="col-md-12">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
                </div>
            <?php endif; ?>  
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="row col-xs-12 col-sm-12 col-md-12 col-xl-12 alert alert-danger alert-dismissable" style="">
                <div class="col-md-12">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
                </div>
            <?php endif; ?>
        <form method="post" class="" id="" enctype="multipart/form-data">
            <div class="form-main">
                <p>Please fill the following form to register as a Transporter.</p>
                <div class="row">
					<div class="col-md-6 form-group">
                        <label>Company Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="company_name" placeholder="Company Name" required>
                    </div>
					<div class="col-md-6 form-group">
                        <label>Designation<span class="required">*</span></label>
						<select class="custom-select" name="designation" required>
						<option value="">Select One</option>
						<option value="Director">Director</option>
						<option value="Logistics Manager">Logistics Manager</option>
						<option value="Manager">Manager</option>
						<option value="Plant SCO">Plant SCO</option>
						<option value="Proprietor">Proprietor</option>
						</select>
                    </div>
					<div class="col-md-6 form-group">
                        <label>First Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="first_name" placeholder="First Name" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Last Name<span class="required">*</span></label>
                        <input type="text" class="form-control" name="last_name" placeholder="Last Name" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Mobile No.<span class="required">*</span></label>
                        <input type="text" class="form-control Number" maxlength="10" minlength="10" name="mobile_number" placeholder="Mobile No." required>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Email ID<span class="required"></span></label>
                        <input type="email" class="form-control" name="email" placeholder="Email ID">
                    </div>
                    <div class="col-md-12 form-group">
                        <label>Address Line1<span class="required">*</span></label>
                        <textarea class="form-control" name="address_line1" placeholder="Address Line1" required></textarea>
                    </div>
					<div class="col-md-12 form-group">
                        <label>Address Line2</label>
                        <textarea class="form-control" name="address_line2" placeholder="Address Line2" required></textarea>
                    </div>
                    <!--<div class="col-md-6 form-group">
                        <label>State<span class="required">*</span></label>
                        <select class="custom-select">
                            <option>State</option>
                        </select>
                    </div>-->
					<input type="hidden" class="form-control" name="state" value="Dadra and Nagar Haveli and Daman and Diu" required>
                    <div class="col-md-6 form-group">
                        <label>District<span class="required">*</span></label>
                        <select class="custom-select" name="district" id="district" required>
						<option value="">Select One</option>
						<?php 
						if(!empty($district)){
						foreach ($district as $key => $value) { ?>
							<option value="<?=$value['id']?>"><?=$value['district']?></option>
						<?php } } ?>
						</select>
                    </div>
                    <!--<div class="col-md-6 form-group">
                        <label>City<span class="required">*</span></label>
                        <select class="custom-select">
                            <option>City</option>
                        </select>
                    </div>-->
                    <div class="col-md-6 form-group">
                        <label>Area<span class="required">*</span></label>
						<select class="custom-select" name="area" id="area" required>
						<option value="">Select One</option>
                        </select>
                       
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Pincode<span class="required">*</span></label>
                        <input type="text" class="form-control pincode Number"  maxlength="6" minlength="6" name="pincode" id="pincode" placeholder="Pincode" required readonly>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Landmark<span class="required">*</span></label>
                        <input type="text" class="form-control" name="landmark" placeholder="Landmark" required>
                    </div>
					<div class="col-md-6 form-group">
                        <label>RCC Number<span class="required">*</span></label>
                        <input type="text" class="form-control" name="rcc_number" placeholder="RCC Number" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Upload File<span class="required">*</span></label>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="customFile1" name="rcc_photo" required>
                          <label class="custom-file-label" for="customFile" id="customFile1Lbl">Choose file</label>
                        </div>
                    </div>
                    <!--<div class="col-md-6 form-group">
                        <label>Incorporation Certificate<span class="required">*</span></label>
                        <input type="text" class="form-control" name="incorporation_certificate_number" placeholder="Incorporation Certificate Number" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Upload File<span class="required">*</span></label>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="customFile1" name="incorporation_certificate_photo" required>
                          <label class="custom-file-label" for="customFile" id="customFile1Lbl">Choose file</label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>PAN Number<span class="required">*</span></label>
                        <input type="text" class="form-control" name="pan_number" placeholder="PAN Number" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Upload File<span class="required">*</span></label>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="customFile2" name="pan_photo" required>
                          <label class="custom-file-label" for="customFile" id="customFile2Lbl">Choose file</label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Shop Establishment Certificate<span class="required">*</span></label>
                        <input type="text" class="form-control" name="shop_establishment_certificate_number" placeholder="Shop Establishment Certificate" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Upload File<span class="required">*</span></label>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="customFile3" name="shop_establishment_certificate_photo" required>
                          <label class="custom-file-label" for="customFile" id="customFile3Lbl">Choose file</label>
                        </div>
                    </div>-->
                    <div class="col-md-6 form-group">
						<input type="checkbox" class="" value="" name="terms" required>
						I accept <a href="<?= SITE_PATH; ?>content/terms" target="_blank">Terms and conditions of services.</a>
                        <input type="submit" class="button w100" value="Submit" name="">
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
            $("#district").on('change', function postinput(){
                var district = $("#district option:selected").val(); // this.value
                //alert(district);
                $.ajax({ 
                    url: 'get-areas',
                    data: { district: district },
                    type: 'post'
                }).done(function(res) {
                      var listItems;    
                      //var areaArray=[], pinArray=[];
                      res = JSON.parse(res);
                      listItems += "<option value=''> Select One </option>";
                      res.forEach(function(item,index) {
                          //item.area;
                          console.log()
                          listItems += "<option value='" + item.area + "' data-pincode='" + item.pincode + "'>" + item.area + "</option>";
                          if(index == 0){
                            $('#pincode').val(item.pincode)  
                          }
                          

                          //areaArray[] = item.area;
                          //pinArray[] = item.pincode;
                      });
                    $("#area").html(listItems);
                }).fail(function() {
                    console.log('Failed');
                });
            });

            
            $('.custom-select').on('change', function() {
                $(this).attr('data-pincode');
                var pincode = $(this).find(':selected').attr('data-pincode')
                $('#pincode').val(pincode)
            });

                $('.Number').keypress(function (event) {
                    var keycode;

                    keycode = event.keyCode ? event.keyCode : event.which;

                    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 ||
                            keycode == 37 ||keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
                        event.preventDefault();
                    }
                });
		
		
		// File Upload Acknowledgement Successfull Message	=============================================		
		$(document).ready(function(){
			$("#customFile1").change(function(){
				var file_size = $('#customFile1')[0].files[0].size;
				var fileExtension = ['jpeg', 'jpg', 'png'];
				if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
					alert("Only formats are allowed : "+fileExtension.join(', '));
					$("#customFile1").val('');
					$("#customFile1Lbl").text('Choose file');
					return false;
				}
				else if(file_size>2097152) {
					alert("File size should not be more than 2 MB");
					$("#customFile1").val('');
					$("#customFile1Lbl").text('Choose file');
					return false;
				}
				else
				{
					alert("File Selected Successfully");
					//$("#customFileDiv").hide();
					$("#customFile1Lbl").text('File Selected Successfully');
				}
			});
			
			$("#customFile2").change(function(){
				var file_size = $('#customFile2')[0].files[0].size;
				if(file_size>2097152) {
					alert("File size should not be more than 2 MB");
					$("#customFile2").val('');
					$("#customFile2Lbl").text('Choose file');
					return false;
				}
				else
				{
					alert("File Selected Successfully");
					//$("#customFile2Div").hide();
					$("#customFile2Lbl").text('File Selected Successfully');
				}
			});
			$("#customFile3").change(function(){
				var file_size = $('#customFile3')[0].files[0].size;
				if(file_size>2097152) {
					alert("File size should not be more than 2 MB");
					$("#customFile3").val('');
					$("#customFile3Lbl").text('Choose file');
					return false;
				}
				else
				{
					alert("File Selected Successfully");
					//$("#customFile2Div").hide();
					$("#customFile3Lbl").text('File Selected Successfully');
				}
			});
		});	
</script>

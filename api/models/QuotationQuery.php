<?php

namespace api\models;

/**
 * This is the ActiveQuery class for [[Quotation]].
 *
 * @see Quotation
 */
class QuotationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Quotation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Quotation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

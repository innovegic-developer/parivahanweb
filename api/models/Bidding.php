<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "bidding".
 *
 * @property int $id ID
 * @property int $load_id Load ID
 * @property int $user_id Transporter/Driver ID
 * @property int $bid_amount Bid Amount
 * @property int $truck_id Truck ID
 * @property string $remark Remark
 * @property int $assigned_to_me Assigned
 * @property string $status Status
 * @property string $created_at Created On
 * @property string $updated_at Updated On
 * @property int $created_by Created By
 * @property int $updated_by Updated By
 */
class Bidding extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bidding';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['load_id', 'user_id'], 'required'],
            [['load_id', 'user_id', 'bid_amount', 'truck_id', 'assigned_to_me', 'created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['remark'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'load_id' => Yii::t('app', 'Load ID'),
            'user_id' => Yii::t('app', 'Transporter/Driver ID'),
            'bid_amount' => Yii::t('app', 'Bid Amount'),
            'truck_id' => Yii::t('app', 'Truck ID'),
            'remark' => Yii::t('app', 'Remark'),
            'assigned_to_me' => Yii::t('app', 'Assigned'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return BiddingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BiddingQuery(get_called_class());
    }
}


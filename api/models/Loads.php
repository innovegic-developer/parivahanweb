<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "loads".
 *
 * @property int $id ID
 * @property int $ref_rejected_load_id
 * @property string $booking_id Booking ID
 * @property int $industry_user_id User ID
 * @property int $industry_id Industry ID
 * @property string $full_name Name
 * @property string $mobile_number Mobile Number
 * @property string $load_address_type
 * @property string $company_name Company name
 * @property int $source_address_id Source Address ID
 * @property string $source_address_line1 Source Address Line1
 * @property string $source_address_line2 Source Address Line2
 * @property string $source_pincode Source Pincode
 * @property string $source_state Source State
 * @property string $source_district Source District
 * @property string $source_city Source City
 * @property string $source_area Source Area
 * @property string $source_landmark Source Landmark
 * @property string $destination_address_line1 Destination Address Line1
 * @property string $destination_address_line2 Destination Address Line2
 * @property string $destination_pincode Destination Pincode
 * @property string $destination_state Destination State
 * @property string $destination_district Destination District
 * @property string $destination_city Destination City
 * @property string $destination_area Destination Area
 * @property string $destination_landmark Destination Landmark
 * @property string $load_type Load Type
 * @property string $scheduling_type Scheduling Type
 * @property string $material_type Material Type
 * @property string $weight Weight
 * @property string $truck_type Truck Type
 * @property int $no_of_trucks No. of Trucks
 * @property string $bid_closing_date Quotation Closing Date
 * @property string $bid_closing_time Quotation Closing Time
 * @property string $reporting_date Reporting Date
 * @property string $reporting_time Reporting Time
 * @property int $max_bid_amount Max. Quotation Amount
 * @property string $description Description
 * @property string $share_to_preferred Shared to Preferred
 * @property string $assigned_status Assigned Status
 * @property string $assigned_at Assigned At
 * @property int $assigned_user_id Assigned Transporter/Driver ID
 * @property string $assigned_full_name Transporter/Driver Name
 * @property string $assigned_mobile_number Transporter/Driver Mobile
 * @property string $assigned_company_name Transporter/Driver Company Name
 * @property int $assigned_bid_amount Assigned Bid Amount
 * @property string $assigned_truck_id Assigned Truck ID
 * @property string $part_load_quotation_date Part Load Quotation Date
 * @property string $reject_reason Reject Reason
 * @property string $rejected_from Rejected From
 * @property int $rejected_by Rejected By
 * @property string $rejected_at Rejected At
 * @property string $status Status
 * @property string $created_at Created On
 * @property string $updated_at Updated On
 * @property int $created_by Created By
 * @property int $updated_by Updated By
 * @property string $quotations
 *
 * @property User $industryUser
 * @property TripTracking[] $tripTrackings
 */
class Loads extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ref_rejected_load_id', 'industry_user_id', 'industry_id', 'source_address_id', 'no_of_trucks', 'max_bid_amount', 'assigned_user_id', 'assigned_bid_amount', 'rejected_by', 'created_by', 'updated_by'], 'integer'],
            [['industry_user_id'], 'required'],
            [['load_address_type', 'load_type', 'scheduling_type', 'share_to_preferred', 'assigned_status', 'rejected_from', 'status'], 'string'],
            [['bid_closing_date', 'reporting_date', 'assigned_at', 'rejected_at', 'created_at', 'updated_at'], 'safe'],
            [['booking_id', 'mobile_number', 'bid_closing_time', 'reporting_time', 'assigned_mobile_number'], 'string', 'max' => 20],
            [['full_name'], 'string', 'max' => 200],
            [['company_name', 'source_address_line1', 'source_address_line2', 'source_state', 'source_district', 'source_city', 'source_area', 'source_landmark', 'destination_address_line1', 'destination_address_line2', 'destination_state', 'destination_district', 'destination_city', 'destination_area', 'destination_landmark', 'material_type', 'truck_type', 'description', 'assigned_full_name', 'assigned_company_name'], 'string', 'max' => 250],
            [['source_pincode', 'destination_pincode', 'part_load_quotation_date'], 'string', 'max' => 10],
            [['weight'], 'string', 'max' => 50],
            [['assigned_truck_id'], 'string', 'max' => 100],
            [['reject_reason'], 'string', 'max' => 500],
            [['quotations'], 'string', 'max' => 1],
            [['industry_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['industry_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ref_rejected_load_id' => Yii::t('app', 'Ref Rejected Load ID'),
            'booking_id' => Yii::t('app', 'Booking ID'),
            'industry_user_id' => Yii::t('app', 'User ID'),
            'industry_id' => Yii::t('app', 'Industry ID'),
            'full_name' => Yii::t('app', 'Name'),
            'mobile_number' => Yii::t('app', 'Mobile Number'),
            'load_address_type' => Yii::t('app', 'Load Address Type'),
            'company_name' => Yii::t('app', 'Company name'),
            'source_address_id' => Yii::t('app', 'Source Address ID'),
            'source_address_line1' => Yii::t('app', 'Source Address Line1'),
            'source_address_line2' => Yii::t('app', 'Source Address Line2'),
            'source_pincode' => Yii::t('app', 'Source Pincode'),
            'source_state' => Yii::t('app', 'Source State'),
            'source_district' => Yii::t('app', 'Source District'),
            'source_city' => Yii::t('app', 'Source City'),
            'source_area' => Yii::t('app', 'Source Area'),
            'source_landmark' => Yii::t('app', 'Source Landmark'),
            'destination_address_line1' => Yii::t('app', 'Destination Address Line1'),
            'destination_address_line2' => Yii::t('app', 'Destination Address Line2'),
            'destination_pincode' => Yii::t('app', 'Destination Pincode'),
            'destination_state' => Yii::t('app', 'Destination State'),
            'destination_district' => Yii::t('app', 'Destination District'),
            'destination_city' => Yii::t('app', 'Destination City'),
            'destination_area' => Yii::t('app', 'Destination Area'),
            'destination_landmark' => Yii::t('app', 'Destination Landmark'),
            'load_type' => Yii::t('app', 'Load Type'),
            'scheduling_type' => Yii::t('app', 'Scheduling Type'),
            'material_type' => Yii::t('app', 'Material Type'),
            'weight' => Yii::t('app', 'Weight'),
            'truck_type' => Yii::t('app', 'Truck Type'),
            'no_of_trucks' => Yii::t('app', 'No. of Trucks'),
            'bid_closing_date' => Yii::t('app', 'Quotation Closing Date'),
            'bid_closing_time' => Yii::t('app', 'Quotation Closing Time'),
            'reporting_date' => Yii::t('app', 'Reporting Date'),
            'reporting_time' => Yii::t('app', 'Reporting Time'),
            'max_bid_amount' => Yii::t('app', 'Max. Quotation Amount'),
            'description' => Yii::t('app', 'Description'),
            'share_to_preferred' => Yii::t('app', 'Shared to Preferred'),
            'assigned_status' => Yii::t('app', 'Assigned Status'),
            'assigned_at' => Yii::t('app', 'Assigned At'),
            'assigned_user_id' => Yii::t('app', 'Assigned Transporter/Driver ID'),
            'assigned_full_name' => Yii::t('app', 'Transporter/Driver Name'),
            'assigned_mobile_number' => Yii::t('app', 'Transporter/Driver Mobile'),
            'assigned_company_name' => Yii::t('app', 'Transporter/Driver Company Name'),
            'assigned_bid_amount' => Yii::t('app', 'Assigned Bid Amount'),
            'assigned_truck_id' => Yii::t('app', 'Assigned Truck ID'),
            'part_load_quotation_date' => Yii::t('app', 'Part Load Quotation Date'),
            'reject_reason' => Yii::t('app', 'Reject Reason'),
            'rejected_from' => Yii::t('app', 'Rejected From'),
            'rejected_by' => Yii::t('app', 'Rejected By'),
            'rejected_at' => Yii::t('app', 'Rejected At'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'quotations' => Yii::t('app', 'Quotations'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustryUser()
    {
        return $this->hasOne(User::className(), ['id' => 'industry_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTripTrackings()
    {
        return $this->hasMany(TripTracking::className(), ['load_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return LoadsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LoadsQuery(get_called_class());
    }
}

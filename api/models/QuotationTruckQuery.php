<?php

namespace api\models;

/**
 * This is the ActiveQuery class for [[QuotationTruck]].
 *
 * @see QuotationTruck
 */
class QuotationTruckQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return QuotationTruck[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return QuotationTruck|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "city_master".
 *
 * @property int $id ID
 * @property string $city City Name
 * @property string|null $city_lat Latitude
 * @property string|null $city_lng Longitude
 * @property int|null $state_id State ID
 * @property string $status Status
 * @property string|null $created_at Created On
 * @property string|null $updated_at Updated On
 * @property int|null $created_by Created By
 * @property int|null $updated_by Updated By
 */
class CityMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city'], 'required'],
            [['state_id', 'created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['city'], 'string', 'max' => 250],
            [['city_lat', 'city_lng'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city' => Yii::t('app', 'City Name'),
            'city_lat' => Yii::t('app', 'Latitude'),
            'city_lng' => Yii::t('app', 'Longitude'),
            'state_id' => Yii::t('app', 'State ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CityMasterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CityMasterQuery(get_called_class());
    }
}

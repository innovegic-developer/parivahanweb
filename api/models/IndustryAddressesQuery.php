<?php

namespace api\models;

/**
 * This is the ActiveQuery class for [[IndustryAddresses]].
 *
 * @see IndustryAddresses
 */
class IndustryAddressesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return IndustryAddresses[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return IndustryAddresses|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

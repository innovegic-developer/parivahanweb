<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "material_type".
 *
 * @property int $id ID
 * @property string $material Material Type
 * @property string $status Status
 * @property string|null $created_at Created On
 * @property string|null $updated_at Updated On
 * @property int|null $created_by Created By
 * @property int|null $updated_by Updated By
 */
class MaterialType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material'], 'required'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['material'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'material' => Yii::t('app', 'Material Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return MaterialTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MaterialTypeQuery(get_called_class());
    }
}

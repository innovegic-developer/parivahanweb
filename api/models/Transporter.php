<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "transporter".
 *
 * @property int $id ID
 * @property int $user_id User ID
 * @property string $designation Designation
 * @property string $company_name Company Name
 * @property string $incorporation_certificate_number incorporation_certificate_number
 * @property string $shop_establishment_certificate_number shop_establishment_certificate_number
 * @property string $pan_number PAN No.
 * @property string $gst_number GST No.
 * @property string $aadhaar_number Aadhaar No.
 * @property string $aadhaar_photo Aadhaar Photo
 * @property string $pan_photo PAN Photo
 * @property string $address_line1 Address line1
 * @property string $address_line2 Address line2
 * @property string $pincode Pincode
 * @property string $country Country
 * @property string $state State
 * @property string $district District
 * @property string $city City
 * @property string $area Area
 * @property string $remark Remark
 * @property string $status Status
 * @property string $created_at Created On
 * @property string $updated_at Updated On
 * @property int $created_by Created By
 * @property int $updated_by Updated By
 */
class Transporter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transporter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'created_by', 'updated_by'], 'integer'],
            [['designation', 'status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['company_name', 'aadhaar_photo', 'pan_photo', 'address_line1', 'address_line2', 'area', 'remark'], 'string', 'max' => 250],
            [['incorporation_certificate_number', 'shop_establishment_certificate_number', 'state', 'district', 'city'], 'string', 'max' => 100],
            [['pan_number', 'gst_number', 'aadhaar_number', 'country'], 'string', 'max' => 50],
            [['pincode'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'designation' => Yii::t('app', 'Designation'),
            'company_name' => Yii::t('app', 'Company Name'),
            'incorporation_certificate_number' => Yii::t('app', 'incorporation_certificate_number'),
            'shop_establishment_certificate_number' => Yii::t('app', 'shop_establishment_certificate_number'),
            'pan_number' => Yii::t('app', 'PAN No.'),
            'gst_number' => Yii::t('app', 'GST No.'),
            'aadhaar_number' => Yii::t('app', 'Aadhaar No.'),
            'aadhaar_photo' => Yii::t('app', 'Aadhaar Photo'),
            'pan_photo' => Yii::t('app', 'PAN Photo'),
            'address_line1' => Yii::t('app', 'Address line1'),
            'address_line2' => Yii::t('app', 'Address line2'),
            'pincode' => Yii::t('app', 'Pincode'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'district' => Yii::t('app', 'District'),
            'city' => Yii::t('app', 'City'),
            'area' => Yii::t('app', 'Area'),
            'remark' => Yii::t('app', 'Remark'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TransporterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TransporterQuery(get_called_class());
    }
}

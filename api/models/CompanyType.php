<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "company_type".
 *
 * @property int $id ID
 * @property string|null $company_type Company Type
 * @property string $created_at Created On
 * @property string|null $updated_at Updated On
 * @property int|null $created_by Created By
 * @property int|null $updated_by Updated By
 * @property string $status Status
 */
class CompanyType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['company_type'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_type' => Yii::t('app', 'Company Type'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CompanyTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanyTypeQuery(get_called_class());
    }
}

<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "industry".
 *
 * @property int $id ID
 * @property int $user_id User ID
 * @property string $company_name Company Name
 * @property int|null $company_type_id Company Type ID
 * @property string|null $company_type Company Type
 * @property string|null $pan_number PAN No.
 * @property string|null $gst_number GST No.
 * @property string|null $aadhaar_number Aadhaar No.
 * @property string|null $aadhaar_photo Aadhaar Photo
 * @property string|null $pan_photo PAN Photo
 * @property string|null $address_line1 Address line1
 * @property string|null $address_line2 Address line2
 * @property string|null $pincode Pincode
 * @property string|null $country Country
 * @property string|null $state State
 * @property string|null $district District
 * @property string|null $city City
 * @property string|null $area Area
 * @property string|null $remark Remark
 * @property string $status Status
 * @property string|null $created_at Created On
 * @property string|null $updated_at Updated On
 * @property int|null $created_by Created By
 * @property int|null $updated_by Updated By
 */
class Industry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'industry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'company_name'], 'required'],
            [['user_id', 'company_type_id', 'created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['company_name', 'company_type', 'aadhaar_photo', 'pan_photo', 'address_line1', 'address_line2', 'area', 'remark'], 'string', 'max' => 250],
            [['pan_number', 'gst_number', 'aadhaar_number', 'country'], 'string', 'max' => 50],
            [['pincode'], 'string', 'max' => 10],
            [['state', 'district', 'city'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'company_name' => Yii::t('app', 'Company Name'),
            'company_type_id' => Yii::t('app', 'Company Type ID'),
            'company_type' => Yii::t('app', 'Company Type'),
            'pan_number' => Yii::t('app', 'PAN No.'),
            'gst_number' => Yii::t('app', 'GST No.'),
            'aadhaar_number' => Yii::t('app', 'Aadhaar No.'),
            'aadhaar_photo' => Yii::t('app', 'Aadhaar Photo'),
            'pan_photo' => Yii::t('app', 'PAN Photo'),
            'address_line1' => Yii::t('app', 'Address line1'),
            'address_line2' => Yii::t('app', 'Address line2'),
            'pincode' => Yii::t('app', 'Pincode'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'district' => Yii::t('app', 'District'),
            'city' => Yii::t('app', 'City'),
            'area' => Yii::t('app', 'Area'),
            'remark' => Yii::t('app', 'Remark'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return IndustryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IndustryQuery(get_called_class());
    }
}

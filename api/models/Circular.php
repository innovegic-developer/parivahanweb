<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "circular".
 *
 * @property int $id ID
 * @property string $circular_title Title
 * @property string $circular_desc Description
 * @property string $circular_date Date
 * @property string $circular_pdf Upload PDF
 * @property string $created_on Created On
 * @property string $updated_on Modified On
 * @property int $created_by Created By
 * @property int $updated_by Modified By
 * @property string $status Status
 */
class Circular extends \yii\db\ActiveRecord
{
  
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'circular';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['circular_title', 'circular_desc', 'circular_date', 'circular_pdf', 'status'], 'required'],
            [['circular_date', 'created_on', 'updated_on'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['circular_title'], 'string', 'max' => 256],
            [['circular_desc'], 'string', 'max' => 5000],
            [['circular_pdf'], 'file', 'extensions' => 'pdf',],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'circular_title' => Yii::t('app', 'Title'),
            'circular_desc' => Yii::t('app', 'Description'),
            'circular_date' => Yii::t('app', 'Date'),
            'circular_pdf' => Yii::t('app', 'Upload PDF'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Modified On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Modified By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CircularQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CircularQuery(get_called_class());
    }
}

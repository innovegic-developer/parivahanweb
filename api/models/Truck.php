<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "truck".
 *
 * @property int $id ID
 * @property int $user_id User ID
 * @property string $vehicle_no Vehicle No.
 * @property string $gps_installed GPS Installed
 * @property string $weight Weight (MT)
 * @property string $truck_type Truck Type
 * @property string $rc_no RC No,
 * @property string $license_no License No.
 * @property string $license_photo License Photo
 * @property string $permit_type Permit Type
 * @property string $permit_photo Permit Photo
 * @property string $insurance_policy_no Insurance Policy No.
 * @property string $insurance_policy_photo Insurance Policy Photo
 * @property string $pollution_no Pollution No.
 * @property string $pollution_photo Pollution Photo
 * @property string $rto_fitness_certificate_no RTO Fitness Certificate No.
 * @property string $rto_fitness_certificate_photo RTO Fitness Certificate Photo
 * @property string $local_registration_no Local Registration No.
 * @property string $local_registration_photo Local Registration Photo
 * @property string $verified Verified
 * @property string $status Status
 * @property string $created_at Created On
 * @property string $updated_at Updated On
 * @property int $created_by Created By
 * @property int $updated_by Updated By
 */
class Truck extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'truck';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'created_by', 'updated_by'], 'integer'],
            [['gps_installed', 'verified', 'status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['vehicle_no'], 'string', 'max' => 50],
            [['weight', 'truck_type', 'rc_no', 'license_no', 'permit_type', 'insurance_policy_no', 'pollution_no', 'rto_fitness_certificate_no', 'local_registration_no'], 'string', 'max' => 100],
            [['license_photo', 'permit_photo', 'insurance_policy_photo', 'pollution_photo', 'rto_fitness_certificate_photo', 'local_registration_photo'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'vehicle_no' => Yii::t('app', 'Vehicle No.'),
            'gps_installed' => Yii::t('app', 'GPS Installed'),
            'weight' => Yii::t('app', 'Weight (MT)'),
            'truck_type' => Yii::t('app', 'Truck Type'),
            'rc_no' => Yii::t('app', 'RC No,'),
            'license_no' => Yii::t('app', 'License No.'),
            'license_photo' => Yii::t('app', 'License Photo'),
            'permit_type' => Yii::t('app', 'Permit Type'),
            'permit_photo' => Yii::t('app', 'Permit Photo'),
            'insurance_policy_no' => Yii::t('app', 'Insurance Policy No.'),
            'insurance_policy_photo' => Yii::t('app', 'Insurance Policy Photo'),
            'pollution_no' => Yii::t('app', 'Pollution No.'),
            'pollution_photo' => Yii::t('app', 'Pollution Photo'),
            'rto_fitness_certificate_no' => Yii::t('app', 'RTO Fitness Certificate No.'),
            'rto_fitness_certificate_photo' => Yii::t('app', 'RTO Fitness Certificate Photo'),
            'local_registration_no' => Yii::t('app', 'Local Registration No.'),
            'local_registration_photo' => Yii::t('app', 'Local Registration Photo'),
            'verified' => Yii::t('app', 'Verified'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TruckQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TruckQuery(get_called_class());
    }
}

<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "truck_type".
 *
 * @property int $id ID
 * @property string $truck Weight
 * @property string $status Status
 * @property string|null $created_at Created On
 * @property string|null $updated_at Updated On
 * @property int|null $created_by Created By
 * @property int|null $updated_by Updated By
 */
class TruckType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'truck_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['truck'], 'required'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['truck'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'truck' => Yii::t('app', 'Weight'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TruckTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TruckTypeQuery(get_called_class());
    }
}

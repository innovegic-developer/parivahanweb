<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "industry_addresses".
 *
 * @property int $id ID
 * @property int $user_id User ID
 * @property string $address_title Address Title
 * @property string $address_line1 Address line1
 * @property string $address_line2 Address line2
 * @property string $pincode Pincode
 * @property string $country Country
 * @property string $state State
 * @property string $district District
 * @property string $city City
 * @property string $area Area
 * @property int $verified 0 - Not verified, 1 - verified
 * @property string $status Status
 * @property string $created_at Created On
 * @property string $updated_at Updated On
 * @property int $created_by Created By
 * @property int $updated_by Updated By
 */
class IndustryAddresses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'industry_addresses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'verified', 'created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['address_title', 'address_line1', 'address_line2', 'area'], 'string', 'max' => 250],
            [['pincode'], 'string', 'max' => 10],
            [['country'], 'string', 'max' => 50],
            [['state', 'district', 'city'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'address_title' => Yii::t('app', 'Address Title'),
            'address_line1' => Yii::t('app', 'Address line1'),
            'address_line2' => Yii::t('app', 'Address line2'),
            'pincode' => Yii::t('app', 'Pincode'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'district' => Yii::t('app', 'District'),
            'city' => Yii::t('app', 'City'),
            'area' => Yii::t('app', 'Area'),
            'verified' => Yii::t('app', '0 - Not verified, 1 - verified'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return IndustryAddressesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IndustryAddressesQuery(get_called_class());
    }
}

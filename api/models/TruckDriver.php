<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "truck_driver".
 *
 * @property int $id ID
 * @property int $user_id User ID
 * @property string $driver_name Name
 * @property string $driver_mobile Mobile No.
 * @property string $driver_current_address Current Address
 * @property string $driver_original_address Original Address
 * @property string $driver_aadhaar_number Aadhaar Number
 * @property string $driver_licence_number Licence Number
 * @property string $driver_pan_number PAN No.
 * @property string $driver_aadhaar_photo Aadhaar Photo
 * @property string $driver_licence_photo Licence Photo
 * @property string $driver_pan_photo PAN Photo
 * @property string $verified 0 - Not verified, 1 - verified
 * @property string $status Status
 * @property string $created_at Created On
 * @property string $updated_at Updated On
 * @property int $created_by Created By
 * @property int $updated_by Updated By
 *
 * @property User $user
 */
class TruckDriver extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'truck_driver';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['driver_name', 'driver_current_address', 'driver_original_address', 'driver_aadhaar_photo', 'driver_licence_photo', 'driver_pan_photo'], 'string', 'max' => 250],
            [['driver_mobile', 'driver_aadhaar_number', 'driver_licence_number', 'driver_pan_number', 'verified'], 'string', 'max' => 20],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'driver_name' => Yii::t('app', 'Name'),
            'driver_mobile' => Yii::t('app', 'Mobile No.'),
            'driver_current_address' => Yii::t('app', 'Current Address'),
            'driver_original_address' => Yii::t('app', 'Original Address'),
            'driver_aadhaar_number' => Yii::t('app', 'Aadhaar Number'),
            'driver_licence_number' => Yii::t('app', 'Licence Number'),
            'driver_pan_number' => Yii::t('app', 'PAN No.'),
            'driver_aadhaar_photo' => Yii::t('app', 'Aadhaar Photo'),
            'driver_licence_photo' => Yii::t('app', 'Licence Photo'),
            'driver_pan_photo' => Yii::t('app', 'PAN Photo'),
            'verified' => Yii::t('app', '0 - Not verified, 1 - verified'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return TruckDriverQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TruckDriverQuery(get_called_class());
    }
}

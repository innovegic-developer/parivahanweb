<?php

namespace api\models;

/**
 * This is the ActiveQuery class for [[CityMaster]].
 *
 * @see CityMaster
 */
class CityMasterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CityMaster[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CityMaster|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

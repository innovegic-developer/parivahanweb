<?php

namespace api\models;

/**
 * This is the ActiveQuery class for [[Transporter]].
 *
 * @see Transporter
 */
class TransporterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Transporter[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Transporter|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "quotation".
 *
 * @property int $id
 * @property int $load_id Load ID
 * @property int $user_id Transporter/Driver ID
 * @property string $full_name
 * @property string $mobile_number
 * @property string $company_name
 * @property int $bid_amount Bid Amount
 * @property string $part_load_quotation_date Part Load Quotation Date
 * @property string $truck_id Truck IDs
 * @property string $remark Remark
 * @property int $assigned_to_me Assigned
 * @property string $status Status
 * @property string $created_at Created On
 * @property string $updated_at Updated On
 * @property int $created_by Created By
 * @property int $updated_by Updated By
 */
class Quotation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quotation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['load_id', 'user_id'], 'required'],
            [['load_id', 'user_id', 'bid_amount', 'assigned_to_me', 'created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['full_name', 'company_name', 'remark'], 'string', 'max' => 250],
            [['mobile_number'], 'string', 'max' => 20],
            [['part_load_quotation_date'], 'string', 'max' => 10],
            [['truck_id'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'load_id' => Yii::t('app', 'Load ID'),
            'user_id' => Yii::t('app', 'Transporter/Driver ID'),
            'full_name' => Yii::t('app', 'Full Name'),
            'mobile_number' => Yii::t('app', 'Mobile Number'),
            'company_name' => Yii::t('app', 'Company Name'),
            'bid_amount' => Yii::t('app', 'Bid Amount'),
            'part_load_quotation_date' => Yii::t('app', 'Part Load Quotation Date'),
            'truck_id' => Yii::t('app', 'Truck IDs'),
            'remark' => Yii::t('app', 'Remark'),
            'assigned_to_me' => Yii::t('app', 'Assigned'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return QuotationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new QuotationQuery(get_called_class());
    }
}

<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
       // 'common\components\IdentitySwitcher',
    ],
    'controllerNamespace' => 'api\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
           // 'baseUrl' => '/maruticloud/backend',
        ],
        // 'user' => [
        //     'identityClass' => 'backend\models\AdminUsers',
        //     //'identityClass' => 'common\models\User',
        //     'enableAutoLogin' => true,
        //     'authTimeout' => 1800,
        //    // 'loginUrl' => ['site/index'],
        //     'on beforeLogout ' => function ($event) {
        //         \backend\models\AdminUsers::updateuserdetailBeforelogout();
        //     },
        //     'on afterLogin ' => function ($event) {
        //         \backend\models\AdminUsers::updateuserdetailAfterlogin();
        //     },
        //     'identityCookie' => ['name' => '_identity-api', 'httpOnly' => true, 'path' => '/api/web'],
        // ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // 'viewPath' => '@app/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'developer.silicon@gmail.com',
                'password' => 'nathing7896$',
                //'port' => '465',
                'port' => '587',
                'encryption' => 'tls',
            ],
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        ],
        'amazonses' => [
            //# To Send Email From Amazon SES
            // send all mails to a file by default. You have to set 'useFileTransport' to false and configure a transport for the mailer to send real emails.
            'class' => 'yii\swiftmailer\Mailer',
            // 'viewPath' => '@app/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'email-smtp.us-east-1.amazonaws.com',
                'username' => 'AKIAJKP377XDQRGFVAIQ',
                'password' => 'Aran0YXzG1VZkSw81ME1PAJasKetfddm5LiWOLULMntc',
                //'port' => '465',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
            'timeout' => 1800,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //'baseUrl' => '/yii2-demo/api/web',
            'rules' => [
                //'' => '/maruticloud/backend/web',
                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            ],
        ],
//        'assetManager' => [
//            'linkAssets' => true,
//        //'appendTimestamp' => true,
//        ],
//        // Amazon S3 Configurations        
//        's3bucket' => [
//            'class' => \frostealth\yii2\aws\s3\Storage::className(),
//            'region' => 'ap-south-1',
//            'credentials' => [// Aws\Credentials\CredentialsInterface|array|callable
//                'key' => 'AKIAIXFHNDGL4GSGAHWQ',
//                'secret' => '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',
//            ],
//            'bucket' => AMAZON_S3_BUCKET_NAME,
//            'cdnHostname' => AMAZON_S3_CDNHHOSTNAME,
//            'defaultAcl' => \frostealth\yii2\aws\s3\Storage::ACL_PUBLIC_READ,
//            'debug' => false, // bool|array
//        ],
    // Amazon S3 Configurations
    ],
    'params' => $params,
];

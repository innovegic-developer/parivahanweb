<?php
namespace api\controllers;
use yii;
use yii\helpers\Url;
use api\models\User;
use api\models\CityMaster;
use api\models\CompanyType;
use api\models\MaterialType;
use api\models\WeightMaster;
use api\models\TruckType;
use api\models\Industry;
use api\models\IndustryAddresses;
use api\models\Transporter;
use api\models\Loads;
use api\models\Truck;
use api\models\Quotation;
use api\models\TruckDriver;
use api\models\QuotationTruck;
use api\models\TripTracking;
use api\models\PreferredList;
use api\models\Circular;
use api\models\Tender;
use api\models\Feedback;
use api\models\Grievance;
use yii\helpers\ArrayHelper;
use yii\db\Query;
define("APP_TITLE","Parivahan Suvidha");
define('ANDROID_APP_VERSION', 7); // 6 - 03/10/2020
define('IPHONE_APP_VERSION', '');
define("API_USERNAME_","parivahan2020");
define("API_PASSWORD_","558c3264f06213fa8e717a95925cb4dc");
define("API_TOKEN_","22c2141c1002586b289e0a036b76423d");
define("ANDROID_PUSH_KEY_","AAAAijwWteE:APA91bHR-2NI4W2BTywnC5RDmxnn31FG2zPsvbQVt1l_4lt02oR6zpFFHMk4cI1ACMS5TFKvynqwRJV8AMTFUIEx1LDyVWSh4VCzqeU9RaLOTmFKq4yuEZJAnWKOO6jDf1UQ4i9Qej8v");
define('SMS_SENDER', 'INNSPL');
define('SMS_API_KEY', 'A78a24827926f3b2897a85ada2d17042a');//INNSPL,DIUSGR,DMNSGR
//define('SMS_API_KEY', 'A6353a955633af12d6c72f01959002156');// OIDCPS, GOVGUJ
define('LIMIT', '20');
//define('BASE_URL', 'https://parivahan.innovegicsolutions.in/');
define('BASE_URL', 'https://oidcparivahansuvidha.com/');
//define('UPLOAD_DIR_PATH_', '/var/www/html/dev/public/parivahan/');
define('UPLOAD_DIR_PATH_', '/var/www/html/dev/public/parivahan-live/');
define('APP_LINK', 'https://oidcparivahansuvidha.com/apk/how-to-update/');
//define('APP_LINK', 'https://play.google.com/store/apps/details?id=com.parivahan');
//define('REL_PATH', '../parivahan/');
//define('DB', 'db2');
class V2ParivahanController extends \yii\web\Controller
{
	/*
     * Before Action
     */
    public function beforeAction($action) {

	        $this->enableCsrfValidation = false;
	        return parent::beforeAction($action);
	  }
 
	/*
	 * API Link : backend/api/v1/sample
	 * Parameters :  param1=param1Value,param2=param2Value
	 * Method:  Post
	 * */
	public function actionCheckVersionUpdate() {
     	$Version = ANDROID_APP_VERSION;
     	$headers = Yii::$app->request->headers;  
		$user_id=$headers->get('USER-ID');
		$completed_profile = '1';
		if($user_id >0)
		{
			$usr = User::find()->select(['completed_profile'])->where(['id'=>$user_id])->one();
			$completed_profile = $usr['completed_profile'];
		}
     	if(isset($headers) && $headers->get('DEVICE-TYPE')!='') {     
			$deviceType =$headers->get('DEVICE-TYPE');
			if($deviceType=='iphone')
			$Version = IPHONE_APP_VERSION;
			else
			$Version = ANDROID_APP_VERSION;
		}
		//$apkLink = 'https://oidcparivahansuvidha.com/apk/how-to-update/';
     	$lng = $this->_getLanguage();
		\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

		if($this->_checkAuth()){
			$data = \yii::$app->request->post();		
			if( isset($data['appVersion']) && trim($data['appVersion']) !='' ) {
				
					if($Version > $data['appVersion']) {
						if($lng=='gu')
						return array('status'=>true,'message'=>'કૃપા કરીને એપ્લિકેશનના નવા સંસ્કરણ પર અપડેટ કરો.','apkLink' => APP_LINK, 'completed_profile'=>$completed_profile);
						elseif($lng=='hi')
						return array('status'=>true,'message'=>'कृपया एप्लिकेशन के नया संस्करण में अपडेट करें.','apkLink' => APP_LINK, 'completed_profile'=>$completed_profile);
						else
						return array('status'=>true,'message'=>'New version available to update.','apkLink' => APP_LINK, 'completed_profile'=>$completed_profile);
					}
					else {
						return array('status'=>false,'message'=>'No new version is available.', 'completed_profile'=>$completed_profile);
					}
				}
		}
		else {
			return $this->_authFailed();
		}
	}
	
	/*
	 * Parameters :  param1=param1Value,param2=param2Value
	 * */
	public function actionGenerateCode(){
		//return '123456';
		$randomNumber = rand(100000,999999);
		$user = User::find()->select('id')->where(['password' => md5($randomNumber)])->one();
		if(!empty($user)){
			$this->actionGenerateCode();
			return false;
		}		
		return $randomNumber;
	}
	
	// user_level
	// 3 - Industry, 4 - Transporter, 5 - Truck Operartor, 6 - Truck Driver
	public function actionCheckAccountExist() {
		\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		if($this->_checkAutoLogout()){
			return $this->_autoLogout();
		}
		$data = \yii::$app->request->post();
		if(!isset($data['mobile_number']) || trim($data['mobile_number']) =='') 
		{
			return array('status'=>false,'message'=>'Please enter mobile number.');
		}
		$user = User::find()->select('id,first_name,last_name,mobile_number,username,password,email,user_image,user_level,status')
		->where("mobile_number='".$data['mobile_number']."'")->andWhere("user_level in (3,4,5,6)")->one();	
		if (empty($user)) {	
			return array('status'=>true, 'message'=>'No account found.', 'is_exist'=>'0', 'user_level_name'=>'');
		}
		elseif($user['status']=='0') {	
			return array('status'=>false, 'message'=>'Your account is inactivated. Please contact Department.',
			'is_exist'=>'0', 'user_level_name'=>'');
		}
		else {
			if($user['user_level']=='3')
			$user_type = 'industry';	
			elseif($user['user_level']=='4')
			$user_type = 'transporter';
			elseif($user['user_level']=='5')
			$user_type = 'truck_operator';
			elseif($user['user_level']=='6')
			$user_type = 'truck_driver';
			return array('status'=>true, 'message'=>'Success', 'is_exist'=>'1', 'user_level_name'=>$user_type);
		}
	}

	public function actionGetCompanyTypeMaster() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		if($this->_checkAutoLogout()){
			return $this->_autoLogout();
		}
		if(!$this->_checkAuth()){ 
    		return $this->_authFailed();
    	}
		$data = \yii::$app->request->post();		
		$result = CompanyType::find()->select(['id','company_type'])
		->where(['status' => 'Active'])->all();			
		return array('status' => true,'message'=>'Success' ,'data'=>$result);
	}	
	
	public function actionIndustryRegister() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['first_name']) || trim($data['first_name']) == '' 
				|| !isset($data['last_name']) || trim($data['last_name']) ==''
				//|| !isset($data['email']) || trim($data['email']) ==''
				|| !isset($data['mobile_number']) || trim($data['mobile_number']) ==''
				//|| !isset($data['company_name']) || trim($data['company_name']) ==''
				//|| !isset($data['company_type_id']) || trim($data['company_type_id']) ==''
				|| !isset($data['address_line1']) || trim($data['address_line1']) ==''
				//|| !isset($data['address_line2']) || trim($data['address_line2']) ==''
				//|| !isset($data['pincode']) || trim($data['pincode']) ==''
				|| !isset($data['state']) || trim($data['state']) ==''
				|| !isset($data['district']) || trim($data['district']) ==''
				//|| !isset($data['city']) || trim($data['city']) ==''
				|| !isset($data['area']) || trim($data['area']) ==''
			) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$user = User::find()->select('id, first_name,last_name,mobile_number,username,password,email,user_image,user_level,status')
				->where("mobile_number='".$data['mobile_number']."'")->one();	
				if (!empty($user)) {	
					return array('status'=>false,'message'=>'Account already exist.');
				}
				if (!empty($user) && $user['mobile_number']==$data['mobile_number']) {	
					return array('status'=>false,'message'=>'Phone number already exist.');
				}
				$randomNumber = $this->actionGenerateCode();
				$model = new User;
				$model->first_name=trim($data['first_name']);
				$model->last_name=trim($data['last_name']);
				$model->mobile_number=trim($data['mobile_number']);
				if(isset($data['email']))
				$model->email=trim($data['email']);
				//$model->username=trim($data['username']);
				//$model->password=md5(trim($data['password']));
				$model->password=md5($randomNumber);
				$model->temp_password=$randomNumber;
				$model->user_level='3';
				$model->user_level_name='industry';
				$model->status='1';
				$model->web = isset($data['web'])?$data['web']:'0';
				//$model->varification_code=$randomNumber;
				/*$msg = "Dear ".$data['first_name']." ".$data['last_name'].", ";
				$msg .= "Thank you for registration. ";
				$msg .= "Username: ".$data['mobile_number']." ";
				$msg .= "Password: ".$randomNumber;
				$msg .= SMS_SUFFIX;*/
				if($model->save(false)) {
					$id = $model->id;
					$model1 = new Industry;
					$model1->user_id = $id;
					$model1->first_name=trim($data['first_name']);
					$model1->last_name=trim($data['last_name']);
					$model1->mobile_number=trim($data['mobile_number']);
					if(isset($data['email']))
					$model1->email=trim($data['email']);
					if(isset($data['company_name']))
					$model1->company_name = trim($data['company_name']);
					if(isset($data['company_type']))
					$model1->company_type = trim($data['company_type']);
					elseif(isset($data['company_type_id']))
					$model1->company_type_id = trim($data['company_type_id']);
					if(isset($data['company_type_id']) && $data['company_type_id'] >0)
					{
						$type =CompanyType::findOne($data['company_type_id']);
						$model1->company_type = $type->company_type;
					}
					$model1->address_line1 = trim($data['address_line1']);
					if(isset($data['address_line2']))
					$model1->address_line2 = trim($data['address_line2']);
					if(isset($data['pincode']))
					$model1->pincode = trim($data['pincode']);
					$model1->state = trim($data['state']);
					if(is_numeric($data['district']))
					{
						$dbConn = Yii::$app->db;
						$command = $dbConn->createCommand("SELECT id,district FROM `district_master` WHERE id='".trim($data['district'])."'");
						$dist = $command->queryOne();
						$data['district'] = $dist['district'];
					}
					$model1->district = trim($data['district']);
					if(isset($data['city']) && $data['city']!='')
					$model1->city = trim($data['city']);
					else
					$model1->city = trim($data['district']);	
					$model1->area = trim($data['area']);
					if(isset($data['landmark']))
					$model1->landmark = trim($data['landmark']);
					//if(isset($data['incorporation_certificate_number']))
					//$model1->incorporation_certificate_number = trim($data['incorporation_certificate_number']);
					//if(isset($data['shop_establishment_certificate_number']))
					//$model1->shop_establishment_certificate_number = trim($data['shop_establishment_certificate_number']);
					//if(isset($data['pan_number']))
					//$model1->pan_number = trim($data['pan_number']);
					if(isset($data['any_government_doc_number']))
					$model1->any_government_doc_number = trim($data['any_government_doc_number']);
					if(isset($data['udhyog_aadhaar_certificate_number']))
					$model1->udhyog_aadhaar_certificate_number = trim($data['udhyog_aadhaar_certificate_number']);
					$path = UPLOAD_DIR_PATH_ . 'uploads/industry/';
					if(isset($_FILES['any_government_doc_photo']['name'])  && trim($_FILES['any_government_doc_photo']['name'])!='') {	
						$tmpName = $_FILES['any_government_doc_photo']['tmp_name'];
						$str = explode(".",$_FILES['any_government_doc_photo']['name']);
						$l=count($str);
						$img =  "any_government_doc_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->any_government_doc_photo = $img;
						}
					}
					if(isset($_FILES['udhyog_aadhaar_certificate_photo']['name'])  && trim($_FILES['udhyog_aadhaar_certificate_photo']['name'])!='') {	
						$tmpName = $_FILES['udhyog_aadhaar_certificate_photo']['tmp_name'];
						$str = explode(".",$_FILES['udhyog_aadhaar_certificate_photo']['name']);
						$l=count($str);
						$img =  "udhyog_aadhaar_certificate_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->udhyog_aadhaar_certificate_photo = $img;
						}
					}
					/*if(isset($_FILES['incorporation_certificate_photo']['name'])  && trim($_FILES['incorporation_certificate_photo']['name'])!='') {	
						$tmpName = $_FILES['incorporation_certificate_photo']['tmp_name'];
						$str = explode(".",$_FILES['incorporation_certificate_photo']['name']);
						$l=count($str);
						$img =  "incorporation_certificate_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->incorporation_certificate_photo = $img;
						}
					}
					if(isset($_FILES['shop_establishment_certificate_photo']['name'])  && trim($_FILES['shop_establishment_certificate_photo']['name'])!='') {	
						$tmpName = $_FILES['shop_establishment_certificate_photo']['tmp_name'];
						$str = explode(".",$_FILES['shop_establishment_certificate_photo']['name']);
						$l=count($str);
						$img =  "shop_establishment_certificate_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->shop_establishment_certificate_photo = $img;
						}
					}
					if(isset($_FILES['pan_photo']['name'])  && trim($_FILES['pan_photo']['name'])!='') {	
						$tmpName = $_FILES['pan_photo']['tmp_name'];
						$str = explode(".",$_FILES['pan_photo']['name']);
						$l=count($str);
						$img =  "pan_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->pan_photo = $img;
						}
					}*/
					/*if(isset($_FILES['aadhaar_photo']['name'])  && trim($_FILES['aadhaar_photo']['name'])!='') {	
						$tmpName = $_FILES['aadhaar_photo']['tmp_name'];
						$str = explode(".",$_FILES['aadhaar_photo']['name']);
						$l=count($str);
						$img =  "aadhaar_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->aadhaar_photo = $img;
						}
					}*/
					$mobile = trim($data['mobile_number']);
					//if($this->_sendTextSMS($mobile,$msg)) {
					if($model1->save(false)) {
						//$this->_sendTextSMS($mobile,$msg);
						$add = new IndustryAddresses;
						$add->user_id = $id;
						$add->address_title='Unit1';
						$add->address_line1=trim($data['address_line1']);
						if(isset($data['address_line2']))
						$add->address_line2=trim($data['address_line2']);
						if(isset($data['pincode']))
						$add->pincode=trim($data['pincode']);
						$add->state=trim($data['state']);
						$add->district=trim($data['district']);
						if(isset($data['city']) && $data['city']!='')
						$add->city=trim($data['city']);
						else
						$add->city = trim($data['district']);
						$add->area=trim($data['area']);
						if(isset($data['landmark']))
						$add->landmark=trim($data['landmark']);	
						$add->save(false);
						
						/*$msg = "Dear ".$data['first_name']." ".$data['last_name'].", ";
						$msg .= "Thank you for registration. ";
						$msg .= "Username: ".$data['mobile_number']." ";
						$msg .= "Password: ".$randomNumber;
						$msg .= SMS_SUFFIX;
						$mobile = trim($data['mobile_number']);
						$this->_sendTextSMS($mobile,$msg) ;*/
					
						$user = User::find()->select('id,first_name,last_name,mobile_number,username,password,email,user_image,user_level,user_level_name,status')
						->where(['mobile_number' => $data['mobile_number']])->one();
						if($user['user_image']!='')
						$user['user_image']= BASE_URL .'uploads/profile/'.$user['user_image'];
					    else
						$user['user_image']= '';
						return array('status' => true,'message'=>'Thank you for signup. We will review your account details and back to you.' ,'data'=>$user);
					}
					else {	
						return $this->_errorMessage1();			
					}
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	
	public function actionTransporterRegister() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['first_name']) || trim($data['first_name']) == '' 
				|| !isset($data['last_name']) || trim($data['last_name']) ==''
				//|| !isset($data['email']) || trim($data['email']) ==''
				|| !isset($data['mobile_number']) || trim($data['mobile_number']) ==''
				//|| !isset($data['company_name']) || trim($data['company_name']) ==''
				//|| !isset($data['designation']) || trim($data['designation']) ==''
				|| !isset($data['address_line1']) || trim($data['address_line1']) ==''
				//|| !isset($data['address_line2']) || trim($data['address_line2']) ==''
				//|| !isset($data['pincode']) || trim($data['pincode']) ==''
				|| !isset($data['state']) || trim($data['state']) ==''
				|| !isset($data['district']) || trim($data['district']) ==''
				//|| !isset($data['city']) || trim($data['city']) ==''
				|| !isset($data['area']) || trim($data['area']) ==''
			) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$user = User::find()->select('id, first_name,last_name,mobile_number,username,password,email,user_image,user_level,status')
				->where("mobile_number='".$data['mobile_number']."'")->one();	
				if (!empty($user)) {	
					return array('status'=>false,'message'=>'Account already exist.');
				}
				if (!empty($user) && $user['mobile_number']==$data['mobile_number']) {	
					return array('status'=>false,'message'=>'Phone number already exist.');
				}
				$randomNumber = $this->actionGenerateCode();
				$model = new User;
				$model->first_name=trim($data['first_name']);
				$model->last_name=trim($data['last_name']);
				$model->mobile_number=trim($data['mobile_number']);
				if(isset($data['email']))
				$model->email=trim($data['email']);
				//$model->username=trim($data['username']);
				//$model->password=md5(trim($data['password']));
				$model->password=md5($randomNumber);
				$model->temp_password=$randomNumber;
				$model->user_level='4';
				$model->user_level_name='transporter';
				$model->status='1';
				$model->web = isset($data['web'])?$data['web']:'0';
				//$model->varification_code=$randomNumber;
				if($model->save(false)) {
					$id = $model->id;
					$model1 = new Transporter;
					$model1->user_id = $id;
					$model1->user_level = '4';
					$model1->first_name=trim($data['first_name']);
					$model1->last_name=trim($data['last_name']);
					$model1->mobile_number=trim($data['mobile_number']);
					if(isset($data['email']))
					$model1->email=trim($data['email']);
					if(isset($data['company_name']))
					$model1->company_name = trim($data['company_name']);
					if(isset($data['designation']))
					$model1->designation = trim($data['designation']);
					$model1->address_line1 = trim($data['address_line1']);
					if(isset($data['address_line2']))
					$model1->address_line2 = trim($data['address_line2']);
					if(isset($data['pincode']))
					$model1->pincode = trim($data['pincode']);
					$model1->state = trim($data['state']);
					if(is_numeric($data['district']))
					{
						$dbConn = Yii::$app->db;
						$command = $dbConn->createCommand("SELECT id,district FROM `district_master` WHERE id='".trim($data['district'])."'");
						$dist = $command->queryOne();
						$data['district'] = $dist['district'];
					}
					$model1->district = trim($data['district']);
					if(isset($data['city']) && $data['city']!='')
					$model1->city = trim($data['city']);
					else
					$model1->city = trim($data['district']);
					$model1->area = trim($data['area']);
					if(isset($data['landmark']))
					$model1->landmark = trim($data['landmark']);
					//if(isset($data['incorporation_certificate_number']))
					//$model1->incorporation_certificate_number = trim($data['incorporation_certificate_number']);
					//if(isset($data['shop_establishment_certificate_number']))
					//$model1->shop_establishment_certificate_number = trim($data['shop_establishment_certificate_number']);
					//if(isset($data['pan_number']))
					//$model1->pan_number = trim($data['pan_number']);
					if(isset($data['rcc_number']))
					$model1->rcc_number = trim($data['rcc_number']);
					$path = UPLOAD_DIR_PATH_ . 'uploads/transporter/';
					if(isset($_FILES['rcc_photo']['name'])  && trim($_FILES['rcc_photo']['name'])!='') {	
						$tmpName = $_FILES['rcc_photo']['tmp_name'];
						$str = explode(".",$_FILES['rcc_photo']['name']);
						$l=count($str);
						$img =  "rcc_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->rcc_photo = $img;
						}
					}
					/*if(isset($_FILES['incorporation_certificate_photo']['name'])  && trim($_FILES['incorporation_certificate_photo']['name'])!='') {	
						$tmpName = $_FILES['incorporation_certificate_photo']['tmp_name'];
						$str = explode(".",$_FILES['incorporation_certificate_photo']['name']);
						$l=count($str);
						$img =  "incorporation_certificate_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->incorporation_certificate_photo = $img;
						}
					}
					if(isset($_FILES['shop_establishment_certificate_photo']['name'])  && trim($_FILES['shop_establishment_certificate_photo']['name'])!='') {	
						$tmpName = $_FILES['shop_establishment_certificate_photo']['tmp_name'];
						$str = explode(".",$_FILES['shop_establishment_certificate_photo']['name']);
						$l=count($str);
						$img =  "shop_establishment_certificate_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->shop_establishment_certificate_photo = $img;
						}
					}
					if(isset($_FILES['pan_photo']['name'])  && trim($_FILES['pan_photo']['name'])!='') {	
						$tmpName = $_FILES['pan_photo']['tmp_name'];
						$str = explode(".",$_FILES['pan_photo']['name']);
						$l=count($str);
						$img =  "pan_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->pan_photo = $img;
						}
					}*/
					/*if(isset($_FILES['aadhaar_photo']['name'])  && trim($_FILES['aadhaar_photo']['name'])!='') {	
						$tmpName = $_FILES['aadhaar_photo']['tmp_name'];
						$str = explode(".",$_FILES['aadhaar_photo']['name']);
						$l=count($str);
						$img =  "aadhaar_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->aadhaar_photo = $img;
						}
					}*/
					$mobile = trim($data['mobile_number']);
					if($model1->save(false)) {	

						// REGISTER TRUCK OWNER AS A DRIVER ALSO
						$model2 = new TruckDriver;
						$model2->transporter_user_id=$model1->user_id;
						$model2->user_id=$model1->user_id;
						$model2->driver_name=$model1->first_name.' '.$model1->last_name;
						$model2->driver_mobile=$model1->mobile_number;
						$model2->driver_current_address=$model1->address_line1.', '.$model1->address_line2.', '.$model1->district.', '.$model1->area.'-'.$model1->pincode;
						$model2->driver_original_address=$model1->address_line1.', '.$model1->address_line2.', '.$model1->district.', '.$model1->area.'-'.$model1->pincode;
						//$model2->driver_aadhaar_number=$model1->aadhaar_number;
						//$model2->driver_licence_number=$model1->driver_licence_number;
						//$model2->driver_pan_number=$model1->pan_number;
						$path = UPLOAD_DIR_PATH_ . 'uploads/driver/';
						if(isset($_FILES['aadhaar_photo']['name'])  && trim($_FILES['aadhaar_photo']['name'])!='') {	
							$tmpName = $_FILES['aadhaar_photo']['tmp_name'];
							$str = explode(".",$_FILES['aadhaar_photo']['name']);
							$l=count($str);
							$img =  "driver_aadhaar_photo"."_".time().".".$str[$l-1];
							if(move_uploaded_file($tmpName,$path.$img)){
								$model2->driver_aadhaar_photo = $img;
							}
						}
						if(isset($_FILES['driver_licence_photo']['name'])  && trim($_FILES['driver_licence_photo']['name'])!='') {	
							$tmpName = $_FILES['driver_licence_photo']['tmp_name'];
							$str = explode(".",$_FILES['driver_licence_photo']['name']);
							$l=count($str);
							$img =  "driver_licence_photo"."_".time().".".$str[$l-1];
							if(move_uploaded_file($tmpName,$path.$img)){
								$model2->driver_licence_photo = $img;
							}
						}
						if(isset($_FILES['pan_photo']['name'])  && trim($_FILES['pan_photo']['name'])!='') {	
							$tmpName = $_FILES['pan_photo']['tmp_name'];
							$str = explode(".",$_FILES['pan_photo']['name']);
							$l=count($str);
							$img =  "driver_pan_photo"."_".time().".".$str[$l-1];
							if(move_uploaded_file($tmpName,$path.$img)){
								$model2->driver_pan_photo = $img;
							}
						}
						$model2->save(false);
						/*$msg = "Dear ".$data['first_name']." ".$data['last_name'].", ";
						$msg .= "Thank you for registration. ";
						$msg .= "Username: ".$data['mobile_number']." ";
						$msg .= "Password: ".$randomNumber;
						$msg .= SMS_SUFFIX;
						$mobile = trim($data['mobile_number']);
						$this->_sendTextSMS($mobile,$msg) ;*/
					
						$user = User::find()->select('id,first_name,last_name,mobile_number,username,password,email,user_image,user_level,user_level_name,status')
						->where(['mobile_number' => $data['mobile_number']])->one();
						if($user['user_image']!='')
						$user['user_image']= BASE_URL .'uploads/profile/'.$user['user_image'];
					    else
						$user['user_image']= '';
						return array('status' => true,'message'=>'Thank you for signup. We will review your account details and back to you.' ,'data'=>$user);
					}
					else {	
						return $this->_errorMessage1();			
					}
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	
	public function actionTruckOperatorRegister() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['first_name']) || trim($data['first_name']) == '' 
				|| !isset($data['last_name']) || trim($data['last_name']) ==''
				//|| !isset($data['email']) || trim($data['email']) ==''
				|| !isset($data['mobile_number']) || trim($data['mobile_number']) ==''
				//|| !isset($data['company_name']) || trim($data['company_name']) ==''
				//|| !isset($data['designation']) || trim($data['designation']) ==''
				|| !isset($data['address_line1']) || trim($data['address_line1']) ==''
				//|| !isset($data['address_line2']) || trim($data['address_line2']) ==''
				//|| !isset($data['pincode']) || trim($data['pincode']) ==''
				|| !isset($data['state']) || trim($data['state']) ==''
				|| !isset($data['district']) || trim($data['district']) ==''
				//|| !isset($data['city']) || trim($data['city']) ==''
				|| !isset($data['area']) || trim($data['area']) ==''
			) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$user = User::find()->select('id, first_name,last_name,mobile_number,username,password,email,user_image,user_level,status')
				->where("mobile_number='".$data['mobile_number']."'")->one();	
				if (!empty($user)) {	
					return array('status'=>false,'message'=>'Account already exist.');
				}
				if (!empty($user) && $user['mobile_number']==$data['mobile_number']) {	
					return array('status'=>false,'message'=>'Phone number already exist.');
				}
				$randomNumber = $this->actionGenerateCode();
				$model = new User;
				$model->first_name=trim($data['first_name']);
				$model->last_name=trim($data['last_name']);
				$model->mobile_number=trim($data['mobile_number']);
				if(isset($data['email']))
				$model->email=trim($data['email']);
				//$model->username=trim($data['username']);
				//$model->password=md5(trim($data['password']));
				$model->password=md5($randomNumber);
				$model->temp_password=$randomNumber;
				$model->user_level='5';
				$model->user_level_name='truck_operator';
				$model->status='1';
				$model->web = isset($data['web'])?$data['web']:'0';
				//$model->varification_code=$randomNumber;
				if($model->save(false)) {
					$id = $model->id;
					$model1 = new Transporter;
					$model1->user_id = $id;
					$model1->user_level = '5';
					$model1->first_name=trim($data['first_name']);
					$model1->last_name=trim($data['last_name']);
					$model1->mobile_number=trim($data['mobile_number']);
					if(isset($data['email']))
					$model1->email=trim($data['email']);
					if(isset($data['driver_operator_type']))
					$model1->driver_operator_type = trim($data['driver_operator_type']);
					$model1->address_line1 = trim($data['address_line1']);
					if(isset($data['address_line2']))
					$model1->address_line2 = trim($data['address_line2']);
					if(isset($data['pincode']))
					$model1->pincode = trim($data['pincode']);
					$model1->state = trim($data['state']);
					if(is_numeric($data['district']))
					{
						$dbConn = Yii::$app->db;
						$command = $dbConn->createCommand("SELECT id,district FROM `district_master` WHERE id='".trim($data['district'])."'");
						$dist = $command->queryOne();
						$data['district'] = $dist['district'];
					}
					$model1->district = trim($data['district']);
					if(isset($data['city']))
					$model1->city = trim($data['city']);
					else
					$model1->city = trim($data['district']);	
					$model1->area = trim($data['area']);
					//if(isset($data['incorporation_certificate_number']))
					//$model1->incorporation_certificate_number = trim($data['incorporation_certificate_number']);
					if(isset($data['aadhaar_number']))
					$model1->aadhaar_number = trim($data['aadhaar_number']);
					if(isset($data['pan_number']))
					$model1->pan_number = trim($data['pan_number']);
					if(isset($data['driver_licence_number']))
					$model1->driver_licence_number = trim($data['driver_licence_number']);
					$path = UPLOAD_DIR_PATH_ . 'uploads/transporter/';
					/*if(isset($_FILES['incorporation_certificate_photo']['name'])  && trim($_FILES['incorporation_certificate_photo']['name'])!='') {	
						$tmpName = $_FILES['incorporation_certificate_photo']['tmp_name'];
						$str = explode(".",$_FILES['incorporation_certificate_photo']['name']);
						$l=count($str);
						$img =  "incorporation_certificate_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->incorporation_certificate_photo = $img;
						}
					}*/
					/*if(isset($_FILES['shop_establishment_certificate_photo']['name'])  && trim($_FILES['shop_establishment_certificate_photo']['name'])!='') {	
						$tmpName = $_FILES['shop_establishment_certificate_photo']['tmp_name'];
						$str = explode(".",$_FILES['shop_establishment_certificate_photo']['name']);
						$l=count($str);
						$img =  "shop_establishment_certificate_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->shop_establishment_certificate_photo = $img;
						}
					}*/
					if(isset($_FILES['pan_photo']['name'])  && trim($_FILES['pan_photo']['name'])!='') {	
						$tmpName = $_FILES['pan_photo']['tmp_name'];
						$str = explode(".",$_FILES['pan_photo']['name']);
						$l=count($str);
						$img =  "pan_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->pan_photo = $img;
						}
					}
					if(isset($_FILES['aadhaar_photo']['name'])  && trim($_FILES['aadhaar_photo']['name'])!='') {	
						$tmpName = $_FILES['aadhaar_photo']['tmp_name'];
						$str = explode(".",$_FILES['aadhaar_photo']['name']);
						$l=count($str);
						$img =  "aadhaar_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->aadhaar_photo = $img;
						}
					}
					if(isset($_FILES['driver_licence_photo']['name'])  && trim($_FILES['driver_licence_photo']['name'])!='') {	
						$tmpName = $_FILES['driver_licence_photo']['tmp_name'];
						$str = explode(".",$_FILES['driver_licence_photo']['name']);
						$l=count($str);
						$img =  "driver_licence_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->driver_licence_photo = $img;
						}
					}
					
					$mobile = trim($data['mobile_number']);
					if($model1->save(false)) {	

						// REGISTER TRUCK OWNER AS A DRIVER ALSO
						$model2 = new TruckDriver;
						$model2->transporter_user_id=$model1->user_id;
						$model2->user_id=$model1->user_id;
						$model2->driver_name=$model1->first_name.' '.$model1->last_name;
						$model2->driver_mobile=$model1->mobile_number;
						$model2->driver_current_address=$model1->address_line1.', '.$model1->address_line2.', '.$model1->district.', '.$model1->area.'-'.$model1->pincode;
						$model2->driver_original_address=$model1->address_line1.', '.$model1->address_line2.', '.$model1->district.', '.$model1->area.'-'.$model1->pincode;
						$model2->driver_aadhaar_number=$model1->aadhaar_number;
						$model2->driver_licence_number=$model1->driver_licence_number;
						$model2->driver_pan_number=$model1->pan_number;
						$path = UPLOAD_DIR_PATH_ . 'uploads/driver/';
						if(isset($_FILES['aadhaar_photo']['name'])  && trim($_FILES['aadhaar_photo']['name'])!='') {	
							$tmpName = $_FILES['aadhaar_photo']['tmp_name'];
							$str = explode(".",$_FILES['aadhaar_photo']['name']);
							$l=count($str);
							$img =  "driver_aadhaar_photo"."_".time().".".$str[$l-1];
							if(move_uploaded_file($tmpName,$path.$img)){
								$model2->driver_aadhaar_photo = $img;
							}
						}
						if(isset($_FILES['driver_licence_photo']['name'])  && trim($_FILES['driver_licence_photo']['name'])!='') {	
							$tmpName = $_FILES['driver_licence_photo']['tmp_name'];
							$str = explode(".",$_FILES['driver_licence_photo']['name']);
							$l=count($str);
							$img =  "driver_licence_photo"."_".time().".".$str[$l-1];
							if(move_uploaded_file($tmpName,$path.$img)){
								$model2->driver_licence_photo = $img;
							}
						}
						if(isset($_FILES['pan_photo']['name'])  && trim($_FILES['pan_photo']['name'])!='') {	
							$tmpName = $_FILES['pan_photo']['tmp_name'];
							$str = explode(".",$_FILES['pan_photo']['name']);
							$l=count($str);
							$img =  "driver_pan_photo"."_".time().".".$str[$l-1];
							if(move_uploaded_file($tmpName,$path.$img)){
								$model2->driver_pan_photo = $img;
							}
						}
						$model2->save(false);	
						/*$msg = "Dear ".$data['first_name']." ".$data['last_name'].", ";
						$msg .= "Thank you for registration. ";
						$msg .= "Username: ".$data['mobile_number']." ";
						$msg .= "Password: ".$randomNumber;
						$msg .= SMS_SUFFIX;
						$mobile = trim($data['mobile_number']);
						$this->_sendTextSMS($mobile,$msg);*/
						$user = User::find()->select('id,first_name,last_name,mobile_number,username,password,email,user_image,user_level,user_level_name,status')
						->where(['mobile_number' => $data['mobile_number']])->one();
						if($user['user_image']!='')
						$user['user_image']= BASE_URL .'uploads/profile/'.$user['user_image'];
					    else
						$user['user_image']= '';
						return array('status' => true,'message'=>'Thank you for signup. We will review your account details and back to you.' ,'data'=>$user);
					}
					else {	
						return $this->_errorMessage1();			
					}
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	
	public function actionLogin() { // Login
			$lng = $this->_getLanguage();
    		\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}	
    		if(!$this->_checkAuth()){ 
    			return $this->_authFailed();
    		}
				$data = \yii::$app->request->post();		
				if( !isset($data['mobile_number']) || trim($data['mobile_number']) == '' 
				|| !isset($data['password']) || trim($data['password']) == '') {
					return $this->_errorMessage1();
				}
				$mobile_number = trim($data['mobile_number']);
				$password = md5($data['password']);
				$user = User::find()->select('id, first_name,last_name,mobile_number,username,password,email,user_image,user_level,user_level_name,verified,status')
				->where("mobile_number='".$mobile_number."' and password='".$password."'")->one();
				
				if (!empty($user) && $user['verified']=='1') {			
					return array('status' => true,'message'=>'Login successfull.', 'data'=>$user);
				}
				elseif (!empty($user) && $user['verified']!='1') {			
					return array('status' => false,'message'=>'Your account is under review for verification.');
				}
				else{	
					return array('status' => false,'message'=>'Incorrect Username or Password.');						
				}
					
	}

	public function actionGetProfile() { 
			$lng = $this->_getLanguage();
    		\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}	
    		if(!$this->_checkAuth()){ 
    			return $this->_authFailed();
    		}
				$data = \yii::$app->request->post();		
				if( !isset($data['user_id']) || trim($data['user_id']) == '') {
					return $this->_errorMessage1();
				}
				$user = User::findOne(trim($data['user_id']));
				if($user['user_level']=='3') {
					$dbConn = Yii::$app->db;
					$command = $dbConn->createCommand("SELECT t2.* FROM `user` t1 INNER JOIN `industry` t2 ON t1.id=t2.user_id 
					WHERE t2.user_id='".trim($data['user_id'])."'");
					$user = $command->queryOne();
				}
				elseif($user['user_level']=='4') {
					$dbConn = Yii::$app->db;
					$command = $dbConn->createCommand("SELECT t2.* FROM `user` t1 INNER JOIN `transporter` t2 ON t1.id=t2.user_id 
					WHERE t2.user_id='".trim($data['user_id'])."'");
					$user = $command->queryOne();
				}
				return array('status' => true,'message'=>'Successfull', 'data'=>$user);	
	}

	public function actionIndustryUpdateProfile() {
			
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['first_name']) || trim($data['first_name']) == '' 
				//|| !isset($data['last_name']) || trim($data['last_name']) ==''
				|| !isset($data['email']) || trim($data['email']) ==''
				|| !isset($data['mobile_number']) || trim($data['mobile_number']) ==''
				|| !isset($data['company_name']) || trim($data['company_name']) ==''
				//|| !isset($data['company_type_id']) || trim($data['company_type_id']) ==''
				|| !isset($data['address_line1']) || trim($data['address_line1']) ==''
				//|| !isset($data['address_line2']) || trim($data['address_line2']) ==''
				|| !isset($data['pincode']) || trim($data['pincode']) ==''
				|| !isset($data['state']) || trim($data['state']) ==''
				|| !isset($data['district']) || trim($data['district']) ==''
				//|| !isset($data['city']) || trim($data['city']) ==''
				|| !isset($data['area']) || trim($data['area']) ==''
			) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$user = User::find()->select('id, first_name,last_name,mobile_number,username,password,email,user_image,user_level,status')
				->where("mobile_number='".$data['mobile_number']."'")->one();	
				if (!empty($user)) {	
					//return array('status'=>false,'message'=>'Account already exist.');
				}
				if (!empty($user) && $user['mobile_number']==$data['mobile_number']) {	
					//return array('status'=>false,'message'=>'Phone number already exist.');
				}
				$randomNumber = $this->actionGenerateCode();
				$model = User::findOne($data['user_id']);
				if(isset($data['first_name']))
				$model->first_name=trim($data['first_name']);
				if(isset($data['last_name']))
				$model->last_name=trim($data['last_name']);
				//$model->mobile_number=trim($data['mobile_number']);
				if(isset($data['email']))
				$model->email=trim($data['email']);
				//$model->password=md5($randomNumber);
				//$model->temp_password=$randomNumber;
				//$model->user_level='3';
				//$model->user_level_name='industry';
				//$model->status='1';
				//$model->varification_code=$randomNumber;
				if($model->save(false)) {
					$id = $model->id;
					$model1 = Industry::find()->where(['user_id' => $data['user_id']])->one();
					$model1->user_id = $id;
					if(isset($data['first_name']))
					$model1->first_name=trim($data['first_name']);
					if(isset($data['last_name']))
					$model1->last_name=trim($data['last_name']);
					//$model1->mobile_number=trim($data['mobile_number']);
					if(isset($data['email']))
					$model1->email=trim($data['email']);
					if(isset($data['company_name']))
					$model1->company_name = trim($data['company_name']);
					if(isset($data['company_type']))
					$model1->company_type = trim($data['company_type']);
					elseif(isset($data['company_type_id']))
					$model1->company_type_id = trim($data['company_type_id']);
					if(isset($data['company_type_id']) && $data['company_type_id'] >0)
					{
						$type =CompanyType::findOne($data['company_type_id']);
						$model1->company_type = $type->company_type;
					}
					$model1->address_line1 = trim($data['address_line1']);
					if(isset($data['address_line2']))
					$model1->address_line2 = trim($data['address_line2']);
					if(isset($data['pincode']))
					$model1->pincode = trim($data['pincode']);
					$model1->state = trim($data['state']);
					$model1->district = trim($data['district']);
					if(isset($data['city']) && $data['city']!='')
					$model1->city = trim($data['city']);
					else
					$model1->city = trim($data['district']);	
					$model1->area = trim($data['area']);
					if(isset($data['landmark']))
					$model1->landmark = trim($data['landmark']);
					//if(isset($data['incorporation_certificate_number']))
					//$model1->incorporation_certificate_number = trim($data['incorporation_certificate_number']);
					//if(isset($data['shop_establishment_certificate_number']))
					//$model1->shop_establishment_certificate_number = trim($data['shop_establishment_certificate_number']);
					//if(isset($data['pan_number']))
					//$model1->pan_number = trim($data['pan_number']);
					if(isset($data['any_government_doc_number']))
					$model1->any_government_doc_number = trim($data['any_government_doc_number']);
					$path = UPLOAD_DIR_PATH_ . 'uploads/industry/';
					if(isset($_FILES['any_government_doc_photo']['name'])  && trim($_FILES['any_government_doc_photo']['name'])!='') {	
						$tmpName = $_FILES['any_government_doc_photo']['tmp_name'];
						$str = explode(".",$_FILES['any_government_doc_photo']['name']);
						$l=count($str);
						$img =  "any_government_doc_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->any_government_doc_photo = $img;
						}
					}
					/*if(isset($_FILES['incorporation_certificate_photo']['name'])  && trim($_FILES['incorporation_certificate_photo']['name'])!='') {	
						$tmpName = $_FILES['incorporation_certificate_photo']['tmp_name'];
						$str = explode(".",$_FILES['incorporation_certificate_photo']['name']);
						$l=count($str);
						$img =  "incorporation_certificate_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->incorporation_certificate_photo = $img;
						}
					}
					if(isset($_FILES['shop_establishment_certificate_photo']['name'])  && trim($_FILES['shop_establishment_certificate_photo']['name'])!='') {	
						$tmpName = $_FILES['shop_establishment_certificate_photo']['tmp_name'];
						$str = explode(".",$_FILES['shop_establishment_certificate_photo']['name']);
						$l=count($str);
						$img =  "shop_establishment_certificate_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->shop_establishment_certificate_photo = $img;
						}
					}
					if(isset($_FILES['pan_photo']['name'])  && trim($_FILES['pan_photo']['name'])!='') {	
						$tmpName = $_FILES['pan_photo']['tmp_name'];
						$str = explode(".",$_FILES['pan_photo']['name']);
						$l=count($str);
						$img =  "pan_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->pan_photo = $img;
						}
					}*/
					/*if(isset($_FILES['aadhaar_photo']['name'])  && trim($_FILES['aadhaar_photo']['name'])!='') {	
						$tmpName = $_FILES['aadhaar_photo']['tmp_name'];
						$str = explode(".",$_FILES['aadhaar_photo']['name']);
						$l=count($str);
						$img =  "aadhaar_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->aadhaar_photo = $img;
						}
					}*/
					$mobile = trim($data['mobile_number']);
					//if($this->_sendTextSMS($mobile,$msg)) {
					if($model1->save(false)) {
						
						$model2 = User::findOne($data['user_id']);
						$model2->completed_profile = '1';
						$model2->save(false);
						
						//$this->_sendTextSMS($mobile,$msg);
						$add = IndustryAddresses::find()->where(['user_id' => $data['user_id']])->one();
						if(empty($add))
						$add = new IndustryAddresses;
						$add->user_id = $id;
						$add->address_title='Unit1';
						$add->address_line1=trim($data['address_line1']);
						if(isset($data['address_line2']))
						$add->address_line2=trim($data['address_line2']);
						if(isset($data['pincode']))
						$add->pincode=trim($data['pincode']);
						$add->state=trim($data['state']);
						$add->district=trim($data['district']);
						if(isset($data['city']) && $data['city']!='')
						$add->city=trim($data['city']);
						else
						$add->city = trim($data['district']);
						$add->area=trim($data['area']);
						if(isset($data['landmark']))
						$add->landmark=trim($data['landmark']);	
						$add->save(false);
						
					
						$user = User::find()->select('id,first_name,last_name,mobile_number,username,password,email,user_image,user_level,user_level_name,status')
						->where(['mobile_number' => $data['mobile_number']])->one();
						if($user['user_image']!='')
						$user['user_image']= BASE_URL .'uploads/profile/'.$user['user_image'];
					    else
						$user['user_image']= '';
						return array('status' => true,'message'=>'Your profile is updated successfully.' ,'data'=>$user);
					}
					else {	
						return $this->_errorMessage1();			
					}
				}
				else {
					return $this->_errorMessage1();
				}	
	}	
		public function actionTransporterUpdateProfile() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['first_name']) || trim($data['first_name']) == '' 
				//|| !isset($data['last_name']) || trim($data['last_name']) ==''
				|| !isset($data['email']) || trim($data['email']) ==''
				|| !isset($data['mobile_number']) || trim($data['mobile_number']) ==''
				|| !isset($data['company_name']) || trim($data['company_name']) ==''
				|| !isset($data['designation']) || trim($data['designation']) ==''
				|| !isset($data['address_line1']) || trim($data['address_line1']) ==''
				//|| !isset($data['address_line2']) || trim($data['address_line2']) ==''
				|| !isset($data['pincode']) || trim($data['pincode']) ==''
				|| !isset($data['state']) || trim($data['state']) ==''
				|| !isset($data['district']) || trim($data['district']) ==''
				//|| !isset($data['city']) || trim($data['city']) ==''
				|| !isset($data['area']) || trim($data['area']) ==''
			) {
					//return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$user = User::find()->select('id, first_name,last_name,mobile_number,username,password,email,user_image,user_level,status')
				->where("mobile_number='".$data['mobile_number']."'")->one();	
				if (!empty($user)) {	
					//return array('status'=>false,'message'=>'Account already exist.');
				}
				if (!empty($user) && $user['mobile_number']==$data['mobile_number']) {	
					//return array('status'=>false,'message'=>'Phone number already exist.');
				}
				//$randomNumber = $this->actionGenerateCode();
				//$model = new User;
				$model = User::findOne($data['user_id']);
				if(isset($data['first_name']))
				$model->first_name=trim($data['first_name']);
				if(isset($data['last_name']))
				$model->last_name=trim($data['last_name']);
				//$model->mobile_number=trim($data['mobile_number']);
				if(isset($data['email']))
				$model->email=trim($data['email']);
				//$model->password=md5($randomNumber);
				//$model->temp_password=$randomNumber;
				//$model->user_level='4';
				//$model->user_level_name='transporter';
				//$model->status='1';
				if($model->save(false)) {
					$id = $model->id;
					//$model1 = new Transporter;
					$model1 = Transporter::find()->where(['user_id' => $data['user_id']])->one();
					//$model1->user_id = $id;
					//$model1->user_level = '4';
					if(isset($data['first_name']))
					$model1->first_name=trim($data['first_name']);
					if(isset($data['last_name']))
					$model1->last_name=trim($data['last_name']);
					//$model1->mobile_number=trim($data['mobile_number']);
					if(isset($data['email']))
					$model1->email=trim($data['email']);
					if(isset($data['company_name']))
					$model1->company_name = trim($data['company_name']);
					if(isset($data['designation']))
					$model1->designation = trim($data['designation']);
					$model1->address_line1 = trim($data['address_line1']);
					if(isset($data['address_line2']))
					$model1->address_line2 = trim($data['address_line2']);
					if(isset($data['pincode']))
					$model1->pincode = trim($data['pincode']);
					$model1->state = trim($data['state']);
					$model1->district = trim($data['district']);
					if(isset($data['city']) && $data['city']!='')
					$model1->city = trim($data['city']);
					else
					$model1->city = trim($data['district']);
					$model1->area = trim($data['area']);
					//if(isset($data['incorporation_certificate_number']))
					//$model1->incorporation_certificate_number = trim($data['incorporation_certificate_number']);
					//if(isset($data['shop_establishment_certificate_number']))
					//$model1->shop_establishment_certificate_number = trim($data['shop_establishment_certificate_number']);
					//if(isset($data['pan_number']))
					//$model1->pan_number = trim($data['pan_number']);
					if(isset($data['rcc_number']))
					$model1->rcc_number = trim($data['rcc_number']);
					$path = UPLOAD_DIR_PATH_ . 'uploads/transporter/';
					if(isset($_FILES['rcc_photo']['name'])  && trim($_FILES['rcc_photo']['name'])!='') {	
						$tmpName = $_FILES['rcc_photo']['tmp_name'];
						$str = explode(".",$_FILES['rcc_photo']['name']);
						$l=count($str);
						$img =  "rcc_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->rcc_photo = $img;
						}
					}
					/*if(isset($_FILES['incorporation_certificate_photo']['name'])  && trim($_FILES['incorporation_certificate_photo']['name'])!='') {	
						$tmpName = $_FILES['incorporation_certificate_photo']['tmp_name'];
						$str = explode(".",$_FILES['incorporation_certificate_photo']['name']);
						$l=count($str);
						$img =  "incorporation_certificate_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->incorporation_certificate_photo = $img;
						}
					}
					if(isset($_FILES['shop_establishment_certificate_photo']['name'])  && trim($_FILES['shop_establishment_certificate_photo']['name'])!='') {	
						$tmpName = $_FILES['shop_establishment_certificate_photo']['tmp_name'];
						$str = explode(".",$_FILES['shop_establishment_certificate_photo']['name']);
						$l=count($str);
						$img =  "shop_establishment_certificate_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->shop_establishment_certificate_photo = $img;
						}
					}
					if(isset($_FILES['pan_photo']['name'])  && trim($_FILES['pan_photo']['name'])!='') {	
						$tmpName = $_FILES['pan_photo']['tmp_name'];
						$str = explode(".",$_FILES['pan_photo']['name']);
						$l=count($str);
						$img =  "pan_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->pan_photo = $img;
						}
					}*/
					/*if(isset($_FILES['aadhaar_photo']['name'])  && trim($_FILES['aadhaar_photo']['name'])!='') {	
						$tmpName = $_FILES['aadhaar_photo']['tmp_name'];
						$str = explode(".",$_FILES['aadhaar_photo']['name']);
						$l=count($str);
						$img =  "aadhaar_photo"."_".time().".".$str[$l-1];
						if(move_uploaded_file($tmpName,$path.$img)){
							$model1->aadhaar_photo = $img;
						}
					}*/
					$mobile = trim($data['mobile_number']);
					if($model1->save(false)) {	
						$model2 = User::findOne($data['user_id']);
						$model2->completed_profile = '1';
						$model2->save(false);
						/*$msg = "Dear ".$data['first_name']." ".$data['last_name'].", ";
						$msg .= "Thank you for registration. ";
						$msg .= "Username: ".$data['mobile_number']." ";
						$msg .= "Password: ".$randomNumber;
						$msg .= SMS_SUFFIX;
						$mobile = trim($data['mobile_number']);
						$this->_sendTextSMS($mobile,$msg) ;*/
					
						$user = User::find()->select('id,first_name,last_name,mobile_number,username,password,email,user_image,user_level,user_level_name,status')
						->where(['mobile_number' => $data['mobile_number']])->one();
						if($user['user_image']!='')
						$user['user_image']= BASE_URL .'uploads/profile/'.$user['user_image'];
					    else
						$user['user_image']= '';
						return array('status' => true,'message'=>'Your profile is updated successfully.' ,'data'=>$user);
					}
					else {	
						return $this->_errorMessage1();			
					}
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	public function actionForgotPassword() {	

		//$lng = $this->_getLanguage();			
		\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        //if($this->_checkAuth()){
		if(true){	
			$data = \yii::$app->request->post();		
			
			if( isset($data['mobile_number']) && trim($data['mobile_number']) !='' ) {				
				$mobile_number = trim($data['mobile_number']);
				$user = User::find()->select('id, mobile_number,temp_password,first_name,last_name')->where(['mobile_number' => $mobile_number])->one();
				if(!empty($user)){
					$msg = "Dear ".$user['first_name']." ".$user['last_name'].". ";
					//$msg .= "Thank you for registration. ";
					$msg .= "Username: ".$user['mobile_number']." ";
					$msg .= "Password: ".$user['temp_password'];
					$msg .= SMS_SUFFIX;
					$mobile = $user['mobile_number'];
					$this->_sendTextSMS($mobile,$msg);
					return array('status' => true,'message'=>'Password has been sent to your mobile number.');
				} else {
					return array('status'=>false,'message'=>'Please provide valid Mobile number.');
				}
			}
			else {
				return array('status'=>false,'message'=>'Mobile Number is Required!');	
			}
		}
		else {
			//return $this->_authFailed();
		}
	}
	public function actionStateList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			$where = "";
			if(isset($data['type']) && $data['type']=='neighbouring')
			$where = "(state='Gujarat' OR state='Maharashtra') AND ";
			$dbConn = Yii::$app->db;
			$command = $dbConn->createCommand("SELECT id,state FROM `state_master` t1 WHERE $where status='Active'");
			$result = $command->queryAll();
			if(!empty($result)) {
				return array('status' => true,'message'=>'Success','data'=>$result);
			}
			else {
				return array('status' => true,'message'=>'No Data Found','data'=>$result);
			}	
	}
	
	public function actionDistrictList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['state_id']) || trim($data['state_id']) == '' ) {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			}
			$where = "";
			//if(isset($data['type']) && $data['type']=='neighbouring')
			//$where = "(district='Valsad' OR district='Palghar') AND ";
			$dbConn = Yii::$app->db;
			$command = $dbConn->createCommand("SELECT id,district FROM `district_master` t1 WHERE 
			state_id='".trim($data['state_id'])."' AND status='Active'");
			$result = $command->queryAll();
			if(!empty($result)) {
				return array('status' => true,'message'=>'Success','data'=>$result);
			}
			else {
				return array('status' => true,'message'=>'No Data Found','data'=>$result);
			}	
	}
	public function actionDistrictAreaList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			$where = "";
			if(isset($data['district_id']) && $data['district_id']!='')	
			$where .= "district_id='".trim($data['district_id'])."' AND";
			$dbConn = Yii::$app->db;
			$command = $dbConn->createCommand("SELECT area,pincode FROM `area_master` t1 WHERE 
			$where status='Active'");
			$result = $command->queryAll();
			if(!empty($result)) {
				return array('status' => true,'message'=>'Success','data'=>$result);
			}
			else {
				return array('status' => true,'message'=>'No Data Found','data'=>$result);
			}	
	}
	
	// Not Used in Next Version
	public function actionCityList() { // No Longer used

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			$where = "";
			//if(isset($data['type']) && $data['type']=='neighbouring')
			//$where = "(city='Valsad' OR city='Vapi' city='Ankleshwar' OR city='Palghar' OR city='Dhule') AND ";
			if(isset($data['district_id']) && $data['district_id']!='')	
			$where .= "district_id='".trim($data['district_id'])."' AND";
			$dbConn = Yii::$app->db;
			$command = $dbConn->createCommand("SELECT city FROM `city_master` t1 WHERE 
			$where status='Active'");
			$result = $command->queryAll();
			if(!empty($result)) {
				return array('status' => true,'message'=>'Success','data'=>$result);
			}
			else {
				return array('status' => true,'message'=>'No Data Found','data'=>$result);
			}	
	}
	
	/*public function actionGetCityMaster() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		if(!$this->_checkAuth()){ 
    		return $this->_authFailed();
    	}
		$data = \yii::$app->request->post();		
		$result = CityMaster::find()->select(['id','city'])
		->where(['status' => 'Active'])->all();			
		return array('status' => true,'message'=>'Success' ,'data'=>$result);
	}
	public function actionGetMaterialTypeMaster() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		if(!$this->_checkAuth()){ 
    		return $this->_authFailed();
    	}
		$data = \yii::$app->request->post();		
		$result = MaterialType::find()->select(['id','material'])
		->where(['status' => 'Active'])->all();			
		return array('status' => true,'message'=>'Success' ,'data'=>$result);
	}
	public function actionGetWeightMaster() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		if(!$this->_checkAuth()){ 
    		return $this->_authFailed();
    	}
		$data = \yii::$app->request->post();		
		$result = WeightMaster::find()->select(['id','weight'])
		->where(['status' => 'Active'])->all();			
		return array('status' => true,'message'=>'Success' ,'data'=>$result);
	}
	public function actionGetTruckTypeMaster() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		if(!$this->_checkAuth()){ 
    		return $this->_authFailed();
    	}
		$data = \yii::$app->request->post();		
		$result = TruckType::find()->select(['id','truck'])
		->where(['status' => 'Active'])->all();			
		return array('status' => true,'message'=>'Success' ,'data'=>$result);
	}*/
	
	
// ===========================================INDUSTRY API==================================================
	public function actionIndustryAddAddress() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['address_title']) || trim($data['address_title']) ==''
				|| !isset($data['address_line1']) || trim($data['address_line1']) ==''
				//|| !isset($data['address_line2']) || trim($data['address_line2']) ==''
				//|| !isset($data['pincode']) || trim($data['pincode']) ==''
				//|| !isset($data['state']) || trim($data['state']) ==''
				|| !isset($data['district']) || trim($data['district']) ==''
				//|| !isset($data['city']) || trim($data['city']) ==''
				|| !isset($data['area']) || trim($data['area']) ==''
				|| !isset($data['landmark']) || trim($data['landmark']) ==''
			) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = new IndustryAddresses;
				$model->user_id=trim($data['user_id']);
				$model->address_title=trim($data['address_title']);
				$model->address_line1=trim($data['address_line1']);
				if(isset($data['address_line2']))
				$model->address_line2=trim($data['address_line2']);
				if(isset($data['pincode']))
				$model->pincode=trim($data['pincode']);
				$model->state=trim($data['state']);
				$model->district=trim($data['district']);
				if(isset($data['city']) && $data['city']!='')
				$model->city=trim($data['city']); // Not used now
				else
				$model->city=trim($data['district']); 	
				$model->area=trim($data['area']);
				$model->landmark=trim($data['landmark']);
				if($model->save(false)) {
					return array('status' => true,'message'=>'New Address Added successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	public function actionIndustryAddressList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '') {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			 }
			  $limit = '';
			  $page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
			  if(isset($data['page']) && $data['page']>0) {
				$limit = $this->_pageLimit($page);
			  }
				$result=[];
				//$result = IndustryAddresses::find()->where(['user_id' => $data['user_id'], 'status' => 'Active'])
				//->orderBy(['id' => SORT_DESC])->all();

				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.* FROM `industry_addresses` t1 WHERE t1.user_id='".$data['user_id']."' AND t1.status='Active' ORDER BY t1.id DESC $limit");
				$result = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `industry_addresses` t1 WHERE t1.user_id='".$data['user_id']."' AND t1.status='Active'");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Address Found','data'=>$result);
				}	
	}
	public function actionIndustryEditAddress() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['address_title']) || trim($data['address_title']) ==''
				|| !isset($data['address_line1']) || trim($data['address_line1']) ==''
				//|| !isset($data['address_line2']) || trim($data['address_line2']) ==''
				//|| !isset($data['pincode']) || trim($data['pincode']) ==''
				//|| !isset($data['state']) || trim($data['state']) ==''
				|| !isset($data['district']) || trim($data['district']) ==''
				//|| !isset($data['city']) || trim($data['city']) ==''
				|| !isset($data['area']) || trim($data['area']) ==''
				|| !isset($data['id']) || trim($data['id']) == '' 
			) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = IndustryAddresses::findOne(trim($data['id']));
				$model->user_id=trim($data['user_id']);
				$model->address_title=trim($data['address_title']);
				$model->address_line1=trim($data['address_line1']);
				if(isset($data['address_line2']))
				$model->address_line2=trim($data['address_line2']);
				if(isset($data['pincode']))
				$model->pincode=trim($data['pincode']);
				if(isset($data['state']) && $data['state']!='')
				$model->state=trim($data['state']);
				$model->district=trim($data['district']);
				if(isset($data['city']) && $data['city']!='')
				$model->city=trim($data['city']);
				else
				$model->city=trim($data['district']);	
				$model->area=trim($data['area']);
				$model->landmark=trim($data['landmark']);
				if($model->save(false)) {
					return array('status' => true,'message'=>'Address updated successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	public function actionIndustryDeleteAddress() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['id']) || trim($data['id']) == '' 
			) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = IndustryAddresses::findOne(trim($data['id']));
				$model->status= 'Deleted';
				if($model->save(false)) {
					return array('status' => true,'message'=>'Your address is deleted successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	
    // actionGetDropdownMasters
	public function actionGetDropdownMasters() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
		if(!$this->_checkAuth()){ 
    		return $this->_authFailed();
    	}
		$data = \yii::$app->request->post();
		$result['Cities'] = CityMaster::find()->select(['id','city'])
		->where(['status' => 'Active'])->all();
		$result['MaterialTypes'] = MaterialType::find()->select(['id','material'])
		->where(['status' => 'Active'])->all();
		$result['Weight'] = WeightMaster::find()->select(['id','weight'])
		->where(['status' => 'Active'])->all();	
		$result['TrukTypes'] = TruckType::find()->select(['id','truck'])
		->where(['status' => 'Active'])->all();
		return array('status' => true,'message'=>'Success' ,'data'=>$result);
	}
	
	// actionIndustryPostLoad
	public function actionIndustryPostLoad() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
				//|| !isset($data['load_type']) || trim($data['load_type']) ==''
				|| !isset($data['scheduling_type']) || trim($data['scheduling_type']) ==''
				//|| !isset($data['source_address_id']) || trim($data['source_address_id']) ==''
				|| !isset($data['destination_address_line1']) || trim($data['destination_address_line1']) ==''
				//|| !isset($data['destination_address_line2']) || trim($data['destination_address_line2']) ==''
				//|| !isset($data['destination_pincode']) || trim($data['destination_pincode']) ==''
				//|| !isset($data['destination_state']) || trim($data['destination_state']) ==''
				//|| !isset($data['destination_district']) || trim($data['destination_district']) ==''
				//|| !isset($data['destination_city']) || trim($data['destination_city']) ==''
				|| !isset($data['destination_area']) || trim($data['destination_area']) ==''
				|| !isset($data['destination_landmark']) || trim($data['destination_landmark']) ==''
				|| !isset($data['material_type']) || trim($data['material_type']) ==''
				|| !isset($data['weight']) || trim($data['weight']) ==''
				|| !isset($data['truck_type']) || trim($data['truck_type']) ==''
				|| !isset($data['no_of_trucks']) || trim($data['no_of_trucks']) ==''
				//|| !isset($data['bid_closing_date']) || trim($data['bid_closing_date']) ==''
				//|| !isset($data['bid_closing_time']) || trim($data['bid_closing_time']) ==''
				//|| !isset($data['reporting_date']) || trim($data['reporting_date']) ==''
				|| !isset($data['reporting_time']) || trim($data['reporting_time']) ==''
				//|| !isset($data['max_bid_amount']) || trim($data['max_bid_amount']) ==''
			) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = new Loads;
				$model->industry_user_id=trim($data['user_id']);
				$user =User::findOne($data['user_id']);
				$model->full_name = $user->first_name.' '.$user->last_name;
				$model->mobile_number = $user->mobile_number;
				$industry =Industry::find()->select(['id','company_name'])
				->where(['user_id' => $data['user_id']])->one();
				$model->industry_id = $industry->id;
				$model->company_name = $industry->company_name;	
				$add =IndustryAddresses::findOne($data['source_address_id']);
				$model->source_address_id=trim($data['source_address_id']);
				$model->source_address_line1 = $add->address_line1;
				$model->source_address_line2 = $add->address_line2;
				$model->source_pincode = $add->pincode;
				$model->source_state = $add->state;
				$model->source_district = $add->district;
				if(!empty($add->city))
				$model->source_city = $add->city;
				else
				$model->source_city = $add->district;	
				$model->source_area = $add->area;
				$model->source_landmark = $add->landmark;
				$model->destination_address_line1 = trim($data['destination_address_line1']);
				if(isset($data['destination_address_line2']))
				$model->destination_address_line2 = trim($data['destination_address_line2']);
				$model->destination_pincode = trim($data['destination_pincode']);
				if(isset($data['destination_state']))
				$model->destination_state = trim($data['destination_state']);
				if(isset($data['destination_district']))
				$model->destination_district = trim($data['destination_district']);
				if(isset($data['destination_city']) && $data['destination_city']!='')
				$model->destination_city = trim($data['destination_city']);
				else
				$model->destination_city = trim($data['destination_district']);	
				$model->destination_area = trim($data['destination_area']);
				$model->destination_landmark = trim($data['destination_landmark']);
				if(isset($data['load_address_type']))
				$model->load_address_type=trim($data['load_address_type']);
				if(isset($data['load_type']))
				$model->load_type=trim($data['load_type']);
				$model->scheduling_type=trim($data['scheduling_type']);
				$model->material_type=trim($data['material_type']);
				$model->weight=trim($data['weight']);
				$model->truck_type=trim($data['truck_type']);
				$model->no_of_trucks=trim($data['no_of_trucks']);
				if(isset($data['reporting_date']))
				$model->reporting_date=date('Y-m-d',strtotime($data['reporting_date']));
				elseif(isset($data['selected_date']))
				$model->reporting_date=date('Y-m-d',strtotime($data['selected_date']));
				$model->reporting_time=trim($data['reporting_time']);
				if(isset($data['bid_closing_date']))
				$model->bid_closing_date=date('Y-m-d',strtotime($data['bid_closing_date']));
				if(isset($data['bid_closing_time']))
				$model->bid_closing_time=trim($data['bid_closing_time']);
				if(isset($data['quotation_amount_type']))
				$model->quotation_amount_type=trim($data['quotation_amount_type']);
				if(isset($data['estimated_quotation_amount']))
				$model->estimated_quotation_amount=trim($data['estimated_quotation_amount']);
				if(isset($data['total_amount']) && $data['total_amount']>0)
				$model->total_amount = trim($data['total_amount']);
				else
				$model->total_amount = trim($data['estimated_quotation_amount']);	
				if(isset($data['description']))
				$model->description=trim($data['description']);
				if(isset($data['share_to_preferred']))
				$model->share_to_preferred=trim($data['share_to_preferred']);
				if($model->save(false)) {
					$id = $model->id;
					$model = Loads::findOne($id);
					$model->booking_id = 'LD'.$id;
					$model->save(false);
					return array('status' => true,'message' => 'Your Post is submitted successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	
	public function actionIndustryEditPostLoad() {

    	\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['id']) || trim($data['id']) ==''
				|| !isset($data['bid_closing_date']) || trim($data['bid_closing_date']) ==''
				|| !isset($data['bid_closing_time']) || trim($data['bid_closing_time']) ==''
			) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = Loads::findOne(trim($data['id']));
				if($model->user_id != trim($data['user_id'])) {
					return array('status'=>false,'message'=>'Unauthorise Access.');
				}
				if($model->bid_closing_date < date('Y-m-d')) {
					return array('status'=>false,'message'=>'Your Quotation Date is Expired.');
				}
				$model->bid_closing_date=date('Y-m-d',strtotime($data['bid_closing_date']));
				$model->bid_closing_time=trim($data['bid_closing_time']);
				if($model->save(false)) {
					return array('status' => true,'message'=>'Quotation Closing Date & Time Extended.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	// actionIndustryPostedLoadList
	public function actionIndustryPostedLoadList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '') {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			 }
			  $limit = '';
			  $page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
			  if(isset($data['page']) && $data['page']>0) {
				$limit = $this->_pageLimit($page);
			  }
			  
				$result=[];
				//$loads = Loads::find()->where(['industry_user_id' => $data['user_id'], 'assigned_status' => 'Open'])
				//->orderBy(['id' => SORT_DESC])->all();
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.* FROM `loads` t1 WHERE t1.industry_user_id='".$data['user_id']."' AND t1.assigned_status='Open' ORDER BY t1.id DESC $limit");
				$loads = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `loads` t1 WHERE t1.industry_user_id='".$data['user_id']."' AND t1.assigned_status='Open'");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);
				foreach($loads as $ld)
				{
					$quotations = Quotation::find()->select(['bid_amount'])->where(['load_id' => $ld['id']])
					->orderBy(['bid_amount' => SORT_ASC])->all();
					$ld['quotations'] = $quotations;
					$result[] = $ld;
				}
				
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	public function actionIndustryPostedLoadsHistory() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '') {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			 }

			  $limit = '';
			  $page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
			  if(isset($data['page']) && $data['page']>0) {
				$limit = $this->_pageLimit($page);
			  }

				$result=[];
				/*$loads = Loads::find()->where(['industry_user_id' => $data['user_id'], 'assigned_status' => 'Assigned'])
				->orderBy(['id' => SORT_DESC])->all();*/
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.* FROM `loads` t1 WHERE t1.industry_user_id='".$data['user_id']."' AND t1.assigned_status='Assigned' ORDER BY t1.id DESC $limit");
				$loads = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `loads` t1 WHERE t1.industry_user_id='".$data['user_id']."' AND t1.assigned_status='Assigned'");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);
				foreach($loads as $ld)
				{
					$quotations = Quotation::find()->select(['id','bid_amount'])->where(['load_id' => $ld['id'],'assigned_to_me' => '1'])
					->orderBy(['bid_amount' => SORT_ASC])->all();
					$ld['quotations'] = $quotations;
					$result[] = $ld;
				}
				
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	public function actionIndustryCancelLoad() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if(!isset($data['user_id']) || trim($data['user_id']) == ''
				|| !isset($data['load_id']) || trim($data['load_id']) == '') {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			 }
				$load = Loads::find()->where(['id' => $data['load_id'], 'industry_user_id' => $data['user_id'], 'assigned_status' => 'Open'])
				->orderBy(['id' => SORT_DESC])->one();
				if(!empty($load)) {
					$model = Loads::findOne($data['load_id']);
					$model->assigned_status = 'Cancelled';
					$model->rejected_at = date('Y-m-d H:i:s');
					if($model->save(false)) {
						return array('status' => true,'message'=>'Your post has been cancelled successfully.');
					}
					else {
						return $this->_errorMessage1();
					}
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	public function actionIndustryRejectLoad() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if(!isset($data['user_id']) || trim($data['user_id']) == ''
				|| !isset($data['load_id']) || trim($data['load_id']) == ''
				|| !isset($data['reject_reason']) || trim($data['reject_reason']) == '') {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			 }
				$load = Loads::find()->where(['id' => $data['load_id'], 'industry_user_id' => $data['user_id'], 'assigned_status' => 'Assigned'])
				->orderBy(['id' => SORT_DESC])->one();
				if(!empty($load)) {
					$model = Loads::findOne($data['load_id']);
					$model->assigned_status = 'Rejected';
					$model->rejected_at = date('Y-m-d H:i:s');
					$model->reject_reason = trim($data['reject_reason']);
					$model->rejected_from = 'Industry';
					$model->rejected_by = trim($data['user_id']);
					if($model->save(false)) {
					$id = $model->id;
					
$sql = "INSERT INTO loads (ref_rejected_load_id, booking_id, industry_user_id, full_name, mobile_number, industry_id, load_address_type, company_name, 
source_address_id, source_address_line1, source_address_line2, source_pincode, source_state, source_district, 
source_city, source_area, source_landmark, destination_address_line1, destination_address_line2, destination_pincode,
destination_state, destination_district, destination_city, destination_area, destination_landmark, load_type,
scheduling_type, material_type, weight, truck_type, no_of_trucks, bid_closing_date, bid_closing_time, 
reporting_date, reporting_time, estimated_quotation_amount, description, share_to_preferred, assigned_status,
part_load_quotation_date, created_at, created_by)
SELECT '".$data['load_id']."', '', industry_user_id, full_name, mobile_number, industry_id, load_address_type, company_name, 
source_address_id, source_address_line1, source_address_line2, source_pincode, source_state, source_district, 
source_city, source_area, source_landmark, destination_address_line1, destination_address_line2, destination_pincode,
destination_state, destination_district, destination_city, destination_area, destination_landmark, load_type,
scheduling_type, material_type, weight, truck_type, no_of_trucks, bid_closing_date, bid_closing_time, 
reporting_date, reporting_time, estimated_quotation_amount, description, share_to_preferred, 'Open',
part_load_quotation_date, NOW(), '".$data['user_id']."' FROM loads WHERE id=".$data['load_id'];
						$dbConn = Yii::$app->db;
						/*$result = $dbConn->createCommand($sql)->execute();
						$id = $dbConn->getLastInsertID();
						$booking_id = 'LD'.$id;
						$model1 = Loads::findOne($id);
						$model1->booking_id = $booking_id;
						$model1->save(false);*/
						// START NOTIFICATION
						$title = 'Industry rejected the post. Rejected UniqueID #'.$model->booking_id;
				        $desc = 'Industry rejected the post. Rejected UniqueID #'.$model->booking_id;
				        $type = 'tr_reject_post';
						//$load =Loads::findOne($data['load_id']);
				        $sender_id = $data['user_id'];
						$receiver_id = $model->assigned_user_id;
				        $this->_pushNotifification($title,$desc,$type,$sender_id,$receiver_id);
						// END NOTIFICATION
						return array('status' => true,'message'=>'Your post has been rejected successfully.');
					}
					else {
						return $this->_errorMessage1();
					}
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	public function actionTransporterRejectLoad() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if(!isset($data['user_id']) || trim($data['user_id']) == ''
				|| !isset($data['load_id']) || trim($data['load_id']) == ''
				|| !isset($data['reject_reason']) || trim($data['reject_reason']) == '') {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			 }
				$load = Loads::find()->where(['id' => $data['load_id'], 'assigned_user_id' => $data['user_id'], 'assigned_status' => 'Assigned'])
				->orderBy(['id' => SORT_DESC])->one();
				if(!empty($load)) {
					$model = Loads::findOne($data['load_id']);
					$model->assigned_status = 'Rejected';
					$model->rejected_at = date('Y-m-d H:i:s');
					$model->reject_reason = trim($data['reject_reason']);
					$model->rejected_from = 'Transporter';
					$model->rejected_by = trim($data['user_id']);
					if($model->save(false)) {
					$id = $model->id;	
$sql = "INSERT INTO loads (ref_rejected_load_id, booking_id, industry_user_id, full_name, mobile_number, industry_id, load_address_type, company_name, 
source_address_id, source_address_line1, source_address_line2, source_pincode, source_state, source_district, 
source_city, source_area, source_landmark, destination_address_line1, destination_address_line2, destination_pincode,
destination_state, destination_district, destination_city, destination_area, destination_landmark, load_type,
scheduling_type, material_type, weight, truck_type, no_of_trucks, bid_closing_date, bid_closing_time, 
reporting_date, reporting_time, estimated_quotation_amount, description, share_to_preferred, assigned_status,
part_load_quotation_date, created_at, created_by)
SELECT '".$data['load_id']."', '', industry_user_id, full_name, mobile_number, industry_id, load_address_type, company_name, 
source_address_id, source_address_line1, source_address_line2, source_pincode, source_state, source_district, 
source_city, source_area, source_landmark, destination_address_line1, destination_address_line2, destination_pincode,
destination_state, destination_district, destination_city, destination_area, destination_landmark, load_type,
scheduling_type, material_type, weight, truck_type, no_of_trucks, bid_closing_date, bid_closing_time, 
reporting_date, reporting_time, estimated_quotation_amount, description, share_to_preferred, 'Open',
part_load_quotation_date, NOW(), '".$data['user_id']."' FROM loads WHERE id=".$data['load_id'];
						$dbConn = Yii::$app->db;
						$result = $dbConn->createCommand($sql)->execute();
						$id = $dbConn->getLastInsertID();
						$booking_id = 'LD'.$id;
						$model1 = Loads::findOne($id);
						$model1->booking_id = $booking_id;
						$model1->save(false);
							
						$sql = $dbConn->createCommand("UPDATE `quotation` SET load_id='$id' WHERE load_id='".$data['load_id']."' and assigned_to_me='0'");	
						$result = $dbConn->createCommand($sql)->execute();
						$sql = $dbConn->createCommand("UPDATE `quotation_truck` SET load_id='$id' WHERE load_id='".$data['load_id']."' and quotation_id NOT IN (SELECT id FROM `quotation` WHERE load_id='".$data['load_id']."')");	
						$result = $dbConn->createCommand($sql)->execute();
						
						// START NOTIFICATION
						//$load =Loads::findOne($data['load_id']);
						$title = 'Transporter rejected your assigned post. Rejected UniqueID #'.$model->booking_id .' New UniqueID #'.$booking_id;
				        $desc = 'Transporter rejected your assigned post. Rejected UniqueID #'.$model->booking_id .' New UniqueID #'.$booking_id;
				        $type = 'in_reject_post';
						$sender_id = $data['user_id'];
						$receiver_id = $model->industry_user_id;
				        $this->_pushNotifification($title,$desc,$type,$sender_id,$receiver_id);
						// END NOTIFICATION
						return array('status' => true,'message'=>'Your post has been rejected successfully.');
					}
					else {
						return $this->_errorMessage1();
					}
				}
				else {
					return $this->_errorMessage1();
				}
	}
	// actionLoadQuotationList
	public function actionLoadQuotationList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == ''
				|| !isset($data['id']) || trim($data['id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			   
			  $limit = '';
			  $page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
			  if(isset($data['page']) && $data['page']>0) {
				$limit = $this->_pageLimit($page);
			  }
			  
				$result="";
				$load = Loads::find()->where(['id' => $data['id']])->orderBy(['id' => SORT_DESC])->one();
				$quotations = Quotation::find()->select(['*'])->where(['load_id' => $load['id']])
				->orderBy(['bid_amount' => SORT_ASC])->all();
				$load['quotations'] = $quotations;
				$result = $load;
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Quotation Found','data'=>$result);
				}	
	}
	// actionLoadQuotationDetail
	public function actionLoadQuotationDetails() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['id']) || trim($data['id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$result="";
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t1.bid_amount,t1.part_load_quotation_date,t1.truck_id,t1.full_name,
				t1.mobile_number,t1.company_name,t3.email,t3.address_line1,t3.address_line2,t3.pincode,
				t3.state,t3.district,t3.city,t3.area,t4.booking_id,t4.scheduling_type,t4.load_type,t4.source_city,t4.destination_city,
				t4.material_type,t4.weight,t4.truck_type,t4.no_of_trucks,t4.reporting_date,t4.reporting_time,
				t4.source_address_line1,t4.source_address_line2,t4.source_pincode,t4.source_state,
				t4.source_district,t4.source_city,t4.source_area,t4.source_landmark,
				t4.destination_address_line1,t4.destination_address_line2,t4.destination_pincode,t4.destination_state,
				t4.destination_district,t4.destination_city,t4.destination_area,t4.destination_landmark,
				t4.bid_closing_date,t4.bid_closing_time,t4.estimated_quotation_amount,
				t4.description FROM `quotation` t1 INNER JOIN `user` t2 ON t1.user_id=t2.id 
				INNER JOIN `transporter` t3 ON t1.user_id=t3.user_id 
				INNER JOIN `loads` t4 ON t1.load_id=t4.id WHERE t1.id='".trim($data['id'])."'");
				$bidder = $command->queryOne();
					
				$command = $dbConn->createCommand("SELECT t1.* FROM `truck` t1 	WHERE t1.id='".trim($bidder['truck_id'])."'");
				$truck = $command->queryOne(); // Temporary then Removed when "trucks" is working
				$command = $dbConn->createCommand("SELECT t1.* FROM `quotation_truck` t1 
				WHERE t1.quotation_id='".trim($data['id'])."'");
				$trucks = $command->queryAll();
				// ONLY FOR INDUSTRY SIDE USE - CHECK PREFERRED Transporter/Driver
				$prefer = PreferredList::find()->where(['industry_user_id' => $data['user_id'], 
				'preferred_user_id' => $truck['user_id'], 'status' => 'Active'])->one();
				if(!empty($prefer))
				$preferred = '1';
				else
				$preferred = '0';	
				// ONLY FOR INDUSTRY SIDE USE - CHECK PREFERRED Transporter/Truck OPERATOR
				$truck['preferred_user'] = $preferred;
				//$trucks['preferred_user'] = $preferred;
				$result = array('bidder' => $bidder, 'truck' => $truck, 'trucks' => $trucks);
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	//actionIndustryAssignQuotation
	public function actionIndustryAssignQuotation() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			  if( !isset($data['user_id']) || trim($data['user_id']) ==''
				  || !isset($data['id']) || trim($data['id']) == '') {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = Quotation::findOne($data['id']);
				$model->assigned_to_me = '1';
				if($model->save(false)) {
					Quotation::updateAll(['status' => 'Assigned'], 'load_id="'.$model->load_id.'"');
					//$industry = Industry::find()->where(['user_id' => $model->user_id])->one();
					$load = Loads::findOne($model->load_id);
					$load->assigned_status = 'Assigned';
					$load->assigned_at = date('Y-m-d H:i:s');
					$load->assigned_user_id = $model->user_id;
					$load->assigned_full_name = $model->full_name;
					$load->assigned_mobile_number = $model->mobile_number;
					$load->assigned_company_name = $model->company_name;
					$load->assigned_bid_amount = $model->bid_amount;
					$load->assigned_truck_id = $model->truck_id;
					$load->save(false);
					// START NOTIFICATION
						$title = 'Your quotation has been approved. UniqueID #'.$load->booking_id;
				        $desc = 'Your quotation has been approved. UniqueID #'.$load->booking_id;
				        $type = 'tr_assign_quotation';
						//$load =Loads::findOne($data['load_id']);
				        $sender_id = $data['user_id'];
						$receiver_id = $load->assigned_user_id;
				        $this->_pushNotifification($title,$desc,$type,$sender_id,$receiver_id);
		            // END NOTIFICATION
					return array('status' => true,'message'=>'Transporter assigned successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	
	public function actionIndustryCurrentBookingList() {

			 \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			 if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			 $data = \yii::$app->request->post();		
			 if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			  
			  $limit = '';
			  $page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
			  if(isset($data['page']) && $data['page']>0) {
				$limit = $this->_pageLimit($page);
			  }
			  
				$result=[];
				$where = '';
				if( isset($data['search_text']) && trim($data['search_text']) != '' )
				{
					$where = ' AND ('; 
					$where .= 'booking_id LIKE "'.trim($data['search_text']).'%" or full_name LIKE "'.trim($data['search_text']).'%"
					or mobile_number LIKE "'.trim($data['search_text']).'%" or company_name LIKE "'.trim($data['search_text']).'%"
					or material_type LIKE "'.trim($data['search_text']).'%" or t1.truck_type LIKE "'.trim($data['search_text']).'%"
					or t2.truck_type LIKE "'.trim($data['search_text']).'%"';
					$where .= ') ';
				}
				/*$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.*,t2.vehicle_no FROM `loads` t1 INNER JOIN `truck` t2 ON 
				t1.assigned_truck_id=t2.id WHERE t1.industry_user_id='".trim($data['user_id'])."' AND assigned_status='Assigned' $where ORDER BY t1.id DESC");
				$loads = $command->queryAll();*/

				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.*,t2.vehicle_no FROM `loads` t1 INNER JOIN `truck` t2 ON t1.assigned_truck_id=t2.id WHERE t1.industry_user_id='".trim($data['user_id'])."' AND assigned_status='Assigned' $where ORDER BY t1.id DESC $limit");
				$loads = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT  COUNT(t1.id) as count FROM `loads` t1 INNER JOIN `truck` t2 ON t1.assigned_truck_id=t2.id WHERE t1.industry_user_id='".trim($data['user_id'])."' AND assigned_status='Assigned' $where");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);

				foreach($loads as $ld)
				{
					$quotations = Quotation::find()->select(['*'])->where(['load_id' => $ld['id'],'assigned_to_me' => '1'])
					->orderBy(['bid_amount' => SORT_ASC])->all();
					$ld['quotations'] = $quotations;
					$result[] = $ld;
				}
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Current Booking Found','data'=>$result);
				}	
	}
	// CALL actionLoadQuotationDetail

	public function actionIndustryTransporterList() {

			  \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			  if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			  }
			  $data = \yii::$app->request->post();		
			  if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			  
			  $limit = '';
			  $page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
			  if(isset($data['page']) && $data['page']>0) {
				$limit = $this->_pageLimit($page);
			  }
				$where = '';
				if( isset($data['search_text']) && trim($data['search_text']) != '' )
				{
					$where = ' AND ('; 
					$where .= 't1.first_name LIKE "'.trim($data['search_text']).'%" or t1.last_name LIKE "'.trim($data['search_text']).'%"
					or t1.mobile_number LIKE "'.trim($data['search_text']).'%" or t2.company_name LIKE "'.trim($data['search_text']).'%"';
					$where .= ') ';
				}
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t1.first_name,t1.last_name,t1.mobile_number,t1.email,t2.company_name,t2.verified,t2.available_status FROM `user` t1 INNER JOIN `transporter` t2 ON t1.id=t2.user_id WHERE t1.user_level='4' AND t1.status='1' AND t1.verified='1' AND t2.status='Active' $where	ORDER BY t2.company_name ASC $limit");
				$result = $command->queryAll();
				
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `user` t1 INNER JOIN `transporter` t2 ON t1.id=t2.user_id WHERE t1.user_level='4' AND t1.status='1' AND t1.verified='1' AND t2.status='Active' $where ORDER BY t2.company_name ASC");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	public function actionIndustryTruckOperatorList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			  
			  $limit = '';
			  $page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
			  if(isset($data['page']) && $data['page']>0) {
				$limit = $this->_pageLimit($page);
			  }
			  
				$where = '';
				if( isset($data['search_text']) && trim($data['search_text']) != '' )
				{
					$where = ' AND ('; 
					$where .= 't1.first_name LIKE "'.trim($data['search_text']).'%" or t1.last_name LIKE "'.trim($data['search_text']).'%"
					or t1.mobile_number LIKE "'.trim($data['search_text']).'%" or t2.company_name LIKE "'.trim($data['search_text']).'%"';
					$where .= ') ';
				}
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t1.first_name,t1.last_name,t1.mobile_number,
				t1.email,t2.company_name,t2.verified,t2.available_status FROM `user` t1 INNER JOIN `transporter` t2 
				ON t1.id=t2.user_id WHERE t1.user_level='5' AND t1.status='1' AND t1.verified='1' AND t2.status='Active' $where ORDER BY t1.first_name ASC");
				$result = $command->queryAll();
				
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	// actionIndustryTransporterTruckList
	public function actionIndustryTransporterTruckList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
			    || !isset($data['id']) || trim($data['id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			  
			  $limit = '';
			  $page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
			  if(isset($data['page']) && $data['page']>0) {
				$limit = $this->_pageLimit($page);
			  }
			  
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t1.first_name,t1.last_name,t1.mobile_number,t1.email,
				t2.company_name,t2.verified,t2.available_status,t2.address_line1,t2.address_line2,t2.pincode,t2.state,
				t2.district,t2.city,t2.area FROM `user` t1 INNER JOIN `transporter` t2 ON t1.id=t2.user_id 
				WHERE t2.user_id=".$data['id']);
				$result['profile'] = $command->queryOne();
				/*$result['truks'] = Truck::find()->where(['user_id' => $data['id'], 'status' => 'Active'])
				->orderBy(['id' => SORT_DESC])->all();
				$result['trucks'] = Truck::find()->where(['user_id' => $data['id'], 'status' => 'Active'])
				->orderBy(['id' => SORT_DESC])->all();
				*/
				//$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.* FROM `truck` t1 WHERE t1.user_id='".$data['id']."' AND t1.status='Active' ORDER BY t1.id DESC $limit");
				$result['truks'] = $command->queryAll();  // NEED TO REMOVE
				$command = $dbConn->createCommand("SELECT t1.* FROM `truck` t1 WHERE t1.user_id='".$data['id']."' AND t1.status='Active' ORDER BY t1.id DESC $limit");
				$result['trucks'] = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `truck` t1 WHERE t1.user_id='".$data['id']."' AND t1.status='Active'");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);

				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	// actionIndustryTransporterTruckDetail
	public function actionTruckDetails() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['id']) || trim($data['id']) == '') {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$path= BASE_URL .'/uploads/truck/';
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.*,concat('".$path."',rc_photo) as rc_photo,
				concat('".$path."',license_photo) as license_photo,concat('".$path."',permit_photo) as permit_photo	
				FROM `truck` t1 WHERE t1.id='".trim($data['id'])."'");
				$result = $command->queryOne();
				
				// ONLY FOR INDUSTRY SIDE USE - CHECK PREFERRED Transporter/Driver
				$prefer = PreferredList::find()->where(['industry_user_id' => $data['user_id'], 
				'preferred_user_id' => $result['user_id'], 'status' => 'Active'])->one();
				if(!empty($prefer))
				$preferred = '1';
				else
				$preferred = '0';	
				// ONLY FOR INDUSTRY SIDE USE - CHECK PREFERRED Transporter/Truck Operator
				$result['preferred_user'] = $preferred;
				
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	// actionIndustryPreferredAction
	public function actionIndustryPreferredAction() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['preferred_user_id']) || trim($data['preferred_user_id']) =='') {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = PreferredList::find()->where(['industry_user_id' => $data['user_id'], 
				'preferred_user_id' => $data['preferred_user_id']])->one();
				if(empty($model))
				{	
					$model = new PreferredList;
					$msg = 'Added to Preferred successfully.';
				}
				elseif($model->status=='Active')
				{
					$model->status = 'Deleted';
					$msg = 'Removed from Preferred successfully.';
				}
				else
				{
					$model->status = 'Active';
					$msg = 'Added to Preferred successfully.';
				}
				$model->industry_user_id=trim($data['user_id']);
				$model->preferred_user_id=trim($data['preferred_user_id']);
				if($model->save(false)) {
					return array('status' => true,'message'=>$msg);
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	// actionIndustryPreferredTransporterList
	public function actionIndustryPreferredTransporterList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
				 return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$where = '';
				if( isset($data['search_text']) && trim($data['search_text']) != '' )
				{
					$where = ' AND ('; 
					$where .= 't1.first_name LIKE "'.trim($data['search_text']).'%" or t1.last_name LIKE "'.trim($data['search_text']).'%"
					or t1.mobile_number LIKE "'.trim($data['search_text']).'%" or t2.company_name LIKE "'.trim($data['search_text']).'%"';
					$where .= ') ';
				}
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t1.first_name,t1.last_name,t1.mobile_number,t1.email,
				t2.company_name,t2.verified,t2.available_status FROM `user` t1 INNER JOIN `transporter` t2 ON 
				t1.id=t2.user_id INNER JOIN `preferred_list` t3 ON t1.id=t3.preferred_user_id WHERE t1.user_level='4'
				AND t1.status='1' AND t2.status='Active' AND t3.industry_user_id='".$data['user_id']."'
				AND t3.status='Active' $where");
				$result = $command->queryAll();
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	public function actionIndustryPreferredTruckOperatorList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$where = '';
				if( isset($data['search_text']) && trim($data['search_text']) != '' )
				{
					$where = ' AND ('; 
					$where .= 't1.first_name LIKE "'.trim($data['search_text']).'%" or t1.last_name LIKE "'.trim($data['search_text']).'%"
					or t1.mobile_number LIKE "'.trim($data['search_text']).'%" or t2.company_name LIKE "'.trim($data['search_text']).'%"';
					$where .= ') ';
				}
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t1.first_name,t1.last_name,t1.mobile_number,t1.email,
				t2.company_name,t2.verified,t2.available_status FROM `user` t1 INNER JOIN `transporter` t2 ON 
				t1.id=t2.user_id INNER JOIN `preferred_list` t3 ON t1.id=t3.preferred_user_id WHERE t1.user_level='5' 
				AND t1.status='1' AND t2.status='Active'  AND t3.industry_user_id='".$data['user_id']."' AND
				t3.status='Active' $where");
				$result = $command->queryAll();
				
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	public function actionIndustryActiveTripList() {

				\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
				if($this->_checkAutoLogout()){
					return $this->_autoLogout();
				}
				$data = \yii::$app->request->post();		
				if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
						return array('status'=>false,'message'=>'Some parameter is missing.');
				}
				$limit = '';
				$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
				if(isset($data['page']) && $data['page']>0) {
					$limit = $this->_pageLimit($page);
				}
				$result=[];
				/*$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t2.id as load_id,t2.booking_id,t2.full_name,
				t2.mobile_number,t2.company_name,t2.source_address_line1,t2.source_address_line2,t2.source_pincode,
				t2.source_state,t2.source_district,t2.source_city,t2.source_area,t2.source_landmark,
				t2.destination_address_line1,t2.destination_address_line2,t2.destination_pincode,t2.destination_state,
				t2.destination_district,t2.destination_city,t2.destination_area,t2.destination_landmark,
				t2.load_type,t2.material_type,t2.weight,t2.truck_type,t2.no_of_trucks,t2.bid_closing_date,
				t2.bid_closing_time,t2.reporting_date,t2.reporting_time,t2.estimated_quotation_amount,t2.description,
				t2.assigned_status,t2.assigned_user_id,t2.assigned_full_name,t2.assigned_mobile_number,
				t2.assigned_company_name,assigned_bid_amount,
				t1.truck_id,t1.vehicle_no,t1.gps_installed,t1.weight,t1.truck_type,t1.rc_no,t1.license_no,
				t1.permit_type,t1.pollution_no,t1.rto_fitness_certificate_no,t1.local_registration_no,
				t1.registration_address,t1.registration_mobile,t1.office_landline_no,t1.email,
				t1.gross_weight,t1.trip_status,t1.trip_start_at,t1.trip_close_at
				FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned'
				AND t2.industry_user_id='".$data['user_id']."' AND (t1.trip_status='Start Trip' OR t1.trip_status='Goods Delivered') ORDER BY t2.id DESC");
				$result = $command->queryAll();*/

				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t2.id as load_id,t2.booking_id,t2.full_name,
				t2.mobile_number,t2.company_name,t2.source_address_line1,t2.source_address_line2,t2.source_pincode,
				t2.source_state,t2.source_district,t2.source_city,t2.source_area,t2.source_landmark,
				t2.destination_address_line1,t2.destination_address_line2,t2.destination_pincode,t2.destination_state,
				t2.destination_district,t2.destination_city,t2.destination_area,t2.destination_landmark,
				t2.load_type,t2.material_type,t2.weight,t2.truck_type,t2.no_of_trucks,t2.bid_closing_date,
				t2.bid_closing_time,t2.reporting_date,t2.reporting_time,t2.estimated_quotation_amount,t2.description,
				t2.assigned_status,t2.assigned_user_id,t2.assigned_full_name,t2.assigned_mobile_number,
				t2.assigned_company_name,assigned_bid_amount,
				t1.truck_id,t1.vehicle_no,t1.gps_installed,t1.weight,t1.truck_type,t1.rc_no,t1.license_no,
				t1.permit_type,t1.pollution_no,t1.rto_fitness_certificate_no,t1.local_registration_no,
				t1.registration_address,t1.registration_mobile,t1.office_landline_no,t1.email,
				t1.gross_weight,t1.trip_status,t1.trip_start_at,t1.trip_close_at
				FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned'	AND t2.industry_user_id='".$data['user_id']."' AND (t1.trip_status='Start Trip' OR t1.trip_status='Goods Delivered') ORDER BY t2.id DESC $limit");
				$result = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned'
				AND t2.industry_user_id='".$data['user_id']."' AND (t1.trip_status='Start Trip' OR t1.trip_status='Goods Delivered')");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);

				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Active Trip Found','data'=>$result);
				}	
	}
	// CALL actionTripDetail
	public function actionShowTripCurrentLocation() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) =='' 
				|| !isset($data['load_id']) || trim($data['load_id']) =='' 
				|| !isset($data['truck_id']) || trim($data['truck_id']) == ''
				) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$result = TripTracking::find()->select(['latitude','longitude','created_on'])
				->where(['load_id' => $data['load_id'], 'truck_id' => $data['truck_id']])
				->orderBy(['id'=>SORT_DESC])->one();	
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success', 'data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Data Found', 'data'=>'');
				}	
	}
	public function actionShowTripTrackingPath() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) =='' 
				|| !isset($data['load_id']) || trim($data['load_id']) =='' 
				|| !isset($data['truck_id']) || trim($data['truck_id']) == ''
				) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$result = TripTracking::find()->select(['latitude','longitude','created_on'])
				->where(['load_id' => $data['load_id'], 'truck_id' => $data['truck_id']])
				->orderBy(['id'=>SORT_ASC])->all();	
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success', 'data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Data Found');
				}	
	}
	public function actionIndustryCloseTrip() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
			|| !isset($data['load_id']) || trim($data['load_id']) == '' 
			|| !isset($data['truck_id']) || trim($data['truck_id']) == '') {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = QuotationTruck::find()->where(['load_id' => $data['load_id'],'truck_id' => $data['truck_id'],
				'trip_status' => 'Goods Delivered'])->one();
				if(!empty($model)) {
					$model->trip_status = 'Close Trip';
					$model->trip_close_at = date('Y-m-d H:i:s');
					if($model->save(false)) {
						// START NOTIFICATION
						$load =Loads::findOne($data['load_id']);
						$title = 'Industry confirmed & closed the trip. UniqueID #'.$load->booking_id;
				        $desc = 'Industry confirmed & closed the trip. UniqueID #'.$load->booking_id;
				        $type = 'tr_close_trip';
						$sender_id = $data['user_id'];
						$receiver_id = $load->assigned_user_id;
				        $this->_pushNotifification($title,$desc,$type,$sender_id,$receiver_id);
						// END NOTIFICATION
						
						$dbConn = Yii::$app->db;
						$command = $dbConn->createCommand("SELECT COUNT(*) AS count FROM `quotation_truck` t1 WHERE load_id='".$data['load_id']."' 
						AND trip_status!='Close Trip'");
						$tripOpen = $command->queryOne();
						if($tripOpen['count']==0)
						{
							$load->assigned_status = 'Closed';
							$load->save(false);
						}
						return array('status' => true,'message'=>'Trip closed successfully.');
					}
					else {
						return $this->_errorMessage1();
					}
				}
				else {
					return $this->_errorMessage1();
				}	
	}
		
//========================================== BIDDER (TRANSPORTER/TRUCK OPERATOR) API ====================================	
	// actionTransporterPostedLoadList
	public function actionTransporterPostedLoadList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
					return $this->_autoLogout();
				}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }

				$limit = '';
				$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
				if(isset($data['page']) && $data['page']>0) {
					$limit = $this->_pageLimit($page);
				}  
				$where = '';
				if( isset($data['search_text']) && trim($data['search_text']) != '' )
				{
					$where = ' AND ('; 
					$where .= 't1.first_name LIKE "'.trim($data['search_text']).'%" or t1.last_name LIKE "'.trim($data['search_text']).'%"
					or t1.mobile_number LIKE "'.trim($data['search_text']).'%" or t1.company_name LIKE "'.trim($data['search_text']).'%"';
					$where .= ') ';
				}
				$result=[];
				//$loads = Loads::find()->where(['assigned_status' => 'Open'])
				//->andWhere('share_to_preferred="Yes" or share_to_preferred="No"')
				//->orderBy(['id' => SORT_DESC])->all();
				/*$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.* FROM `loads` t1 WHERE assigned_status='Open' 
				AND id NOT IN (SELECT load_id FROM `quotation` t2 WHERE t2.user_id='".trim($data['user_id'])."')
				AND t1.status='Active' $where ORDER BY t1.id DESC");
				$loads = $command->queryAll();*/

				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.* FROM `loads` t1 WHERE assigned_status='Open' 
				AND id NOT IN (SELECT load_id FROM `quotation` t2 WHERE t2.user_id='".trim($data['user_id'])."') AND t1.status='Active' $where ORDER BY t1.id DESC $limit");
				$loads = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `loads` t1 WHERE assigned_status='Open' AND id NOT IN (SELECT load_id FROM `quotation` t2 WHERE t2.user_id='".trim($data['user_id'])."') AND t1.status='Active' $where");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);

				foreach($loads as $ld)
				{
					$quotations = Quotation::find()->select(['bid_amount'])->where(['load_id' => $ld['id']])
					->orderBy(['bid_amount' => SORT_ASC])->all();
					$ld['quotations'] = $quotations;
					$result[] = $ld;
				}
				
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	//public function actionLoadDetails()
	
	// actionTransporterSubmitQuotation
	public function actionTransporterSubmitQuotation() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) =='' 
				|| !isset($data['load_id']) || trim($data['load_id']) == ''
				|| !isset($data['bid_amount']) || trim($data['bid_amount']) ==''
				|| !isset($data['truck_id']) || trim($data['truck_id']) ==''
				//|| !isset($data['remark']) || trim($data['remark']) =='' 
				) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = new Quotation;
				$model->load_id=trim($data['load_id']);
				$model->user_id=trim($data['user_id']);
				$model->bid_amount=trim($data['bid_amount']);
				if(isset($data['part_load_quotation_date']) && $data['part_load_quotation_date']!='')
				$model->part_load_quotation_date=date('Y-m-d',strtotime($data['part_load_quotation_date']));	
				$truck_ids = trim(rtrim($data['truck_id'], ','));
				$truck_ids = str_replace(",,",",",$truck_ids);
				$model->truck_id=trim($truck_ids);
				if(isset($data['remark']))
				$model->remark=trim($data['remark']);
				//$user =User::findOne($data['user_id']);
				$trans =Transporter::find()->where(['user_id' => $data['user_id']])->one();
				$model->full_name = $trans->first_name.' '.$trans->last_name;
				$model->mobile_number = $trans->mobile_number;
				$model->company_name = $trans->company_name;	
				if($model->save(false)) {
					$id = $model->id;
					$dbConn = Yii::$app->db;
					$trucks = explode(",", $truck_ids);
					foreach ($trucks as $truck_id) {
						$sql = "INSERT INTO quotation_truck (quotation_id,load_id,truck_id,user_id,vehicle_no,gps_installed,
						weight,truck_type,rc_no,rc_photo,license_no,license_photo,permit_type,permit_photo,
						insurance_policy_no,insurance_policy_photo,pollution_no,pollution_photo,rto_fitness_certificate_no,
						rto_fitness_certificate_photo,local_registration_no,local_registration_photo,registration_address,
						registration_mobile,office_landline_no,email,gross_weight,verified,status,created_at)
						SELECT '$id','".trim($data['load_id'])."',id,user_id,vehicle_no,gps_installed,weight,truck_type,rc_no,rc_photo,
						license_no,license_photo,permit_type,permit_photo,insurance_policy_no,insurance_policy_photo,
						pollution_no,pollution_photo,rto_fitness_certificate_no,rto_fitness_certificate_photo,
						local_registration_no,local_registration_photo,registration_address,registration_mobile,
						office_landline_no,email,gross_weight,verified,status,NOW() FROM truck WHERE id=".$truck_id;
						$result = $dbConn->createCommand($sql)->execute();
					}
					
					// START NOTIFICATION
						$load =Loads::findOne($data['load_id']);
						$title = 'You have received new quotation. UniqueID #'.$load->booking_id;
				        $desc = 'You have received new quotation. UniqueID #'.$load->booking_id;
				        $type = 'in_received_quotation';
						$sender_id = $data['user_id'];
						$receiver_id = $load->industry_user_id;
				        $this->_pushNotifification($title,$desc,$type,$sender_id,$receiver_id);
		            // END NOTIFICATION
						
					return array('status' => true,'message'=>'Your quotation submitted successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	
	// actionTransporterCurrentBookingList
	public function actionTransporterCurrentBookingList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
					return $this->_autoLogout();
				}
			$data = \yii::$app->request->post();		
			 if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			  	$limit = '';
				$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
				if(isset($data['page']) && $data['page']>0) {
					$limit = $this->_pageLimit($page);
				}
				$result=[];
				$where = '';
				if( isset($data['search_text']) && trim($data['search_text']) != '' )
				{
					$where = ' AND ('; 
					$where .= 'booking_id LIKE "'.trim($data['search_text']).'%" or full_name LIKE "'.trim($data['search_text']).'%"
					or mobile_number LIKE "'.trim($data['search_text']).'%" or company_name LIKE "'.trim($data['search_text']).'%"
					or material_type LIKE "'.trim($data['search_text']).'%" or t1.truck_type LIKE "'.trim($data['search_text']).'%"
					or t2.truck_type LIKE "'.trim($data['search_text']).'%"';
					$where .= ') ';
				}
				/*$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.*,t2.vehicle_no,t2.truck_type as truck_type1 FROM 
				`loads` t1 INNER JOIN `truck` t2 ON t1.assigned_truck_id=t2.id WHERE t1.assigned_user_id='".trim($data['user_id'])."'
				AND assigned_status='Assigned' $where ORDER BY t1.id DESC");
				$loads = $command->queryAll();*/

				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.*,t2.vehicle_no,t2.truck_type as truck_type1 FROM `loads` t1 INNER JOIN `truck` t2 ON t1.assigned_truck_id=t2.id WHERE t1.assigned_user_id='".trim($data['user_id'])."' AND assigned_status='Assigned' $where ORDER BY t1.id DESC $limit");
				$loads = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `loads` t1 INNER JOIN `truck` t2 ON t1.assigned_truck_id=t2.id WHERE t1.assigned_user_id= '".trim($data['user_id'])."' AND assigned_status='Assigned' $where");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);

				foreach($loads as $ld)
				{
					$quotations = Quotation::find()->select(['*'])->where(['load_id' => $ld['id'],'assigned_to_me' => '1'])
					->orderBy(['bid_amount' => SORT_ASC])->all();
					$ld['quotations'] = $quotations;
					$result[] = $ld;
				}
				
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	
	public function actionTransporterAssignTruckDriver() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) =='' 
				|| !isset($data['quotation_id']) || trim($data['quotation_id']) == ''
				|| !isset($data['truck_id']) || trim($data['truck_id']) ==''
				|| !isset($data['driver_id']) || trim($data['driver_id']) ==''
				|| !isset($data['driver_mobile']) || trim($data['driver_mobile']) ==''
				) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$driver = TruckDriver::findOne($data['driver_id']);
				$user = User::find()->select('id,first_name,last_name,mobile_number,username,password,email,user_image,user_level,status')
				->where("mobile_number='".$data['driver_mobile']."'")->andWhere("id!='".$driver->user_id."'")
				->andWhere("id!='".$data['user_id']."'")->one();	
				if (!empty($user)) {	
					//return array('status'=>false,'message'=>'This Mobile number is already exist with another account. Please try with another Mobile number.');
				}
				$isExist = QuotationTruck::find()->where(['quotation_id' => $data['quotation_id'], 'assigned_driver_id' => $data['driver_id']])->one();
				if(!empty($isExist))
				{
					return array('status'=>false,'message'=>'This Driver already assigned.');
				}
				$model = QuotationTruck::find()->where(['quotation_id' => $data['quotation_id'], 'truck_id' => $data['truck_id'], 'user_id' => $data['user_id']])->one();
				$model->assigned_driver_user_id = $driver->user_id;
				$model->assigned_driver_id = trim($data['driver_id']);
				$model->assigned_driver_name = $driver->driver_name;
				$model->assigned_driver_mobile = $driver->driver_mobile;
				$model->assigned_driver_aadhaar_number = $driver->driver_aadhaar_number;
				$model->assigned_driver_at=date('Y-m-d H:i:s');
				if($model->save(false)) {
					$user = User::find()->where(['id' => $driver->user_id])->one();
					$msg = "";
					//$msg = "Dear ".$user['first_name']." ".$user['last_name'].", ";
					$msg .= "You have been assigned as a driver. Please download & login Parivahan Suvidha App:-".APK_LINK;
					$msg .= " ";
					$msg .= "Username: ".$user['mobile_number']." ";
					$msg .= "Password: ".$user['temp_password'];
					$msg .= SMS_SUFFIX;
					$mobile = trim($data['driver_mobile']);
					$this->_sendTextSMS($mobile,$msg) ;		
					return array('status' => true,'message'=>'Driver assigned successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	// actionTransporterAddTruck
	public function actionTransporterAddTruck() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['vehicle_no']) || trim($data['vehicle_no']) ==''
				|| !isset($data['gps_installed']) || trim($data['gps_installed']) ==''
				|| !isset($data['weight']) || trim($data['weight']) ==''
				|| !isset($data['truck_type']) || trim($data['truck_type']) ==''
				|| !isset($data['rc_no']) || trim($data['rc_no']) ==''
				|| !isset($data['license_no']) || trim($data['license_no']) ==''
				//|| !isset($data['license_photo']) || trim($data['license_photo']) ==''
				|| !isset($data['permit_type']) || trim($data['permit_type']) ==''
				//|| !isset($data['permit_photo']) || trim($data['permit_photo']) ==''
				//|| !isset($data['insurance_policy_no']) || trim($data['insurance_policy_no']) ==''
				//|| !isset($data['insurance_policy_photo']) || trim($data['insurance_policy_photo']) ==''
				//|| !isset($data['pollution_no']) || trim($data['pollution_no']) ==''
				//|| !isset($data['pollution_photo']) || trim($data['pollution_photo']) ==''
				|| !isset($data['rto_fitness_certificate_no']) || trim($data['rto_fitness_certificate_no']) ==''
				//|| !isset($data['rto_fitness_certificate_photo']) || trim($data['rto_fitness_certificate_photo']) ==''
				//|| !isset($data['local_registration_no']) || trim($data['local_registration_no']) ==''
				//|| !isset($data['local_registration_photo']) || trim($data['local_registration_photo']) ==''
			) {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = new Truck;
				$model->user_id=trim($data['user_id']);
				$model->vehicle_no=trim($data['vehicle_no']);
				$model->gps_installed=trim($data['gps_installed']);
				$model->weight=trim($data['weight']);
				$model->truck_type=trim($data['truck_type']);
				$model->rc_no=trim($data['rc_no']);
				$model->license_no=trim($data['license_no']);
				$model->permit_type=trim($data['permit_type']);
				if(isset($data['insurance_policy_no']))
				$model->insurance_policy_no=trim($data['insurance_policy_no']);
				if(isset($data['pollution_no']))
				$model->pollution_no=trim($data['pollution_no']);
				$model->rto_fitness_certificate_no=trim($data['rto_fitness_certificate_no']);
				//$model->local_registration_no=trim($data['local_registration_no']);
				if(isset($data['registration_address']))
				$model->registration_address=trim($data['registration_address']);
				if(isset($data['registration_mobile']))
				$model->registration_mobile=trim($data['registration_mobile']);
				if(isset($data['office_landline_no']))
				$model->office_landline_no=trim($data['office_landline_no']);
				if(isset($data['email']))
				$model->email=trim($data['email']);
				if(isset($data['gross_weight']))
				$model->gross_weight=trim($data['gross_weight']);
				$path = UPLOAD_DIR_PATH_ . 'uploads/truck/';
				if(isset($_FILES['rc_photo']['name'])  && trim($_FILES['rc_photo']['name'])!='') {	
					$tmpName = $_FILES['rc_photo']['tmp_name'];
					$str = explode(".",$_FILES['rc_photo']['name']);
					$l=count($str);
					$img =  "rc_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->rc_photo = $img;
					}
				}
				if(isset($_FILES['license_photo']['name'])  && trim($_FILES['license_photo']['name'])!='') {	
					$tmpName = $_FILES['license_photo']['tmp_name'];
					$str = explode(".",$_FILES['license_photo']['name']);
					$l=count($str);
					$img =  "license_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->license_photo = $img;
					}
				}
				if(isset($_FILES['permit_photo']['name'])  && trim($_FILES['permit_photo']['name'])!='') {	
					$tmpName = $_FILES['permit_photo']['tmp_name'];
					$str = explode(".",$_FILES['permit_photo']['name']);
					$l=count($str);
					$img =  "permit_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->permit_photo = $img;
					}
				}
				if(isset($_FILES['insurance_policy_photo']['name'])  && trim($_FILES['insurance_policy_photo']['name'])!='') {	
					$tmpName = $_FILES['insurance_policy_photo']['tmp_name'];
					$str = explode(".",$_FILES['insurance_policy_photo']['name']);
					$l=count($str);
					$img =  "insurance_policy_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->insurance_policy_photo = $img;
					}
				}
				if(isset($_FILES['pollution_photo']['name'])  && trim($_FILES['pollution_photo']['name'])!='') {	
					$tmpName = $_FILES['pollution_photo']['tmp_name'];
					$str = explode(".",$_FILES['pollution_photo']['name']);
					$l=count($str);
					$img =  "pollution_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->pollution_photo = $img;
					}
				}
				if(isset($_FILES['rto_fitness_certificate_photo']['name'])  && trim($_FILES['rto_fitness_certificate_photo']['name'])!='') {	
					$tmpName = $_FILES['rto_fitness_certificate_photo']['tmp_name'];
					$str = explode(".",$_FILES['rto_fitness_certificate_photo']['name']);
					$l=count($str);
					$img =  "rto_fitness_certificate_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->rto_fitness_certificate_photo = $img;
					}
				}
				/*if(isset($_FILES['local_registration_photo']['name'])  && trim($_FILES['local_registration_photo']['name'])!='') {	
					$tmpName = $_FILES['local_registration_photo']['tmp_name'];
					$str = explode(".",$_FILES['local_registration_photo']['name']);
					$l=count($str);
					$img =  "local_registration_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->local_registration_photo = $img;
					}
				}*/
				if($model->save(false)) {
					return array('status' => true,'message'=>'Truck data submitted successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	
	// actionTransporterEditTruck
	public function actionTransporterEditTruck() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['id']) || trim($data['id']) == '' 
				|| !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['vehicle_no']) || trim($data['vehicle_no']) ==''
				|| !isset($data['gps_installed']) || trim($data['gps_installed']) ==''
				|| !isset($data['weight']) || trim($data['weight']) ==''
				|| !isset($data['truck_type']) || trim($data['truck_type']) ==''
				|| !isset($data['rc_no']) || trim($data['rc_no']) ==''
				|| !isset($data['license_no']) || trim($data['license_no']) ==''
				//|| !isset($data['license_photo']) || trim($data['license_photo']) ==''
				|| !isset($data['permit_type']) || trim($data['permit_type']) ==''
				//|| !isset($data['permit_photo']) || trim($data['permit_photo']) ==''
				//|| !isset($data['insurance_policy_no']) || trim($data['insurance_policy_no']) ==''
				//|| !isset($data['insurance_policy_photo']) || trim($data['insurance_policy_photo']) ==''
				//|| !isset($data['pollution_no']) || trim($data['pollution_no']) ==''
				//|| !isset($data['pollution_photo']) || trim($data['pollution_photo']) ==''
				|| !isset($data['rto_fitness_certificate_no']) || trim($data['rto_fitness_certificate_no']) ==''
				//|| !isset($data['rto_fitness_certificate_photo']) || trim($data['rto_fitness_certificate_photo']) ==''
				//|| !isset($data['local_registration_no']) || trim($data['local_registration_no']) ==''
				//|| !isset($data['local_registration_photo']) || trim($data['local_registration_photo']) ==''
			) {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = Truck::findOne($data['id']);
				$model->user_id=trim($data['user_id']);
				$model->vehicle_no=trim($data['vehicle_no']);
				$model->gps_installed=trim($data['gps_installed']);
				$model->weight=trim($data['weight']);
				$model->truck_type=trim($data['truck_type']);
				$model->rc_no=trim($data['rc_no']);
				$model->license_no=trim($data['license_no']);
				$model->permit_type=trim($data['permit_type']);
				$model->insurance_policy_no=trim($data['insurance_policy_no']);
				$model->pollution_no=trim($data['pollution_no']);
				$model->rto_fitness_certificate_no=trim($data['rto_fitness_certificate_no']);
				//$model->local_registration_no=trim($data['local_registration_no']);
				if(isset($data['registration_address']))
				$model->registration_address=trim($data['registration_address']);
				if(isset($data['registration_mobile']))
				$model->registration_mobile=trim($data['registration_mobile']);
				if(isset($data['office_landline_no']))
				$model->office_landline_no=trim($data['office_landline_no']);
				if(isset($data['email']))
				$model->email=trim($data['email']);
				if(isset($data['gross_weight']))
				$model->gross_weight=trim($data['gross_weight']);
				$path = UPLOAD_DIR_PATH_ . 'uploads/truck/';
				if(isset($_FILES['rc_photo']['name'])  && trim($_FILES['rc_photo']['name'])!='') {	
					$tmpName = $_FILES['rc_photo']['tmp_name'];
					$str = explode(".",$_FILES['rc_photo']['name']);
					$l=count($str);
					$img =  "rc_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->rc_photo = $img;
					}
				}
				if(isset($_FILES['license_photo']['name'])  && trim($_FILES['license_photo']['name'])!='') {	
					$tmpName = $_FILES['license_photo']['tmp_name'];
					$str = explode(".",$_FILES['license_photo']['name']);
					$l=count($str);
					$img =  "license_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->license_photo = $img;
					}
				}
				if(isset($_FILES['permit_photo']['name'])  && trim($_FILES['permit_photo']['name'])!='') {	
					$tmpName = $_FILES['permit_photo']['tmp_name'];
					$str = explode(".",$_FILES['permit_photo']['name']);
					$l=count($str);
					$img =  "permit_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->permit_photo = $img;
					}
				}
				if(isset($_FILES['insurance_policy_photo']['name'])  && trim($_FILES['insurance_policy_photo']['name'])!='') {	
					$tmpName = $_FILES['insurance_policy_photo']['tmp_name'];
					$str = explode(".",$_FILES['insurance_policy_photo']['name']);
					$l=count($str);
					$img =  "insurance_policy_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->insurance_policy_photo = $img;
					}
				}
				if(isset($_FILES['pollution_photo']['name'])  && trim($_FILES['pollution_photo']['name'])!='') {	
					$tmpName = $_FILES['pollution_photo']['tmp_name'];
					$str = explode(".",$_FILES['pollution_photo']['name']);
					$l=count($str);
					$img =  "pollution_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->pollution_photo = $img;
					}
				}
				if(isset($_FILES['rto_fitness_certificate_photo']['name'])  && trim($_FILES['rto_fitness_certificate_photo']['name'])!='') {	
					$tmpName = $_FILES['rto_fitness_certificate_photo']['tmp_name'];
					$str = explode(".",$_FILES['rto_fitness_certificate_photo']['name']);
					$l=count($str);
					$img =  "rto_fitness_certificate_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->rto_fitness_certificate_photo = $img;
					}
				}
				if(isset($_FILES['local_registration_photo']['name'])  && trim($_FILES['local_registration_photo']['name'])!='') {	
					$tmpName = $_FILES['local_registration_photo']['tmp_name'];
					$str = explode(".",$_FILES['local_registration_photo']['name']);
					$l=count($str);
					$img =  "local_registration_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->local_registration_photo = $img;
					}
				}
				if($model->save(false)) {
					return array('status' => true,'message'=>'Truck data updated successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	
	// actionTransporterDeleteTruck
	public function actionTransporterDeleteTruck() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['id']) || trim($data['id']) == '' 
				|| !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = Truck::findOne($data['id']);
				$model->status= 'Deleted';
				if($model->save(false)) {
					return array('status' => true,'message'=>'Truck deleted successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	
	// actionTransporterTruckList
	public function actionTransporterTruckList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
					return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			  	$limit = '';
				$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
				if(isset($data['page']) && $data['page']>0) {
					$limit = $this->_pageLimit($page);
				}
				/*$result = Truck::find()->where(['user_id' => $data['user_id'], 'status' => 'Active'])
				->orderBy(['id' => SORT_DESC])->all();
*/
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.* FROM `truck` t1 WHERE t1.user_id='".trim($data['user_id'])."' AND status='Active' ORDER BY t1.id DESC $limit");
				$result = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `truck` t1 WHERE t1.user_id='".trim($data['user_id'])."' AND status='Active'");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	
	public function actionTransporterAddTruckDriver() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['driver_name']) || trim($data['driver_name']) ==''
				|| !isset($data['driver_mobile']) || trim($data['driver_mobile']) ==''
				|| !isset($data['driver_current_address']) || trim($data['driver_current_address']) ==''
				|| !isset($data['driver_original_address']) || trim($data['driver_original_address']) ==''
				|| !isset($data['driver_aadhaar_number']) || trim($data['driver_aadhaar_number']) ==''
				|| !isset($data['driver_licence_number']) || trim($data['driver_licence_number']) ==''
				|| !isset($data['driver_pan_number']) || trim($data['driver_pan_number']) ==''
			) {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				
				$user = User::find()->select('id,first_name,last_name,mobile_number,username,password,email,user_image,user_level,status')
				->where("mobile_number='".$data['driver_mobile']."'")->one();	
				if (!empty($user)) {	
					return array('status'=>false,'message'=>'Account already exist.');
				}
				$randomNumber = $this->actionGenerateCode();
				$model = new User;
				$model->first_name=trim($data['driver_name']);
				//$model->last_name='';
				$model->mobile_number=trim($data['driver_mobile']);
				//if(isset($data['email']))
				//$model->email=trim($data['email']);
				//$model->username=trim($data['username']);
				//$model->password=md5(trim($data['password']));
				$model->password=md5($randomNumber);
				$model->temp_password=$randomNumber;
				$model->user_level='6';
				$model->user_level_name='truck_driver';
				$model->status='1';
				$model->verified='1';
				//$model->varification_code=$randomNumber;
				if(!$model->save(false)) {
					return $this->_errorMessage1();
				}	
				$user_id = $model->id;
				
				$model = new TruckDriver;
				$model->transporter_user_id=trim($data['user_id']);
				$model->user_id=trim($user_id);
				$model->driver_name=trim($data['driver_name']);
				$model->driver_mobile=trim($data['driver_mobile']);
				$model->driver_current_address=trim($data['driver_current_address']);
				$model->driver_original_address=trim($data['driver_original_address']);
				$model->driver_aadhaar_number=trim($data['driver_aadhaar_number']);
				$model->driver_licence_number=trim($data['driver_licence_number']);
				if(isset($data['driver_pan_number']))
				$model->driver_pan_number=trim($data['driver_pan_number']);
				$path = UPLOAD_DIR_PATH_ . 'uploads/driver/';
				if(isset($_FILES['driver_aadhaar_photo']['name'])  && trim($_FILES['driver_aadhaar_photo']['name'])!='') {	
					$tmpName = $_FILES['driver_aadhaar_photo']['tmp_name'];
					$str = explode(".",$_FILES['driver_aadhaar_photo']['name']);
					$l=count($str);
					$img =  "driver_aadhaar_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->driver_aadhaar_photo = $img;
					}
				}
				if(isset($_FILES['driver_licence_photo']['name'])  && trim($_FILES['driver_licence_photo']['name'])!='') {	
					$tmpName = $_FILES['driver_licence_photo']['tmp_name'];
					$str = explode(".",$_FILES['driver_licence_photo']['name']);
					$l=count($str);
					$img =  "driver_licence_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->driver_licence_photo = $img;
					}
				}
				if(isset($_FILES['driver_pan_photo']['name'])  && trim($_FILES['driver_pan_photo']['name'])!='') {	
					$tmpName = $_FILES['driver_pan_photo']['tmp_name'];
					$str = explode(".",$_FILES['driver_pan_photo']['name']);
					$l=count($str);
					$img =  "driver_pan_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->driver_pan_photo = $img;
					}
				}
				
				if($model->save(false)) {
					return array('status' => true,'message'=>'Driver data is submitted successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	public function actionTransporterEditTruckDriver() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['id']) || trim($data['id']) == '' 
				|| !isset($data['user_id']) || trim($data['user_id']) == '' 
				|| !isset($data['driver_name']) || trim($data['driver_name']) ==''
				|| !isset($data['driver_mobile']) || trim($data['driver_mobile']) ==''
				|| !isset($data['driver_current_address']) || trim($data['driver_current_address']) ==''
				|| !isset($data['driver_original_address']) || trim($data['driver_original_address']) ==''
				|| !isset($data['driver_aadhaar_number']) || trim($data['driver_aadhaar_number']) ==''
				|| !isset($data['driver_licence_number']) || trim($data['driver_licence_number']) ==''
				|| !isset($data['driver_pan_number']) || trim($data['driver_pan_number']) ==''
			) {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = TruckDriver::findOne($data['id']);
				$user = User::find()->select('id,first_name,last_name,mobile_number,username,password,email,user_image,user_level,status')
				->where("mobile_number='".$data['driver_mobile']."'")->andWhere("id!='".$model->user_id."'")->one();	
				if (!empty($user)) {	
					return array('status'=>false,'message'=>'Account already exist.');
				}
				
				//$model->transporter_user_id=trim($data['user_id']);
				$model->driver_name=trim($data['driver_name']);
				$model->driver_mobile=trim($data['driver_mobile']);
				$model->driver_current_address=trim($data['driver_current_address']);
				$model->driver_original_address=trim($data['driver_original_address']);
				$model->driver_aadhaar_number=trim($data['driver_aadhaar_number']);
				$model->driver_licence_number=trim($data['driver_licence_number']);
				if(isset($data['driver_pan_number']))
				$model->driver_pan_number=trim($data['driver_pan_number']);
				$path = UPLOAD_DIR_PATH_ . 'uploads/driver/';
				if(isset($_FILES['driver_aadhaar_photo']['name'])  && trim($_FILES['driver_aadhaar_photo']['name'])!='') {	
					$tmpName = $_FILES['driver_aadhaar_photo']['tmp_name'];
					$str = explode(".",$_FILES['driver_aadhaar_photo']['name']);
					$l=count($str);
					$img =  "driver_aadhaar_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->driver_aadhaar_photo = $img;
					}
				}
				if(isset($_FILES['driver_licence_photo']['name'])  && trim($_FILES['driver_licence_photo']['name'])!='') {	
					$tmpName = $_FILES['driver_licence_photo']['tmp_name'];
					$str = explode(".",$_FILES['driver_licence_photo']['name']);
					$l=count($str);
					$img =  "driver_licence_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->driver_licence_photo = $img;
					}
				}
				if(isset($_FILES['driver_pan_photo']['name'])  && trim($_FILES['driver_pan_photo']['name'])!='') {	
					$tmpName = $_FILES['driver_pan_photo']['tmp_name'];
					$str = explode(".",$_FILES['driver_pan_photo']['name']);
					$l=count($str);
					$img =  "driver_pan_photo"."_".time().".".$str[$l-1];
					if(move_uploaded_file($tmpName,$path.$img)){
						$model->driver_pan_photo = $img;
					}
				}
				if($model->save(false)) {
					$model = User::findOne($model->user_id);
					$model->first_name=trim($data['driver_name']);
					//$model->last_name='';
					$model->mobile_number=trim($data['driver_mobile']);
					$model->save(false);
					return array('status' => true,'message'=>'Driver data updated successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	public function actionTransporterDeleteTruckDriver() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['id']) || trim($data['id']) == '' 
				|| !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = TruckDriver::findOne($data['id']);
				$model->status= 'Deleted';
				if($model->save(false)) {
					return array('status' => true,'message'=>'Driver deleted successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	public function actionTransporterTruckDriverList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
					return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			  $limit = '';
				$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
				if(isset($data['page']) && $data['page']>0) {
					$limit = $this->_pageLimit($page);
				}
			  $where = "";
			  if(isset($data['quotation_id']) && trim($data['quotation_id']) !='') {
				 $where = "AND id NOT IN (SELECT assigned_driver_id FROM `quotation_truck` WHERE quotation_id='".trim($data['quotation_id'])."')";
			  }
				//$result = TruckDriver::find()->where(['transporter_user_id' => $data['user_id'], 'status' => 'Active'])
				//->orderBy(['id' => SORT_DESC])->all();
				/*$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT * FROM `truck_driver` WHERE transporter_user_id='".$data['user_id']."'
				AND status='Active' $where");
				$result = $command->queryAll();*/

				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.* FROM `truck_driver` t1 WHERE t1.transporter_user_id='".trim($data['user_id'])."'	AND status='Active'  $where ORDER BY t1.id DESC $limit");
				$result = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `truck_driver` t1 WHERE t1.transporter_user_id='".trim($data['user_id'])."' AND status='Active' $where");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);

				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	public function actionTransporterTruckDriverDetails() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == ''
				|| !isset($data['id']) || trim($data['id']) == '') {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$result = TruckDriver::find()->where(['id' => $data['id'], 'status' => 'Active'])
				->orderBy(['id' => SORT_DESC])->one();
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	// actionTransporterQuotationHistoryList
	public function actionTransporterQuotationHistoryList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			    $limit = '';
				$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
				if(isset($data['page']) && $data['page']>0) {
					$limit = $this->_pageLimit($page);
				}
				$result=[];
				$where = '';
				if( isset($data['search_text']) && trim($data['search_text']) != '' )
				{
					$where = ' AND ('; 
					$where .= 'full_name LIKE "'.trim($data['search_text']).'%"
					or mobile_number LIKE "'.trim($data['search_text']).'%" or company_name LIKE "'.trim($data['search_text']).'%"';
					$where .= ') ';
				}
				/*$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.* FROM `loads` t1 WHERE id IN 
				( SELECT DISTINCT load_id FROM `quotation` t2 WHERE t2.user_id='".trim($data['user_id'])."' 
				AND t2.status='Open') $where ORDER BY t1.id DESC");
				$loads = $command->queryAll();*/

				//======================================//	
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.* FROM `loads` t1 WHERE id IN 
				( SELECT DISTINCT load_id FROM `quotation` t2 WHERE t2.user_id='".trim($data['user_id'])."' 
				AND t2.status='Open') $where ORDER BY t1.id DESC $limit");
				$loads = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `loads` t1 WHERE id IN ( SELECT DISTINCT load_id FROM `quotation` t2 WHERE t2.user_id='".trim($data['user_id'])."' AND t2.status='Open') $where");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);
				
				foreach($loads as $ld)
				{
					$quotations = Quotation::find()->select(['bid_amount'])->where(['load_id' => $ld['id']])
					->orderBy(['bid_amount' => SORT_ASC])->all();
					$ld['quotations'] = $quotations;
					$result[] = $ld;
				}
				
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	
	// actionTransporterQuotationEditMode
	public function actionTransporterQuotationEditMode() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
		    	|| !isset($data['load_id']) || trim($data['load_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$result=[];
				$dbConn = Yii::$app->db;
				/*$command = $dbConn->createCommand("SELECT t1.* FROM	`loads` t1 INNER JOIN `quotation` t2 ON
				t1.id=t2.load_id WHERE t1.id='".trim($data['load_id'])."' AND t1.assigned_status='Open'");*/
				$command = $dbConn->createCommand("SELECT t1.* FROM	`loads` t1 INNER JOIN `quotation` t2 ON
				t1.id=t2.load_id WHERE t1.id='".trim($data['load_id'])."'");
				$ld = $command->queryOne();
				
				$quotations = Quotation::find()->select(['*'])->where(['load_id' => $ld['id'],
				'user_id' => $data['user_id']])->orderBy(['bid_amount' => SORT_ASC])->one();
				$ld['quotations'] = $quotations;
				$result = $ld;
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	
	// actionTransporterQuotationEdit
	public function actionTransporterQuotationEdit() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
			|| !isset($data['load_id']) || trim($data['load_id']) == '' 
			|| !isset($data['bid_amount']) || trim($data['bid_amount']) == '' 
			) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$result=[];
				$dbConn = Yii::$app->db;
				$quotation = Quotation::find()->select(['*'])->where(['load_id' => $data['load_id'],
				'user_id' => $data['user_id']])->orderBy(['bid_amount' => SORT_ASC])->one();
				$quotation->bid_amount = trim($data['bid_amount']);
				if($quotation->save(false)) {
					return array('status' => true,'message'=>'Quotation Amount updated successfully.');
				}
				else {
					return array('status' => true,'message'=> $this->_errorMessage1());
				}	
	}
	
	// actionTransporterMyTripList
	public function actionTransporterMyTripList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			  	$limit = '';
				$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
				if(isset($data['page']) && $data['page']>0) {
					$limit = $this->_pageLimit($page);
				}
				$result=[];
				$where = '';
				if( isset($data['search_text']) && trim($data['search_text']) != '' )
				{
					$where = ' AND ('; 
					$where .= 'full_name LIKE "'.trim($data['search_text']).'%"
					or mobile_number LIKE "'.trim($data['search_text']).'%" or company_name LIKE "'.trim($data['search_text']).'%"';
					$where .= ') ';
				}
				/*$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.* FROM `loads` t1 INNER JOIN `quotation` t2 
				ON t1.id=t2.load_id WHERE t2.user_id='".trim($data['user_id'])."' AND t2.assigned_to_me='1' 
				AND t1.assigned_status='Assigned' AND ((t1.scheduling_type='One Time' AND 
				t1.reporting_date <'".date('Y-m-d')."') OR (t1.scheduling_type='Daily'))	AND 
				DATE(t1.updated_at) <'".date('Y-m-d')."' $where ORDER BY t1.id DESC");
				$loads = $command->queryAll();*/

				//======================================//	
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.* FROM `loads` t1 INNER JOIN `quotation` t2 
				ON t1.id=t2.load_id WHERE t2.user_id='".trim($data['user_id'])."' AND t2.assigned_to_me='1' 
				AND t1.assigned_status='Assigned' AND ((t1.scheduling_type='One Time' AND 
				t1.reporting_date <'".date('Y-m-d')."') OR (t1.scheduling_type='Daily')) AND 
				DATE(t1.updated_at) <'".date('Y-m-d')."' $where ORDER BY t1.id DESC $limit");
				$loads = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `loads` t1 INNER JOIN `quotation` t2 ON t1.id=t2.load_id WHERE t2.user_id='".trim($data['user_id'])."' AND t2.assigned_to_me='1' AND t1.assigned_status='Assigned' AND ((t1.scheduling_type='One Time' AND t1.reporting_date <'".date('Y-m-d')."') OR (t1.scheduling_type='Daily'))	AND DATE(t1.updated_at) <'".date('Y-m-d')."' $where ORDER BY t1.id DESC");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);

				foreach($loads as $ld)
				{
					$quotations = Quotation::find()->select(['bid_amount'])->where(['load_id' => $ld['id']])
					->orderBy(['bid_amount' => SORT_ASC])->all();
					$ld['quotations'] = $quotations;
					$result[] = $ld;
				}
				
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	
	public function actionTransporterActiveTripList() {

				\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
				if($this->_checkAutoLogout()){
					return $this->_autoLogout();
				}
				$data = \yii::$app->request->post();		
				if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
				}
				$limit = '';
				$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
				if(isset($data['page']) && $data['page']>0) {
					$limit = $this->_pageLimit($page);
				}
				$result=[];
				/*$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t2.id as load_id,t2.booking_id,t2.full_name,
				t2.mobile_number,t2.company_name,t2.source_address_line1,t2.source_address_line2,t2.source_pincode,
				t2.source_state,t2.source_district,t2.source_city,t2.source_area,t2.source_landmark,
				t2.destination_address_line1,t2.destination_address_line2,t2.destination_pincode,t2.destination_state,
				t2.destination_district,t2.destination_city,t2.destination_area,t2.destination_landmark,
				t2.load_type,t2.material_type,t2.weight,t2.truck_type,t2.no_of_trucks,t2.bid_closing_date,
				t2.bid_closing_time,t2.reporting_date,t2.reporting_time,t2.estimated_quotation_amount,t2.description,
				t2.assigned_status,t2.assigned_user_id,t2.assigned_full_name,t2.assigned_mobile_number,
				t2.assigned_company_name,assigned_bid_amount,
				t1.truck_id,t1.vehicle_no,t1.gps_installed,t1.weight,t1.truck_type,t1.rc_no,t1.license_no,
				t1.permit_type,t1.pollution_no,t1.rto_fitness_certificate_no,t1.local_registration_no,
				t1.registration_address,t1.registration_mobile,t1.office_landline_no,t1.email,
				t1.gross_weight,t1.trip_status,t1.trip_start_at,t1.trip_close_at
				FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned'
				AND t2.assigned_user_id='".$data['user_id']."' AND (t1.trip_status='Start Trip' OR t1.trip_status='Goods Delivered') ORDER BY t2.id DESC");
				$result = $command->queryAll();*/

				//======================================//	
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t2.id as load_id,t2.booking_id,t2.full_name,
				t2.mobile_number,t2.company_name,t2.source_address_line1,t2.source_address_line2,t2.source_pincode,
				t2.source_state,t2.source_district,t2.source_city,t2.source_area,t2.source_landmark,
				t2.destination_address_line1,t2.destination_address_line2,t2.destination_pincode,t2.destination_state,
				t2.destination_district,t2.destination_city,t2.destination_area,t2.destination_landmark,
				t2.load_type,t2.material_type,t2.weight,t2.truck_type,t2.no_of_trucks,t2.bid_closing_date,
				t2.bid_closing_time,t2.reporting_date,t2.reporting_time,t2.estimated_quotation_amount,t2.description,
				t2.assigned_status,t2.assigned_user_id,t2.assigned_full_name,t2.assigned_mobile_number,
				t2.assigned_company_name,assigned_bid_amount,
				t1.truck_id,t1.vehicle_no,t1.gps_installed,t1.weight,t1.truck_type,t1.rc_no,t1.license_no,
				t1.permit_type,t1.pollution_no,t1.rto_fitness_certificate_no,t1.local_registration_no,
				t1.registration_address,t1.registration_mobile,t1.office_landline_no,t1.email,
				t1.gross_weight,t1.trip_status,t1.trip_start_at,t1.trip_close_at
				FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned'	AND t2.assigned_user_id='".$data['user_id']."' AND (t1.trip_status='Start Trip' OR t1.trip_status='Goods Delivered') ORDER BY t2.id DESC $limit");
				$result = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned'
				AND t2.assigned_user_id='".$data['user_id']."' AND (t1.trip_status='Start Trip' OR t1.trip_status='Goods Delivered')");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);
				
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Active Trip Found','data'=>$result);
				}	
	}
	// CALL actionTripDetail
	
	// actionTransporterPreferredIndustryList
	public function actionTransporterPreferredIndustryList() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			  	$limit = '';
				$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
				if(isset($data['page']) && $data['page']>0) {
					$limit = $this->_pageLimit($page);
				}
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t1.first_name,t1.last_name,t1.mobile_number,t1.email,
				t2.company_name FROM `user` t1 INNER JOIN `industry` t2 ON t1.id=t2.user_id INNER JOIN 
				`preferred_list` t3 ON t1.id=t3.industry_user_id WHERE t1.user_level='3' AND t1.status='1' 
				AND t2.status='Active' AND t3.preferred_user_id='".$data['user_id']."' AND t3.status='Active'");
				$result = $command->queryAll();
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
	
	// actionTransporterPreferredIndustryDetail
	public function actionTransporterPreferredIndustryDetails() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
			|| !isset($data['id']) || trim($data['id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t1.first_name,t1.last_name,t1.mobile_number,t1.email,
				t2.company_name,t2.address_line1,t2.address_line2,t2.pincode,t2.state,t2.district,t2.city,t2.area FROM 
				`user` t1 INNER JOIN `industry` t2 ON t1.id=t2.user_id WHERE t1.id='".$data['id']."'");
				$result = $command->queryOne();
				
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}

	public function actionTransporterIndustryList() {

			  \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			  if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			  $data = \yii::$app->request->post();		
			  if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			  	$limit = '';
				$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
				if(isset($data['page']) && $data['page']>0) {
					$limit = $this->_pageLimit($page);
				}
				$where = '';
				if( isset($data['search_text']) && trim($data['search_text']) != '' )
				{
					$where = ' AND ('; 
					$where .= 't1.first_name LIKE "'.trim($data['search_text']).'%" or t1.last_name LIKE "'.trim($data['search_text']).'%"
					or t1.mobile_number LIKE "'.trim($data['search_text']).'%" or t2.company_name LIKE "'.trim($data['search_text']).'%"';
					$where .= ') ';
				}
				/*$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t1.first_name,t1.last_name,t1.mobile_number,t1.email,
				t2.company_name,t2.verified FROM `user` t1 INNER JOIN `industry` t2 ON 
				t1.id=t2.user_id WHERE t1.user_level='3' AND t1.status='1' AND t1.verified='1' AND t2.status='Active' $where ORDER BY t2.company_name ASC");
				$result = $command->queryAll();*/

				//======================================//	
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t1.first_name,t1.last_name,t1.mobile_number,t1.email, t2.company_name,t2.verified FROM `user` t1 INNER JOIN `industry` t2 ON 
				t1.id=t2.user_id WHERE t1.user_level='3' AND t1.status='1' AND t1.verified='1' AND t2.status='Active' $where ORDER BY t2.company_name ASC $limit");
				$result = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `user` t1 INNER JOIN `industry` t2 ON t1.id=t2.user_id WHERE t1.user_level='3' AND t1.status='1' AND t1.verified='1' AND t2.status='Active' $where");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);

				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Data Found','data'=>$result);
				}	
	}
//=========================================== AFTER TRUCK DRIVER LOGIN===============================================
	public function actionDriverActiveTrips() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }

			  	$limit = '';
				$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
				if(isset($data['page']) && $data['page']>0) {
					$limit = $this->_pageLimit($page);
				}
				$result=[];
				/*$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t2.id as load_id,t2.booking_id,t2.full_name,
				t2.mobile_number,t2.company_name,t2.source_address_line1,t2.source_address_line2,t2.source_pincode,
				t2.source_state,t2.source_district,t2.source_city,t2.source_area,t2.source_landmark,
				t2.destination_address_line1,t2.destination_address_line2,t2.destination_pincode,t2.destination_state,
				t2.destination_district,t2.destination_city,t2.destination_area,t2.destination_landmark,
				t2.load_type,t2.material_type,t2.weight,t2.truck_type,t2.no_of_trucks,t2.bid_closing_date,
				t2.bid_closing_time,t2.reporting_date,t2.reporting_time,t2.estimated_quotation_amount,t2.description,
				t2.assigned_status,t2.assigned_user_id,t2.assigned_full_name,t2.assigned_mobile_number,
				t2.assigned_company_name,assigned_bid_amount,
				t1.truck_id,t1.vehicle_no,t1.gps_installed,t1.weight,t1.truck_type,t1.rc_no,t1.license_no,
				t1.permit_type,t1.pollution_no,t1.rto_fitness_certificate_no,t1.local_registration_no,
				t1.registration_address,t1.registration_mobile,t1.office_landline_no,t1.email,
				t1.gross_weight,t1.trip_status,t1.trip_start_at,t1.trip_close_at
				FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned'
				AND t1.assigned_driver_user_id='".$data['user_id']."' AND (t1.trip_status='Pending' OR t1.trip_status='Start Trip') ORDER BY t2.id DESC");
				$result = $command->queryAll();*/

				//======================================//	
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t2.id as load_id,t2.booking_id,t2.full_name,
				t2.mobile_number,t2.company_name,t2.source_address_line1,t2.source_address_line2,t2.source_pincode,
				t2.source_state,t2.source_district,t2.source_city,t2.source_area,t2.source_landmark,
				t2.destination_address_line1,t2.destination_address_line2,t2.destination_pincode,t2.destination_state,
				t2.destination_district,t2.destination_city,t2.destination_area,t2.destination_landmark,
				t2.load_type,t2.material_type,t2.weight,t2.truck_type,t2.no_of_trucks,t2.bid_closing_date,
				t2.bid_closing_time,t2.reporting_date,t2.reporting_time,t2.estimated_quotation_amount,t2.description,
				t2.assigned_status,t2.assigned_user_id,t2.assigned_full_name,t2.assigned_mobile_number,
				t2.assigned_company_name,assigned_bid_amount,
				t1.truck_id,t1.vehicle_no,t1.gps_installed,t1.weight,t1.truck_type,t1.rc_no,t1.license_no,
				t1.permit_type,t1.pollution_no,t1.rto_fitness_certificate_no,t1.local_registration_no,
				t1.registration_address,t1.registration_mobile,t1.office_landline_no,t1.email,
				t1.gross_weight,t1.trip_status,t1.trip_start_at,t1.trip_close_at
				FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned'	AND t1.assigned_driver_user_id='".$data['user_id']."' AND (t1.trip_status='Pending' OR t1.trip_status='Start Trip') ORDER BY t2.id DESC");
				$result = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned' AND t1.assigned_driver_user_id='".$data['user_id']."' AND (t1.trip_status='Pending' OR t1.trip_status='Start Trip')");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Active Trip Found','data'=>$result);
				}	
	}
	public function actionTripDetails() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			  if(!isset($data['user_id']) || trim($data['user_id']) == ''
				 || !isset($data['id']) || trim($data['id']) == '') {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				//$result=[];
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t2.id as load_id,t2.booking_id,t2.full_name,
				t2.mobile_number,t2.company_name,t2.source_address_line1,t2.source_address_line2,t2.source_pincode,
				t2.source_state,t2.source_district,t2.source_city,t2.source_area,t2.source_landmark,
				t2.destination_address_line1,t2.destination_address_line2,t2.destination_pincode,t2.destination_state,
				t2.destination_district,t2.destination_city,t2.destination_area,t2.destination_landmark,
				t2.load_type,t2.material_type,t2.weight,t2.truck_type,t2.no_of_trucks,t2.bid_closing_date,
				t2.bid_closing_time,t2.reporting_date,t2.reporting_time,t2.estimated_quotation_amount,t2.description,
				t2.assigned_status,t2.assigned_user_id,t2.assigned_full_name,t2.assigned_mobile_number,
				t2.assigned_company_name,assigned_bid_amount,
				t1.truck_id,t1.vehicle_no,t1.gps_installed,t1.weight,t1.truck_type,t1.rc_no,t1.license_no,
				t1.permit_type,t1.pollution_no,t1.rto_fitness_certificate_no,t1.local_registration_no,
				t1.registration_address,t1.registration_mobile,t1.office_landline_no,t1.email,
				t1.gross_weight,t1.trip_status,t1.trip_start_at,t1.trip_close_at
				FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned'
				AND t1.id='".$data['id']."' ORDER BY t2.id DESC");
				$result = $command->queryOne();
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result);
				}
				else {
					return array('status' => true,'message'=>'No Data Found');
				}	
	}
	public function actionDriverStartTrip() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
			|| !isset($data['load_id']) || trim($data['load_id']) == '' 
			|| !isset($data['truck_id']) || trim($data['truck_id']) == '') {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = QuotationTruck::find()->where(['load_id' => $data['load_id'],'truck_id' => $data['truck_id'],
				'assigned_driver_user_id' => $data['user_id'],'trip_status' => 'Pending'])->one();
				if(!empty($model)) {
					$model->trip_status = 'Start Trip';
					if(isset($data['eWay_bill_no']))
					$model->eWay_bill_no = $data['eWay_bill_no'];	
					$model->trip_start_at = date('Y-m-d H:i:s');
					if($model->save(false)) {
					// START NOTIFICATION
						$load =Loads::findOne($data['load_id']);
						$title = 'Driver started trip. UniqueID #'.$load->booking_id;
				        $desc = 'Driver started trip. UniqueID #'.$load->booking_id;
				        $type = 'in_start_trip';
				        $sender_id = $data['user_id'];
						$receiver_id = $load->industry_user_id;
				        $this->_pushNotifification($title,$desc,$type,$sender_id,$receiver_id);
						$type = 'tr_start_trip';
						$receiver_id = $load->assigned_user_id;
				        $this->_pushNotifification($title,$desc,$type,$sender_id,$receiver_id);
		            // END NOTIFICATION
						return array('status' => true,'message'=>'Trip started successfully.');
					}
					else {
						return $this->_errorMessage1();
					}
				}
				else {
					return $this->_errorMessage1();
				}	
	}	
	public function actionDriverTripTracking() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) =='' 
				|| !isset($data['load_id']) || trim($data['load_id']) =='' 
				|| !isset($data['truck_id']) || trim($data['truck_id']) == ''
				|| !isset($data['latitude']) || trim($data['latitude']) ==''
				|| !isset($data['longitude']) || trim($data['longitude']) ==''
				) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = new TripTracking();	
				$model->load_id=$data['load_id'];
				$model->truck_id=$data['truck_id'];
				$model->latitude=$data['latitude'];
				$model->longitude=$data['longitude'];
				$model->driver_user_id=$data['user_id'];
				if($model->save(false)) {
					return array('status' => true,'message'=>'Data submitted successfully.');
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	public function actionDriverTripTrackingV2() {				
		$lng = $this->_getLanguage();
		\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

		if($this->_checkAuth()){
			$data = \yii::$app->request->post();		
			if( isset($data['user_id']) && trim($data['user_id']) !=''
			&& isset($data['load_id']) && trim($data['load_id']) !=''
			&& isset($data['truck_id']) && trim($data['truck_id']) !=''
			&& isset($data['locationData']) && !empty($data['locationData'])		
		 ) {								
				$user_id = trim($data['user_id']);
				$load_id = trim($data['load_id']);
				$truck_id = trim($data['truck_id']);
				$locationData = json_decode($data['locationData']);
				//if($this->_isAllowedGpsTracking($UserID,$VesselID,$TripID,$TokenNo))
				if(count($locationData)>0)
				{
					$IsSaved = false;
					foreach ($locationData as $loc) {
					
					$model = new TripTracking();	
					$model->load_id=$load_id;
					$model->truck_id=$truck_id;
					$model->driver_user_id=$user_id;
					$model->latitude=$loc->latitude;
					$model->longitude=$loc->longitude;
					$model->created_on=$loc->datetime;
					$model->updated_on=new \yii\db\Expression('NOW()'); //date('Y-m-d H:i:s'),
					$Exist = TripTracking::find()->where(['load_id' => $load_id, 'truck_id' => $truck_id, 'latitude' => $loc->latitude, 'longitude' => $loc->longitude])->one();
            		if (empty($Exist) && $model->save())
            		{
            			$IsSaved = true;
            		}	
            			
				}
					//if ($model->save())
					if ($IsSaved)
					{	
						return array('status' => true,'message'=> 'Data submitted successfully.');
					}
					else {
						return $this->_errorMessage1();
					} 
				}
				else
				{
					if($lng=='Gu')
						return array('status'=>false,'message'=>'તમને  વિનંતી કરવાની પરવાનગી નથી');
					elseif($lng=='Hi')
						return array('status'=>false,'message'=>'आपको अनुरोध करने की अनुमति नहीं है');
					else
						return array('status'=>false,'message'=>'You are not allowed to request');
				}
			}
			else {
				return $this->_errorMessage1();
			}
		}
		else {
			return $this->_authFailed();
		}
	}
	public function actionDriverGoodsDelivered() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' 
			|| !isset($data['load_id']) || trim($data['load_id']) == '' 
			|| !isset($data['truck_id']) || trim($data['truck_id']) == '') {
				return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
				$model = QuotationTruck::find()->where(['load_id' => $data['load_id'],'truck_id' => $data['truck_id'],
				'assigned_driver_user_id' => $data['user_id'],'trip_status' => 'Start Trip'])->one();
				if(!empty($model)) {
					$model->trip_status = 'Goods Delivered';
					$model->goods_delivered_at = date('Y-m-d H:i:s');
					if($model->save(false)) {
						// START NOTIFICATION
						$load =Loads::findOne($data['load_id']);
						$title = 'Driver delivered goods. UniqueID #'.$load->booking_id;
				        $desc = 'Driver delivered goods. UniqueID #'.$load->booking_id;
				        $type = 'in_goods_delivered';
						$sender_id = $data['user_id'];
						$receiver_id = $load->industry_user_id;
				        $this->_pushNotifification($title,$desc,$type,$sender_id,$receiver_id);
						$type = 'tr_goods_delivered';
						$receiver_id = $load->assigned_user_id;
				        $this->_pushNotifification($title,$desc,$type,$sender_id,$receiver_id);
						// END NOTIFICATION
						return array('status' => true,'message'=>'Goods delivered successfully.');
					}
					else {
						return $this->_errorMessage1();
					}
				}
				else {
					return $this->_errorMessage1();
				}	
	}
	public function actionDriverPastTrips() {

			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			if($this->_checkAutoLogout()){
				return $this->_autoLogout();
			}
			$data = \yii::$app->request->post();		
			if( !isset($data['user_id']) || trim($data['user_id']) == '' ) {
					return array('status'=>false,'message'=>'Some parameter is missing.');
			  }
			  	$limit = '';
				$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
				if(isset($data['page']) && $data['page']>0) {
					$limit = $this->_pageLimit($page);
				}

				$result=[];
				/*$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t2.id as load_id,t2.booking_id,t2.full_name,
				t2.mobile_number,t2.company_name,t2.source_address_line1,t2.source_address_line2,t2.source_pincode,
				t2.source_state,t2.source_district,t2.source_city,t2.source_area,t2.source_landmark,
				t2.destination_address_line1,t2.destination_address_line2,t2.destination_pincode,t2.destination_state,
				t2.destination_district,t2.destination_city,t2.destination_area,t2.destination_landmark,
				t2.load_type,t2.material_type,t2.weight,t2.truck_type,t2.no_of_trucks,t2.bid_closing_date,
				t2.bid_closing_time,t2.reporting_date,t2.reporting_time,t2.estimated_quotation_amount,t2.description,
				t2.assigned_status,t2.assigned_user_id,t2.assigned_full_name,t2.assigned_mobile_number,
				t2.assigned_company_name,assigned_bid_amount,
				t1.truck_id,t1.vehicle_no,t1.gps_installed,t1.weight,t1.truck_type,t1.rc_no,t1.license_no,
				t1.permit_type,t1.pollution_no,t1.rto_fitness_certificate_no,t1.local_registration_no,
				t1.registration_address,t1.registration_mobile,t1.office_landline_no,t1.email,
				t1.gross_weight,t1.trip_status,t1.trip_start_at,t1.trip_close_at
				FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned' 
				AND	t1.assigned_driver_user_id='".$data['user_id']."' AND (t1.trip_status='Goods Delivered' OR t1.trip_status='Close Trip') ORDER BY t2.id DESC");
				$result = $command->queryAll();*/
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("SELECT t1.id,t2.id as load_id,t2.booking_id,t2.full_name,
				t2.mobile_number,t2.company_name,t2.source_address_line1,t2.source_address_line2,t2.source_pincode,
				t2.source_state,t2.source_district,t2.source_city,t2.source_area,t2.source_landmark,
				t2.destination_address_line1,t2.destination_address_line2,t2.destination_pincode,t2.destination_state,
				t2.destination_district,t2.destination_city,t2.destination_area,t2.destination_landmark,
				t2.load_type,t2.material_type,t2.weight,t2.truck_type,t2.no_of_trucks,t2.bid_closing_date,
				t2.bid_closing_time,t2.reporting_date,t2.reporting_time,t2.estimated_quotation_amount,t2.description,
				t2.assigned_status,t2.assigned_user_id,t2.assigned_full_name,t2.assigned_mobile_number,
				t2.assigned_company_name,assigned_bid_amount,
				t1.truck_id,t1.vehicle_no,t1.gps_installed,t1.weight,t1.truck_type,t1.rc_no,t1.license_no,
				t1.permit_type,t1.pollution_no,t1.rto_fitness_certificate_no,t1.local_registration_no,
				t1.registration_address,t1.registration_mobile,t1.office_landline_no,t1.email,
				t1.gross_weight,t1.trip_status,t1.trip_start_at,t1.trip_close_at
				FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned' AND	t1.assigned_driver_user_id='".$data['user_id']."' AND (t1.trip_status = 'Goods Delivered' OR t1.trip_status='Close Trip') ORDER BY t2.id DESC $limit");
				$result = $command->queryAll();
				// PAGING		
				$command1 = $dbConn->createCommand("SELECT COUNT(t1.id) as count FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE t2.assigned_status='Assigned' AND t1.assigned_driver_user_id='".$data['user_id']."' AND (t1.trip_status='Goods Delivered' OR t1.trip_status='Close Trip')");
				$result1 = $command1->queryOne();
				$totalRecord = $result1['count'];
				$totalPage = ceil($totalRecord / LIMIT);
				if(!empty($result)) {
					return array('status' => true,'message'=>'Success','data'=>$result, 'totalRecord'=> $totalRecord, 'recordPerPage'=> LIMIT, 'totalPage'=> $totalPage, 'nextPage'=>$page+1);
				}
				else {
					return array('status' => true,'message'=>'No Trip Found','data'=>$result);
				}	
	}
	//================================== COMMON ===============================================
	public function actionTenderList() {
		
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		//if($this->_checkAuth()){
        if(true){
			$data = \yii::$app->request->post();
			$limit = '';
			$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
			if(isset($data['page']) && $data['page']>0) {
				$limit = $this->_pageLimit($page);
			}		

			if( isset($data) ) {				
				$path = BASE_URL .'uploads/tender/';
				$result = Tender::find()
				->select(['id','tender_title','tender_desc','DATE_FORMAT(created_on,"%d-%m-%Y") AS created_on' ,'concat("'.$path.'",tender_pdf) as tender_pdf','status'])
				->where("tender_pdf !=''")->andWhere(['status' => 'Active'])->orderBy(['id' => SORT_DESC])->limit(30)->all();
				return array('status' => true,'message'=>'success', 'data'=> $result);
			}
			else {
				return $this->_errorMessage1();
			}
		}
		else {
			return $this->_authFailed();
		}
    }
	public function actionCircularList() {
		
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		//if($this->_checkAuth()){
        if(true){
			$data = \yii::$app->request->post();
			$limit = '';
			$page = (isset($data['page']) && $data['page']>0)?$data['page']:'1';
			if(isset($data['page']) && $data['page']>0) {
				$limit = $this->_pageLimit($page);
			}		
			if( isset($data) ) {				
				$path = BASE_URL .'uploads/circular/';
				$result = Circular::find()
				->select([ 'id','circular_title','circular_desc','DATE_FORMAT(circular_date,"%d-%m-%Y") AS circular_date' ,'concat("'.$path.'",circular_pdf) as circular_pdf','status'])
				->where("circular_pdf !=''")->andWhere(['status' => 'Active'])->orderBy(['id' => SORT_DESC])->limit(30)->all();
				return array('status' => true,'message'=>'success', 'data'=> $result);
			}
			else {
				return $this->_errorMessage1();
			}
		}
		else {
			return $this->_authFailed();
		}
    }
	public function actionSubmitFeedback() {	
		
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();
			if(isset($data['subject']) && trim($data['subject']) !=''
			&& isset($data['description']) && trim($data['description']) !=''
			&& isset($data['user_id']) && trim($data['user_id']) !='') {				
				
				$subject = trim($data['subject']);
				$description = trim($data['description']);
				$user_id = trim($data['user_id']);
				$user = User::findOne($user_id);
					$model = new Feedback();			
					$model->subject=$subject;
					$model->description=$description;					
					$model->user_id=$user_id;
					$model->full_name=trim($user->first_name.' '.$user->last_name);
					$model->mobile_number=$user->mobile_number;
					//$model->created_at=new \yii\db\Expression('NOW()');	
					if ($model->save()){
						return array('status' => true,'message'=>'Your feedback submitted successfully.');
					}
					else {
						return $this->_errorMessage1();
					}
				
			}
			else {
					return $this->_errorMessage2();
			}
		
	}
	public function actionSubmitFeedbackWeb() {	
		
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();
			if(isset($data['subject']) && trim($data['subject']) !=''
			&& isset($data['description']) && trim($data['description']) !=''
			&& isset($data['full_name']) && trim($data['full_name']) !=''
			&& isset($data['mobile_number']) && trim($data['mobile_number']) !='') {				
				
				$subject = trim($data['subject']);
				$description = trim($data['description']);
				$full_name = trim($data['full_name']);
				$mobile_number = trim($data['mobile_number']);
				//$user_id = trim($data['user_id']);
				//$user = User::findOne($user_id);
					$model = new Feedback();			
					$model->subject=$subject;
					$model->description=$description;					
					//$model->user_id=$user_id;
					$model->full_name=$full_name;
					$model->mobile_number=$mobile_number;
					//$model->created_at=new \yii\db\Expression('NOW()');	
					if ($model->save(false)){
						return array('status' => true,'message'=>'Your feedback submitted successfully.');
					}
					else {
						return $this->_errorMessage1();
					}
				
			}
			else {
					return $this->_errorMessage2();
			}
		
	}
	public function actionSubmitGrievance() {	
		
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$data = \yii::$app->request->post();
			if(isset($data['user_id']) && trim($data['user_id']) !=''
			&& isset($data['user_type']) && trim($data['user_type']) !=''
			&& isset($data['grievance_user_id']) && trim($data['grievance_user_id']) !=''
			&& isset($data['grievance_user_type']) && trim($data['grievance_user_type']) !=''
			&& isset($data['comment']) && trim($data['comment']) !='') {				
				
					$model = new Grievance();			
					$model->user_id=trim($data['user_id']);
					$model->user_type=trim($data['user_type']);					
					$model->grievance_user_id=trim($data['grievance_user_id']);
					$model->grievance_user_type=trim($data['grievance_user_type']);
					$model->comment=trim($data['comment']);
					//$model->created_at=new \yii\db\Expression('NOW()');	
					if ($model->save()){
						return array('status' => true,'message'=>'Your Grievance submitted successfully.');
					}
					else {
						return $this->_errorMessage1();
					}
				
			}
			else {
				return $this->_errorMessage2();
			}
		
	}
	protected function getTime($ptime){
		
			$etime = time() - strtotime($ptime);
			if ($etime < 1)
			{
				return '0 sec';
			}

			$a = array( 365 * 24 * 60 * 60  =>  'yr',
						 30 * 24 * 60 * 60  =>  'mon',
							  24 * 60 * 60  =>  'day',
								   60 * 60  =>  'hr',
										60  =>  'min',
										 1  =>  'sec'
						);
			$a_plural = array( 'yr'   => 'years',
							   'mon'  => 'months',
							   'day'    => 'days',
							   'hr'   => 'hours',
							   'min' => 'minutes',
							   'sec' => 'seconds'
						);

			foreach ($a as $secs => $str)
			{
				$d = $etime / $secs;
				if ($d >= 1)
				{
					$r = round($d);
					return $r .' '. ($r > 1 ? $a_plural[$str] : $str);
				}
			}
	}
	protected function _checkAuth() {
		return true;
		$headers = Yii::$app->request->headers;        
		$token = $headers->get('TOKEN');
		$username=$headers->get('USERNAME');
		$password=$headers->get('PASSWORD');
		
        if (empty($token) || empty($username) || empty($password)) {
            return false;
        } else {
			if ($username == API_USERNAME_ && $password == API_PASSWORD_ && $token == API_TOKEN_) {
                return true;
            } else {
                return false;
            }
        }
    }

    protected function _getLanguage(){
		return $headers = 'en';
		//$headers = Yii::$app->request->headers;
		//return  $headers->get('LANGUAGE');		
	}
	protected function _getTheme(){
		$headers = Yii::$app->request->headers;
		return  $headers->get('THEME');		
	}
	protected function _checkAutoLogout() {
		//return false;		
		$headers = Yii::$app->request->headers;        
		$device_id=$headers->get('DEVICE-ID');
		$device_token=$headers->get('DEVICE-TOKEN');
		$device_type=$headers->get('DEVICE-TYPE');
		$device_model=$headers->get('DEVICE-MODEL');
		$device_network=$headers->get('DEVICE-NETWORK');
		$app_version=$headers->get('APP-VERSION');
		$api_version=$headers->get('API-VERSION');
		$user_id=$headers->get('USER-ID');

		if($user_id >0 && $device_token!='') {

			$usr = User::find()->select('*')->where(['id'=>$user_id])->one();
			if($usr['device_token']!=$device_token && $usr['device_token']!=NULL){
				//return true;
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("UPDATE user SET device_id='$device_id', device_token='$device_token', device_type='$device_type', device_model='$device_model', device_network='$device_network', app_version='$app_version', api_version='$api_version' WHERE id=".$user_id);
				$Updated =$command->execute();
				return false;
			}
			else {
				//if(DB=='db2') $dbConn = Yii::$app->db2; else $dbConn = Yii::$app->db;
				$dbConn = Yii::$app->db;
				$command = $dbConn->createCommand("UPDATE user SET device_id='$device_id', device_token='$device_token', device_type='$device_type', device_model='$device_model', device_network='$device_network', app_version='$app_version', api_version='$api_version' WHERE id=".$user_id);
				$Updated =$command->execute();
				return false;
			}
		}
		return false;
    }

    protected function _autoLogout() {
    	$lng = $this->_getLanguage();
		if($lng=='gu')
			return array('status'=>true, 'autologout' => 'yes','message'=>'તમે કેટલાક અન્ય ઉપકરણ પર સાઇન ઇન કર્યું છે અને હવે આ ઉપકરણથી આપમેળે સાઇન આઉટ થઈ જશે.');
		elseif($lng=='hi')
			return array('status'=>true, 'autologout' => 'yes','message'=>'आपने किसी अन्य डिवाइस में साइन-इन किया है और अब स्वचालित रूप से इस डिवाइस से साइन आउट हो जाएगा.');
		else
			return array('status'=>true, 'autologout' => 'yes','message'=>'You have signed in on some other device and now automatically signed out from this device.');
    }
    protected function _authFailed(){

		$lng = $this->_getLanguage();
		if($lng == 'gu')
		return array('status' => false,'message' => 'તમને આ સ્રોતને ઍક્સેસ કરવાની મંજૂરી નથી.');
		elseif($lng == 'hi')
		return array('status' => false,'message' => 'आपको इस संसाधन तक पहुंचने की अनुमति नहीं है.');
		else
		return array('status' => false,'message' => 'You are not allowed to access this resource.');		
	}
	
	protected function _errorMessage1(){
		
		$lng = $this->_getLanguage();
		if($lng == 'gu')
		return array('status'=>false,'message'=>'કંઈક ખોટું થયું!');
		elseif($lng == 'hi')
		return array('status'=>false,'message'=>'कुछ गलत हो गया।');
		else
		return array('status'=>false,'message'=>'Something went wrong!');
	}
	
	protected function _errorMessage2(){

		$lng = $this->_getLanguage();
		if($lng == 'gu')
		return array('status'=>false,'message'=>'કંઈક ખોટું થયું!!');
		elseif($lng == 'hi')
		return array('status'=>false,'message'=>'कुछ गलत हो गया।');
		else
		return array('status'=>false,'message'=>'Something went wrong!!');
	}

	protected function _noRecordFound(){

		$lng = $this->_getLanguage();
		if($lng == 'gu')
		return array('status'=>false,'message'=>'રેકોર્ડ અસ્તિત્વમાં નથી!');
		elseif($lng == 'hi')
		return array('status'=>false,'message'=>'रिकॉर्ड मौजूद नहीं है!');
		else
		return array('status'=>false,'message'=>'Record does not exist!');
	}

	protected function _errorOnSaveMessage(){

		$lng = $this->_getLanguage();
		if($lng=='gu')
		return array('status'=>false,'message'=>'કંઈક ખોટું થયું. ભૂલ રેકોર્ડ સાચવતી વખતે!');
		elseif($lng=='hi')
		return array('status'=>false,'message'=>'कुछ गलत हो गया। त्रुटि रिकॉर्ड सहेजते समय!');
		else
		return array('status'=>false,'message'=>'Something went wrong. Error while saving record!!');
	}

	protected function _dataSavedMessage(){

		$lng = $this->_getLanguage();
		if($lng == 'gu')
		$result = 'તમારી માહિતી સફળતાપૂર્વક સબમિટ કરવામાં આવી છે.';
		elseif($lng == 'hi')
		$result = 'आपकी जानकारी सफलतापूर्वक सबमिट की गई है।';
		else
		$result = 'Data is submitted Successfully.';
		return $result;
	}

	protected function _successMessage(){

		$lng = $this->_getLanguage();
		if($lng == 'gu')
		$result = 'સફળતાપૂર્વક કરી!';
		elseif($lng == 'hi')
		$result = 'सफलतापूर्वक किया गया!';
		else
		$result = 'Successfully done!';
		return $result;
	}
	protected function _passwordChangedMessage(){

		$lng = $this->_getLanguage();
		if($lng == 'gu')
		$result = 'પાસવર્ડ સફળતાપૂર્વક બદલવામાં આવ્યો છે.';
		elseif($lng == 'hi')
		$result = 'पासवर्ड सफलतापूर्वक बदल दिया गया है.';
		else
		$result = 'Password has been changed successfully.';
		return $result;
	}

	protected function _oldPasswordMessage(){

		$lng = $this->_getLanguage();
		if($lng == 'gu')
		$result = 'કૃપા કરીને જૂનો પાસવર્ડ સાચો દાખલ કરો.';
		elseif($lng == 'hi')
		$result = 'कृपया सही पुराना पासवर्ड डालें.';
		else
		$result = 'Please enter correct old password.';
		return $result;
	}

	protected function _oldNewPasswordMessage(){

		$lng = $this->_getLanguage();
		if($lng == 'gu')
		$result = 'જૂનો પાસવર્ડ અને નવો પાસવર્ડ સમાન ના હોવો જોઈ.';
		elseif($lng == 'hi')
		$result = 'पुराना पासवर्ड और नवा पासवर्ड वही नहीं होना चाइये.';
		else
		$result = 'Old password and new password cannot be same.';
		return $result;
	}
	protected function _pushNotifification($title,$desc,$type,$sender_id,$receiver_id,$push=true) {

    	/*$not = new Notifications();
		$not->title=$title;
		$not->description=$desc;
		$not->action=$type;
		$not->owner_id=$owner_id;
		$not->user_id=$user_id;
		$not->created_by=$created_by;
		$not->created_on=date('Y-m-d H:i:s');
		$not->status='Active';
		$not->save();*/
		if($push) {
			$user = User::findOne($receiver_id);
			if($user['device_type']=='android') {
			   $result = $this->_sendToAndroid($user['device_token'],$title,$type,'1');
			}
		}
    }
    protected function _pageLimit($page=1) {
		$start = LIMIT * ($page-1); // $data['page'] received '1' as 1st page
		$end = LIMIT;
		return $limit = "LIMIT ". $start . "," .$end;
	}
	protected function _sendToAndroid($deviceToken,$message,$type,$badge)
    {
    	 //API URL of FCM
	    $url = 'https://fcm.googleapis.com/fcm/send';
	    $api_key = ANDROID_PUSH_KEY_ ;
	                
	    $fields = array (
	        'registration_ids' => array (
	                $deviceToken
	        ),
	        'data' => array (
	                "message" => $message,
	                "type" => $type
	        )
	    );

	    //header includes Content type and api key
	    $headers = array(
	        'Content-Type:application/json',
	        'Authorization:key='.$api_key
	    );
	                
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	    $result = curl_exec($ch);
	    // if ($result === FALSE) {
	    //     die('FCM Send Error: ' . curl_error($ch));
	    // }
	    curl_close($ch);
        $data = json_decode($result);
        return $data->success;
    }

    protected function _sendToIphone($deviceToken,$message,$type,$badge,$enableNotification=1)
    {   

        $final_message = $message;
        $passphrase = '';
        $ctx = stream_context_create();
        
        stream_context_set_option($ctx, 'ssl', 'local_cert', '../include/apns-dev-cert-ws.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', '');
        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp){
            //exit("Failed to connect: $err $errstr" . PHP_EOL);
        }
       // var_dump($fp); 
        //$type = ($data['type']!='')?$data['type']:'0';
            $sound = 'default';
            if($data['image']) // image notification
            {   
                $body['aps'] = array(
                'alert' => stripslashes($final_message),
                'title' => APP_TITLE,
                'type'=> $type,
                'sound' => $sound,
                "mutable-content"=> 1,
                        "category"=> "rich-apns",
                "image-url"=> $data['image'],
                'badge' => intval($badge)
                );
                $body['att'] = array('id' => $data['image']);   
            }
            else
            {
                $body['aps'] = array(
                'alert' => stripslashes($final_message),
                'title' => APP_TITLE,
                'type'=> $type,
                'sound' => $sound,
                "mutable-content"=> 1,
                "category"=> "rich-apns",
                "image-url"=> "",
                'badge' => intval($badge)
                );
                $body['att'] = array('id' => "");   
            }   
        $payload = json_encode($body);
                
        $msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack('n', strlen($payload)) . $payload;
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);
        if(!$result)
        return "0";
        else
        return $result;
    }
	protected function _sendTextSMS($mobile,$message) {	

		//return true;
		if($mobile != '' && $message != ''){
			$baseUri = 'https://alerts.solutionsinfini.com/api/v4';
	 		$params = array(
	 			'api_key' => SMS_API_KEY,
	            'message' => $message,
	            'to'      => $mobile,
	            'method'  => 'sms',
	            'sender'  => SMS_SENDER,
	            'format'  => 'php',
	            'unicode' => '0',
	        );
	 		$request_url = $baseUri.'/?'.http_build_query($params);
	        $request = curl_init($request_url);
	        curl_setopt($request,CURLOPT_RETURNTRANSFER,true );
	        curl_setopt($request,CURLOPT_SSL_VERIFYPEER,false);
	        $response = curl_exec($request);
			$res = unserialize($response);
	        return $res; 
		}
		else
			return false;
		
	}
}
<?php
			// https://oidcparivahansuvidha.com/pages/statistics_cron.php
			include("include/config.php");

			// INDUSTRY
			$sql = mysqli_query($dbConn,"select count(*) as cnt from industry t1 where district='Dadra and Nagar Haveli' and status='Active' and 
			first_name not like 'test%' and last_name not like 'test%' and company_name not like 'test%'");	
			$row = mysqli_fetch_assoc($sql);
			$TotalIndustryDnh = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from industry t1 where district='Daman' and status='Active' and 
			first_name not like 'test%' and last_name not like 'test%' and company_name not like 'test%'");	
			$row = mysqli_fetch_assoc($sql);
			$TotalIndustryDaman = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from industry t1 where district='Diu' and status='Active' and 
			first_name not like 'test%' and last_name not like 'test%' and company_name not like 'test%'");	
			$row = mysqli_fetch_assoc($sql);
			$TotalIndustryDiu = $row['cnt'];
			
			// TRANSPORTER
			$sql = mysqli_query($dbConn,"select count(*) as cnt from transporter t1 where district='Dadra and Nagar Haveli' and status='Active' and 
			first_name not like 'test%' and last_name not like 'test%' and company_name not like 'test%'");	
			$row = mysqli_fetch_assoc($sql);
			$TotalTransporterDnh = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from transporter t1 where district='Daman' and status='Active' and 
			first_name not like 'test%' and last_name not like 'test%' and company_name not like 'test%'");	
			$row = mysqli_fetch_assoc($sql);
			$TotalTransporterDaman = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from transporter t1 where district='Diu' and status='Active' and 
			first_name not like 'test%' and last_name not like 'test%' and company_name not like 'test%'");	
			$row = mysqli_fetch_assoc($sql);
			$TotalTransporterDiu = $row['cnt'];
			
			// TOTAL LOADS
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district like 'Dadra%' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%'");	
			$row = mysqli_fetch_assoc($sql);
			$TotalLoadsDnh = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district='Daman' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%'");	
			$row = mysqli_fetch_assoc($sql);
			$TotalLoadsDaman = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district='Diu' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%'");	
			$row = mysqli_fetch_assoc($sql);
			$TotalLoadsDiu = $row['cnt'];
			
			// TODAYS LOADS
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district like 'Dadra%' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%' and DATE(created_at)='".date('Y-m-d')."'");	
			$row = mysqli_fetch_assoc($sql);
			$TodayLoadsDnh = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district='Daman' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%' and DATE(created_at)='".date('Y-m-d')."'");	
			$row = mysqli_fetch_assoc($sql);
			$TodayLoadsDaman = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district='Diu' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%' and DATE(created_at)='".date('Y-m-d')."'");	
			$row = mysqli_fetch_assoc($sql);
			$TodayLoadsDiu = $row['cnt'];
			
			// OPEN LOADS
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district like 'Dadra%' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%' and assigned_status='Open'");	
			$row = mysqli_fetch_assoc($sql);
			$OpenLoadsDnh = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district='Daman' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%' and assigned_status='Open'");	
			$row = mysqli_fetch_assoc($sql);
			$OpenLoadsDaman = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district='Diu' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%' and assigned_status='Open'");	
			$row = mysqli_fetch_assoc($sql);
			$OpenLoadsDiu = $row['cnt'];
			
			// ASSIGNED LOADS
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district like 'Dadra%' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%' and assigned_status='Assigned'");	
			$row = mysqli_fetch_assoc($sql);
			$AssignedLoadsDnh = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district='Daman' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%' and assigned_status='Assigned'");	
			$row = mysqli_fetch_assoc($sql);
			$AssignedLoadsDaman = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district='Diu' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%' and assigned_status='Assigned'");	
			$row = mysqli_fetch_assoc($sql);
			$AssignedLoadsDiu = $row['cnt'];
			
			// CLOSED LOADS
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district like 'Dadra%' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%' and assigned_status='Closed'");	
			$row = mysqli_fetch_assoc($sql);
			$ClosedLoadsDnh = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district='Daman' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%' and assigned_status='Closed'");	
			$row = mysqli_fetch_assoc($sql);
			$ClosedLoadsDaman = $row['cnt'];
			
			$sql = mysqli_query($dbConn,"select count(*) as cnt from loads t1 where source_district='Diu' and status='Active' and 
			full_name not like 'test%' and company_name not like 'test%' and assigned_status='Closed'");	
			$row = mysqli_fetch_assoc($sql);
			$ClosedLoadsDiu = $row['cnt'];
			
			// ACTIVE TRIP
			$sql = mysqli_query($dbConn,"SELECT count(t1.id) as cnt
				FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE source_district like 'Dadra%' and 
			full_name not like 'test%' and company_name not like 'test%' AND t2.assigned_status='Assigned'	
				AND (t1.trip_status='Start Trip' OR t1.trip_status='Goods Delivered')");
			$row = mysqli_fetch_assoc($sql);
			$ActiveTripDnh = $row['cnt'];
			$sql = mysqli_query($dbConn,"SELECT count(t1.id) as cnt
				FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE source_district='Daman' and 
			full_name not like 'test%' and company_name not like 'test%' AND t2.assigned_status='Assigned'	
				AND (t1.trip_status='Start Trip' OR t1.trip_status='Goods Delivered')");
			$row = mysqli_fetch_assoc($sql);
			$ActiveTripDaman = $row['cnt'];
			$sql = mysqli_query($dbConn,"SELECT count(t1.id) as cnt
				FROM `quotation_truck` t1 INNER JOIN `loads` t2 ON t1.load_id=t2.id WHERE source_district='Diu' and 
			full_name not like 'test%' and company_name not like 'test%' AND t2.assigned_status='Assigned'	
				AND (t1.trip_status='Start Trip' OR t1.trip_status='Goods Delivered')");
			$row = mysqli_fetch_assoc($sql);
			$ActiveTripDiu = $row['cnt'];
			
			$jsonArray['TotalIndustryDnh'] = ($TotalIndustryDnh >0)? $TotalIndustryDnh: 0;
			$jsonArray['TotalIndustryDaman'] = ($TotalIndustryDaman >0)? $TotalIndustryDaman: 0;
			$jsonArray['TotalIndustryDiu'] = ($TotalIndustryDiu >0)? $TotalIndustryDiu: 0;
			
			$jsonArray['TotalTransporterDnh'] = ($TotalTransporterDnh >0)? $TotalTransporterDnh: 0;
			$jsonArray['TotalTransporterDaman'] = ($TotalTransporterDaman >0)? $TotalTransporterDaman: 0;
			$jsonArray['TotalTransporterDiu'] = ($TotalTransporterDiu >0)? $TotalTransporterDiu: 0;
			
			$jsonArray['TotalLoadsDnh'] = ($TotalLoadsDnh >0)? $TotalLoadsDnh: 0;
			$jsonArray['TotalLoadsDaman'] = ($TotalLoadsDaman >0)? $TotalLoadsDaman: 0;
			$jsonArray['TotalLoadsDiu'] = ($TotalLoadsDiu >0)? $TotalLoadsDiu: 0;
			
			$jsonArray['TodaysLoadsDnh'] = ($TodaysLoadsDnh >0)? $TodaysLoadsDnh: 0;
			$jsonArray['TodaysLoadsDaman'] = ($TodaysLoadsDaman >0)? $TodaysLoadsDaman: 0;
			$jsonArray['TodaysLoadsDiu'] = ($TodaysLoadsDiu >0)? $TodaysLoadsDiu: 0;
			
			$jsonArray['OpenLoadsDnh'] = ($OpenLoadsDnh >0)? $OpenLoadsDnh: 0;
			$jsonArray['OpenLoadsDaman'] = ($OpenLoadsDaman >0)? $OpenLoadsDaman: 0;
			$jsonArray['OpenLoadsDiu'] = ($OpenLoadsDiu >0)? $OpenLoadsDiu: 0;
			
			$jsonArray['AssignedLoadsDnh'] = ($AssignedLoadsDnh >0)? $AssignedLoadsDnh: 0;
			$jsonArray['AssignedLoadsDaman'] = ($AssignedLoadsDaman >0)? $AssignedLoadsDaman: 0;
			$jsonArray['AssignedLoadsDiu'] = ($AssignedLoadsDiu >0)? $AssignedLoadsDiu: 0;
			
			$jsonArray['ClosedLoadsDnh'] = ($ClosedLoadsDnh >0)? $ClosedLoadsDnh: 0;
			$jsonArray['ClosedLoadsDaman'] = ($ClosedLoadsDaman >0)? $ClosedLoadsDaman: 0;
			$jsonArray['ClosedLoadsDiu'] = ($ClosedLoadsDiu >0)? $ClosedLoadsDiu: 0;
			
			$jsonArray['ActiveTripDnh'] = ($ActiveTripDnh >0)? $ActiveTripDnh: 0;
			$jsonArray['ActiveTripDaman'] = ($ActiveTripDaman >0)? $ActiveTripDaman: 0;
			$jsonArray['ActiveTripDiu'] = ($ActiveTripDiu >0)? $ActiveTripDiu: 0;
			
			mysqli_query($dbConn,"update dashboard_statistics set data='".json_encode($jsonArray)."'");
			mysqli_close($dbConn);
			$jsonArray1['Success'] = true;
			$jsonArray1['Message'] = 'Successful';
			//echo json_encode($jsonArray1);
?>

<?php

namespace backend\controllers;

use Yii;
use app\models\Circular;
use app\models\CircularSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
/**
 * CircularController implements the CRUD actions for Circular model.
 */
class CircularController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
             /*'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $action_name = \Yii::$app->controller->action->id;                           
                            
                            $accessible_actions_list = \Yii::$app->common->getAccessRulesFromSession();
                            

                            if (in_array($action_name, $accessible_actions_list)) {
                                return true;
                            } else {
                                return false;
                            }
                        },
                        'denyCallback' => function ($rule, $action) {
                            return $this->redirect(Yii::$app->urlManager->createUrl('site/index'));
                        },
                    ],
                    // deny all guest users
                    [
                        'allow' => false,
                        'roles' => ['?'],
                        'denyCallback' => function ($rule, $action) {
                            return $this->redirect(Yii::$app->urlManager->createUrl('site/index'));
                        },
                    ],
                ],
            ],*/
        ];
    }

    /**
     * Lists all Circular models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CircularSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Circular model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Circular model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Circular();

        if (Yii::$app->request->isPost) {
            if (empty($validate)) {

                foreach ($_POST['Circular'] as $key => $val) {
                    if (!empty($_POST['Circular'][$key])) {
                        $model->$key = $_POST['Circular'][$key];
                    }
                }
                $profile_pic = $_FILES['Circular']['name']['circular_pdf'];

                if (!empty($profile_pic)) {
                    $image = UploadedFile::getInstance($model, 'circular_pdf');

                    $extensions = explode(".", $image->name);
                    $extension = end($extensions);
                    $new_image_name = "Circular_".date('dmYHis'). ".{$extension}";

                    $original_path = UPLOAD_DIR_PATH. 'circular/'  . $new_image_name;
                    //$thumb_path = UPLOAD_DIR_PATH. 'fishing_zone/thumbnail/'  . $new_image_name;

                    $upload_pic = $image->saveAs($original_path);
                    if (file_exists($original_path)) {
                        $model->circular_pdf = $new_image_name;
                        //Start: For delete previous image
                        if (!empty($pre_profile_picture) && file_exists(UPLOAD_DIR_PATH. 'circular/'  . $pre_profile_picture)) {
                            unlink(UPLOAD_DIR_PATH. 'circular/'  . $pre_profile_picture);
                        }
                        //#unlink(PROFILE_PICTURE_ORIGINAL.$pre_profile_picture);
                       /* if (!empty($pre_profile_picture) && file_exists(UPLOAD_DIR_PATH. 'fishing_zone/thumbnail/'  . $pre_profile_picture)) {
                            unlink(UPLOAD_DIR_PATH. 'fishing_zone/thumbnail/'  . . $pre_profile_picture);
                        }*/
                        //End: For delete previous image
                        /*Image::thumbnail($original_path, THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT)
                                ->save($thumb_path, ['quality' => 100]);*/
                    } /*else {
                        $model->fishing_zone_pdf = $pre_profile_picture;
                    }*/
                }

                if ($model->save(False)) {
                    \Yii::$app->getSession()->setFlash('success', "Data has been updated successfully.");
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('circular/index/'));
                    Yii::$app->end();
                } else {
                    \Yii::$app->getSession()->setFlash('error', "Data not updated.");
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('circular/index/'));
                    Yii::$app->end();
                }
            } else {

                if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            }
        }
else {

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
}

    /**
     * Updates an existing Circular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
         if (Yii::$app->request->isPost) {
            if (empty($validate)) {

                foreach ($_POST['Circular'] as $key => $val) {
                    if (!empty($_POST['Circular'][$key])) {
                        $model->$key = $_POST['Circular'][$key];
                    }
                }
                $profile_pic = $_FILES['Circular']['name']['circular_pdf'];

                if (!empty($profile_pic)) {
                    $image = UploadedFile::getInstance($model, 'circular_pdf');

                    $extensions = explode(".", $image->name);
                    $extension = end($extensions);
                    $new_image_name = "Circular_".date('dmYHis'). ".{$extension}";

                    $original_path = UPLOAD_DIR_PATH. 'circular/'  . $new_image_name;
                    //$thumb_path = UPLOAD_DIR_PATH. 'fishing_zone/thumbnail/'  . $new_image_name;

                    $upload_pic = $image->saveAs($original_path);
                    if (file_exists($original_path)) {
                        $model->circular_pdf = $new_image_name;
                        //Start: For delete previous image
                        if (!empty($pre_profile_picture) && file_exists(UPLOAD_DIR_PATH. 'circular/'  . $pre_profile_picture)) {
                            unlink(UPLOAD_DIR_PATH. 'circular/'  . $pre_profile_picture);
                        }
                        //#unlink(PROFILE_PICTURE_ORIGINAL.$pre_profile_picture);
                       /* if (!empty($pre_profile_picture) && file_exists(UPLOAD_DIR_PATH. 'fishing_zone/thumbnail/'  . $pre_profile_picture)) {
                            unlink(UPLOAD_DIR_PATH. 'fishing_zone/thumbnail/'  . . $pre_profile_picture);
                        }*/
                        //End: For delete previous image
                        /*Image::thumbnail($original_path, THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT)
                                ->save($thumb_path, ['quality' => 100]);*/
                    } /*else {
                        $model->fishing_zone_pdf = $pre_profile_picture;
                    }*/
                }

                if ($model->save(False)) {
                    \Yii::$app->getSession()->setFlash('success', "Data has been updated successfully.");
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('circular/index/'));
                    Yii::$app->end();
                } else {
                    \Yii::$app->getSession()->setFlash('error', "Data not updated.");
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('circular/index/'));
                    Yii::$app->end();
                }
            } else {

                if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            }
        } else {

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}

    /**
     * Deletes an existing Circular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Circular model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Circular the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Circular::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

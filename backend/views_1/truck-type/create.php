<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TruckType */

$this->title = Yii::t('app', 'Create Truck Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Truck Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="truck-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

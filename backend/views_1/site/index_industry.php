<!-- Content Header (Page header) -->
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
?>
<!--<meta http-equiv="refresh" content="60">-->
<style>
.fc-right{  display:none;  }
.fc-center{ float:right; }
</style>
<?php //echo Yii::$app->common->get_flash_message('success', Yii::$app->session->getFlash('success')); ?>
<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="row">
        <!-- <div  class="col-lg-2 col-md-2 col-sm-12 col-xs-12 margin_bottom_30  top-btn"><h1><?php echo $this->title = Yii::t('app', 'Dashboard'); ?></h1></div> -->
            
        <div class="col-md-12 col-sm-12 col-xs-12 dashboard-box">
            
      <div class="row">
    
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="<?php echo Yii::$app->urlManager->createUrl('trip-diesel-request/index');  ?>" class="small-box-footer">
                <div class="info-box bg-dash-sec">
                    <!--<span class="info-box-icon bg-blue"><i class="fa fa-file-text-o"></i></span>-->
                    <span class="icon dashbord_dieselrequest"></span>
                    <div class="info-box-content">
                        <span class="info-box-number"><?=$currentBookingCount?></span>
                        <span class="info-box-text"> CURRENT BOOKING </span>
                       
                    </div>
                    <!-- /.info-box-content -->
                </div>
                </a>
                <!-- /.info-box -->
          </div>

          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="<?php echo Yii::$app->urlManager->createUrl('trip-go-out-request/index');?>" class="small-box-footer">
                <div class="info-box bg-dash-sec">
                    <!--<span class="info-box-icon bg-blue"><i class="fa fa-file-text-o"></i></span>-->
                    <span class="icon dashbord_goout"><!--<img src="../../uploads/go-out.jpg" />--></span>
                    <div class="info-box-content">
                        <span class="info-box-number"><?=$activeTripCount?></span>
                        <span class="info-box-text">ACTIVE TRIP</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                </a>
                <!-- /.info-box -->
          </div>

          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="<?php echo Yii::$app->urlManager->createUrl('trip-return-request/index');  ?>" class="small-box-footer">
                <div class="info-box bg-dash-sec">
                    <!--  <span class="info-box-icon bg-blue"><i class="fa fa-file-text-o"></i></span>-->
                      <span class="icon dashbord_returnreq"><!--<img src="../../uploads/returnrequest.jpg" />--></span>
                    <div class="info-box-content">
                        <span class="info-box-number"><?=$postedLoadCount?></span>
                
                        <span class="info-box-text">POSTED LOAD</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                </a>
                <!-- /.info-box -->
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="<?php echo Yii::$app->urlManager->createUrl('owners/index');  ?>" class="small-box-footer">
                <div class="info-box bg-dash-sec">
                     <!--<span class="info-box-icon bg-blue"><i class="fa fa-file-text-o"></i></span>
                   <span class="bg_blue icon"> <img src="../../uploads/alert.jpg" /></span>-->
                    <span class="icon dashbord_veseel"><!--<img src="../../uploads/veseel.jpg" />--></span>
                     <div class="info-box-content">
                        <span class="info-box-number"><?=$transporterCount?></span>
                        <span class="info-box-text">TRANSPORTER</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                </a>

                <!-- /.info-box -->
            </div>

         
    </div>
  </div>

</div>
    <!-- /.row -->
</section>


<style type="text/css">
    .fc button{padding: 0 0.4em;}
    .fc-center h2{ font-size: 20px; margin-top: 10px;}
    .fc-toolbar.fc-header-toolbar{margin-bottom: 0px;}
    .fc-basic-view .fc-body .fc-row {min-height: 0;}
    .fc-scroller{overflow-y: auto !important;height: 216px !important;}
    .content-header h1{padding-left: 13px;}
    .info-box-text {color: #fff;}
    .info-box-number {color: #fff;}
    div.summary{display: none}
</style>

<script>
$(document).ready(function(){
    
    $('.fc-today-button').text('Today');
    
});
</script>
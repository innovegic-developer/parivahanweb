<?php
//use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\models\User;
use yii\web\Response;
use yii\web\UploadedFile;
$this->title = Yii::t('app', 'Update Profile');
?>
<!-- <section class="content-header">
    <h1>
        <?php echo 'Update Profile' ?>
    </h1>
</section> -->
<!-- Main content -->
<!-- <section class="content"> -->
    <div class="row">
         <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="row col-xs-12 col-sm-12 col-md-12 col-xl-12 alert alert-error alert-dismissable" style="width: 97%; margin-left: 15px;">
                <div class="col-md-12">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
                </div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="row col-xs-12 col-sm-12 col-md-12 col-xl-12 alert alert-success alert-dismissable" style="width: 97%; margin-left: 15px;">
                <div class="col-md-12">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
                </div>
            <?php endif; ?>
        <div class="col-xs-12">

            <div class="box box-primary">
                <div class="box-body table-responsive">

                    <?php
                    $form = ActiveForm::begin(['enableAjaxValidation' => FALSE,
                                'enableClientValidation' => FALSE,
                                'options' => ['enctype' => 'multipart/form-data']]);
                    ?>


                    <div class="box-body">


                        <div class="form-group">
                            <?=
                            $form->field($model, 'first_name')->textInput(
                                    ['placeholder' => 'First name',
                                        'maxlength' => 32])->label('First name')
                            ?>
                        </div>

                        <div class="form-group">
                            <?=
                            $form->field($model, 'last_name')->textInput(
                                    ['placeholder' => 'Last name',
                                        'maxlength' => 32])->label('Last name')
                            ?>
                        </div>
						
						<div class="form-group">
                            <?=
                            $form->field($model, 'mobile_number')->textInput(
                                    ['placeholder' => 'Mobile Number',
                                        'maxlength' => 10, 'type' => 'number'])->label('Mobile Number')
                            ?>
                        </div>
						
						<div class="form-group">
                            <?=
                            $form->field($model, 'email')->textInput(
                                    ['placeholder' => 'Email',
                                        'maxlength' => 32])->label('Email')
                            ?>
                        </div>

                        <div class="form-group">
                            <?=
                                    $form->field($model, 'user_image')->fileInput()
                                    ->label('Profile picture')
                            ?>
                        </div>

                        <div class="form-group">
                            <?php
                            if (!empty($model->user_image)) {
                                $profile_img_path = SITE_ABS_UPLOAD_PATH . 'profile/' . $model->user_image;
                            } else {
                                $profile_img_path = Yii::$app->request->baseUrl . "/web/dist/img/avatar.png";
                            }
                            //p($profile_img_path);
                            ?>
                            <img src="<?php echo $profile_img_path; ?>" alt="User Image" width="120px" height="120px"/>                        
                        </div>

                        <div class="box-footer1">                    
                            <div class="form-group">
                                <?= Html::submitButton('Update Profile', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- </section> -->
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IndustryAddresses */

$this->title = Yii::t('app', 'Create Industry Addresses');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Industry Addresses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="industry-addresses-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

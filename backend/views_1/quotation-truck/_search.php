<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QuotationTruckSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="quotation-truck-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'quotation_id') ?>

    <?= $form->field($model, 'load_id') ?>

    <?= $form->field($model, 'truck_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'vehicle_no') ?>

    <?php // echo $form->field($model, 'gps_installed') ?>

    <?php // echo $form->field($model, 'weight') ?>

    <?php // echo $form->field($model, 'truck_type') ?>

    <?php // echo $form->field($model, 'rc_no') ?>

    <?php // echo $form->field($model, 'rc_photo') ?>

    <?php // echo $form->field($model, 'license_no') ?>

    <?php // echo $form->field($model, 'license_photo') ?>

    <?php // echo $form->field($model, 'permit_type') ?>

    <?php // echo $form->field($model, 'permit_photo') ?>

    <?php // echo $form->field($model, 'insurance_policy_no') ?>

    <?php // echo $form->field($model, 'insurance_policy_photo') ?>

    <?php // echo $form->field($model, 'pollution_no') ?>

    <?php // echo $form->field($model, 'pollution_photo') ?>

    <?php // echo $form->field($model, 'rto_fitness_certificate_no') ?>

    <?php // echo $form->field($model, 'rto_fitness_certificate_photo') ?>

    <?php // echo $form->field($model, 'local_registration_no') ?>

    <?php // echo $form->field($model, 'local_registration_photo') ?>

    <?php // echo $form->field($model, 'registration_address') ?>

    <?php // echo $form->field($model, 'registration_mobile') ?>

    <?php // echo $form->field($model, 'office_landline_no') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'gross_weight') ?>

    <?php // echo $form->field($model, 'verified') ?>

    <?php // echo $form->field($model, 'assigned_driver_id') ?>

    <?php // echo $form->field($model, 'assigned_driver_name') ?>

    <?php // echo $form->field($model, 'assigned_driver_mobile') ?>

    <?php // echo $form->field($model, 'assigned_driver_aadhaar_number') ?>

    <?php // echo $form->field($model, 'assigned_driver_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

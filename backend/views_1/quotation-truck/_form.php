<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QuotationTruck */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="quotation-truck-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'quotation_id')->textInput() ?>

    <?= $form->field($model, 'load_id')->textInput() ?>

    <?= $form->field($model, 'truck_id')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'vehicle_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gps_installed')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'truck_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rc_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rc_photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'license_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'license_photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'permit_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'permit_photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'insurance_policy_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'insurance_policy_photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pollution_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pollution_photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rto_fitness_certificate_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rto_fitness_certificate_photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'local_registration_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'local_registration_photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registration_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registration_mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'office_landline_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gross_weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'verified')->dropDownList([ 'Pending' => 'Pending', 'Approved' => 'Approved', 'Rejected' => 'Rejected', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'assigned_driver_id')->textInput() ?>

    <?= $form->field($model, 'assigned_driver_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assigned_driver_mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assigned_driver_aadhaar_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assigned_driver_at')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', 'Deleted' => 'Deleted', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

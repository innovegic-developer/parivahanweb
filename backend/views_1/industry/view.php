<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Industry */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Industries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="industry-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'user_id',
            'first_name',
            'last_name',
            'email:email',
            'mobile_number',
            'company_name',
            'company_type_id',
            'company_type',
            'incorporation_certificate_number',
            'shop_establishment_certificate_number',
            'pan_number',
            'incorporation_certificate_photo',
            'shop_establishment_certificate_photo',
            'gst_number',
            'aadhaar_number',
            'aadhaar_photo',
            'pan_photo',
            'address_line1',
            'address_line2',
            'pincode',
            'country',
            'state',
            'district',
            'city',
            'area',
            'landmark',
            //'remark',
            'verified',
            'status',
            'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',
        ],
    ]) ?>

</div>

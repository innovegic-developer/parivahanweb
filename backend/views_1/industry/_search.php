<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IndustrySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="industry-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'first_name') ?>

    <?= $form->field($model, 'last_name') ?>

    <?= $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'mobile_number') ?>

    <?php // echo $form->field($model, 'company_name') ?>

    <?php // echo $form->field($model, 'company_type_id') ?>

    <?php // echo $form->field($model, 'company_type') ?>

    <?php // echo $form->field($model, 'incorporation_certificate_number') ?>

    <?php // echo $form->field($model, 'shop_establishment_certificate_number') ?>

    <?php // echo $form->field($model, 'pan_number') ?>

    <?php // echo $form->field($model, 'incorporation_certificate_photo') ?>

    <?php // echo $form->field($model, 'shop_establishment_certificate_photo') ?>

    <?php // echo $form->field($model, 'gst_number') ?>

    <?php // echo $form->field($model, 'aadhaar_number') ?>

    <?php // echo $form->field($model, 'aadhaar_photo') ?>

    <?php // echo $form->field($model, 'pan_photo') ?>

    <?php // echo $form->field($model, 'address_line1') ?>

    <?php // echo $form->field($model, 'address_line2') ?>

    <?php // echo $form->field($model, 'pincode') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'district') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'area') ?>

    <?php // echo $form->field($model, 'landmark') ?>

    <?php // echo $form->field($model, 'remark') ?>

    <?php // echo $form->field($model, 'verified') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

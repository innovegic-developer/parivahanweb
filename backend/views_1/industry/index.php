<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\IndustrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Service Seeker (Industry, Trader and Private User)');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="industry-index">

<?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'user_id',
			'company_name',
            'first_name',
            'last_name',
            'email:email',
            'mobile_number',
            //'company_type_id',
            //'company_type',
            //'incorporation_certificate_number',
            //'shop_establishment_certificate_number',
            //'pan_number',
            //'incorporation_certificate_photo',
            //'shop_establishment_certificate_photo',
            //'gst_number',
            //'aadhaar_number',
            //'aadhaar_photo',
            //'pan_photo',
            //'address_line1',
            //'address_line2',
            //'pincode',
            //'country',
            //'state',
            'district',
            //'city',
            //'area',
            //'landmark',
            //'remark',
            //'status',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',
			[
				'label' => 'Registered At',
				'attribute' => 'created_at',
				'format' => ['date', 'php:d-m-Y'],
				'headerOptions' => ['style' => 'width:10%'],
				'filter' => false,
            ],
			/* [
				'label' => 'DIC Verification',
				'attribute' => 'verified',
				'value' => function($model){
                         if($model->dic_verified == '0'){
                            return 'Pending';
                         }
						 elseif($model->dic_verified == '1'){
                            return 'Verified';
                         }
						 elseif($model->dic_verified == '2'){
                            return 'Rejected';
                         }
               },
			   'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'width:10%; color:' 
                        .(($model->dic_verified=='0')? 'blue' :
                         (($model->dic_verified=='1')? 'green' :
                          'red'))];
                },
				'filter' => false
			],*/
			[
				//'label' => 'OIDC Approval',
				'label' => 'Approval',
				'attribute' => 'verified',
				'value' => function($model){
                         if($model->verified == '0'){
                            return 'Pending';
                         }
						 elseif($model->verified == '1'){
                            return 'Approved';
                         }
						 elseif($model->verified == '2'){
                            return 'Rejected';
                         }
               },
			   'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'width:10%; color:' 
                        .(($model->verified=='0')? 'blue' :
                         (($model->verified=='1')? 'green' :
                          'red'))];
                },
				'filter' => false
			],
            [
                'header' => 'Action',
                'headerOptions' => ['width' => '5%','class' => ' table-th'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                /*'buttons' =>
                [
                    'update' => function($url, $model) {
                         $url = Yii::$app->urlManager->createUrl(['/transporter/update-transporter']).'&user_id=' . $model->id;
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Edit'),
                                    'data-pjax' => '0',
                        ]);
                    },
                ],*/
                'visible' => TRUE
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

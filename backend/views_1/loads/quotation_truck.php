<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\QuotationTruckSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = Yii::t('app', 'Quotation Trucks');
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quotation-truck-index">
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'quotation_id',
            //'load_id',
            //'truck_id',
            //'user_id',
            'vehicle_no',
            //'gps_installed',
            'weight',
            'truck_type',
            //'rc_no',
            //'rc_photo',
            'license_no',
            //'license_photo',
            //'permit_type',
            //'permit_photo',
            //'insurance_policy_no',
            //'insurance_policy_photo',
            //'pollution_no',
            //'pollution_photo',
            //'rto_fitness_certificate_no',
            //'rto_fitness_certificate_photo',
            //'local_registration_no',
            //'local_registration_photo',
            //'registration_address',
            //'registration_mobile',
            //'office_landline_no',
            //'email:email',
            //'gross_weight',
            //'verified',
            //'assigned_driver_id',
            'assigned_driver_name',
            'assigned_driver_mobile',
            //'assigned_driver_aadhaar_number',
            //'assigned_driver_at',
			'eWay_bill_no',
			'trip_status',
            //'status',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            [
                'header' => 'Action',
                'headerOptions' => ['width' => '4%','class' => ' table-th'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{tracking}',
                'buttons' =>
                [
                    'tracking' => function($url, $model) {
                         $url = Yii::$app->urlManager->createUrl(['/loads/tracking']).'&lid='.$model->load_id.'&tid='.$model->truck_id.'&id='.$model->id;
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'Tracking'),
                                    'data-pjax' => '0',
									'target' => '_blank'
                        ]);
                    },
					
                ],
                'visible' => TRUE
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

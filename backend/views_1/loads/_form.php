<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Loads */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loads-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ref_rejected_load_id')->textInput() ?>

    <?= $form->field($model, 'booking_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'industry_user_id')->textInput() ?>

    <?= $form->field($model, 'industry_id')->textInput() ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'load_address_type')->dropDownList([ 'Intrastate' => 'Intrastate', 'Interstate' => 'Interstate', 'Neighboring City' => 'Neighboring City', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'source_address_id')->textInput() ?>

    <?= $form->field($model, 'source_address_line1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'source_address_line2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'source_pincode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'source_state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'source_district')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'source_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'source_area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'source_landmark')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'destination_address_line1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'destination_address_line2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'destination_pincode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'destination_state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'destination_district')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'destination_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'destination_area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'destination_landmark')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'load_type')->dropDownList([ 'Full Load' => 'Full Load', 'Part Load' => 'Part Load', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'scheduling_type')->dropDownList([ 'One Time' => 'One Time', 'Daily' => 'Daily', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'material_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'truck_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_of_trucks')->textInput() ?>

    <?= $form->field($model, 'bid_closing_date')->textInput() ?>

    <?= $form->field($model, 'bid_closing_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reporting_date')->textInput() ?>

    <?= $form->field($model, 'reporting_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'max_bid_amount')->textInput() ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'share_to_preferred')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'assigned_status')->dropDownList([ 'Open' => 'Open', 'Assigned' => 'Assigned', 'Cancelled' => 'Cancelled', 'Rejected' => 'Rejected', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'assigned_at')->textInput() ?>

    <?= $form->field($model, 'assigned_user_id')->textInput() ?>

    <?= $form->field($model, 'assigned_full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assigned_mobile_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assigned_company_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assigned_bid_amount')->textInput() ?>

    <?= $form->field($model, 'assigned_truck_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'part_load_quotation_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reject_reason')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rejected_from')->dropDownList([ '' => '', 'Industry' => 'Industry', 'Transporter' => 'Transporter', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'rejected_by')->textInput() ?>

    <?= $form->field($model, 'rejected_at')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', 'Deleted' => 'Deleted', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'quotations')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LoadsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loads-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ref_rejected_load_id') ?>

    <?= $form->field($model, 'booking_id') ?>

    <?= $form->field($model, 'industry_user_id') ?>

    <?= $form->field($model, 'industry_id') ?>

    <?php // echo $form->field($model, 'full_name') ?>

    <?php // echo $form->field($model, 'mobile_number') ?>

    <?php // echo $form->field($model, 'load_address_type') ?>

    <?php // echo $form->field($model, 'company_name') ?>

    <?php // echo $form->field($model, 'source_address_id') ?>

    <?php // echo $form->field($model, 'source_address_line1') ?>

    <?php // echo $form->field($model, 'source_address_line2') ?>

    <?php // echo $form->field($model, 'source_pincode') ?>

    <?php // echo $form->field($model, 'source_state') ?>

    <?php // echo $form->field($model, 'source_district') ?>

    <?php // echo $form->field($model, 'source_city') ?>

    <?php // echo $form->field($model, 'source_area') ?>

    <?php // echo $form->field($model, 'source_landmark') ?>

    <?php // echo $form->field($model, 'destination_address_line1') ?>

    <?php // echo $form->field($model, 'destination_address_line2') ?>

    <?php // echo $form->field($model, 'destination_pincode') ?>

    <?php // echo $form->field($model, 'destination_state') ?>

    <?php // echo $form->field($model, 'destination_district') ?>

    <?php // echo $form->field($model, 'destination_city') ?>

    <?php // echo $form->field($model, 'destination_area') ?>

    <?php // echo $form->field($model, 'destination_landmark') ?>

    <?php // echo $form->field($model, 'load_type') ?>

    <?php // echo $form->field($model, 'scheduling_type') ?>

    <?php // echo $form->field($model, 'material_type') ?>

    <?php // echo $form->field($model, 'weight') ?>

    <?php // echo $form->field($model, 'truck_type') ?>

    <?php // echo $form->field($model, 'no_of_trucks') ?>

    <?php // echo $form->field($model, 'bid_closing_date') ?>

    <?php // echo $form->field($model, 'bid_closing_time') ?>

    <?php // echo $form->field($model, 'reporting_date') ?>

    <?php // echo $form->field($model, 'reporting_time') ?>

    <?php // echo $form->field($model, 'max_bid_amount') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'share_to_preferred') ?>

    <?php // echo $form->field($model, 'assigned_status') ?>

    <?php // echo $form->field($model, 'assigned_at') ?>

    <?php // echo $form->field($model, 'assigned_user_id') ?>

    <?php // echo $form->field($model, 'assigned_full_name') ?>

    <?php // echo $form->field($model, 'assigned_mobile_number') ?>

    <?php // echo $form->field($model, 'assigned_company_name') ?>

    <?php // echo $form->field($model, 'assigned_bid_amount') ?>

    <?php // echo $form->field($model, 'assigned_truck_id') ?>

    <?php // echo $form->field($model, 'part_load_quotation_date') ?>

    <?php // echo $form->field($model, 'reject_reason') ?>

    <?php // echo $form->field($model, 'rejected_from') ?>

    <?php // echo $form->field($model, 'rejected_by') ?>

    <?php // echo $form->field($model, 'rejected_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'quotations') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

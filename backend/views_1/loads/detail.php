<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Loads */

$this->title = Yii::t('app', 'Load Detail', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Loads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Detail');
function dateFormat($date,$time=false){
	if($time)
	$str = date('d-m-Y h:i A',strtotime($date));	
	else	
	$str = date('d-m-Y',strtotime($date));
	return $str;
}
?>
<style>
.sub-title {
	background: #367fa9;
	color: #fff;
	font-size: 20px;
	padding-left: 7px;
	/*margin-top:20px;
	margin-bottom:20px;*/
}
</style>
<div class="loads-update">
<div class="sub-title">Load Detail</div>
	<table id="w-0" class="table table-striped table-bordered detail-view">
        <tbody>
         <tr>
            <td><b>Booking ID:</b>&nbsp;&nbsp;<?=$model->booking_id?></td>
            <td><b>Industry Name:</b>&nbsp;&nbsp;<?=$model->company_name?></td> 
            <td><b>Contact Person :</b>&nbsp;&nbsp;<?=$model->full_name?></td>
            <td><b>Mobile Number :</b>&nbsp;&nbsp;<?=$model->mobile_number?></td>
         </tr>

         <tr> 
            <td><b>Source Address :</b>&nbsp;&nbsp;<?=$model->source_address_line1.' '.$model->source_address_line2?></td> 
            <td><b>Source City/District :</b>&nbsp;&nbsp;<?=$model->source_city.', '.$model->source_area.'-'.$model->source_pincode?></td>
            <td><b>Destination Address:</b>&nbsp;&nbsp;<?=$model->destination_address_line1.' '.$model->destination_address_line2?></td>
            <td><b>Destination City/District:</b>&nbsp;&nbsp;<?=$model->destination_city.', '.$model->destination_area.'-'.$model->destination_pincode?></td>
         </tr>
		 <tr>
            <td><b>Scheduling Type:</b>&nbsp;&nbsp;<?=$model->scheduling_type?></td> 
			<td><b>Load Type:</b>&nbsp;&nbsp;<?=$model->load_type?></td>
            <td><b>Material Type :</b>&nbsp;&nbsp;<?=$model->material_type?></td>
            <td><b>Weight :</b>&nbsp;&nbsp;<?=$model->weight?></td>
         </tr>
		 <tr>
            <td><b>Truck Type:</b>&nbsp;&nbsp;<?=$model->truck_type?></td>
            <td><b>No. of Trucks:</b>&nbsp;&nbsp;<?=$model->no_of_trucks?></td> 
            <td><b>Reporting Date :</b>&nbsp;&nbsp;<?=dateFormat($model->reporting_date)?></td>
            <td><b>Reporting Time :</b>&nbsp;&nbsp;<?=$model->reporting_time?></td>
         </tr>
		 <tr>
            <td><b>Quotation Closing Date:</b>&nbsp;&nbsp;<?=dateFormat($model->bid_closing_date)?></td>
            <td><b>Quotation Closing Time:</b>&nbsp;&nbsp;<?=$model->bid_closing_time?></td> 
            <td><b>Estimated Quotation Amount :</b>&nbsp;&nbsp;Rs. <?=$model->total_amount?></td>
            <td><b>Current Status :</b>&nbsp;&nbsp;<?=$model->assigned_status?></td>
         </tr>
		 <tr>
            <td colspan="4"><b>Description :</b>&nbsp;&nbsp;<?=$model->description?></td>
         </tr>
        </tbody>
	</table>
<?php if($model->assigned_status=='Assigned' || $model->assigned_status=='Rejected') { ?>	
<div class="sub-title">Assigned Transporter</div>
	<table id="w-0" class="table table-striped table-bordered detail-view">
        <tbody>
         <tr>
            <td><b>Name:</b>&nbsp;&nbsp;<?=$model->assigned_full_name?></td>
            <td><b>Mobile Number:</b>&nbsp;&nbsp;<?=$model->assigned_mobile_number?></td> 
            <td><b>Company Name :</b>&nbsp;&nbsp;<?=$model->assigned_company_name?></td>
            <td><b>Quotation Amount :</b>&nbsp;&nbsp;Rs. <?=$model->assigned_bid_amount?></td>
         </tr>
        </tbody>
	</table>	
<?php } ?>

<?php if($model->assigned_status=='Rejected') { ?>	
<div class="sub-title">Rejection Detail</div>
	<table id="w-0" class="table table-striped table-bordered detail-view">
        <tbody>
         <tr>
            <td><b>Rejected By:</b>&nbsp;&nbsp;<?=$model->rejected_from?></td>
            <td><b>Rejected At:</b>&nbsp;&nbsp;<?=dateFormat($model->rejected_at,true)?></td> 
         </tr>
		 <tr>
            <td colspan="2"><b>Reason :</b>&nbsp;&nbsp;<?=$model->reject_reason?></td>
         </tr>
        </tbody>
	</table>	
<?php } ?>

<?php if($model->assigned_status=='Assigned') { ?>	
<div class="sub-title">List of Assigned Trucks & Drivers</div>
	<?= $this->render('quotation_truck', [
    	'model' => $model,
        'searchModel' => $truckModel,
        'dataProvider' => $dataProviderTruck,
    ]) ?>
<?php } ?>
	
<div class="sub-title">List of Quotations</div>
	<?= $this->render('quotation', [
    	'model' => $model,
        'searchModel' => $quotModel,
        'dataProvider' => $dataProviderQuot,
    ]) ?>	
</div>   
</div>

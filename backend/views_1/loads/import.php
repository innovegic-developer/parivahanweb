<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\MandiProduct */

$this->title = Yii::t('app', 'Import Post Load in bulk');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Post Load'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mandi-product-create">
	
	<!--Success/Error Message --->
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="row col-xs-12 col-sm-12 col-md-12 col-xl-12 alert alert-success alert-dismissable" style="width: 100%; margin-left: 0px;">
        <div class="col-md-12">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
        </div>
    <?php endif; ?>
	<?php if (Yii::$app->session->hasFlash('error')): ?>
		<div class="row col-xs-12 col-sm-12 col-md-12 col-xl-12 alert alert-error alert-dismissable" style="width: 100%; margin-left: 0px;">
		<div class="col-md-12">
		 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			 <!--<h4><i class="icon fa fa-check"></i>Saved!</h4>-->
			 <?= Yii::$app->session->getFlash('error') ?>
		</div>
		</div>
   <?php endif; ?>
    <!--Success/Error Message --->
	
    <div class="mandi-product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

     <div class="row"> 
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="form-group field-article-gallary_image1">
            <label class="control-label" for="">Upload Excel</label>
            <input id="" name="uploadExcel" type="file" accept=".xls,.xlsx" required="true">
            <div class="help-block"></div>
            </div>
        </div>
        </div>
	</div>

    <div class="row">
    
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
          <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
             &nbsp;&nbsp;&nbsp;&nbsp;
          <?= Html::a('Cancel', ['index'], ['class'=>'btn btn-primary']) ?>
        </div>
        </div>
    
    </div> 

    <?php ActiveForm::end(); ?>

</div>

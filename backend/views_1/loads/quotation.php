<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\QuotationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = Yii::t('app', 'Quotations');
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quotation-index">
	<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'load_id',
            //'user_id',
            'full_name',
            'mobile_number',
            'company_name',
            'bid_amount',
            //'part_load_quotation_date',
            //'truck_id',
            //'remark',
            //'assigned_to_me',
            //'status',
            [
				'label' => 'Created At',
				'attribute' => 'created_at',
				'format' => ['date', 'php:d-m-Y'],
				'headerOptions' => ['style' => 'width:8%'],
				//'filter' => false,
            ],
            //'updated_at',
            //'created_by',
            //'updated_by',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

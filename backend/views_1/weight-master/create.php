<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WeightMaster */

$this->title = Yii::t('app', 'Create Weight Master');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Weight Masters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="weight-master-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TruckDriver */

$this->title = Yii::t('app', 'Create Truck Driver');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Truck Drivers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="truck-driver-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

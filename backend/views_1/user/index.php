<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.table-th {
	color:#3c8dbc;
}
</style>
<div class="user-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<div class="pull-right margin_bottom_15">
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> New Officer', ['create'], ['class' => 'btn btn-success']) ?>  
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['style' => 'width:2%'],
            ],

            //'id',
            /*[
                'attribute' => 'first_name',
                'headerOptions' => ['style' => 'width:15%;'],
            ],
            [
                'attribute' => 'last_name',
                'headerOptions' => ['style' => 'width:15%;'],
            ],*/
			[
                'attribute' => 'full_name',
                'headerOptions' => ['style' => 'width:15%;'],
            ],
            [
				'label' => 'Username',
                'attribute' => 'username',
                'headerOptions' => ['style' => 'width:12%;'],
            ],
            [
                'attribute' => 'mobile_number',
                'headerOptions' => ['style' => 'width:15%;'],
            ],
            [
                'attribute' => 'email',
                'headerOptions' => ['style' => 'width:23%;'],
            ],
			[
                'label' => 'User Type',
                'attribute' => 'user_type',
                'value' => 'userLevel.RoleType',
                'headerOptions' => ['style' => 'width:10%','class' => 'table-th'],
            ],
            [
                'attribute' => 'created_at',
                'headerOptions' => ['style' => 'width:10%;'],
                'format' => ['date', 'php:d-m-Y']
            ],
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            //'email:email',
            //'status',
            //'created_at',
            //'updated_at',
            //'verification_token',

            [
                'header' => 'Action',
                'headerOptions' => ['width' => '9%','class' => ' table-th'],
                'class' => 'yii\grid\ActionColumn',
                //'template' => ' {view} {update} {delete} {changepassword}',
                'template' => '{update} {delete} {changepassword1}',
                'buttons' =>
                [
                    'changepassword' => function($url, $model) {
                         $url = Yii::$app->urlManager->createUrl(['/user/changepassword']).'&id=' . $model->id;
                        return Html::a('<span class="glyphicon glyphicon-lock"></span>', $url, [
                                    'title' => Yii::t('app', 'Change password'),
                                    'data-pjax' => '0',
                        ]);
                    },
                ],
                'visible' => TRUE
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Transporters');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.table-th {
	color:#3c8dbc;
}
</style>
<div class="user-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<!--<div class="pull-right margin_bottom_15">
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> New Officer', ['create'], ['class' => 'btn btn-success']) ?>  
    </div>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
               [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['style' => 'width:2%'],
            ],

            //'id',
            [
                'attribute' => 'first_name',
                'headerOptions' => ['style' => 'width:15%;'],
            ],
            [
                'attribute' => 'last_name',
                'headerOptions' => ['style' => 'width:15%;'],
            ],
			[
                'attribute' => 'mobile_number',
                'headerOptions' => ['style' => 'width:15%;'],
            ],
            [
                'attribute' => 'email',
                'headerOptions' => ['style' => 'width:23%;'],
            ],
			[
                'attribute' => 'created_at',
                'headerOptions' => ['style' => 'width:10%;'],
                'format' => ['date', 'php:d-m-Y']
            ],
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            //'email:email',
            //'status',
            //'created_at',
            //'updated_at',
            //'verification_token',

            [
                'header' => 'Action',
                'headerOptions' => ['width' => '5%','class' => ' table-th'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons' =>
                [
                    'update' => function($url, $model) {
                         $url = Yii::$app->urlManager->createUrl(['/transporter/update-transporter']).'&user_id=' . $model->id;
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Edit'),
                                    'data-pjax' => '0',
                        ]);
                    },
                ],
                'visible' => TRUE
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>


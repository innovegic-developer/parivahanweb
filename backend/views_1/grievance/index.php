<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\GrievanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Grievances');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grievance-index">
	<?php Pjax::begin(); ?> 
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
			'user_type',
            'user.first_name',
			'user.mobile_number',
			[
				'label' => 'Grievance Name',
				'attribute' => 'grievanceUser.first_name',
				//'filter' => false,
            ],
            [
				'label' => 'Grievance Mobile',
				'attribute' => 'grievanceUser.mobile_number',
				//'filter' => false,
            ],
            //'grievance_user_type',
            'comment',
            //'status',
            [
				'label' => 'Created At',
				'attribute' => 'created_at',
				'format' => ['date', 'php:d-m-Y'],
				'headerOptions' => ['style' => 'width:9%'],
				//'filter' => false,
            ],
            //'updated_at',
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

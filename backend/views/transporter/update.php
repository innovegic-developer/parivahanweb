<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model app\models\Transporter */

$this->title = Yii::t('app', 'Update Transporter: {name}', [
    'name' => $model->first_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Transporters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->first_name, 'url' => ['index', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<style>
.sub-title {
	background: #367fa9;
	color: #fff;
	font-size: 20px;
	padding-left: 7px;
	/*margin-top:20px;
	margin-bottom:20px;*/
}
</style>
<div class="transporter-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
<div class="clearfix"></div>
<div class="sub-title">Tempo List</div>
	<div class="truck-index">
	<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProviderTruck,
        'filterModel' => $truckModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'load_id',
            //'user_id',
            'vehicle_no',
            'truck_type',
            'weight',
            'gps_installed',
            'rc_no',
            'license_no',
            'permit_type',
            'insurance_policy_no',
            'pollution_no',
            /*[
				'label' => 'Created At',
				'attribute' => 'created_at',
				'format' => ['date', 'php:d-m-Y'],
				'headerOptions' => ['style' => 'width:8%'],
				//'filter' => false,
            ],*/
            //'updated_at',
            //'created_by',
            //'updated_by',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
	</div>	

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Transporter */
/* @var $form yii\widgets\ActiveForm */
$full_path = SITE_PATH. 'uploads/transporter/';
?>

<div class="transporter-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
	<div class="blue-title"><h4>&nbsp;</h4></div>
	<div class="table-responsive">
	 <table id="w-0" class="table table-striped table-bordered detail-view">
			<tbody>
			<tr>
				<td><span>Aadhaar Photo  : </span></td><td><a target="_blank" href="<?=$full_path.$model->aadhaar_photo?>">click here</a></td>
				<td><span>PAN Photo  : </span></td><td><a target="_blank" href="<?=$full_path.$model->pan_photo?>">click here</a></td>
			</tr>	
			<tr>
				<td><span>Licence Photo  : </span></td><td><a target="_blank" href="<?=$full_path.$model->driver_licence_photo?>">click here</a></td>
			</tr>	
			</tbody>
	</table>
	</div>
	</div>
	

    <?php //echo $form->field($model, 'user_id')->textInput() ?>

	<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'mobile_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<!--<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'designation')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>-->
	<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'driver_operator_type')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<!--<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>-->   
	<!--<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?php //echo $form->field($model, 'company_type')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>-->   
	<!--<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'incorporation_certificate_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'shop_establishment_certificate_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   -->
	<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'pan_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<!--<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'incorporation_certificate_photo')->textInput(['maxlength' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'shop_establishment_certificate_photo')->textInput(['maxlength' => true]) ?>
	</div>   -->
	<!--<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'gst_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>-->   
	<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'aadhaar_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'driver_licence_number')->textInput(['maxlength' => true, 'readonly' => true])->label('Licence Number') ?>
	</div>	
	<!--<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'aadhaar_photo')->textInput(['maxlength' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'pan_photo')->textInput(['maxlength' => true]) ?>
	</div>-->   
	<div class="col-xs-12 col-sm-6 col-md-4 col-xl-4">
    <?= $form->field($model, 'verified')->dropDownList([ '0' => 'Not Verified', '1' => 'Verified']) ?>   
	</div>
	<!--<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
    <?= $form->field($model, 'status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive'], ['prompt' => '']) ?>
	</div>-->
    <div class="clearfix"></div>
    <div class="col-sm-6 col-md-6 col-xl-6" style="margin-top:20px;">
	<div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <?= Html::a('Cancel', ['/truck-operator/index'], ['class'=>'btn btn-primary']) ?>
    </div>
	</div>

    <?php ActiveForm::end(); ?>

</div>

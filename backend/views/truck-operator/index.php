<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TransporterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Truck Operators');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transporter-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'user_id',
            //'user_level',
            'first_name',
            'last_name',
            'email:email',
            'mobile_number',
            //'designation',
            //'company_name',
            //'incorporation_certificate_number',
            //'shop_establishment_certificate_number',
            //'pan_number',
            //'incorporation_certificate_photo',
            //'shop_establishment_certificate_photo',
            //'gst_number',
            //'aadhaar_number',
            //'aadhaar_photo',
            //'pan_photo',
            'driver_operator_type',
            //'address_line1',
            //'address_line2',
            //'pincode',
            //'country',
            //'state',
            //'district',
            //'city',
            //'area',
            //'landmark',
            //'remark',
            //'verified',
            //'available_status',
			[
				'label' => 'Registered At',
				'attribute' => 'created_at',
				'format' => ['date', 'php:d-m-Y'],
				'headerOptions' => ['style' => 'width:10%'],
				'filter' => false,
            ],
			[
				'label' => 'Verification',
				'attribute' => 'verified',
				'value' => function($model){
                         if($model->verified == '0'){
                            return 'Pending';
                         }
						 elseif($model->verified == '1'){
                            return 'Verified';
                         }
               },
			   'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'width:10%; color:' 
                        .(($model->verified=='0')? 'red' :
                         (($model->verified=='1')? 'green' :
                          'red'))];
                },
			],
            //'status',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',
            [
                'header' => 'Action',
                'headerOptions' => ['width' => '5%','class' => ' table-th'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                /*'buttons' =>
                [
                    'update' => function($url, $model) {
                         $url = Yii::$app->urlManager->createUrl(['/transporter/update-transporter']).'&user_id=' . $model->id;
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Edit'),
                                    'data-pjax' => '0',
                        ]);
                    },
                ],*/
                'visible' => TRUE
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

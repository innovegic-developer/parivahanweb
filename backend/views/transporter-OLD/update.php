<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Transporter */

$this->title = Yii::t('app', 'Update', [
    'first_name' => $model->first_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Edit'), 'url' => ['']];
$this->params['breadcrumbs'][] = ['label' => $model->first_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="transporter-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>


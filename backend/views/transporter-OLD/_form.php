<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Transporter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transporter-form">

    <?php $form = ActiveForm::begin(); ?>

	
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'designation')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'incorporation_certificate_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'shop_establishment_certificate_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'pan_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'gst_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'aadhaar_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
    <?php //echo $form->field($model, 'aadhaar_photo')->textInput(['maxlength' => true, 'readonly' => true]) ?>

    <?php //echo $form->field($model, 'pan_photo')->textInput(['maxlength' => true, 'readonly' => true]) ?>

	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'address_line1')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'address_line2')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'pincode')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	
    <?php //echo $form->field($model, 'country')->textInput(['maxlength' => true, 'readonly' => true]) ?>

	
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'state')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'district')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
    <?= $form->field($model, 'area')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	
	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
	<?= $form->field($model, 'verified')->dropDownList([ '0' => 'Not Verified', '1' => 'Verified']) ?>   
	</div>
	
    <?php //echo $form->field($model, 'remark')->textInput(['maxlength' => true, 'readonly' => true]) ?>

    <?php //echo $form->field($model, 'status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', 'Deleted' => 'Deleted', ], ['prompt' => '']) ?>

    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
	</div>
    <?php ActiveForm::end(); ?>

</div>


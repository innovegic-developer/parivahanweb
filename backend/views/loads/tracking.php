<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \app\models\Loads;
use \app\models\QuotationTruck;
use \app\models\TripTracking;
$this->title = Yii::t('app', 'Tracking Detail', [
    'nameAttribute' => '',
]);
$load = Loads::find()->where(['id' => $_GET['lid']])->one();
$trip = QuotationTruck::find()->where(['id' => $_GET['id']])->one();
$gps = TripTracking::find()->where(['load_id' => $_GET['lid'], 'truck_id' => $_GET['tid']])->asArray()->all();
//print_r($gps);exit;
$c = 0;
$total = 0;
if(empty($gps))
{
    $gps[$c]['latitude'] = 0;
    $gps[$c]['longitude'] = 0;
}
else
{
    $total = count($gps);
    $c = intval($total / 2);
}

function distCal($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
  // Calculate the distance in degrees
  $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
 
  // Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
  switch($unit) {
    case 'km':
      $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
      break;
    case 'mi':
      $distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
      break;
    case 'nmi':
      $distance =  $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
  }
  return round($distance, $decimals);
}

?>
<style>
#map {
        height: 425px;
      }
.sub-title {
	background: #367fa9;
	color: #fff;
	font-size: 20px;
	padding-left: 7px;
	/*margin-top:20px;
	margin-bottom:20px;*/
}
</style>
 <script>
      // This example converts a polyline to a dashed line, by
      // setting the opacity of the polyline to 0, and drawing an opaque symbol
      // at a regular interval on the polyline.

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: {lat: <?php echo $gps[$c]['latitude'];?>, lng: <?php echo $gps[$c]['longitude'];?>},
          mapTypeId: 'terrain'
        });
        /*
		  var flightPlanCoordinates = [
          <?php foreach ($gps as $g) { 
            echo '{lat:'.$g['latitude'].', lng:'.$g['longitude'].'},';
           } ?> 
        ];
        var flightPath = new google.maps.Polyline({
          path: flightPlanCoordinates,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2,
        });
        flightPath.setMap(map);*/

        var lineSymbol = {
          path: 'M 0,-1 0,1',
          strokeOpacity: 1,
          //scale: 4
        };

        var line = new google.maps.Polyline({
          path: [<?php foreach ($gps as $g) { 
            echo '{lat:'.$g['latitude'].', lng:'.$g['longitude'].'},';
           } ?> ],
          strokeColor: '#FF0000', 
          strokeOpacity: 0,
          icons: [{
            icon: lineSymbol,
            offset: '0',
            repeat: '20px'
          }],
          map: map
        });
      }

    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_KEY?>&callback=initMap">
    </script>


<div class="trip-master-update">

<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
<div class="sub-title">Load Detail</div>
	<table id="w-0" class="table table-striped table-bordered detail-view">
        <tbody>
         <tr>
            <td><b>Unique ID:</b>&nbsp;&nbsp;<?=$load->booking_id?></td>
            <td><b>Company Name:</b>&nbsp;&nbsp;<?=$load->company_name?></td> 
            <td><b>Person Name:</b>&nbsp;&nbsp;<?=$load->full_name?></td>
            <td><b>Mobile Number :</b>&nbsp;&nbsp;<?=$load->mobile_number?></td>
         </tr>

         <tr> 
            <td><b>Source Address :</b>&nbsp;&nbsp;<?=$load->source_address_line1.' '.$load->source_address_line2?></td> 
            <td><b>Source City :</b>&nbsp;&nbsp;<?=$load->source_district.', '.$load->source_area.'-'.$load->source_pincode?></td>
            <td><b>Destination Address:</b>&nbsp;&nbsp;<?=$load->destination_address_line1.' '.$load->destination_address_line2?></td>
            <td><b>Destination City:</b>&nbsp;&nbsp;<?=$load->destination_district.', '.$load->destination_area.'-'.$load->destination_pincode?></td>
         </tr>
        </tbody>
	</table>
</div>
</div>

<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
<div class="sub-title">Truck Trip Detail</div>
	<table id="w-0" class="table table-striped table-bordered detail-view">
        <tbody>
         <tr>
            <td><b>Vehicle No:</b>&nbsp;&nbsp;<?=$trip->vehicle_no?></td>
            <td><b>Truck Type:</b>&nbsp;&nbsp;<?=$trip->truck_type?></td> 
            <td><b>Weight :</b>&nbsp;&nbsp;<?=$trip->weight?></td>
            <td><b>GPS Installed :</b>&nbsp;&nbsp;<?=$trip->gps_installed?></td>
         </tr>
		<tr>
            <td><b>RC No:</b>&nbsp;&nbsp;<?=$trip->rc_no?></td>
            <td><b>License No:</b>&nbsp;&nbsp;<?=$trip->license_no?></td> 
            <td><b>Permit Type:</b>&nbsp;&nbsp;<?=$trip->permit_type?></td>
            <td><b>Insurance Policy No:</b>&nbsp;&nbsp;<?=$trip->insurance_policy_no?></td>
         </tr>
		<tr>
            <td><b>Driver Name:</b>&nbsp;&nbsp;<?=$trip->assigned_driver_name?></td>
            <td><b>Driver Mobile:</b>&nbsp;&nbsp;<?=$trip->assigned_driver_mobile?></td> 
            <td><b>Aadhaar Number:</b>&nbsp;&nbsp;<?=$trip->assigned_driver_aadhaar_number?></td>
            <td><b>Trip Status:</b>&nbsp;&nbsp;<?=$trip->trip_status?></td>
         </tr>	
         
        </tbody>
	</table>
</div>
</div>	

<div class="clearfix"></div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">

<?php if($total >0) { ?>

<div id="map" class="col-xs-8 col-sm-8 col-md-8 col-xl-8"></div>
<div class="col-xs-4 col-sm-4 col-md-4 col-xl-4" style="overflow-y: scroll;
    max-height: 425px;">
  <table width="100%" border="1">
    <tr>
      <th>&nbsp;Date & Time</th>
      <th>&nbsp;Latitude</th>
      <th>&nbsp;Longitude</th>
    </tr>
    <?php foreach ($gps as $g) { ?>
      <tr>
      <td>&nbsp;<?=date('d-m-Y h:i A',strtotime($g['created_on']))?></td>
      <td>&nbsp;<?=$g['latitude']?></td>
      <td>&nbsp;<?=$g['longitude']?></td>
    </tr>
    <?php } ?>

  </table>
</div>

<?php } ?>
    <!--<img src="../../uploads/map-trace-green.jpg" style="width: 100%;height: 614px;">-->
    <!--<img src="../../uploads/map-trace-red.jpg" style="width: 100%;height: 614px;">-->

</div>
</div>


</div>

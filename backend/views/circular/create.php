<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Circular */

$this->title = Yii::t('app', 'Create Circular');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Circulars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="circular-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CircularSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Circulars');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="circular-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="pull-right margin_bottom_15">
   
    <?= Html::a(Yii::t('app', '<i class="fa fa-plus" aria-hidden="true"></i> New Circular'), ['create'], ['class' => 'btn btn-success']) ?>

  </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'circular_title',
            //'circular_desc',
            [
                'attribute' =>  'circular_title',
                'label' => 'Title',
                'contentOptions' => ['style' => 'width:35%'],
            ],
            [
                'attribute' =>  'circular_desc',
                'label' => 'Description',
                'contentOptions' => ['style' => 'width:45%'],
            ],

            [
                'attribute'=>'circular_date',
                'label' => 'Date',
                'format'=>['Date','php:d-m-Y'],
                'contentOptions' => ['style' => 'width:10%'],
            ],
            //'circular_pdf',
            //'created_on',
            //'updated_on',
            //'created_by',
            //'updated_by',
            //'status',

            [
                'header' => 'Action',
                'headerOptions' => ['width' => '5%'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',                
                'visible' => TRUE
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

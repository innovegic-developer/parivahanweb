<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Circular */

$this->title = Yii::t('app', 'Update Circular: ' . $model->circular_title, [
    'nameAttribute' => '' . $model->circular_title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Circular'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="circular-update">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

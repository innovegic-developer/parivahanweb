<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tender */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tender-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
    <?= $form->field($model, 'tender_title')->textInput(['maxlength' => '256'])->label('Title') ?>
</div>

<!--<div class="col-xs-12 col-sm-6 col-md-6 col-xl-6">
     <?= $form->field($model, 'tender_title_hi')->textInput(['maxlength' => '256']) ?>
</div>

     <div class="col-xs-12 col-sm-6 col-md-6 col-xl-6">
    <?= $form->field($model, 'tender_title_gu')->textInput(['maxlength' => '256']) ?>
</div>-->

   <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
    <?php 
    if($model->tender_pdf){
    echo $form->field($model, 'tender_pdf', ['enableClientValidation' => false])->fileInput()->label('Upload PDF');
}
else{
 echo $form->field($model, 'tender_pdf')->fileInput()->label('Upload PDF');
}
if($model->tender_pdf){
    echo '<a target="_blank" href="'.SITE_PATH.'uploads/tender/'.$model->tender_pdf.'" >'.$model->tender_pdf.'</a>';
}
 ?>
</div>
   
   <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
    <?= $form->field($model, 'tender_desc')->textarea(['rows' => 4])->label('Description') ?>
</div>

<!--<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
    <?= $form->field($model, 'tender_desc_hi')->textarea(['rows' => 8]) ?>
</div>

  <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
    <?= $form->field($model, 'tender_desc_gu')->textarea(['rows' => 8]) ?>
</div>-->
   
   <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
     <?= $form->field($model, 'status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', 'Deleted' => 'Deleted', ], ['prompt' => '']) ?>
 </div>

   <?php 
    if($model->id=='')
    {
       //echo $form->field($model, 'created_on')->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false);
       echo $form->field($model, 'created_by')->hiddenInput(['value' => Yii::$app->user->id])->label(false);
    }
    else
    {    
       //echo $form->field($model, 'updated_on')->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false);
     
       echo $form->field($model, 'updated_by')->hiddenInput(['value' => Yii::$app->user->id])->label(false);
    }
    ?>

   

    <div class="clearfix"></div>
    <div class="col-sm-6 col-md-6 col-xl-6" style="margin-top:20px;">
	<div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
	</div>
    <?php ActiveForm::end(); ?>

</div>

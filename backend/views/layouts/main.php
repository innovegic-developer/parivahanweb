<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
//use app\assets\AppAsset;
use backend\assets\AdminLteAsset;

//AppAsset::register($this);
$asset      = AdminLteAsset::register($this);
$baseUrl    = $asset->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
	<?php  if(!empty($this->title)) { ?>
    <title><?= Html::encode($this->title) ?></title>
	<?php } else { ?>
	<title><?= SITE_TITLE ?></title>
	<?php } ?>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">
    <?= $this->render('header.php', ['baserUrl' => $baseUrl, 'title'=>Yii::$app->name]) ?>
    <?= $this->render('leftside.php', ['baserUrl' => $baseUrl]) ?>
    <?= $this->render('content.php', ['content' => $content]) ?>
    <?= $this->render('footer.php', ['baserUrl' => $baseUrl]) ?>
    <?= $this->render('rightside.php', ['baserUrl' => $baseUrl]) ?>
</div>

<!--footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?//= date('Y') ?></p>

        <p class="pull-right"><?//= Yii::powered() ?></p>
    </div>
</footer-->
<script>
            function lang_chnage()
            {
                var  lcode = document.getElementById("lang_chng").value;
                //alert(lcode);
                $.ajax({
                    url: '<?php echo Yii::$app->urlManager->createUrl('/site/setlanguage'); ?>',
                    type: 'post',
                    data: {
                        lang_code: lcode,
                        _csrf: '<?= Yii::$app->request->getCsrfToken() ?>',
                        current_url: '<?=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>',
                    },
                    success: function (data) {
                        //alert(data); return false;
                        //console.log(data);
                    },
                    error: function (error) {
                       //alert('hii1'); return false;
                       //console.log("fff");
                    }
                });
            }

            function loc_change()
            {
                var  lcode = document.getElementById("loc_chng").value;
                $.ajax({
                    url: '<?php echo Yii::$app->urlManager->createUrl('/site/setlocation'); ?>',
                    type: 'post',
                    data: {
                        location: lcode,
                        _csrf: '<?= Yii::$app->request->getCsrfToken() ?>',
                        current_url: '<?=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>',
                    },
                    success: function (data) {
                        //alert(data); return false;
                        //console.log(data);
                    },
                    error: function (error) {
                       //alert('hii1'); return false;
                       //console.log("fff");
                    }
                });
            }
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

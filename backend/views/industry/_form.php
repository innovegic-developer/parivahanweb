<?php
use yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Industry */
/* @var $form yii\widgets\ActiveForm */
$full_path = SITE_PATH. 'uploads/industry/';    
$user_level = Yii::$app->user->identity->user_level;
?>

<div class="industry-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
	<div class="blue-title"><h4>&nbsp;</h4></div>
	<div class="table-responsive">
	 <table id="w-0" class="table table-striped table-bordered detail-view">
			<tbody>
			<!--<tr>
				<td><span>Incorporation Certificate Photo  : </span></td><td><a target="_blank" href="<?=$full_path.$model->incorporation_certificate_photo?>">click here</a></td>
				<td><span>Shop Establishment Certificate Photo  : </span></td><td><a target="_blank" href="<?=$full_path.$model->shop_establishment_certificate_photo?>">click here</a></td>
			</tr>-->
			<tr>
				<!--<td><span>Aadhar Photo  : </span></td><td><a target="_blank" href="<?=$full_path.$model->aadhaar_photo?>">click here</a></td>-->
				<!--<td><span>PAN Photo  : </span></td><td><a target="_blank" href="<?=$full_path.$model->pan_photo?>">click here</a></td>-->
				<td><b>Any of the Government registered certificate / GST Certificate / VAT Certificate / Labour Department Certificate / Electricity Bill Copy  : </b>&nbsp;&nbsp;&nbsp;<a target="_blank" href="<?=$full_path.$model->any_government_doc_photo?>">click here</a></td>
			</tr>
			<?php if(!empty($model->udhyog_aadhaar_certificate_photo)) { ?>
			<tr>
				<td><b>Udhyog Aadhaar Certificate Copy  : </b>&nbsp;&nbsp;&nbsp;<a target="_blank" href="<?=$full_path.$model->udhyog_aadhaar_certificate_photo?>">click here</a></td>
			</tr>
			<?Php } ?>
			</tbody>
	</table>
	</div>
	</div>
	

    <?php //echo $form->field($model, 'user_id')->textInput() ?>

	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'mobile_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'company_type')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'address_line1')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'address_line2')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'district')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'area')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'pincode')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'landmark')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>	
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'any_government_doc_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<?php if(!empty($model->udhyog_aadhaar_certificate_number)) { ?>
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'udhyog_aadhaar_certificate_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>
	<?php } ?>
	<!--<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'incorporation_certificate_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'shop_establishment_certificate_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'pan_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   -->
	<!--<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'incorporation_certificate_photo')->textInput(['maxlength' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'shop_establishment_certificate_photo')->textInput(['maxlength' => true]) ?>
	</div>   -->
	<!--<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'gst_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'aadhaar_number')->textInput(['maxlength' => true, 'readonly' => true]) ?>
	</div>-->   
	<!--<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'aadhaar_photo')->textInput(['maxlength' => true]) ?>
	</div>   
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'pan_photo')->textInput(['maxlength' => true]) ?>
	</div>-->   
	<?php if($user_level == 1 || $user_level== 2) { ?>
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'verified')->dropDownList([ '0' => 'Pending', '1' => 'Approved', '2' => 'Rejected'])->label('OIDC Approval') ?>   
	</div>
	<?php } else { ?>
	<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
	<?= $form->field($model, 'verified')->dropDownList([ '0' => 'Pending', '1' => 'Approved', '2' => 'Rejected'])->label('OIDC Approval') ?>   
    <?php //echo $form->field($model, 'dic_verified')->dropDownList([ '0' => 'Pending', '1' => 'Verified', '2' => 'Rejected'])->label('DIC Verification') ?>   
	</div>
	<?php } ?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
    <?= $form->field($model, 'rejection_remark')->textarea(['rows' => 2]) ?>
	</div>
	<!--<div class="col-xs-12 col-sm-4 col-md-4 col-xl-4">
    <?= $form->field($model, 'status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive'], ['prompt' => '']) ?>
	</div>-->
    <div class="clearfix"></div>
    <div class="col-sm-6 col-md-6 col-xl-6" style="margin-top:20px;">
	<div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <?= Html::a('Cancel', ['/industry/index'], ['class'=>'btn btn-primary']) ?>
    </div>
	</div>
    <?php ActiveForm::end(); ?>

</div>

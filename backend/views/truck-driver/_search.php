<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TruckDriverSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="truck-driver-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'driver_name') ?>

    <?= $form->field($model, 'driver_mobile') ?>

    <?= $form->field($model, 'driver_current_address') ?>

    <?php // echo $form->field($model, 'driver_original_address') ?>

    <?php // echo $form->field($model, 'driver_aadhaar_number') ?>

    <?php // echo $form->field($model, 'driver_licence_number') ?>

    <?php // echo $form->field($model, 'driver_pan_number') ?>

    <?php // echo $form->field($model, 'driver_aadhaar_photo') ?>

    <?php // echo $form->field($model, 'driver_licence_photo') ?>

    <?php // echo $form->field($model, 'driver_pan_photo') ?>

    <?php // echo $form->field($model, 'verified') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

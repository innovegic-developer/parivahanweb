<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TruckDriver */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="truck-driver-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'driver_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_current_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_original_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_aadhaar_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_licence_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_pan_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_aadhaar_photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_licence_photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_pan_photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'verified')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', 'Deleted' => 'Deleted', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

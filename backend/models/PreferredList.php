<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "preferred_list".
 *
 * @property int $id ID
 * @property int $industry_user_id Industry ID
 * @property int $preferred_user_id Preferred User ID
 * @property string $status Status
 * @property string $created_at Created On
 * @property string $updated_at Updated On
 * @property int $created_by Created By
 * @property int $updated_by Updated By
 *
 * @property User $industryUser
 * @property User $preferredUser
 */
class PreferredList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'preferred_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['industry_user_id'], 'required'],
            [['industry_user_id', 'preferred_user_id', 'created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['industry_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['industry_user_id' => 'id']],
            [['preferred_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['preferred_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'industry_user_id' => Yii::t('app', 'Industry ID'),
            'preferred_user_id' => Yii::t('app', 'Preferred User ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustryUser()
    {
        return $this->hasOne(User::className(), ['id' => 'industry_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreferredUser()
    {
        return $this->hasOne(User::className(), ['id' => 'preferred_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return PreferredListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PreferredListQuery(get_called_class());
    }
}

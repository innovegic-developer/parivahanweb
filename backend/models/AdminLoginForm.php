<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class AdminLoginForm extends Model {

    public $username;
    public $email_username;
    public $password;
    public $rememberMe = false;
    public $captcha;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // username and password are both required
            [['password'], 'required'],
            
            [['username'],'required','message' => 'Email / Username can not be blank.'],
            
            //[['username'],'match', 'pattern' => '/^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/','message' => 'Please use letters only (a-z or A-Z or 0-9) and space in username.'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // email_username is validated by validateEmailUsername()
          //  ['email_username', 'validateEmailUsername'],
            // password is validated by validatePassword()
           ['password', 'validatePassword'],
            
            //['captcha', 'captcha'],
            
        ];
    }

    /**
     * Validates the email or username.
     * This method serves as the inline validation for email or username.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateEmailUsername($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validateEmailUsername($this->email_username)) {
                $this->addError($attribute, 'Incorrect email or username.');
            }
        }
    }
    
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            //p($user);

            /* if (!$user || !$user->validatePassword($this->password)) {
              $this->addError($attribute, 'Incorrect username or password.');
              }
             * 
             */
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            } 
            // if ($_SESSION['captcha']!=$_SESSION['captchaHidden']) {
            //     $this->addError($attribute, 'Wrong Captcha.');
            // }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login() {
        
        if ($this->validate()) {                        
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser() {
        
        if ($this->_user === false) {   
            
            $this->_user = AdminUsers::findByEmailAddressOrUserName($this->username);                
        }
        //p($this->_user);
        return $this->_user;
    }

     public function validateCaptcha($captcha, $captchaHidden) {
        
            if ($captcha!=$captchaHidden) {
                $this->addError('password', 'Wrong Captcha.');
        }
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trip_tracking".
 *
 * @property string $id ID
 * @property int $load_id Load ID
 * @property int $truck_id Truck ID
 * @property string $latitude Latitude
 * @property string $longitude Longitude
 * @property int $driver_user_id Driver User ID
 * @property string $created_on Created On
 * @property string $updated_on Updated On
 *
 * @property Loads $load
 * @property Truck $truck
 * @property User $driverUser
 */
class TripTracking extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trip_tracking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['load_id', 'truck_id'], 'required'],
            [['load_id', 'truck_id', 'driver_user_id'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['latitude', 'longitude'], 'string', 'max' => 50],
            [['load_id'], 'exist', 'skipOnError' => true, 'targetClass' => Loads::className(), 'targetAttribute' => ['load_id' => 'id']],
            [['truck_id'], 'exist', 'skipOnError' => true, 'targetClass' => Truck::className(), 'targetAttribute' => ['truck_id' => 'id']],
            [['driver_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['driver_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'load_id' => Yii::t('app', 'Load ID'),
            'truck_id' => Yii::t('app', 'Truck ID'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'driver_user_id' => Yii::t('app', 'Driver User ID'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Updated On'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoad()
    {
        return $this->hasOne(Loads::className(), ['id' => 'load_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTruck()
    {
        return $this->hasOne(Truck::className(), ['id' => 'truck_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverUser()
    {
        return $this->hasOne(User::className(), ['id' => 'driver_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return TripTrackingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TripTrackingQuery(get_called_class());
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quotation_truck".
 *
 * @property int $id ID
 * @property int $quotation_id Quotation ID
 * @property int $load_id Load ID
 * @property int $truck_id Truck ID
 * @property int $user_id User ID
 * @property string $vehicle_no Vehicle No.
 * @property string $gps_installed GPS Installed
 * @property string $weight Weight (MT)
 * @property string $truck_type Truck Type
 * @property string $rc_no RC No,
 * @property string $rc_photo RC Book Photo
 * @property string $license_no License No.
 * @property string $license_photo License Photo
 * @property string $permit_type Permit Type
 * @property string $permit_photo Permit Photo
 * @property string $insurance_policy_no Insurance Policy No.
 * @property string $insurance_policy_photo Insurance Policy Photo
 * @property string $pollution_no Pollution No.
 * @property string $pollution_photo Pollution Photo
 * @property string $rto_fitness_certificate_no RTO Fitness Certificate No.
 * @property string $rto_fitness_certificate_photo RTO Fitness Certificate Photo
 * @property string $local_registration_no Local Registration No.
 * @property string $local_registration_photo Local Registration Photo
 * @property string $registration_address Registration Address
 * @property string $registration_mobile Registration Mobile No.
 * @property string $office_landline_no Office Landline No.
 * @property string $email Email
 * @property string $gross_weight Gross Weight
 * @property string $verified Verified
 * @property int $assigned_driver_id
 * @property string $assigned_driver_name
 * @property string $assigned_driver_mobile
 * @property string $assigned_driver_aadhaar_number
 * @property string $assigned_driver_at
 * @property string $status Status
 * @property string $created_at Created On
 * @property string $updated_at Updated On
 * @property int $created_by Created By
 * @property int $updated_by Updated By
 *
 * @property Truck $truck
 * @property User $user
 */
class QuotationTruck extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quotation_truck';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quotation_id', 'load_id', 'truck_id', 'user_id'], 'required'],
            [['quotation_id', 'load_id', 'truck_id', 'user_id', 'assigned_driver_id', 'created_by', 'updated_by'], 'integer'],
            [['gps_installed', 'verified', 'status'], 'string'],
            [['assigned_driver_at', 'created_at', 'updated_at'], 'safe'],
            [['vehicle_no'], 'string', 'max' => 50],
            [['weight', 'truck_type', 'rc_no', 'license_no', 'permit_type', 'insurance_policy_no', 'pollution_no', 'rto_fitness_certificate_no', 'local_registration_no'], 'string', 'max' => 100],
            [['rc_photo', 'license_photo', 'permit_photo', 'insurance_policy_photo', 'pollution_photo', 'rto_fitness_certificate_photo', 'local_registration_photo', 'registration_address', 'email'], 'string', 'max' => 250],
            [['registration_mobile', 'office_landline_no', 'assigned_driver_mobile', 'assigned_driver_aadhaar_number'], 'string', 'max' => 20],
            [['gross_weight'], 'string', 'max' => 10],
            [['assigned_driver_name'], 'string', 'max' => 256],
            [['truck_id'], 'exist', 'skipOnError' => true, 'targetClass' => Truck::className(), 'targetAttribute' => ['truck_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'quotation_id' => Yii::t('app', 'Quotation ID'),
            'load_id' => Yii::t('app', 'Load ID'),
            'truck_id' => Yii::t('app', 'Truck ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'vehicle_no' => Yii::t('app', 'Vehicle No.'),
            'gps_installed' => Yii::t('app', 'GPS Installed'),
            'weight' => Yii::t('app', 'Weight (MT)'),
            'truck_type' => Yii::t('app', 'Truck Type'),
            'rc_no' => Yii::t('app', 'RC No,'),
            'rc_photo' => Yii::t('app', 'RC Book Photo'),
            'license_no' => Yii::t('app', 'License No.'),
            'license_photo' => Yii::t('app', 'License Photo'),
            'permit_type' => Yii::t('app', 'Permit Type'),
            'permit_photo' => Yii::t('app', 'Permit Photo'),
            'insurance_policy_no' => Yii::t('app', 'Insurance Policy No.'),
            'insurance_policy_photo' => Yii::t('app', 'Insurance Policy Photo'),
            'pollution_no' => Yii::t('app', 'Pollution No.'),
            'pollution_photo' => Yii::t('app', 'Pollution Photo'),
            'rto_fitness_certificate_no' => Yii::t('app', 'RTO Fitness Certificate No.'),
            'rto_fitness_certificate_photo' => Yii::t('app', 'RTO Fitness Certificate Photo'),
            'local_registration_no' => Yii::t('app', 'Local Registration No.'),
            'local_registration_photo' => Yii::t('app', 'Local Registration Photo'),
            'registration_address' => Yii::t('app', 'Registration Address'),
            'registration_mobile' => Yii::t('app', 'Registration Mobile No.'),
            'office_landline_no' => Yii::t('app', 'Office Landline No.'),
            'email' => Yii::t('app', 'Email'),
            'gross_weight' => Yii::t('app', 'Gross Weight'),
            'verified' => Yii::t('app', 'Verified'),
            'assigned_driver_id' => Yii::t('app', 'Assigned Driver ID'),
            'assigned_driver_name' => Yii::t('app', 'Assigned Driver Name'),
            'assigned_driver_mobile' => Yii::t('app', 'Assigned Driver Mobile'),
            'assigned_driver_aadhaar_number' => Yii::t('app', 'Assigned Driver Aadhaar Number'),
            'assigned_driver_at' => Yii::t('app', 'Assigned Driver At'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTruck()
    {
        return $this->hasOne(Truck::className(), ['id' => 'truck_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return QuotationTruckQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new QuotationTruckQuery(get_called_class());
    }
}

<?php

namespace app\models;

use Yii;
use app\models\UserType;
/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $mobile_number
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $password_hash
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $user_image
 * @property int $user_level
 * @property int $diesel_society_code
 * @property int $officer_area
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property RoleTypes $userLevel
 */
class User extends \yii\db\ActiveRecord
{
    public static function getDb() {
        if (!defined('DB'))  define('DB', '');
        if(DB=='db1') return Yii::$app->db1; else return Yii::$app->db;    
    }
    
    public $current_password;
    public $new_password;
    public $repeat_password;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'mobile_number'], 'required'],
            [['user_level', 'status', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'last_name', 'username', 'password', 'auth_key', 'password_reset_token'], 'string', 'max' => 250],
            [['mobile_number'], 'string', 'max' => 30],
            [['email'], 'string', 'max' => 500],
            ['email', 'email'],
            [['password_hash'], 'string', 'max' => 255],
            [['username'], 'unique','message' => 'This username has already exist.'],
            [['user_image'], 'file', 'skipOnEmpty' => true],
            [['user_image'], 'file', 'extensions' => 'png, jpg, jpeg',],
            // [['password'], 'match', 'pattern' => '$\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$', 'message' => 'Password should contains at least 1 lowercase and 1 uppercase, 1 digit, 1 special character "@$#%&".'],
           // [['user_level'], 'exist', 'skipOnError' => true, 'targetClass' => UserType::className(), 'targetAttribute' => ['user_level' => 'role_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'mobile_number' => 'Mobile Number',
            'username' => 'Username/Aadhar No.',
            'email' => 'Email',
            'password' => 'Password',
            'password_reset_token' => 'Password Reset Token',
            'user_image' => 'User Image',
            'user_level' => 'User Level',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLevel()
    {
        return $this->hasOne(UserType::className(), ['TypeID' => 'user_level']);
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }


   
}

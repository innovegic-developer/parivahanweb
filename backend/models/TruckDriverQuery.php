<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TruckDriver]].
 *
 * @see TruckDriver
 */
class TruckDriverQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TruckDriver[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TruckDriver|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

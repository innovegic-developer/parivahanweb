<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Industry;

/**
 * IndustrySearch represents the model behind the search form of `app\models\Industry`.
 */
class IndustrySearch extends Industry
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'company_type_id', 'verified', 'created_by', 'updated_by'], 'integer'],
            [['first_name', 'last_name', 'email', 'mobile_number', 'company_name', 'company_type', 'incorporation_certificate_number', 'shop_establishment_certificate_number', 'pan_number', 'incorporation_certificate_photo', 'shop_establishment_certificate_photo', 'gst_number', 'aadhaar_number', 'aadhaar_photo', 'pan_photo', 'address_line1', 'address_line2', 'pincode', 'country', 'state', 'district', 'city', 'area', 'landmark', 'remark', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Industry::find();

        // add conditions that should always apply here
		$query->orderBy(['id' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'company_type_id' => $this->company_type_id,
            'verified' => $this->verified,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'mobile_number', $this->mobile_number])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'company_type', $this->company_type])
            ->andFilterWhere(['like', 'incorporation_certificate_number', $this->incorporation_certificate_number])
            ->andFilterWhere(['like', 'shop_establishment_certificate_number', $this->shop_establishment_certificate_number])
            ->andFilterWhere(['like', 'pan_number', $this->pan_number])
            ->andFilterWhere(['like', 'incorporation_certificate_photo', $this->incorporation_certificate_photo])
            ->andFilterWhere(['like', 'shop_establishment_certificate_photo', $this->shop_establishment_certificate_photo])
            ->andFilterWhere(['like', 'gst_number', $this->gst_number])
            ->andFilterWhere(['like', 'aadhaar_number', $this->aadhaar_number])
            ->andFilterWhere(['like', 'aadhaar_photo', $this->aadhaar_photo])
            ->andFilterWhere(['like', 'pan_photo', $this->pan_photo])
            ->andFilterWhere(['like', 'address_line1', $this->address_line1])
            ->andFilterWhere(['like', 'address_line2', $this->address_line2])
            ->andFilterWhere(['like', 'pincode', $this->pincode])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'district', $this->district])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'area', $this->area])
            ->andFilterWhere(['like', 'landmark', $this->landmark])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

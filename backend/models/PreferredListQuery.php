<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PreferredList]].
 *
 * @see PreferredList
 */
class PreferredListQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PreferredList[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PreferredList|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

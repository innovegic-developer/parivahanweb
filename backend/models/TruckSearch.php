<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Truck;

/**
 * TruckSearch represents the model behind the search form of `app\models\Truck`.
 */
class TruckSearch extends Truck
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'created_by', 'updated_by'], 'integer'],
            [['vehicle_no', 'gps_installed', 'weight', 'truck_type', 'rc_no', 'license_no', 'license_photo', 'permit_type', 'permit_photo', 'insurance_policy_no', 'insurance_policy_photo', 'pollution_no', 'pollution_photo', 'rto_fitness_certificate_no', 'rto_fitness_certificate_photo', 'local_registration_no', 'local_registration_photo', 'verified', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Truck::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'vehicle_no', $this->vehicle_no])
            ->andFilterWhere(['like', 'gps_installed', $this->gps_installed])
            ->andFilterWhere(['like', 'weight', $this->weight])
            ->andFilterWhere(['like', 'truck_type', $this->truck_type])
            ->andFilterWhere(['like', 'rc_no', $this->rc_no])
            ->andFilterWhere(['like', 'license_no', $this->license_no])
            ->andFilterWhere(['like', 'license_photo', $this->license_photo])
            ->andFilterWhere(['like', 'permit_type', $this->permit_type])
            ->andFilterWhere(['like', 'permit_photo', $this->permit_photo])
            ->andFilterWhere(['like', 'insurance_policy_no', $this->insurance_policy_no])
            ->andFilterWhere(['like', 'insurance_policy_photo', $this->insurance_policy_photo])
            ->andFilterWhere(['like', 'pollution_no', $this->pollution_no])
            ->andFilterWhere(['like', 'pollution_photo', $this->pollution_photo])
            ->andFilterWhere(['like', 'rto_fitness_certificate_no', $this->rto_fitness_certificate_no])
            ->andFilterWhere(['like', 'rto_fitness_certificate_photo', $this->rto_fitness_certificate_photo])
            ->andFilterWhere(['like', 'local_registration_no', $this->local_registration_no])
            ->andFilterWhere(['like', 'local_registration_photo', $this->local_registration_photo])
            ->andFilterWhere(['like', 'verified', $this->verified])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

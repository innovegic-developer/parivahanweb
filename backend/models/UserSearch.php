<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
/**
 * UsersSearch represents the model behind the search form of `app\models\Users`.
 */
class UserSearch extends User
{
    public $user_level;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_level', 'status', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'last_name', 'mobile_number', 'username', 'email', 'password', 'password_reset_token', 'user_image', 'user_image', 'user_level'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
        $query->joinWith(['userLevel']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['status'=> '1']);
        // grid filtering conditions
        $query->andFilterWhere(['not in', 'id', '28247']); // test account
        $query->andFilterWhere(['not in', 'id', '28186']); // test account
        $query->andFilterWhere(['not in', 'id', '28191']); // test account
        $query->andFilterWhere([
            'id' => $this->id,
            'user_level' => $this->user_level,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        //$query->andFilterWhere('id not in (28247,28186,28191)');
        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'mobile_number', $this->mobile_number])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'user_type_master.RoleType', $this->user_level]);

        return $dataProvider;
    }

     public function searchUsers($params)
    {
        $query = User::find();
        $query->joinWith(['userLevel']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
      
        $query->andFilterWhere([
            'id' => $this->id,
            'user_level' => $this->user_level,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'mobile_number', $this->mobile_number])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'user_type_master.RoleType', $this->user_level]);

        /*
        $query->andFilterWhere(['or',
        ['user_level'=>'28'],
        ['user_level'=>'29'],
        ['user_level'=>'33'],
        ['user_level'=>'34'],
        ['user_level'=>'35'],
        ['user_level'=>'36'],
        ['user_level'=>'37'],
        ['user_level'=>'38'],
        ['user_level'=>'39'],
        ['user_level'=>'40'],]);*/
        return $dataProvider;
    }
}
    
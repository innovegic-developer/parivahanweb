<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user_type_master}}".
 *
 * @property int $TypeID
 * @property string $Alias
 * @property string $RoleType User type based on role of user of different departments of HO / RO / SP / Customer / Development
 * @property int $IsActive 1 = Active, 0 = Inactive
 * @property string $CreatedDateTime
 * @property string $UpdatedDateTime
 * @property int $CreatedBy
 * @property int $UpdatedBy
 * @property int $IsDeleted 1 = Yes, 0 = No
 *
 * @property User[] $users
 */
class UserType extends \yii\db\ActiveRecord
{
    public static function getDb() {
        if (!defined('DB'))  define('DB', '');
        if(DB=='db1') return Yii::$app->db1; else return Yii::$app->db;    
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_type}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['RoleType'], 'required'],
            [['IsActive', 'CreatedBy', 'UpdatedBy', 'IsDeleted'], 'integer'],
            [['CreatedDateTime', 'UpdatedDateTime'], 'safe'],
            [['Alias'], 'string', 'max' => 20],
            [['RoleType'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TypeID' => Yii::t('app', 'Type ID'),
            'Alias' => Yii::t('app', 'Alias'),
            'RoleType' => Yii::t('app', 'User type based on role of user of different departments of HO / RO / SP / Customer / Development'),
            'IsActive' => Yii::t('app', '1 = Active, 0 = Inactive'),
            'CreatedDateTime' => Yii::t('app', 'Created Date Time'),
            'UpdatedDateTime' => Yii::t('app', 'Updated Date Time'),
            'CreatedBy' => Yii::t('app', 'Created By'),
            'UpdatedBy' => Yii::t('app', 'Updated By'),
            'IsDeleted' => Yii::t('app', '1 = Yes, 0 = No'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['user_level' => 'TypeID']);
    }

    /**
     * {@inheritdoc}
     * @return UserTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserTypeQuery(get_called_class());
    }
}

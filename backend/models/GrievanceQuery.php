<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Grievance]].
 *
 * @see Grievance
 */
class GrievanceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Grievance[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Grievance|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

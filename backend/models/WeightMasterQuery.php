<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[WeightMaster]].
 *
 * @see WeightMaster
 */
class WeightMasterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return WeightMaster[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return WeightMaster|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

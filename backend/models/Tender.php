<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tender".
 *
 * @property int $id ID
 * @property string $tender_title Title (En)	
 * @property string $tender_desc Description (En)	
 * @property string $tender_title_hi Title (Hi)	
 * @property string $tender_desc_hi Description (Hi)	
 * @property string $tender_title_gu Title (Gu)	
 * @property string $tender_desc_gu Description (Gu)	
 * @property string $tender_pdf Upload PDF	
 * @property string $created_on Created On	
 * @property string $updated_on Modified On	
 * @property int $created_by Created By	
 * @property int $updated_by Modified By	
 * @property string $status Status
 */
class Tender extends \yii\db\ActiveRecord
{
    public static function getDb() {
        if (!defined('DB'))  define('DB', '');
        if(DB=='db1') return Yii::$app->db1; else return Yii::$app->db;    
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tender';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tender_title', 'tender_desc', 'tender_title_hi', 'tender_desc_hi', 'tender_title_gu', 'tender_desc_gu', 'tender_pdf', 'status'], 'required'],
            [['created_on', 'updated_on'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['tender_title', 'tender_title_hi', 'tender_title_gu'], 'string', 'max' => 256],
            [['tender_desc', 'tender_desc_hi', 'tender_desc_gu'], 'string', 'max' => 5000],
             [['tender_pdf'], 'file', 'extensions' => 'pdf',],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tender_title' => Yii::t('app', 'Title (En)	'),
            'tender_desc' => Yii::t('app', 'Description (En)	'),
            'tender_title_hi' => Yii::t('app', 'Title (Hi)	'),
            'tender_desc_hi' => Yii::t('app', 'Description (Hi)	'),
            'tender_title_gu' => Yii::t('app', 'Title (Gu)	'),
            'tender_desc_gu' => Yii::t('app', 'Description (Gu)	'),
            'tender_pdf' => Yii::t('app', 'Upload PDF	'),
            'created_on' => Yii::t('app', 'Created On	'),
            'updated_on' => Yii::t('app', 'Modified On	'),
            'created_by' => Yii::t('app', 'Created By	'),
            'updated_by' => Yii::t('app', 'Modified By	'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TenderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TenderQuery(get_called_class());
    }
}

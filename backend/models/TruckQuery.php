<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Truck]].
 *
 * @see Truck
 */
class TruckQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Truck[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Truck|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

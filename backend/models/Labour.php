<?php

namespace app\models;

use app\models\PensionScheme;

use Yii;

/**
 * This is the model class for table "labour".
 *
 * @property int $id ID
 * @property string $registration_no Registration No.
 * @property string $registration_date Registration Date
 * @property string $birth_date Birth Date
 * @property string $gender Gender
 * @property string $profile_photo Photo
 * @property string $beneficiary_name Beneficiary Name
 * @property string $aadhaar_number Aadhaar Number
 * @property string $father_husband Father/Husband Name
 * @property string $mobile_no Mobile No
 * @property string $email E-Mail ID
 * @property string $category Category
 * @property string $temporary_address1 Address1
 * @property string $temporary_address2 Address2
 * @property string $temporary_landmark Landmark
 * @property string $temporary_village Village/Town/City
 * @property string $temporary_taluka Taluka
 * @property string $temporary_district District
 * @property string $temporary_state State
 * @property string $temporary_pincode Pincode
 * @property string $permanent_address1 Address1
 * @property string $permanent_address2 Address2
 * @property string $permanent_landmark Landmark
 * @property string $permanent_village Village/Town/City
 * @property string $permanent_taluka Taluka
 * @property string $permanent_district District
 * @property string $permanent_state State
 * @property string $permanent_pincode Pincode
 * @property string $bank_account Bank A/C Number
 * @property string $bank_name Bank Name
 * @property string $bank_branch Bank Branch
 * @property string $employer_name Employer Name
 * @property string $employer_address Employer Address
 * @property string $employer_registration_no Employer Registration No.
 * @property string $aadhaar_card_photo Upload Aadhaar Card
 * @property string $bank_detail_photo Upload Bank Detail
 * @property string $created_at Created On
 * @property string $updated_at Updated On
 * @property int $created_by Created By
 * @property int $updated_by Updated By
 */
class Labour extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'labour';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['registration_date', 'created_at', 'updated_at'], 'safe'],
            [['gender'], 'string'],
            [['aadhaar_card_photo', 'bank_detail_photo', 'created_by', 'updated_by'], 'required'],
            [['created_by', 'updated_by'], 'integer'],
            [['registration_no', 'birth_date', 'profile_photo', 'beneficiary_name', 'aadhaar_number', 'father_husband', 'category', 'temporary_address1', 'temporary_landmark', 'temporary_village', 'temporary_taluka', 'temporary_district', 'temporary_state', 'permanent_address1', 'permanent_landmark', 'permanent_village', 'permanent_taluka', 'permanent_district', 'permanent_state', 'employer_name', 'employer_address', 'employer_registration_no', 'aadhaar_card_photo'], 'required'],
            [['registration_no', 'birth_date', 'profile_photo', 'beneficiary_name', 'aadhaar_number', 'father_husband', 'mobile_no', 'email', 'category', 'temporary_address1', 'temporary_address2', 'temporary_landmark', 'temporary_village', 'temporary_taluka', 'temporary_district', 'temporary_state', 'permanent_address1', 'permanent_address2', 'permanent_landmark', 'permanent_village', 'permanent_taluka', 'permanent_district', 'permanent_state', 'bank_name', 'employer_name', 'employer_address', 'employer_registration_no', 'aadhaar_card_photo', 'bank_detail_photo'], 'string', 'max' => 250],
            [['temporary_pincode', 'permanent_pincode'], 'string', 'max' => 10],
            [['bank_account'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'registration_no' => Yii::t('app', 'Registration No.'),
            'registration_date' => Yii::t('app', 'Registration Date'),
            'birth_date' => Yii::t('app', 'Birth Date'),
            'gender' => Yii::t('app', 'Gender'),
            'profile_photo' => Yii::t('app', 'Photo'),
            'beneficiary_name' => Yii::t('app', 'Beneficiary Name'),
            'aadhaar_number' => Yii::t('app', 'Aadhaar Number'),
            'father_husband' => Yii::t('app', 'Father/Husband Name'),
            'mobile_no' => Yii::t('app', 'Mobile No'),
            'email' => Yii::t('app', 'E-Mail ID'),
            'category' => Yii::t('app', 'Category'),
            'temporary_address1' => Yii::t('app', 'Address1'),
            'temporary_address2' => Yii::t('app', 'Address2'),
            'temporary_landmark' => Yii::t('app', 'Landmark'),
            'temporary_village' => Yii::t('app', 'Village/Town/City'),
            'temporary_taluka' => Yii::t('app', 'Taluka'),
            'temporary_district' => Yii::t('app', 'District'),
            'temporary_state' => Yii::t('app', 'State'),
            'temporary_pincode' => Yii::t('app', 'Pincode'),
            'permanent_address1' => Yii::t('app', 'Address1'),
            'permanent_address2' => Yii::t('app', 'Address2'),
            'permanent_landmark' => Yii::t('app', 'Landmark'),
            'permanent_village' => Yii::t('app', 'Village/Town/City'),
            'permanent_taluka' => Yii::t('app', 'Taluka'),
            'permanent_district' => Yii::t('app', 'District'),
            'permanent_state' => Yii::t('app', 'State'),
            'permanent_pincode' => Yii::t('app', 'Pincode'),
            'bank_account' => Yii::t('app', 'Bank A/C Number'),
            'bank_name' => Yii::t('app', 'Bank Name'),
            'bank_branch' => Yii::t('app', 'Bank Branch'),
            'employer_name' => Yii::t('app', 'Employer Name'),
            'employer_address' => Yii::t('app', 'Employer Address'),
            'employer_registration_no' => Yii::t('app', 'Employer Registration No.'),
            'aadhaar_card_photo' => Yii::t('app', 'Upload Aadhaar Card'),
            'bank_detail_photo' => Yii::t('app', 'Upload Bank Detail'),
            'created_at' => Yii::t('app', 'Created On'),
            'updated_at' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return LabourQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LabourQuery(get_called_class());
    }

    public function getPensionScheme()
    {
        return $this->hasMany(PensionScheme::className(), ['labour_id' => 'id']);
    }
    public function getPension_scheme()
    {
        return $this->hasMany(PensionScheme::className(), ['labour_id' => 'id']);
    }
}

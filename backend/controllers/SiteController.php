<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\controllers\CommonController;
use backend\models\AdminForgotPasswordForm;
use backend\models\ResetSitePasswordForm;
use yii\bootstrap\ActiveForm;
use yii\web\UploadedFile;
use yii\imagine\Image;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','captcha'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['login', 'error','setlanguage'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['login', 'error','setlocation'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['profile'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    public function init() {

        $session = Yii::$app->session;
        if (isset($session['current_language']) && !empty($session['current_language'])) {
            $site_language = $session['current_language'];
        } else {
            $site_language = "en";
        }
        Yii::$app->language = $site_language;

        if (isset($_SESSION['location']) && !empty($_SESSION['location'])) {
            $location = $_SESSION['location'];
        } else {
            //$location = DEFAULT_LOCATION;
	   $location = "";	
        }
        $_SESSION['location'] = $location;
                
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($action='')
    {
		$user_level = Yii::$app->user->identity->user_level;
		
		if($user_level==1 || $user_level==2)
		{
			// Other State To DNH
			/*$TotalIndustryDnh = \app\models\Industry::find()->where(['district' => 'Dadra and Nagar Haveli', 'status' => 'Active'])
			->andWhere('first_name!="test" and last_name!="test" and company_name!="test"')->count();
			$TotalIndustryDaman = \app\models\Industry::find()->where(['district' => 'Daman', 'status' => 'Active'])
			->andWhere('first_name!="test" and last_name!="test" and company_name!="test"')->count();
			$TotalIndustryDiu = \app\models\Industry::find()->where(['district' => 'Diu', 'status' => 'Active'])
			->andWhere('first_name!="test" and last_name!="test" and company_name!="test"')->count();
			
			$TotalTransporterDnh = \app\models\Transporter::find()->where(['district' => 'Dadra and Nagar Haveli', 'status' => 'Active'])
			->andWhere('first_name!="test" and last_name!="test" and company_name!="test"')->count();
			$TotalTransporterDaman = \app\models\Transporter::find()->where(['district' => 'Daman', 'status' => 'Active'])
			->andWhere('first_name!="test" and last_name!="test" and company_name!="test"')->count();
			$TotalTransporterDiu = \app\models\Transporter::find()->where(['district' => 'Diu', 'status' => 'Active'])
			->andWhere('first_name!="test" and last_name!="test" and company_name!="test"')->count();*/
			
			if($action=='refresh')
			{
				$post['test']='1';
				$endURL = SITE_PATH. '/pages/statistics_cron.php';
				$curl = curl_init($endURL);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
				curl_setopt($curl, CURLOPT_TIMEOUT, 1); 
				curl_setopt($curl, CURLOPT_HEADER, 0);
				curl_setopt($curl,  CURLOPT_RETURNTRANSFER, false);
				curl_setopt($curl, CURLOPT_FORBID_REUSE, true);
				curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 1);
				curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT, 5);
				curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
				$response = curl_exec($curl);	
			}				
			$sql = 'SELECT * FROM dashboard_statistics';
			$Rows = \app\models\Industry::findBySql($sql)->asArray()->one();
			$row = json_decode($Rows['data'],true);
			extract($row);
			$TotalLoads = $TotalLoadsDnh + $TotalLoadsDaman + $TotalLoadsDiu;
			return $this->render('index', [
			
                'TotalIndustryDnh' => $TotalIndustryDnh,
				'TotalIndustryDaman' => $TotalIndustryDaman,
				'TotalIndustryDiu' => $TotalIndustryDiu,
				
				'TotalTransporterDnh' => $TotalTransporterDnh,
				'TotalTransporterDaman' => $TotalTransporterDaman,
				'TotalTransporterDiu' => $TotalTransporterDiu,
				
				'TotalLoadsDnh' => $TotalLoadsDnh,
				'TotalLoadsDaman' => $TotalLoadsDaman,
				'TotalLoadsDiu' => $TotalLoadsDiu,
				
				'TotalLoads' => $TotalLoads,
				
				'TodaysLoadsDnh' => $TodaysLoadsDnh,
				'TodaysLoadsDaman' => $TodaysLoadsDaman,
				'TodaysLoadsDiu' => $TodaysLoadsDiu,
				
				'OpenLoadsDnh' => $OpenLoadsDnh,
				'OpenLoadsDaman' => $OpenLoadsDaman,
				'OpenLoadsDiu' => $OpenLoadsDiu,
				
				'AssignedLoadsDnh' => $AssignedLoadsDnh,
				'AssignedLoadsDaman' => $AssignedLoadsDaman,
				'AssignedLoadsDiu' => $AssignedLoadsDiu,
				
				'ClosedLoadsDnh' => $ClosedLoadsDnh,
				'ClosedLoadsDaman' => $ClosedLoadsDaman,
				'ClosedLoadsDiu' => $ClosedLoadsDiu,
				
				'ActiveTripDnh' => $ActiveTripDnh,
				'ActiveTripDaman' => $ActiveTripDaman,
				'ActiveTripDiu' => $ActiveTripDiu,
                   
			]);			 
		}	
		return $this->render('index');
    }
	
	/**
     * Login action.
     *
     * @return string
     */
    // public function actionLogin()
    // {
    //     $this->layout = 'login';
    //     if (!Yii::$app->user->isGuest) {
    //         return $this->goHome();
    //     }

    //     $model = new LoginForm();
    //     if ($model->load(Yii::$app->request->post()) && $model->login()) {
    //         return $this->goBack();
    //     } else {
    //         $model->password = '';

    //         return $this->render('login', [
    //             'model' => $model,
    //         ]);
    //     }
    // }
     public function actionLogin() {
        $this->layout = 'login';

        if (!Yii::$app->user->isGuest) {
             //return $this->redirect(Url::toRoute(['dashboard/index']));
             return $this->goHome(); 
        }

        $model = new \backend\models\AdminLoginForm();
        // if ($model->load(Yii::$app->request->post()) )
        // {    
        //     if(isset($_POST['AdminLoginForm']['Captcha']))
        //     {
        //         $_SESSION['captcha'] = $_POST['AdminLoginForm']['Captcha'];
        //         $_SESSION['captchaHidden'] = $_POST['AdminLoginForm']['CaptchaHidden'];
        //     }
        //     else
        //     {
        //         $_SESSION['captcha'] = '1234';
        //         $_SESSION['captchaHidden'] = '1234';
        //     }
        // }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            // $this->redirect(\Yii::$app->getUrlManager()->createUrl('dashboard'));
            return $this->goBack();
        } else {
            $model->password = '';
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    public function actionProfile() {
        //echo "sddgds";exit;
        $current_user_id = Yii::$app->user->identity->id;
        // $current_user_id = \Yii::$app->common->get_current_user_data_by_field('id');
        $model = new \app\models\User();
        $model = \app\models\User::findOne($current_user_id);
        
      //  p($model);

        // $pre_profile_picture = $model->profile_picture;

        if (Yii::$app->request->isPost) {
            if (empty($validate)) {

                foreach ($_POST['User'] as $key => $val) {
                    if (!empty($_POST['User'][$key])) {
                        $model->$key = $_POST['User'][$key];
                    }
                }
                $profile_pic = $_FILES['User']['name']['user_image'];

                if (!empty($profile_pic)) {
                    $image = UploadedFile::getInstance($model, 'user_image');

                    $extensions = explode(".", $image->name);
                    $extension = end($extensions);
                    $new_image_name = Yii::$app->security->generateRandomString() . ".{$extension}";

                    $original_path = PROFILE_PICTURE_ORIGINAL . $new_image_name;
                    $thumb_path = PROFILE_PICTURE_THUMBNAIL . $new_image_name;

                    $upload_pic = $image->saveAs($original_path);
                    if (file_exists($original_path)) {
                        $model->user_image = $new_image_name;
                        //Start: For delete previous image
                        if (!empty($pre_profile_picture) && file_exists(PROFILE_PICTURE_ORIGINAL . $pre_profile_picture)) {
                            unlink(PROFILE_PICTURE_ORIGINAL . $pre_profile_picture);
                        }
                        //#unlink(PROFILE_PICTURE_ORIGINAL.$pre_profile_picture);
                        if (!empty($pre_profile_picture) && file_exists(PROFILE_PICTURE_THUMBNAIL . $pre_profile_picture)) {
                            unlink(PROFILE_PICTURE_THUMBNAIL . $pre_profile_picture);
                        }
                        //End: For delete previous image
                        // Image::thumbnail($original_path, THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT)
                        //         ->save($thumb_path, ['quality' => 100]);
                    } else {
                        $model->user_image = $pre_profile_picture;
                    }
                }

                if ($model->save(False)) {
                    \Yii::$app->getSession()->setFlash('success', "Profile has been updated successfully.");
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('site/profile/'));
                    Yii::$app->end();
                } else {
                    \Yii::$app->getSession()->setFlash('error', "Profile not updated.");
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('site/profile/'));
                    Yii::$app->end();
                }
            } else {

                if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            }
        } else {
            return $this->render('profile', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSetlanguage() {

        $data = Yii::$app->request->post();
        $session = Yii::$app->session;
        $session['current_language'] = trim($data['lang_code']);   
        //$url = preg_replace('/\?.*/', '', $_SERVER['HTTP_REFERER']);
        $url = trim($data['current_url']);  
        $this->redirect($url);
    }

    public function actionSetlocation() {

        $data = Yii::$app->request->post();
        $session = Yii::$app->session;
        $_SESSION['location'] = trim($data['location']);   
        //$url = preg_replace('/\?.*/', '', $_SERVER['HTTP_REFERER']);
        $url = trim($data['current_url']);  
        $this->redirect($url);
    }
}

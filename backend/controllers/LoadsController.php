<?php

namespace backend\controllers;

use Yii;
use app\models\Loads;
use app\models\LoadsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LoadsController implements the CRUD actions for Loads model.
 */
class LoadsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function actionImport()
    {
        $model = new Loads();
        if(isset($_FILES['uploadExcel']) && isset($_POST['Loads'])) {
            $post = $_POST['Loads']; 
            $fileName = $_FILES["uploadExcel"]["name"];
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            $fileName = time().'.'.$ext;
            // $file = UploadedFile::getInstances($model, 'uploadExcel');
            // $expload = explode(".", $file->name);
            // $ext = end($expload);
            // $excel = time() . ".{$ext}";
            // $path = UPLOAD_DIR_PATH . 'mandi/' . $excel;
            // $upload_file = $file->saveAs($path,false);
            $path = UPLOAD_DIR_PATH . '/' .$fileName;
            if(move_uploaded_file($_FILES["uploadExcel"]["tmp_name"], $path)) {
                $inputFile = '../uploads/'.$fileName;
           
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }
                //$objPHPExcel1 = \PHPExcel_IOFactory::load($inputFile);
                //$sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                //print_r($sheetData);exit;
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
           
                for($row=1; $row <= $highestRow; $row++)  {

                    $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,TRUE);
                    if($row==1)
                    {
                        continue;
                    }
                    if($rowData[0][0]=='')
                    {
                        continue;
                    }
					
					$i=0;
					//$model = Loads::find()->where(['mobile_number' => trim($rowData[0][0])])->one();
					//if(empty($model)) {
						//continue;
					//}
					$model = new Loads();
					$ind = Industry::find()->where(['mobile_number' => trim($rowData[0][0])])->one();
					//print_r($rowData[0]);exit;
					if(!empty(trim($rowData[0][$i])))
					$model->mobile_number  = trim($rowData[0][$i]);
					$model->industry_id	  = $ind->id;
					$model->industry_user_id  = $ind->user_id;
					$model->full_name  = $ind->first_name.' '.$ind->last_name;
					$model->company_name  = $ind->company_name;
				
					$i=1;
					if(!empty(trim($rowData[0][++$i])))
                    $model->source_address_line1 = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->source_address_line2 = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->source_pincode = trim($rowData[0][$i]); 
				
					//if(!empty(trim($rowData[0][++$i])))
                    //$model->source_state  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
                    $model->source_district = trim($rowData[0][$i]);
				
					//if(!empty(trim($rowData[0][++$i])))
                    $model->source_city  = $model->source_district;
				
					if(!empty(trim($rowData[0][++$i])))
					$model->source_area  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->source_landmark  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->destination_address_line1  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->destination_address_line2  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->destination_pincode  = trim($rowData[0][$i]);
				
					//if(!empty(trim($rowData[0][++$i])))
					//$model->destination_state  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->destination_district  = trim($rowData[0][$i]);
				
					//if(!empty(trim($rowData[0][++$i])))
					$model->destination_city  = $model->destination_district;
				
					if(!empty(trim($rowData[0][++$i])))
					$model->destination_area  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->destination_landmark  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->load_type  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->scheduling_type  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->material_type  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->weight  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->truck_type  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->no_of_trucks  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->reporting_date  = date('Y-m-d',strtotime(trim($rowData[0][$i])));
				
					if(!empty(trim($rowData[0][++$i])))
					$model->reporting_time  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->bid_closing_date  = date('Y-m-d',strtotime(trim($rowData[0][$i])));
				
					if(!empty(trim($rowData[0][++$i])))
					$model->bid_closing_time  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->quotation_amount_type  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->estimated_quotation_amount  = trim($rowData[0][$i]);
				
					if(!empty(trim($rowData[0][++$i])))
					$model->total_amount  = trim($rowData[0][$i]);
					else
					$model->total_amount  = $model->estimated_quotation_amount;
				
					if(!empty(trim($rowData[0][++$i])))
					$model->description  = trim($rowData[0][$i]);
				
					$model->created_by  = Yii::$app->user->id;
                    if($model->save(false)) {
						$id = $model->id;
						$model = Loads::findOne($id);
						$model->booking_id = 'LD'.$id;
						$model->save(false);
					}
                }
                unlink($path);
                return $this->redirect(['index']);
            }
        }
        return $this->render('import', [
            'model' => $model,
        ]);
    }
    /**
     * Lists all Loads models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LoadsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Loads model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

	public function actionDetail($id)
    {
        $quotModel = new \app\models\QuotationSearch();
        $dataProviderQuot = $quotModel->search(Yii::$app->request->queryParams);
		$dataProviderQuot->query->andWhere(['load_id' => $id]);
		
		$truckModel = new \app\models\QuotationTruckSearch();
        $dataProviderTruck = $truckModel->search(Yii::$app->request->queryParams);
		$dataProviderTruck->query->andWhere(['load_id' => $id])->andWhere('assigned_driver_user_id >0');
		
        return $this->render('detail', [
            'model' => $this->findModel($id),
			'quotModel' => $quotModel,
            'dataProviderQuot' => $dataProviderQuot,
			'truckModel' => $truckModel,
            'dataProviderTruck' => $dataProviderTruck,
        ]);
    }
	public function actionTracking()
    {
        return $this->render('tracking');
    }
    /**
     * Creates a new Loads model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Loads();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Loads model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Loads model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Loads model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Loads the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Loads::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

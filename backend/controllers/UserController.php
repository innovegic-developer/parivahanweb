<?php

namespace backend\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionIndustry()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->andWhere(['user_level' => 3]); 
        return $this->render('industry', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionTransporter()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->andWhere(['user_level' => 4]); 
        return $this->render('transporter', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionDriver()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->andWhere(['user_level' => 5]); 
        return $this->render('driver', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate() {
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {

            if (!empty(Yii::$app->request->post()['User']['password'])) {
                $model->password = md5(Yii::$app->request->post()['User']['password']);
            }
            /*$model->user_image ='no-image.png';
            if ($model->save(FALSE)) {
                return $this->redirect(['index', 'id' => $model->id]);
            }*/
            if (Yii::$app->request->isPost) {
            if ($model->validate()) {

                foreach ($_POST['User'] as $key => $val) {
                    if (!empty($_POST['User'][$key])) {
                        $model->$key = $_POST['User'][$key];
                    }
                }
                $profile_pic = $_FILES['User']['name']['user_image'];

                if (!empty($profile_pic)) {
                    $image = UploadedFile::getInstance($model, 'user_image');

                    $extensions = explode(".", $image->name);
                    $extension = end($extensions);
                    $new_image_name = Yii::$app->security->generateRandomString() . ".{$extension}";

                    $original_path = PROFILE_PICTURE_ORIGINAL . $new_image_name;
                    $thumb_path = PROFILE_PICTURE_THUMBNAIL . $new_image_name;

                    $upload_pic = $image->saveAs($original_path);
                    if (file_exists($original_path)) {
                        $model->user_image = $new_image_name;
                        //Start: For delete previous image
                        if (!empty($pre_profile_picture) && file_exists(PROFILE_PICTURE_ORIGINAL . $pre_profile_picture)) {
                            unlink(PROFILE_PICTURE_ORIGINAL . $pre_profile_picture);
                        }
                        //#unlink(PROFILE_PICTURE_ORIGINAL.$pre_profile_picture);
                        if (!empty($pre_profile_picture) && file_exists(PROFILE_PICTURE_THUMBNAIL . $pre_profile_picture)) {
                            unlink(PROFILE_PICTURE_THUMBNAIL . $pre_profile_picture);
                        }
                        //End: For delete previous image
                        Image::thumbnail($original_path, THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT)
                                ->save($thumb_path, ['quality' => 100]);
                    } else {
                        $model->user_image = $pre_profile_picture;
                    }
                }
                else {
                    $model->user_image = 'no-image.png';
                }

                if (!empty(Yii::$app->request->post()['User']['password'])) {
                $model->password = md5(Yii::$app->request->post()['User']['password']);
                }

                if ($model->save(False)) {

                    return $this->redirect(['index', 'id' => $model->id]);

                } else {

                   return $this->render('create', [
                            'model' => $model,
                    ]);

                }
            } else {

                if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            }
         } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        }

        return $this->render('create', [
                    'model' => $model,
                    'page' => 'New',
        ]);
    }
	
	

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
   public function actionUpdate($id) {
        $model = $this->findModel($id);

        //$searchModel = new UserSearch();
        //$dataProvider = $searchModel->searchByUser(Yii::$app->request->queryParams,$id);

        if (Yii::$app->request->isPost) {
            //if ($model->validate()) {
			if (true) {	

                foreach ($_POST['User'] as $key => $val) {
                    if (!empty($_POST['User'][$key])) {
                        $model->$key = $_POST['User'][$key];
                    }
                }
                $profile_pic = $_FILES['User']['name']['user_image'];
				 if (!empty($profile_pic)) {
                    $image = UploadedFile::getInstance($model, 'user_image');

                    $extensions = explode(".", $image->name);
                    $extension = end($extensions);
                    $new_image_name = Yii::$app->security->generateRandomString() . ".{$extension}";

                    $original_path = PROFILE_PICTURE_ORIGINAL . $new_image_name;
                    $thumb_path = PROFILE_PICTURE_THUMBNAIL . $new_image_name;

                    $upload_pic = $image->saveAs($original_path);
                    if (file_exists($original_path)) {
                        $model->user_image = $new_image_name;
                        //Start: For delete previous image
                        if (!empty($pre_profile_picture) && file_exists(PROFILE_PICTURE_ORIGINAL . $pre_profile_picture)) {
                            unlink(PROFILE_PICTURE_ORIGINAL . $pre_profile_picture);
                        }
                        //#unlink(PROFILE_PICTURE_ORIGINAL.$pre_profile_picture);
                        if (!empty($pre_profile_picture) && file_exists(PROFILE_PICTURE_THUMBNAIL . $pre_profile_picture)) {
                            unlink(PROFILE_PICTURE_THUMBNAIL . $pre_profile_picture);
                        }
                        //End: For delete previous image
                        Image::thumbnail($original_path, THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT)
                                ->save($thumb_path, ['quality' => 100]);
                    } else {
                        $model->user_image = $pre_profile_picture;
                    }
                }

                if (!empty(Yii::$app->request->post()['User']['password'])) {
                $model->password = md5(Yii::$app->request->post()['User']['password']);
                }

                if ($model->save(False)) {

                    return $this->redirect(['index', 'id' => $model->id]);

                } else {

                   return $this->render('update', [
                            'model' => $model,
                            //'searchModel' => $searchModel,
                           // 'dataProvider' => $dataProvider,
                    ]);

                }
            } else {

                if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            }
        } else {
           return $this->render('update', [
                    'model' => $model,
                    //'searchModel' => $searchModel,
                    //'dataProvider' => $dataProvider,
            ]);
        }
      
        
    }
	
	public function actionChangepassword($id) {
        $user_id = $id;
        // exit;
        $model = new AdminChangePasswordForm();

        if (Yii::$app->request->isPost) {

            $model->attributes = $_POST['AdminChangePasswordForm'];

            $validate = ActiveForm::validate($model);
            if (empty($validate)) {
                // $user_id = Yii::$app->common->get_current_user_data_by_field('id');
                $user_data = \app\models\User::find()
                        ->where(['id' => $user_id])
                        ->one();

                $new_pass = md5($_POST['AdminChangePasswordForm']['new_password']); //exit;

                $user_data->password = $new_pass;

                if ($user_data->save(False)) {
                    \Yii::$app->getSession()->setFlash('success', "Password has been changed successfully.");
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('users/index'));
                    Yii::$app->end();
                } else {
                    \Yii::$app->getSession()->setFlash('error', "Password is not changed, Please try again.!");
                    $this->redirect(\Yii::$app->getUrlManager()->createUrl('users/change_password/'));
                    Yii::$app->end();
                }
            } else {

                \Yii::$app->getSession()->setFlash('error', "Password is not changed, Please try again.!");

                if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            }

            return $this->render('change_password', ['model' => $model,]);
        } else {
            return $this->render('change_password', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}


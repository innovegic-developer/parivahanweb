<?php

namespace backend\controllers;

use Yii;
use app\models\Transporter;
use app\models\TransporterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TransporterController implements the CRUD actions for Transporter model.
 */
class TruckOperatorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Transporter models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TransporterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->andWhere(['user_level' => 5]); 
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Transporter model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Transporter model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Transporter();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Transporter model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$old_verified = $model->verified;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
			$post = Yii::$app->request->post('Transporter');
			if($post['verified']=='1' && $post['verified']!=$old_verified)
			{
				$user = \app\models\User::findOne($model->user_id);
				$user->verified = '1';
				if($user->save(false))
				{
					$msg = "Dear ".$user->first_name." ".$user->last_name.", ";
					$msg .= "Credential for Parivahan App. ";
					$msg .= "Username:-".$user->mobile_number." ";
					$msg .= "Password:-".$user->temp_password;
                    $msg .= SMS_SUFFIX;
					$mobile = $user->mobile_number;
					$this->_sendTextSMS($mobile,$msg);
				}
			}
            return $this->redirect(['index', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Transporter model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Transporter model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transporter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transporter::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
	protected function _sendTextSMS($mobile,$message) {	

		if(!WEB_SMS) return true;
		if($mobile != '' && $message != ''){
			$baseUri = 'https://alerts.solutionsinfini.com/api/v4';
	 		$params = array(
	 			'api_key' => WEB_SMS_API_KEY,
	            'message' => $message,
	            'to'      => $mobile,
	            'method'  => 'sms',
	            'sender'  => WEB_SMS_SENDER,
	            'format'  => 'php',
	            'unicode' => '0',
	        );
	 		$request_url = $baseUri.'/?'.http_build_query($params);
	        $request = curl_init($request_url);
	        curl_setopt($request,CURLOPT_RETURNTRANSFER,true );
	        curl_setopt($request,CURLOPT_SSL_VERIFYPEER,false);
	        $response = curl_exec($request);
			$res = unserialize($response);
	        return $res; 
		}
		else
			return false;
		
	}
}

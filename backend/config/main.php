<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-admin',
    'name' => 'MY DDD',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
     'aliases' => [
        '@adminlte/widgets' => '@vendor/adminlte/yii2-widgets'
    ],
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'i18n'=>array(
            'translations' => array(
                'msg'=>array(
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => "@app/messages",
                    'fileMap' => array(
                        'msg'=>'frontend.php'
                    ),
                ),
            ),
        ),
        'request' => [
            'csrfParam' => '_csrf-admin',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-admin', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'myut-admin',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];

<?php

$params = array_merge(
        require __DIR__ . '/../../common/config/params.php', require __DIR__ . '/../../common/config/params-local.php', require __DIR__ . '/params.php', require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
	'name' => 'FISHERIES DEPT.',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
	'defaultRoute'=>'dashboard/index',
    'aliases' => [
        '@adminlte/widgets' => '@vendor/adminlte/yii2-widgets'
    ],
    'bootstrap' => ['log'],
    'modules' => [//add by Scott
        'gridview' => [
            'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to  
        // use your own export download action or custom translation 
        // message source
        // 'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
        ],
    ],
    'components' => [
       'i18n'=>array(
            'translations' => array(
                'frontend'=>array(
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => "@app/messages",
                    'fileMap' => array(
                        'frontend'=>'frontend.php'
                    ),
                ),
            ),
        ),
        'request' => [
            'csrfParam' => '_csrf-backend',
             'enableCsrfValidation' => false,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'Permission' => [
            'class' => 'app\components\ModulesPermission',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'tapan.silicon@gmail.com',
                'password' => 'TaPan#55',
                'port' => '587',
                'encryption' => 'tls',
                'StreamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => TRUE,
                        'verify_peer' => 0,
                    ],
                ],
            ],
        ],
        /* 'cache' => [
            'class' => 'yii\caching\DbCache',
        // 'db' => 'mydb',
        // 'cacheTable' => 'my_cache',
        ],
        'session' => [
            'class' => 'yii\web\DbSession',
        // 'db' => 'mydb',
        // 'sessionTable' => 'my_session',
        ],*/
        
        
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    /*
      'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'rules' => [
      ],
      ],
     */
    ],
    'params' => $params,
];

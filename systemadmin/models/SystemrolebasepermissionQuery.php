<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Systemrolebasepermission]].
 *
 * @see Systemrolebasepermission
 */
class SystemrolebasepermissionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Systemrolebasepermission[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Systemrolebasepermission|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

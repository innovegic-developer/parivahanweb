<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%owners}}".
 *
 * @property int $id ID
 * @property string $boat_reg_no Registration No.
 * @property string $official_full_name Official Full Name
 * @property string $aadhar_card_full_name Aadhar Card Name
 * @property string $full_address Full Address
 * @property string $mobile1 Mobile 1
 * @property string $aadhar_number Aadhar Number
 * @property string $first_name First Name
 * @property string $middle_name Middle Name
 * @property string $last_name Last Name
 * @property string $name Name
 * @property string $addr1 Address 1
 * @property string $addr2 Address 2
 * @property string $addr3 Address 3
 * @property string $city City
 * @property string $state State
 * @property int $zipcode Zipcode
 * @property int $mobile2 Mobile 2
 * @property int $mobile3 Mobile 3
 * @property string $designation Designation
 * @property string $tracking_reference Tracking Reference
 * @property string $dat_id DAT ID
 * @property string $licence_number Licence Number
 * @property string $licence_date Licence Date
 * @property string $licence_validity Licence Validity
 * @property string $boat_type Boat Type
 * @property int $boat_bhp Boat BHP
 * @property string $boat_class Boat Class(MM/MO/Others)
 * @property string $place Place
 * @property string $entry_date Entry Date
 * @property string $registration_date Registration Date
 * @property string $photo Photo
 * @property string $remark Remark
 * @property string $bank Bank
 * @property string $date_of_birth Date Of Birth
 * @property int $age Age
 * @property int $user_id User ID
 * @property string $id_card_type ID Card
 * @property string $id_card_number ID Card Number
 * @property string $locationwise_group Locationwise Group
 * @property string $approval_status Approval Status
 * @property int $approved_by Approved By
 * @property string $approval_remark Approval Remark
 * @property string $created_on Created On
 * @property string $updated_on Updated On
 * @property int $created_by Created By
 * @property int $updated_by Updated By
 * @property string $status Status
 * @property string $temp_aadhar_no Temporary Aadhar Number
 * @property string $diesel_society_code Diesel Society Code
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property TripMaster[] $tripMasters
 */
class Owners extends \yii\db\ActiveRecord
{
    public $dr_requested_qty;
    public $dr_remark;
    public $dr_approved_qty;
    public $dr_approval_status;
    public $diesel_bill_no;
    public $dr_approval_remark;
    public $dr_requested_by;
    public $owner_id;
    
    //public $dr_approval_remark;
    //public $dr_approval_remark;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%owners}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['zipcode', 'mobile2', 'mobile3', 'boat_bhp', 'age', 'user_id', 'approved_by', 'created_by', 'updated_by'], 'integer'],
            [['boat_type', 'boat_class', 'remark', 'id_card_type', 'approval_status', 'status'], 'string'],
            [['registration_date', 'date_of_birth', 'created_on', 'updated_on'], 'safe'],
            [['boat_reg_no', 'mobile1', 'aadhar_number', 'licence_number', 'licence_date', 'licence_validity', 'place', 'entry_date', 'temp_aadhar_no'], 'string', 'max' => 50],
            [['official_full_name', 'aadhar_card_full_name', 'first_name', 'middle_name', 'last_name', 'name', 'addr1', 'addr2', 'addr3', 'city', 'state', 'designation', 'photo', 'bank', 'approval_remark','dat_id'], 'string', 'max' => 250],
            [['full_address'], 'string', 'max' => 1000],
            [['diesel_society_code'], 'string', 'max' => 255],
            [['tracking_reference'], 'string', 'max' => 20],
            [['id_card_number', 'locationwise_group'], 'string', 'max' => 100],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'boat_reg_no' => Yii::t('app', 'Registration No.'),
            'official_full_name' => Yii::t('app', 'Official Full Name'),
            'aadhar_card_full_name' => Yii::t('app', 'Aadhar Card Name'),
            'full_address' => Yii::t('app', 'Full Address'),
            'mobile1' => Yii::t('app', 'Mobile 1'),
            'aadhar_number' => Yii::t('app', 'Aadhar Number'),
            'first_name' => Yii::t('app', 'First Name'),
            'middle_name' => Yii::t('app', 'Middle Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'name' => Yii::t('app', 'Vessel Name'),
            'addr1' => Yii::t('app', 'Address 1'),
            'addr2' => Yii::t('app', 'Address 2'),
            'addr3' => Yii::t('app', 'Address 3'),
            'city' => Yii::t('app', 'City'),
            'state' => Yii::t('app', 'State'),
            'zipcode' => Yii::t('app', 'Zipcode'),
            'mobile2' => Yii::t('app', 'Mobile 2'),
            'mobile3' => Yii::t('app', 'Mobile 3'),
            'designation' => Yii::t('app', 'Designation'),
            'tracking_reference' => Yii::t('app', 'Tracking Reference'),
            'dat_id' => Yii::t('app', 'Dat ID'),
            'licence_number' => Yii::t('app', 'Licence Number'),
            'licence_date' => Yii::t('app', 'Licence Date'),
            'licence_validity' => Yii::t('app', 'Licence Validity'),
            'boat_type' => Yii::t('app', 'Vessel Type'),
            'boat_bhp' => Yii::t('app', 'Vessel BHP'),
            'boat_class' => Yii::t('app', 'Vessel Class'),
            'place' => Yii::t('app', 'Place'),
            'entry_date' => Yii::t('app', 'Entry Date'),
            'registration_date' => Yii::t('app', 'Registration Date'),
            'photo' => Yii::t('app', 'Photo'),
            'remark' => Yii::t('app', 'Remarks'),
            'bank' => Yii::t('app', 'Bank'),
            'date_of_birth' => Yii::t('app', 'Date Of Birth'),
            'age' => Yii::t('app', 'Age'),
            'user_id' => Yii::t('app', 'User ID'),
            'id_card_type' => Yii::t('app', 'ID Card'),
            'id_card_number' => Yii::t('app', 'ID Card Number'),
            'locationwise_group' => Yii::t('app', 'Locationwise Group'),
            'approval_status' => Yii::t('app', 'Approval Status'),
            'approved_by' => Yii::t('app', 'Approved By'),
            'approval_remark' => Yii::t('app', 'Approver Remarks'),
            'diesel_society_code' => Yii::t('app', 'Diesel Society Code'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
            'temp_aadhar_no' => Yii::t('app', 'Temporary Aadhar Number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTripMasters()
    {
        return $this->hasMany(TripMaster::className(), ['owner_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return OwnersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OwnersQuery(get_called_class());
    }
}

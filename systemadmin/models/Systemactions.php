<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "system_actions".
 *
 * @property int $id
 * @property int $controller_id
 * @property string $action_name
 * @property string $status
 */
class Systemactions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'system_actions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['controller_id', 'action_name'], 'required'],
            [['controller_id'], 'integer'],
            [['status'], 'string'],
            [['action_name'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'controller_id' => 'Controller ID',
            'action_name' => 'Action Name',
            'status' => 'Status',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SystemactionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SystemactionsQuery(get_called_class());
    }
}

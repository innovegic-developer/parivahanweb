<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Usertypemaster]].
 *
 * @see Usertypemaster
 */
class UsertypemasterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Usertypemaster[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Usertypemaster|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

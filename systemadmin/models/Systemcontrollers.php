<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "system_controllers".
 *
 * @property int $id
 * @property string $controller_name
 * @property string $status
 */
class Systemcontrollers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'system_controllers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['controller_name'], 'required'],
            [['status'], 'string'],
            [['controller_name'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'controller_name' => 'Controller Name',
            'status' => 'Status',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SystemcontrollersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SystemcontrollersQuery(get_called_class());
    }
}

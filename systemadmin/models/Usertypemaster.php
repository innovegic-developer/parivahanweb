<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_type_master".
 *
 * @property int $TypeID
 * @property string $Alias
 * @property string $RoleType User type based on role of user of different departments of HO / RO / SP / Customer / Development
 * @property int $IsActive 1 = Active, 0 = Inactive
 * @property string $CreatedDateTime
 * @property string $UpdatedDateTime
 * @property int $CreatedBy
 * @property int $UpdatedBy
 * @property int $IsDeleted 1 = Yes, 0 = No
 */
class Usertypemaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_type_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['RoleType'], 'required'],
            [['IsActive', 'CreatedBy', 'UpdatedBy', 'IsDeleted'], 'integer'],
            [['CreatedDateTime', 'UpdatedDateTime'], 'safe'],
            [['Alias'], 'string', 'max' => 20],
            [['RoleType'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TypeID' => 'Type ID',
            'Alias' => 'Alias',
            'RoleType' => 'Role Type',
            'IsActive' => 'Is Active',
            'CreatedDateTime' => 'Created Date Time',
            'UpdatedDateTime' => 'Updated Date Time',
            'CreatedBy' => 'Created By',
            'UpdatedBy' => 'Updated By',
            'IsDeleted' => 'Is Deleted',
        ];
    }

    /**
     * {@inheritdoc}
     * @return UsertypemasterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsertypemasterQuery(get_called_class());
    }
}

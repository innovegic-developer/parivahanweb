<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "system_role_base_permission".
 *
 * @property int $id
 * @property int $role_id
 * @property int $user_id
 * @property int $controller_id
 * @property int $action_id
 * @property string $allow_all_actions
 * @property string $status
 */
class Systemrolebasepermission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'system_role_base_permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role_id', 'controller_id','allow_all_actions'], 'required'],
             [['action_id'], 'required', 'when' => function($model) {
                            return $model->allow_all_actions == "N";
                        }, 'whenClient' => "function (attribute, value) {
                return $('#systemrolebasepermission-allow_all_actions').val() == 'N';
            }"],
           // [['action_id'], 'required', 'when' => function ($model) {return $model->allow_all_actions == 'N'; }],
            [['role_id', 'controller_id', 'action_id'], 'integer'],
            [['allow_all_actions', 'status'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_id' => 'User Role',
            'user_id' => 'User ID',
            'controller_id' => 'Controller',
            'action_id' => 'Action',
            'allow_all_actions' => 'Allow All Actions',
            'status' => 'Status',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SystemrolebasepermissionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SystemrolebasepermissionQuery(get_called_class());
    }
}

<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Systemcontrollers]].
 *
 * @see Systemcontrollers
 */
class SystemcontrollersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Systemcontrollers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Systemcontrollers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Owners;

/**
 * OwnersSearch represents the model behind the search form of `app\models\Owners`.
 */
class OwnersSearch extends Owners
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'zipcode', 'mobile2', 'mobile3', 'boat_bhp', 'age', 'user_id', 'approved_by', 'created_by', 'updated_by'], 'integer'],
            [['boat_reg_no', 'official_full_name', 'aadhar_card_full_name', 'full_address', 'mobile1', 'aadhar_number', 'first_name', 'middle_name', 'last_name', 'name', 'addr1', 'addr2', 'addr3', 'city', 'state', 'designation', 'tracking_reference', 'licence_number', 'licence_date', 'licence_validity', 'boat_type', 'boat_class', 'place', 'entry_date', 'registration_date', 'photo', 'remark', 'bank', 'date_of_birth', 'id_card_type', 'id_card_number', 'locationwise_group', 'approval_status', 'approval_remark', 'created_on', 'updated_on', 'status', 'temp_aadhar_no'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Owners::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'zipcode' => $this->zipcode,
            'mobile2' => $this->mobile2,
            'mobile3' => $this->mobile3,
            'boat_bhp' => $this->boat_bhp,
            'registration_date' => $this->registration_date,
            'date_of_birth' => $this->date_of_birth,
            'age' => $this->age,
            'user_id' => $this->user_id,
            'approved_by' => $this->approved_by,
            'created_on' => $this->created_on,
            'updated_on' => $this->updated_on,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'boat_reg_no', $this->boat_reg_no])
            ->andFilterWhere(['like', 'official_full_name', $this->official_full_name])
            ->andFilterWhere(['like', 'aadhar_card_full_name', $this->aadhar_card_full_name])
            ->andFilterWhere(['like', 'full_address', $this->full_address])
            ->andFilterWhere(['like', 'mobile1', $this->mobile1])
            ->andFilterWhere(['like', 'aadhar_number', $this->aadhar_number])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'addr1', $this->addr1])
            ->andFilterWhere(['like', 'addr2', $this->addr2])
            ->andFilterWhere(['like', 'addr3', $this->addr3])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'designation', $this->designation])
            ->andFilterWhere(['like', 'tracking_reference', $this->tracking_reference])
            ->andFilterWhere(['like', 'licence_number', $this->licence_number])
            ->andFilterWhere(['like', 'licence_date', $this->licence_date])
            ->andFilterWhere(['like', 'licence_validity', $this->licence_validity])
            ->andFilterWhere(['like', 'boat_type', $this->boat_type])
            ->andFilterWhere(['like', 'boat_class', $this->boat_class])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'entry_date', $this->entry_date])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'bank', $this->bank])
            ->andFilterWhere(['like', 'id_card_type', $this->id_card_type])
            ->andFilterWhere(['like', 'id_card_number', $this->id_card_number])
            ->andFilterWhere(['like', 'locationwise_group', $this->locationwise_group])
            ->andFilterWhere(['like', 'approval_status', $this->approval_status])
            ->andFilterWhere(['like', 'approval_remark', $this->approval_remark])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'temp_aadhar_no', $this->temp_aadhar_no]);

        return $dataProvider;
    }

    public function searchTop10($params)
    {
        $query = Owners::find()->limit(5);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'sort'=> ['defaultOrder' => ['id' => SORT_ASC]],
             'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'zipcode' => $this->zipcode,
            'mobile2' => $this->mobile2,
            'mobile3' => $this->mobile3,
            'boat_bhp' => $this->boat_bhp,
            'registration_date' => $this->registration_date,
            'date_of_birth' => $this->date_of_birth,
            'age' => $this->age,
            'user_id' => $this->user_id,
            'approved_by' => $this->approved_by,
            'created_on' => $this->created_on,
            'updated_on' => $this->updated_on,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'boat_reg_no', $this->boat_reg_no])
            ->andFilterWhere(['like', 'official_full_name', $this->official_full_name])
            ->andFilterWhere(['like', 'aadhar_card_full_name', $this->aadhar_card_full_name])
            ->andFilterWhere(['like', 'full_address', $this->full_address])
            ->andFilterWhere(['like', 'mobile1', $this->mobile1])
            ->andFilterWhere(['like', 'aadhar_number', $this->aadhar_number])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'addr1', $this->addr1])
            ->andFilterWhere(['like', 'addr2', $this->addr2])
            ->andFilterWhere(['like', 'addr3', $this->addr3])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'designation', $this->designation])
            ->andFilterWhere(['like', 'tracking_reference', $this->tracking_reference])
            ->andFilterWhere(['like', 'licence_number', $this->licence_number])
            ->andFilterWhere(['like', 'licence_date', $this->licence_date])
            ->andFilterWhere(['like', 'licence_validity', $this->licence_validity])
            ->andFilterWhere(['like', 'boat_type', $this->boat_type])
            ->andFilterWhere(['like', 'boat_class', $this->boat_class])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'entry_date', $this->entry_date])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'bank', $this->bank])
            ->andFilterWhere(['like', 'id_card_type', $this->id_card_type])
            ->andFilterWhere(['like', 'id_card_number', $this->id_card_number])
            ->andFilterWhere(['like', 'locationwise_group', $this->locationwise_group])
            ->andFilterWhere(['like', 'approval_status', $this->approval_status])
            ->andFilterWhere(['like', 'approval_remark', $this->approval_remark])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'temp_aadhar_no', $this->temp_aadhar_no]);

        return $dataProvider;
    }

    public function searchLast10($params)
    {
        $query = Owners::find()->limit(5);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'sort'=> ['defaultOrder' => ['id' => SORT_DESC]],
             'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'zipcode' => $this->zipcode,
            'mobile2' => $this->mobile2,
            'mobile3' => $this->mobile3,
            'boat_bhp' => $this->boat_bhp,
            'registration_date' => $this->registration_date,
            'date_of_birth' => $this->date_of_birth,
            'age' => $this->age,
            'user_id' => $this->user_id,
            'approved_by' => $this->approved_by,
            'created_on' => $this->created_on,
            'updated_on' => $this->updated_on,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'boat_reg_no', $this->boat_reg_no])
            ->andFilterWhere(['like', 'official_full_name', $this->official_full_name])
            ->andFilterWhere(['like', 'aadhar_card_full_name', $this->aadhar_card_full_name])
            ->andFilterWhere(['like', 'full_address', $this->full_address])
            ->andFilterWhere(['like', 'mobile1', $this->mobile1])
            ->andFilterWhere(['like', 'aadhar_number', $this->aadhar_number])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'addr1', $this->addr1])
            ->andFilterWhere(['like', 'addr2', $this->addr2])
            ->andFilterWhere(['like', 'addr3', $this->addr3])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'designation', $this->designation])
            ->andFilterWhere(['like', 'tracking_reference', $this->tracking_reference])
            ->andFilterWhere(['like', 'licence_number', $this->licence_number])
            ->andFilterWhere(['like', 'licence_date', $this->licence_date])
            ->andFilterWhere(['like', 'licence_validity', $this->licence_validity])
            ->andFilterWhere(['like', 'boat_type', $this->boat_type])
            ->andFilterWhere(['like', 'boat_class', $this->boat_class])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'entry_date', $this->entry_date])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'bank', $this->bank])
            ->andFilterWhere(['like', 'id_card_type', $this->id_card_type])
            ->andFilterWhere(['like', 'id_card_number', $this->id_card_number])
            ->andFilterWhere(['like', 'locationwise_group', $this->locationwise_group])
            ->andFilterWhere(['like', 'approval_status', $this->approval_status])
            ->andFilterWhere(['like', 'approval_remark', $this->approval_remark])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'temp_aadhar_no', $this->temp_aadhar_no]);

        return $dataProvider;
    }

     /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchByUser($params,$id)
    {

        $query = Owners::find($id);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'zipcode' => $this->zipcode,
            'mobile2' => $this->mobile2,
            'mobile3' => $this->mobile3,
            'boat_bhp' => $this->boat_bhp,
            'registration_date' => $this->registration_date,
            'date_of_birth' => $this->date_of_birth,
            'age' => $this->age,
            'user_id' => $this->user_id,
            'approved_by' => $this->approved_by,
            'created_on' => $this->created_on,
            'updated_on' => $this->updated_on,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'boat_reg_no', $this->boat_reg_no])
            ->andFilterWhere(['like', 'user_id', $id])
            ->andFilterWhere(['like', 'official_full_name', $this->official_full_name])
            ->andFilterWhere(['like', 'aadhar_card_full_name', $this->aadhar_card_full_name])
            ->andFilterWhere(['like', 'full_address', $this->full_address])
            ->andFilterWhere(['like', 'mobile1', $this->mobile1])
            ->andFilterWhere(['like', 'aadhar_number', $this->aadhar_number])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'addr1', $this->addr1])
            ->andFilterWhere(['like', 'addr2', $this->addr2])
            ->andFilterWhere(['like', 'addr3', $this->addr3])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'designation', $this->designation])
            ->andFilterWhere(['like', 'tracking_reference', $this->tracking_reference])
            ->andFilterWhere(['like', 'licence_number', $this->licence_number])
            ->andFilterWhere(['like', 'licence_date', $this->licence_date])
            ->andFilterWhere(['like', 'licence_validity', $this->licence_validity])
            ->andFilterWhere(['like', 'boat_type', $this->boat_type])
            ->andFilterWhere(['like', 'boat_class', $this->boat_class])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'entry_date', $this->entry_date])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'bank', $this->bank])
            ->andFilterWhere(['like', 'id_card_type', $this->id_card_type])
            ->andFilterWhere(['like', 'id_card_number', $this->id_card_number])
            ->andFilterWhere(['like', 'locationwise_group', $this->locationwise_group])
            ->andFilterWhere(['like', 'approval_status', $this->approval_status])
            ->andFilterWhere(['like', 'approval_remark', $this->approval_remark])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'temp_aadhar_no', $this->temp_aadhar_no]);

        return $dataProvider;
    }
}

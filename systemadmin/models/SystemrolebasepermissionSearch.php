<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Systemrolebasepermission;

/**
 * SystemrolebasepermissionSearch represents the model behind the search form of `app\models\Systemrolebasepermission`.
 */
class SystemrolebasepermissionSearch extends Systemrolebasepermission
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'role_id', 'user_id', 'controller_id', 'action_id'], 'integer'],
            [['allow_all_actions', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Systemrolebasepermission::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role_id' => $this->role_id,
            'user_id' => $this->user_id,
            'controller_id' => $this->controller_id,
            'action_id' => $this->action_id,
        ]);

        $query->andFilterWhere(['like', 'allow_all_actions', $this->allow_all_actions])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

<?php
namespace systemadmin\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\models\Users;

/**
 * This is the model class for table "admin_users".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $created_date
 * @property string $updated_date
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $status
 */
class AdminUsers extends ActiveRecord implements IdentityInterface {

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    public $authKey;
    public $created_by;
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['first_name', 'last_name', 'email', 'username', 'password', 'created_by', 'updated_by'], 'required'],
            [['created_date', 'updated_date'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['status'], 'string'],
            [['first_name', 'last_name'], 'string', 'max' => 25],
            [['email', 'password'], 'string', 'max' => 35],
            [['username'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        $dbUser = Users::find()
                ->where([
                    "id" => $id
                ])
                ->one();
        if (!count($dbUser)) {
            return null;
        }
        return new static($dbUser);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $userType = null) {

        $dbUser = Users::find()
                ->where(["accessToken" => $token])
                ->one();
        if (!count($dbUser)) {
            return null;
        }
        return new static($dbUser);
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username) {
        $dbUser = Users::find()
                ->where([
                    "username" => $username
                ])
                ->one();
        if (!count($dbUser)) {
            return null;
        }
        return new static($dbUser);
    }
    
     public static function findByEmailAddressOrUserName($email_username)
    {
         
        //echo $email_username;exit;
        $dbUser = Users::find()
                ->where("(email = '{$email_username}' OR username = '{$email_username}') AND user_level IN (1) AND status = '1'")
                ->one();
                
        //p($dbUser);
        if (!count($dbUser)) {
            return null;
        }
        return new static($dbUser);
        
        //return static::find()->where("email = '{$email_username}' OR username = '{$email_username}'")->one();
    }
    
    
     /*
     * Check is user admin
     */

    public static function isUserAdmin($username = '', $usertype = '') {
        if (static::findOne(['username' => $username, 'user_role_id' => $usertype])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return $this->password === md5($password);
    }
    /*
     * Validate Email or Password
     */
    public function validateEmailUsername($email_username)
    {
        //return Yii::$app->security->validatePassword($password, $this->password_hash);
        if($this->email == $email_username){
            return $this->email;
        }elseif($this->username == $email_username){
            return $this->username;
        }
    }
}

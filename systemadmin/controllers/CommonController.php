<?php

namespace systemadmin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class CommonController extends \yii\web\Controller {

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionCall_ajax() {
        if (Yii::$app->request->isAjax) {

            $session = Yii::$app->session;
            $result = '';

            if (!empty($_GET)) {
                //Get Action By Controller
                if ($_GET['process_type'] == 'get_action_by_controller') {
                    $result = '<option value="">Please Select Controller</option>';
                    if (!empty($_GET['process_id'])) {
                        $models = \app\models\Systemactions::find()
                                ->select(['id', 'action_name'])
                                ->where("controller_id='" . $_GET['process_id'] . "' AND Status='Y'")->asArray()->orderBy('action_name ASC')->all();
                        if (!empty($models)) {
                            foreach ($models as $row) {
                                $result .= '<option value="' . $row['id'] . '">' . $row['action_name'] . '</option>';
                            }
                        }
                    }
                    echo $result;
                } 
                
            }
            \Yii::$app->end();
        } else {
            $this->redirect(\Yii::$app->getUrlManager()->createUrl('error'));
        }
    }

}

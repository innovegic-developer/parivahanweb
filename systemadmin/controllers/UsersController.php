<?php

namespace systemadmin\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Users();

        if ($model->load(Yii::$app->request->post())) {

            if (!empty(Yii::$app->request->post()['Users']['password'])) {
                $model->password = md5(Yii::$app->request->post()['Users']['password']);
            }

            if ($model->save(FALSE)) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if (!empty(Yii::$app->request->post()['Users']['password'])) {
                $model->password = md5(Yii::$app->request->post()['Users']['password']);
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        }
          return $this->render('update', [
                            'model' => $model,
                ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionExport(){
        //if(isset($_POST['export']))
       // {
            $users = Users::find()
                    ->select(['id','first_name','last_name','username','email'])
                    ->asArray()->all();
            
             $data = array();
            $i = 0;

            foreach ($users as $key => $val) {
                //$count_created = TransactionLog::find()->where("bag_id = '{$val['id']}' AND transaction_status = 'bagcreated' AND status='Y'")->count();

                $data[$i]['Sr.no'] = $i + 1;
                $data[$i]['uid'] = $val['id'];
                $data[$i]['first_name'] = $val['first_name'];
                $data[$i]['last_name'] = $val['last_name'];
                $data[$i]['username'] = $val['username'];
                $data[$i]['email'] = $val['email'];
               

                $i++;
            }
            
           if (!empty($data)) {
                $file_name = 'report.xlsx';
                $objPHPExcel = new \PHPExcel();

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename=' . $file_name);
                header('Cache-Control: max-age=0');

                $objPHPExcel->getActiveSheet()->fromArray($data, null, 'A2');
                $objPHPExcel->getActiveSheet()->setTitle('Reports');

                $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Sr. No');
                $objPHPExcel->getActiveSheet()->setCellValue('B1', 'User ID');
                $objPHPExcel->getActiveSheet()->setCellValue('C1', 'First Name');
                $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Last Name');
                $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Username');
                $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Email');
                

                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                
                $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                // Write file to the browser
                // ob_clean();
                //$objWriter->save($file_name);
                //$objWriter->save(__DIR__."/$file_name");
                $objWriter->save('php://output');
                exit;
            } else {
                \Yii::$app->getSession()->setFlash('error', "No data available for selected range!");
                return $this->redirect('index');
            }
        //}
    }

}

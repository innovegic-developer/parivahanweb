<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsertypemasterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usertypemaster-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'TypeID') ?>

    <?= $form->field($model, 'Alias') ?>

    <?= $form->field($model, 'RoleType') ?>

    <?= $form->field($model, 'IsActive') ?>

    <?= $form->field($model, 'CreatedDateTime') ?>

    <?php // echo $form->field($model, 'UpdatedDateTime') ?>

    <?php // echo $form->field($model, 'CreatedBy') ?>

    <?php // echo $form->field($model, 'UpdatedBy') ?>

    <?php // echo $form->field($model, 'IsDeleted') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usertypemaster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usertypemaster-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php // $form->field($model, 'Alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'RoleType')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'IsActive')->textInput() ?>

    <?php // $form->field($model, 'CreatedDateTime')->textInput() ?>

    <?php // $form->field($model, 'UpdatedDateTime')->textInput() ?>

    <?php // $form->field($model, 'CreatedBy')->textInput() ?>

    <?php // $form->field($model, 'UpdatedBy')->textInput() ?>

    <?php // $form->field($model, 'IsDeleted')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usertypemaster */

$this->title = 'Update Usertypemaster: ' . $model->TypeID;
$this->params['breadcrumbs'][] = ['label' => 'Usertypemasters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->TypeID, 'url' => ['view', 'id' => $model->TypeID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usertypemaster-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

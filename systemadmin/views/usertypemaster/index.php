<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UsertypemasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usertypemasters';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usertypemaster-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Usertypemaster', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'TypeID',
           // 'Alias',
            'RoleType',
           // 'IsActive',
          //  'CreatedDateTime',
            //'UpdatedDateTime',
            //'CreatedBy',
            //'UpdatedBy',
            //'IsDeleted',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

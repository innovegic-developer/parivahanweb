<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Systemcontrollers */

$this->title = 'Create Systemcontrollers';
$this->params['breadcrumbs'][] = ['label' => 'Systemcontrollers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="systemcontrollers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SystemcontrollersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Systemcontrollers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="systemcontrollers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
<?= Html::a('Create Systemcontrollers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'controller_name',
            [
                'attribute' => 'status',
                //'filter' => ['Y' => 'Active', 'N' => 'Inactive'],
                'filter' => Html::activeDropDownList($searchModel, 'status', ['Y' => 'Active', 'N' => 'Inactive'], ['class' => 'form-control', 'prompt' => 'All']),
                'value' => function ($data) {
                    return Yii::$app->common->displayLabel('act_ict', $data->status);
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
<?php Pjax::end(); ?>
</div>

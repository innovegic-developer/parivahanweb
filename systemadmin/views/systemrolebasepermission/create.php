<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Systemrolebasepermission */

$this->title = 'Create Systemrolebasepermission';
$this->params['breadcrumbs'][] = ['label' => 'Systemrolebasepermissions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="systemrolebasepermission-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

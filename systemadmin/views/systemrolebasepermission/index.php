<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SystemrolebasepermissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Systemrolebasepermissions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="systemrolebasepermission-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a('Create Systemrolebasepermission', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'role_id',
            // 'user_id',
            [
                'attribute' => 'role_id',
                'label' => 'Role Type',
                'value' => function($model) {
                    $cname = \app\models\Usertypemaster::find()
                                    ->select(['RoleType'])->where(['TypeID' => $model->role_id])
                                    ->asArray()->one();

                    return $cname['RoleType'];
                },
                //'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(app\models\Usertypemaster::find()
                                ->asArray()->all(), 'TypeID', 'RoleType'),
            ],
            [
                'attribute' => 'controller_id',
                'label' => 'Controller',
                'value' => function($model) {
                    $cname = app\models\Systemcontrollers::find()
                                    ->select(['controller_name'])->where(['id' => $model->controller_id])
                                    ->asArray()->one();

                    return $cname['controller_name'];
                },
                //'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(app\models\Systemcontrollers::find()
                                ->asArray()->all(), 'id', 'controller_name'),
            ],
            [
                'attribute' => 'allow_all_actions',
                'value' => function ($data) {
                    return Yii::$app->common->displayLabel('allow_disallow', $data->allow_all_actions);
                },
                'filter' => Html::activeDropDownList($searchModel, 'allow_all_actions', ['Y' => 'Yes', 'N' => 'No'], ['class' => 'form-control', 'prompt' => 'All']),
            ],
            // 'action_id',
            // 'allow_all_actions',
            [
                'attribute' => 'status',
                //'filter' => ['Y' => 'Active', 'N' => 'Inactive'],
                'filter' => Html::activeDropDownList($searchModel, 'status', ['Y' => 'Active', 'N' => 'Inactive'], ['class' => 'form-control', 'prompt' => 'All']),
                'value' => function ($data) {
                    return Yii::$app->common->displayLabel('yes_no', $data->status);
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>

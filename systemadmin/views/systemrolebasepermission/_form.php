<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use nex\chosen\Chosen;

/* @var $this yii\web\View */
/* @var $model app\models\Systemrolebasepermission */
/* @var $form yii\widgets\ActiveForm */
$user_roles1 = array();
$user_roles = \app\models\Usertypemaster::find()->select(['TypeID', 'RoleType'])->asArray()->all();
if (!empty($user_roles)) {
    foreach ($user_roles as $k => $v) {
        $user_roles1[$v['TypeID']] = $v['RoleType'];
    }
}


$system_controllers1 = array();
$system_controllers = \app\models\Systemcontrollers::find()
                ->select(['id', 'controller_name'])
                ->asArray()->all();

if (!empty($system_controllers)) {
    foreach ($system_controllers as $k => $v) {
        $system_controllers1[$v['id']] = $v['controller_name'];
    }
}

$actions1 = array();



//p($user_roles);
?>

<div class="systemrolebasepermission-form">

    <?php $form = ActiveForm::begin(); ?>



    <?php // $form->field($model, 'role_id')->dropDownList(ArrayHelper::map($user_roles, 'TypeID', 'RoleType'), ['prompt' => 'Select User Role'])  ?>
    <?=
    $form->field($model, 'role_id')->widget(
            Chosen::className(), [
        'items' => $user_roles1,
        'disableSearch' => 1, // Search input will be disabled while there are fewer than 5 items
        'clientOptions' => [
            'search_contains' => true,
            'single_backstroke_delete' => false,
        ]
    ]);
    ?>

    <?=
    $form->field($model, 'controller_id')->widget(
            Chosen::className(), [
        'items' => $system_controllers1,
        'disableSearch' => 1, // Search input will be disabled while there are fewer than 5 items
        'clientOptions' => [
            'search_contains' => true,
            'single_backstroke_delete' => false,
        ]
    ]);
    ?>

    <?= $form->field($model, 'allow_all_actions')->dropDownList(['Y' => 'Y', 'N' => 'N',], ['prompt' => 'Please Select']) ?>



    <?=
    $form->field($model, 'action_id')->widget(
            Chosen::className(), [
        'items' => $actions1,
        'disableSearch' => 1, // Search input will be disabled while there are fewer than 5 items
        'clientOptions' => [
            'search_contains' => true,
            'single_backstroke_delete' => false,
        ]
    ]);
    ?>


    <?php // $form->field($model, 'status')->dropDownList(['Y' => 'Y', 'N' => 'N',], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


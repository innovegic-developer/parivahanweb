<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
//use app\assets\AppAsset;
use app\assets\AdminLteAsset;

//AppAsset::register($this);
$asset = AdminLteAsset::register($this);
$baseUrl = $asset->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <?php $this->beginBody() ?>

        <div class="wrapper">
            <?= $this->render('header.php', ['baserUrl' => $baseUrl, 'title' => Yii::$app->name]) ?>
            <?= $this->render('leftside.php', ['baserUrl' => $baseUrl]) ?>
            <?= $this->render('content.php', ['content' => $content]) ?>
            <?= $this->render('footer.php', ['baserUrl' => $baseUrl]) ?>
            <?= $this->render('rightside.php', ['baserUrl' => $baseUrl]) ?>
        </div>

        <!--footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company <?//= date('Y') ?></p>
        
                <p class="pull-right"><?//= Yii::powered() ?></p>
            </div>
        </footer-->

        <?php $this->endBody() ?>
        <script>

            
            $(document).ready(function () {
                //alert("gigigig"); return false;
                $("#systemrolebasepermission-controller_id").change(function () {
                    var controller_id = $('#systemrolebasepermission-controller_id').val();

                    $.ajax({
                        type: "GET",
                        url: "<?php echo Yii::$app->urlManager->createUrl(['common/call_ajax']); ?>",
                        data: {process_type: 'get_action_by_controller', process_id: controller_id, update_id: 'companydetail-state'},
                        async: false,
                        success: function (data) {
                            //$('#systemrolebasepermission-action_id').attr('disabled', false);
                            $('#systemrolebasepermission-action_id').html(data);
                            $('#systemrolebasepermission-action_id').trigger("chosen:updated");
                        }
                    });
                });

                $("#systemrolebasepermission-allow_all_actions").change(function () {
                    var allow_all_action = $('#systemrolebasepermission-allow_all_actions').val();

                    if (allow_all_action == 'Y') {
                        $('#systemrolebasepermission-action_id').html('');
                        $('#systemrolebasepermission-action_id').trigger("chosen:updated");
                    } else if (allow_all_action == 'N') {

                        var controller_id = $('#systemrolebasepermission-controller_id').val();

                        $.ajax({
                            type: "GET",
                            url: "<?php echo Yii::$app->urlManager->createUrl(['common/call_ajax']); ?>",
                            data: {process_type: 'get_action_by_controller', process_id: controller_id, update_id: 'companydetail-state'},
                            async: false,
                            success: function (data) {
                                //$('#systemrolebasepermission-action_id').attr('disabled', false);
                                $('#systemrolebasepermission-action_id').html(data);
                                $('#systemrolebasepermission-action_id').trigger("chosen:updated");
                            }
                        });

                    } else {
                        $('#systemrolebasepermission-action_id').html('');
                        $('#systemrolebasepermission-action_id').trigger("chosen:updated");
                    }

                });

            });
        </script>
    </body>
</html>
<?php $this->endPage() ?>


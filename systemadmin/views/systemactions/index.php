<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SystemactionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Systemactions';
$this->params['breadcrumbs'][] = $this->title;

$system_controllers = app\models\Systemcontrollers::find()->select(['id', 'controller_name'])->asArray()->all();
?>
<div class="systemactions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a('Create Systemactions', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
           // 'controller_id',
            [
                'attribute' => 'controller_id',
                'label' => 'Controller',
                'value' => function($model) {
                    $cname = app\models\Systemcontrollers::find()
                            ->select(['controller_name'])->where(['id' => $model->controller_id])
                            ->asArray()->one();
                    
                    return $cname['controller_name'];
                },
                //'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(app\models\Systemcontrollers::find()
                        ->asArray()->all(), 'id', 'controller_name'),
                
            ],
            
            'action_name',
            // 'status',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>

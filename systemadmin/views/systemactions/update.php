<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Systemactions */

$this->title = 'Update Systemactions: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Systemactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="systemactions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Systemactions */

$this->title = 'Create Systemactions';
$this->params['breadcrumbs'][] = ['label' => 'Systemactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="systemactions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Systemactions */
/* @var $form yii\widgets\ActiveForm */

$system_controllers = \app\models\Systemcontrollers::find()->asArray()->all();

//p($system_controllers);

?>

<div class="systemactions-form">

    <?php $form = ActiveForm::begin(); ?>

    
    
    <?= $form->field($model, 'controller_id')
            ->dropDownList(ArrayHelper::map($system_controllers, 'id', 'controller_name'), ['prompt' => 'Select Controller']) ?>

    <?=  $form->field($model, 'action_name')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'status')->dropDownList([ 'Y' => 'Y', 'N' => 'N', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

server {
	listen 80;
	listen [::]:80;

	server_name df.innovegicsolutions.com www.df.innovegicsolutions.com;
	
    set $base_root /var/www/html/dev/public/diu-fisheries;
    root $base_root;

    #error_log /var/log/nginx/advanced.local.error.log warn;
    #access_log /var/log/nginx/advanced.local.access.log main;
    charset UTF-8;
    index index.php index.html;

    location / {
        root $base_root/frontend/web;
        try_files $uri $uri/ /frontend/web/index.php$is_args$args;

        # omit static files logging, and if they don't exist, avoid processing by Yii (uncomment if necessary)
        #location ~ ^/.+\.(css|js|ico|png|jpe?g|gif|svg|ttf|mp4|mov|swf|pdf|zip|rar)$ {
        #    log_not_found off;
        #    access_log off;
        #    try_files $uri =404;
        #}
        location ~ ^/assets/.+\.php(/|$) {
            deny all;
        }
    }

    location /admin {
        alias $base_root/backend/web/;

        # redirect to the URL without a trailing slash (uncomment if necessary)
        #location = /admin/ {
        #    return 301 /admin;
        #}

        # prevent the directory redirect to the URL with a trailing slash
        location = /admin {
            # if your location is "/backend", try use "/backend/backend/web/index.php$is_args$args"
            # bug ticket: https://trac.nginx.org/nginx/ticket/97
            try_files $uri /backend/web/index.php$is_args$args;
        }

        # if your location is "/backend", try use "/backend/backend/web/index.php$is_args$args"
        # bug ticket: https://trac.nginx.org/nginx/ticket/97
        try_files $uri $uri/ /backend/web/index.php$is_args$args;

        # omit static files logging, and if they don't exist, avoid processing by Yii (uncomment if necessary)
        #location ~ ^/admin/.+\.(css|js|ico|png|jpe?g|gif|svg|ttf|mp4|mov|swf|pdf|zip|rar)$ {
        #    log_not_found off;
        #    access_log off;
        #    try_files $uri =404;
        #}

        location ~ ^/admin/assets/.+\.php(/|$) {
            deny all;
        }
    }

	 location ~ \.php$ {
			fastcgi_pass unix:/run/php/php7.1-fpm.sock;
			include snippets/fastcgi-php.conf;
			#fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
		}

	# Enable PHP Support
	#location ~ \.php$ {
	#       include snippets/fastcgi-php.conf;

	#       # With php-fpm (or other unix sockets):
	#       fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
	#}

	
    #location ~ ^/.+\.php(/|$) {
     #   rewrite (?!^/((frontend|backend)/web|admin))^ /frontend/web$uri break;
     #   rewrite (?!^/backend/web)^/admin(/.+)$ /backend/web$1 break;

      #  fastcgi_pass 127.0.0.1:9000; # proxy requests to a TCP socket
      #  #fastcgi_pass unix:/var/run/php-fpm.sock; # proxy requests to a UNIX domain socket (check your www.conf file)
      #  fastcgi_split_path_info ^(.+\.php)(.*)$;
      #  include /etc/nginx/fastcgi_params;
      #  fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
      #  try_files $fastcgi_script_name =404;
    #}

    location ~ /\. {
        deny all;
    }
}


server {
        listen 80;
        listen [::]:80;

        server_name df.innovegicsolutions.com www.df.innovegicsolutions.com;
        root /var/www/html/dev/public/diu-fisheries;
        index index.php;

        location / {
        #        try_files $uri $uri/ =404;
                 try_files $uri $uri/ /index.php$is_args$args;
        }

        location ~ \.php$ {
            fastcgi_pass unix:/run/php/php7.1-fpm.sock;
            include snippets/fastcgi-php.conf;
            #fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        }

        # Enable PHP Support
        #location ~ \.php$ {
        #       include snippets/fastcgi-php.conf;

        #       # With php-fpm (or other unix sockets):
        #       fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        #}


        #location ~ /\.ht {
        #        deny all;
        #}

        # Deny Access to Hidden Files
        location ~ /\. {
                deny all;
                access_log off;
                log_not_found off;
        }

}
